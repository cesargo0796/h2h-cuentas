<%@page import="java.io.InputStream"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page import="com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.exportacion.ResultadoExportacion"%>
<%
ResultadoExportacion resultadoExportacion = (ResultadoExportacion)request.getAttribute("resultadoExportacion");
String contentDisposition  = (String)request.getAttribute("contentDisposition");
response.setContentType (resultadoExportacion.getTipoExportacion().getMimeType());
if(contentDisposition!=null){
	response.setHeader ("Content-Disposition", contentDisposition);	
}
InputStream ins = null;
try{
	ins = resultadoExportacion.getIns();
	IOUtils.copy(resultadoExportacion.getIns(), out);
	out.flush();
}finally{
	if(ins!=null)IOUtils.closeQuietly(ins);	
}
%>