<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsConvenioEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsConvenio"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsConvenio.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenio.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsConvenio/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsConvenio.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Cliente"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.cliente}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Nombre"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.nombre}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Cuenta"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.cuenta}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Nombrecta"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.nombrecta}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Tipomoneda"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.tipomoneda}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Categoria"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.categoria}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Descripcion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.descripcion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Diaaplicacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.diaaplicacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Numreintentos"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.numreintentos}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Polparticipante"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.polparticipante}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Poldianoexistente"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.poldianoexistente}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Estatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.estatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Fechaestatus"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenio.fechaestatus}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Intervaloreintento"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.intervaloreintento}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Polmontofijo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.polmontofijo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Montofijo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.montofijo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Intervaloaplica"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.intervaloaplica}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Creditosparciales"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.creditosparciales}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Autorizacionweb"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.autorizacionweb}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Clasificacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.clasificacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Comisionxoperacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.comisionxoperacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Comisionxoperacionpp"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.comisionxoperacionpp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsConvenio.formulario.etiqueta.Multiplescuentas"/>
			</span>	 
			
			<span class="formvalue">
                ${bsConvenio.multiplescuentas}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
