<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsConvenioachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsConvenioachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsConvenioach"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="tipoconvenioMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsConvenioach.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsConvenioach.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="tipoconvenioMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="39" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="40" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="41" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsConvenioach/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsConvenioach.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoconvenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Tipoconvenio"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integerMin" type="text" id="tipoconvenioMin" name="tipoconvenioMin" value="${bsConvenioachFiltro['tipoconvenioMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="2" alt="integerMax" type="text" id="tipoconvenioMax" name="tipoconvenioMax" value="${bsConvenioachFiltro['tipoconvenioMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoconvenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Cliente"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="cliente" name="cliente" value="${bsConvenioachFiltro['bsCliente'].cliente}"  size="10"  readonly="readonly"/>
                <input type="text" id="bsCliente.nombre" name="bsCliente.nombre" value="${bsConvenioachFiltro['bsCliente'].nombre}"  size="40"  readonly="readonly"/>		
				<button tabindex="3" id="btnbsCliente" class="sq" onClick="doForaneo('bsCliente', 'bsConvenioach/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Nombre"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="nombre" name="nombre" value="${bsConvenioachFiltro['nombre']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Cuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="cuenta" name="cuenta" value="${bsConvenioachFiltro['cuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Nombrecta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="nombrecta" name="nombrecta" value="${bsConvenioachFiltro['nombrecta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipomoneda']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Tipomoneda"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="7" alt="integerMin" type="text" id="tipomonedaMin" name="tipomonedaMin" value="${bsConvenioachFiltro['tipomonedaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="7" alt="integerMax" type="text" id="tipomonedaMax" name="tipomonedaMax" value="${bsConvenioachFiltro['tipomonedaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipomoneda'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Descripcion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="descripcion" name="descripcion" value="${bsConvenioachFiltro['descripcion']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['polparticipante']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Polparticipante"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="9" alt="integerMin" type="text" id="polparticipanteMin" name="polparticipanteMin" value="${bsConvenioachFiltro['polparticipanteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="9" alt="integerMax" type="text" id="polparticipanteMax" name="polparticipanteMax" value="${bsConvenioachFiltro['polparticipanteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['polparticipante'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsConvenioachFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsConvenioachFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenioachFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="11" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsConvenioachFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionweb']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Autorizacionweb"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integerMin" type="text" id="autorizacionwebMin" name="autorizacionwebMin" value="${bsConvenioachFiltro['autorizacionwebMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="12" alt="integerMax" type="text" id="autorizacionwebMax" name="autorizacionwebMax" value="${bsConvenioachFiltro['autorizacionwebMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionweb'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['clasificacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Clasificacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integerMin" type="text" id="clasificacionMin" name="clasificacionMin" value="${bsConvenioachFiltro['clasificacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="13" alt="integerMax" type="text" id="clasificacionMax" name="clasificacionMax" value="${bsConvenioachFiltro['clasificacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['clasificacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionpropiaok']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Comisionxoperacionpropiaok"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="14" alt="numericMin" type="text" id="comisionxoperacionpropiaokMin" name="comisionxoperacionpropiaokMin" value="${bsConvenioachFiltro['comisionxoperacionpropiaokMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="14" alt="numericMax" type="text" id="comisionxoperacionpropiaokMax" name="comisionxoperacionpropiaokMax" value="${bsConvenioachFiltro['comisionxoperacionpropiaokMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionpropiaok'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionpropiaerror']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Comisionxoperacionpropiaerror"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="15" alt="numericMin" type="text" id="comisionxoperacionpropiaerrorMin" name="comisionxoperacionpropiaerrorMin" value="${bsConvenioachFiltro['comisionxoperacionpropiaerrorMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="15" alt="numericMax" type="text" id="comisionxoperacionpropiaerrorMax" name="comisionxoperacionpropiaerrorMax" value="${bsConvenioachFiltro['comisionxoperacionpropiaerrorMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionpropiaerror'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionnopropiaok']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Comisionxoperacionnopropiaok"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" alt="numericMin" type="text" id="comisionxoperacionnopropiaokMin" name="comisionxoperacionnopropiaokMin" value="${bsConvenioachFiltro['comisionxoperacionnopropiaokMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="16" alt="numericMax" type="text" id="comisionxoperacionnopropiaokMax" name="comisionxoperacionnopropiaokMax" value="${bsConvenioachFiltro['comisionxoperacionnopropiaokMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionnopropiaok'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionxoperacionnopropiaerror']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Comisionxoperacionnopropiaerror"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="17" alt="numericMin" type="text" id="comisionxoperacionnopropiaerrorMin" name="comisionxoperacionnopropiaerrorMin" value="${bsConvenioachFiltro['comisionxoperacionnopropiaerrorMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="17" alt="numericMax" type="text" id="comisionxoperacionnopropiaerrorMax" name="comisionxoperacionnopropiaerrorMax" value="${bsConvenioachFiltro['comisionxoperacionnopropiaerrorMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionxoperacionnopropiaerror'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['multiplescuentas']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Multiplescuentas"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="18" type="text" id="multiplescuentas" name="multiplescuentas" value="${bsConvenioachFiltro['multiplescuentas']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['multiplescuentas'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['activo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Activo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" type="text" id="activo" name="activo" value="${bsConvenioachFiltro['activo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['activo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['bitborrado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsConvenioach.filtro.etiqueta.Bitborrado"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="20" type="text" id="bitborrado" name="bitborrado" value="${bsConvenioachFiltro['bitborrado']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['bitborrado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>