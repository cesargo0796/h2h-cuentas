<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="idTablaDestino" name="idTablaDestino" value="${hthTablaDestinoDtoCabeza.idTablaDestino}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${hthTablaDestinoEnlaceDetalle == 'hthColumnaDestino'}">
	<c:set var="nombre" value="hthColumnaDestino"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('hthTablaDestino/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('hthTablaDestino/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="hthTablaDestino.encabezado.etiqueta.titulo"/> ${hthTablaDestinoDtoCabeza.idTablaDestino} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="hthTablaDestino.encabezado.etiqueta.NombreTabla"/>:</span>
		    ${hthTablaDestinoDtoCabeza.nombreTabla} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="hthTablaDestino.encabezado.etiqueta.Descripcion"/>:</span>
		    ${hthTablaDestinoDtoCabeza.descripcion} 
 &nbsp;|     	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="hthTablaDestino.encabezado.etiqueta.NombreTabla"/></strong>&nbsp;
		${hthTablaDestinoDtoCabeza.nombreTabla}
		</div>
		
	    <div>
		<strong><fmt:message key="hthTablaDestino.encabezado.etiqueta.Descripcion"/></strong>&nbsp;
		${hthTablaDestinoDtoCabeza.descripcion}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${hthTablaDestinoEnlaceDetalle == 'hthColumnaDestino'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthColumnaDestino');doSubmit('hthTablaDestino/detalle')" class="ui-selected"><fmt:message key="hthTablaDestino.encabezado.etiqueta.hthColumnaDestinoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthTablaDestinoEnlaceDetalle != 'hthColumnaDestino'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthColumnaDestino');doSubmit('hthTablaDestino/detalle')"><fmt:message key="hthTablaDestino.encabezado.etiqueta.hthColumnaDestinoDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>