<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthBitacoraProcesoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthBitacoraProcesoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="hthBitacoraProceso"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="fechaProcesoMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthBitacoraProceso.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthBitacoraProceso.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="fechaProcesoMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="17" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthBitacoraProceso/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="18" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="19" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthBitacoraProceso/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthBitacoraProceso.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaProceso']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.FechaProceso"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="2" type="text" cdate="fechaProcesoMax" cattr="minDate" id="fechaProcesoMin" name="fechaProcesoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${hthBitacoraProcesoFiltro['fechaProcesoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="2" type="text" cdate="fechaProcesoMin" cattr="maxDate" id="fechaProcesoMax" name="fechaProcesoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${hthBitacoraProcesoFiltro['fechaProcesoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaProceso'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['resultadoProceso']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.ResultadoProceso"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="3" id="resultadoProceso" name="resultadoProceso" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="O"<c:if test="${hthBitacoraProcesoFiltro['resultadoProceso'] == 'O'}">selected="selected"</c:if>	><fmt:message key="hthBitacoraProceso.formulario.comboLabel.OK"/></option>
				    <option value="E"<c:if test="${hthBitacoraProcesoFiltro['resultadoProceso'] == 'E'}">selected="selected"</c:if>	><fmt:message key="hthBitacoraProceso.formulario.comboLabel.ERROR"/></option>
				    <option value="A"<c:if test="${hthBitacoraProcesoFiltro['resultadoProceso'] == 'A'}">selected="selected"</c:if>	><fmt:message key="hthBitacoraProceso.formulario.comboLabel.ADVERTENCIA"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['resultadoProceso'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcionResultado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.DescripcionResultado"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="descripcionResultado" name="descripcionResultado" value="${hthBitacoraProcesoFiltro['descripcionResultado']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcionResultado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['stackTrace']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.StackTrace"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="stackTrace" name="stackTrace" value="${hthBitacoraProcesoFiltro['stackTrace']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['stackTrace'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.Cliente"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="cliente" name="cliente" value="${hthBitacoraProcesoFiltro['cliente']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.Convenio"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" type="text" id="convenio" name="convenio" value="${hthBitacoraProcesoFiltro['convenio']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['archivo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.Archivo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="archivo" name="archivo" value="${hthBitacoraProcesoFiltro['archivo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['archivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['lineasBitacora']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthBitacoraProceso.filtro.etiqueta.LineasBitacora"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="lineasBitacora" name="lineasBitacora" value="${hthBitacoraProcesoFiltro['lineasBitacora']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['lineasBitacora'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>
