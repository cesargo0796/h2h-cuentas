<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty hthBitacoraProcesoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthBitacoraProcesoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="hthBitacoraProceso"/>
<c:set var="ubicacion" value="actualizar"/>	
<c:set var="primerCampo" value="fechaProceso"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">

	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthBitacoraProceso.bc.etiqueta.actualizar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>	
	
<div id="iProcessing" style="visibility: hidden" align="center">
	<img src="${ctx}/include/images/design/processing_request.gif"></img>
</div>

<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthBitacoraProceso.formulario.etiqueta.actualizar.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="actualizar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="fechaProceso"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	<input type="hidden" id="idBitacoraProceso" name="idBitacoraProceso" value="${hthBitacoraProceso.idBitacoraProceso}"/>	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="9" id="btnGuardar" type="submit" name="actualizar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('hthBitacoraProceso/actualizarEjecutar');"><fmt:message key="globales.formulario.boton.actualizar.guardar"/></button>
	<button tabindex="10" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('hthBitacoraProceso/cancelarActualizar');"><fmt:message key="globales.formulario.boton.actualizar.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthBitacoraProceso.formulario.etiqueta.actualizar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaProceso']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.FechaProceso"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" type="text" cdate="fechaProceso" id="fechaProceso" name="fechaProceso"  value="<fmt:formatDate pattern="dd-MM-yyyy" value="${hthBitacoraProceso.fechaProceso}"/>" size="10" />
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','fechaProceso')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaProceso'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['resultadoProceso']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.ResultadoProceso"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select  tabindex="2" id="resultadoProceso" name="resultadoProceso" class="ui-widget ui-state-default">
		            <option value="O" <c:if test="${hthBitacoraProceso.resultadoProceso == 'O'}">selected</c:if>><fmt:message key="hthBitacoraProceso.formulario.comboLabel.OK"/></option>
		            <option value="E" <c:if test="${hthBitacoraProceso.resultadoProceso == 'E'}">selected</c:if>><fmt:message key="hthBitacoraProceso.formulario.comboLabel.ERROR"/></option>
		            <option value="A" <c:if test="${hthBitacoraProceso.resultadoProceso == 'A'}">selected</c:if>><fmt:message key="hthBitacoraProceso.formulario.comboLabel.ADVERTENCIA"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','resultadoProceso')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['resultadoProceso'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcionResultado']}">	
		<div class="row">
			<span class="label" style="color: #AA0707;"> 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.DescripcionResultado"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="3" id="descripcionResultado" name="descripcionResultado" cols="45" rows="3"><c:out value="${hthBitacoraProceso.descripcionResultado}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','descripcionResultado')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcionResultado'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['stackTrace']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.StackTrace"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="4" id="stackTrace" name="stackTrace" cols="45" rows="3"><c:out value="${hthBitacoraProceso.stackTrace}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','stackTrace')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['stackTrace'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cliente']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.Cliente"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="5" type="text" id="cliente" name="cliente"  value="${hthBitacoraProceso.cliente}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','cliente')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cliente'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.Convenio"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="6" type="text" id="convenio" name="convenio"  value="${hthBitacoraProceso.convenio}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','convenio')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['archivo']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.Archivo"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="7" id="archivo" name="archivo" cols="45" rows="3"><c:out value="${hthBitacoraProceso.archivo}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','archivo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['archivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['lineasBitacora']}">	
		<div class="row">
			<span class="label" > 
				<fmt:message key="hthBitacoraProceso.formulario.etiqueta.LineasBitacora"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="8" id="lineasBitacora" name="lineasBitacora" cols="45" rows="3"><c:out value="${hthBitacoraProceso.lineasBitacora}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthBitacoraProceso/ayudaPropiedadJson','lineasBitacora')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['lineasBitacora'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
