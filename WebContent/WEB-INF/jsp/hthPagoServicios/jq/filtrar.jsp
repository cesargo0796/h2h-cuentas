<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle
	basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	<c:if test="${ not empty hthPagoServiciosEstado.enlaceDetalle}">
		<c:set var="detalle" value="${hthPagoServiciosEstado.enlaceDetalle}" />
		<c:set var="detalleEnllavador" value="${detalle.enllavador}" />
	</c:if>

	<c:set var="nombre" value="hthPagoServicios" />
	<c:set var="ubicacion" value="filtrar" />
	<c:set var="primerCampo" value="idCliente" />
	<%@ include file="/WEB-INF/global/jsp/jq-header.jsp"%>

	<form name="mainForm" id="mainForm"
		action="${NewtPageRenderData.actionUrl}" method="post">
		<c:if test="${ not empty detalle.cabecera}">
			<jsp:include
				page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
		</c:if>
		<div id="mainContent">
			<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
				<div id="breadcrumb"
					class="breadCrumb module ui-widget ui-widget-content"
					style="width: 99%;">
					<ul>
						<li><a href="${ctx}/inicio/blank/ria"> HOME </a></li>
						<c:if test="${ not empty ubicacionAplicacion.ruta }">
							<input type="hidden" name="idUbicacionAplicacionActual"
								id="idUbicacionAplicacionActual"
								value="${ ubicacionAplicacion.idUbicacionAplicacionActual }" />
							<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
								<li><a
									href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out
											value="${ ub.label }" escapeXml="false" /></a></li>
							</c:forEach>
						</c:if>
						<li><fmt:message key="hthPagoServicios.bc.etiqueta.filtrar" /></li>
					</ul>
				</div>
				<input type="hidden" name="id_ubicacion_retorno"
					id="id_ubicacion_retorno" value="" />
			</div>

			<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthPagoServicios.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
			<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar" /> <input
				type="hidden" id="primerCampo" name="primerCampo" value="idCliente" />
			<input type="hidden" id="eag" name="eag" value="${eag}" /> <input
				type="hidden" id="movimientoPagina" name="movimientoPagina" /> <input
				type="hidden" id="columna" name="columna" /> <input type="hidden"
				id="fenixf31seguridad" name="fenixf31seguridad"
				value="<c:out value='${fenixf31seguridad}' />" />
			<c:if test="${not empty scrollTo }">
				<input type="hidden" id="scrollSmooth"
					value="<c:out value='${scrollTo}' />" />
			</c:if>
			<!-- valores de llaves -->
			<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


			<!-- mensajes de error-->
			<c:if test="${mensajesUsuario.erroresGlobales}">
				<div id="iError">
					<b class="ui-round-top"> <b class="ui-line-one"></b><b
						class="ui-line-two"></b><b class="ui-line-three"></b><b
						class="ui-line-four"></b>
					</b>
					<table>
						<tr>
							<td>
								<ol>
									<c:forEach items="${mensajesUsuario.mensajesDeError}"
										var="mensaje">
										<li><c:out value="${mensaje}" escapeXml="false" /></li>
									</c:forEach>
								</ol>
							</td>
						</tr>
					</table>
					<b class="ui-round-bottom"> <b class="ui-line-four"></b><b
						class="ui-line-three"></b><b class="ui-line-two"></b><b
						class="ui-line-one"></b>
					</b>
				</div>
			</c:if>

			<div id="action-buttons">
				<div id="action-bar">
					<button tabindex="41" id="btnAplicarFiltro" type="submit"
						name="Filtrar"
						onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthPagoServicios/filtrarEjecutar');">
						<fmt:message key="globales.filtro.boton.aplicarFiltro" />
					</button>
					<button tabindex="42" id="btnLimpiar" type="button" name="Limpiar"
						onClick="limpiarCampos('mainForm');">
						<fmt:message key="globales.filtro.boton.limpiar" />
					</button>
					<button tabindex="43" id="btnCancelar" type="button"
						name="Cancelar"
						onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthPagoServicios/cancelarFiltrar');">
						<fmt:message key="globales.filtro.boton.cancelar" />
					</button>
				</div>
			</div>
			<br />

			<!-- datos de formulario -->
			<div class="ui-widget ui-widget-content ui-corner-all"
				style="width: 99%">
				<div id="formInputHeader"
					class="ui-widget ui-widget-header ui-corner-top"
					style="padding-left: 5px;">
					<fmt:message key="hthPagoServicios.formulario.etiqueta.filtrar.titulo" />
				</div>
				<div style="display: inline;" id="formInput">	
				<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idCliente']}">
							<div class="row" >
								<span class="label"> 
									<fmt:message key="hthPagoServicios.filtro.etiqueta.idCliente"/>
								</span>
								<span class="forminput" style="display: inline">
						            	            <input type="text" id="idCliente" name="idCliente" value="${hthPagoServiciosFiltro['bsCliente'].cliente}"  size="10"  readonly="readonly"/>
					                <input type="text" id="bsCliente.nombre" name="bsCliente.nombre" value="${hthPagoServiciosFiltro['bsCliente'].nombre}"  size="50"  readonly="readonly"/>		
									<button tabindex="1" id="btnbsCliente" class="sq" onClick="doForaneo('bsCliente', 'hthPagoServicios/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
								</span>
								<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idCliente'].mensaje}" var="mensaje">
								<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
								</c:forEach>
							</div>	
					</c:if>				
					
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numCuenta']}">
						<div class="row">
							<span class="label"> <fmt:message key="hthPagoServicios.formulario.etiqueta.numCuenta" />
							</span> 
							<span class="forminput" style="display: inline"> 
								<input tabindex="2" type="text" id="idnumCuenta" name="numCuenta" value="${hthPagoServiciosFiltro['numCuenta']}" />
							</span>
							<c:forEach
								items="${mensajesUsuario.mapaDeErroresEnPropiedades['numCuenta'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>

						</div>
					</c:if>			
				
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estado']}">
						<div class="row">
							<span class="label"> <fmt:message key="hthPagoServicios.formulario.etiqueta.estado" />
							</span> 
							<span class="forminput" style="display: inline"> 											
				                <select  id="idEstado" name="estado" tabindex="3">			                
			            			<option value="ACTIVO" 
			            				<c:if test="${hthPagoServiciosFiltro['estado'] == 'ACTIVO'}">selected="selected">selected</c:if>
			            			>
			            				<fmt:message key="hthPagoServicios.formulario.comboLabel.estadoActivo"/>
			            			</option>	                
			            			<option value="INACTIVO" 
			            				<c:if test="${hthPagoServiciosFiltro['estado'] == 'INACTIVO'}">selected="selected">selected</c:if>
			            			>
			            				<fmt:message key="hthPagoServicios.formulario.comboLabel.estadoInactivo"/>
			            			</option>
				                </select>
							</span>
							<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estado'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>
						</div>
					</c:if>
					<c:if
						test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nit']}">
						<div class="row">
							<span class="label"> <fmt:message key="hthPagoServicios.formulario.etiqueta.nit" />
							</span> 
							<span class="forminput" style="display: inline"> 
								<input tabindex="4" type="text" id="idnit" name="nit" value="${hthPagoServiciosFiltro['nit']}" />
							</span>
							<c:forEach
								items="${mensajesUsuario.mapaDeErroresEnPropiedades['nit'].mensaje}" var="mensaje">
								<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false" />
								</span>
							</c:forEach>

						</div>
					</c:if>	
				</div>


			</div>
		</div>
	</form>

	<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp"%>
</fmt:bundle>
</body>