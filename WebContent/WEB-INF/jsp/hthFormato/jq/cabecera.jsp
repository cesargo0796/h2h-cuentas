<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="idFormato" name="idFormato" value="${hthFormatoDtoCabeza.idFormato}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${hthFormatoEnlaceDetalle == 'hthAtributoFormato'}">
	<c:set var="nombre" value="hthAtributoFormato"/>
</c:if>
<c:if test="${hthFormatoEnlaceDetalle == 'hthProcesoMonitoreo'}">
	<c:set var="nombre" value="hthProcesoMonitoreo"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('hthFormato/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('hthFormato/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="hthFormato.encabezado.etiqueta.titulo"/> ${hthFormatoDtoCabeza.idFormato} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="hthFormato.encabezado.etiqueta.NombreFormato"/>:</span>
		    ${hthFormatoDtoCabeza.nombreFormato} 
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="hthFormato.encabezado.etiqueta.TipoFormato"/>:</span>
		    <c:if test="${hthFormatoDtoCabeza.tipoFormato == 'D'}"><fmt:message key="hthFormato.formulario.comboLabel.Detalle"/></c:if><c:if test="${hthFormatoDtoCabeza.tipoFormato == 'E'}"><fmt:message key="hthFormato.formulario.comboLabel.Encabezado"/></c:if> &nbsp;|        	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="hthFormato.encabezado.etiqueta.NombreFormato"/></strong>&nbsp;
		${hthFormatoDtoCabeza.nombreFormato}
		</div>
		
	    <div>
		<strong><fmt:message key="hthFormato.encabezado.etiqueta.TipoFormato"/></strong>&nbsp;
		<c:if test="${hthFormatoDtoCabeza.tipoFormato == 'D'}"><fmt:message key="hthFormato.formulario.comboLabel.Detalle"/></c:if> 
		<c:if test="${hthFormatoDtoCabeza.tipoFormato == 'E'}"><fmt:message key="hthFormato.formulario.comboLabel.Encabezado"/></c:if> 
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${hthFormatoEnlaceDetalle == 'hthAtributoFormato'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthAtributoFormato');doSubmit('hthFormato/detalle')" class="ui-selected"><fmt:message key="hthFormato.encabezado.etiqueta.hthAtributoFormatoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthFormatoEnlaceDetalle != 'hthAtributoFormato'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthAtributoFormato');doSubmit('hthFormato/detalle')"><fmt:message key="hthFormato.encabezado.etiqueta.hthAtributoFormatoDrillDown"/></a>
		</li>
	</c:if>		
	<%-- <c:if test="${hthFormatoEnlaceDetalle == 'hthProcesoMonitoreo'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthProcesoMonitoreo');doSubmit('hthFormato/detalle')" class="ui-selected"><fmt:message key="hthFormato.encabezado.etiqueta.hthProcesoMonitoreoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthFormatoEnlaceDetalle != 'hthProcesoMonitoreo'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthProcesoMonitoreo');doSubmit('hthFormato/detalle')"><fmt:message key="hthFormato.encabezado.etiqueta.hthProcesoMonitoreoDrillDown"/></a>
		</li>
	</c:if>		 --%>
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>