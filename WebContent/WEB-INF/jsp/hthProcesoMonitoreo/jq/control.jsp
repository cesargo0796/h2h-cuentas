<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${hthProcesoMonitoreoEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty hthProcesoMonitoreoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthProcesoMonitoreoEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${hthProcesoMonitoreoEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${hthProcesoMonitoreoEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty hthProcesoMonitoreoSeg}">
	<c:set var="restriccion" value="${hthProcesoMonitoreoSeg}"/>
</c:if>

<c:set var="nombre" value="hthProcesoMonitoreo"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<script>

	function obtenerEstado(){
		var action = 'hthProcesoMonitoreo/estadoProcesos';
		var mainForm = $("#mainForm");	
				mainForm.ajaxForm().ajaxSubmit({
				url: '${ctx}${sPath}/' + action + '/${tOut}',
				type: "POST",
				success: function(data){
					console.log('Obtenido: ' + data);
					var divEstatus = $('#estatus');
					divEstatus.empty();
					divEstatus.append('<ul>')
					divEstatus.append('<li><strong>Motor de procesos: </strong> ' + data.estadoCalendarizador + "</li>");
					divEstatus.append('<li><strong>Motor de procesos iniciado desde: </strong> ' + data.calendarizadorIniciado + "</li>");
					divEstatus.append('<li><strong>Estado: </strong> ' + data.estado + "</li>");
					divEstatus.append('<li><strong>Inicio de Ejecuci&oacute;n Monitor Principal: </strong> ' + data.horaInicio + "</li>");
					divEstatus.append('<li><strong>Finalizacion de Ejecuci&oacute;n Monitor Principal: </strong> ' + data.horaFin + "</li>");
					divEstatus.append('<li><strong>Procesos en Ejecuci&oacute;n: </strong> ' + data.procesos + "</li>");
					divEstatus.append('</ul>')
				  },
				error: function(jqXHR, textStatus, errorThrown){
					
					var divEstatus = $('#estatus');
					divEstatus.empty();
					divEstatus.append('<div align="center" style="margin-top:30px;"><div class="ui-widget ui-widget-content">Ha ocurrido un error inesperado en el servidor.</div></div>');
				}
			});
	}
				
	$(document).ready(function(){
		console.log('Enviando obtener estado');
		obtenerEstado();
		console.log('Finalizado');
		setTimeout(obtenerEstado, 5000);	
	});
</script>
	

<!-- Elementos no visibles -->
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="dialog-activar" title='Activar'  style="display: none;">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;display:inline;"></span>
	&iquest;Esta seguro que desea <strong><span id="ActDesact"></span></strong> el proceso seleccionado?
</p>
</div>

<div id="dialog-ejecutar" title='Ejecutar'  style="display: none;">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;display:inline;"></span>
	&iquest;Esta seguro que desea <strong>Ejecutar</strong> el proceso seleccionado?
</p>
</div>

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> <fmt:message key="globales.bc.etiqueta.home" /> </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthProcesoMonitoreo.bc.etiqueta.principalcp" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
	
<div id="mainContent">
    <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
    	<fmt:message key="hthProcesoMonitoreo.listado.etiqueta.titulocp"/>
    </div>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="hthProcesoMonitoreo"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="idProcesoMonitoreo" name="idProcesoMonitoreo" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  
	 <input type="hidden" id="estadoPA" name="estadoPA"/>



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div  class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">

	<button id="btnDetener" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/detenerProcesos'); "><fmt:message key="globales.listado.boton.detener"/></button>

	<button id="btnReiniciar" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthProcesoMonitoreo/reiniciarProcesos');"><fmt:message key="globales.listado.boton.reiniciar"/></button>
	
</div>

</div>
<br/>
<div class="ui-widget ui-panel-content ui-corner-all">
<div class="ui-state-default ui-widget-header ui-corner-top">
   &nbsp;&nbsp;&nbsp; Estado de Calendarizador de Procesos
</div>
<div id="estatus" class="ui-panel-content ui-widget-content ui-corner-bottom">
	
</div>
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>