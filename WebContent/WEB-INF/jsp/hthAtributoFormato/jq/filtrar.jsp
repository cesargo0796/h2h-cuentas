<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthAtributoFormatoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthAtributoFormatoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="hthAtributoFormato"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="btnhthAtributoEncabezado"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> <fmt:message key="globales.bc.etiqueta.home" /> </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthAtributoFormato.bc.etiqueta.filtrar" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	
    <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
    	<fmt:message key="hthAtributoFormato.formulario.etiqueta.filtrar.titulo"/>
    </div>
	
	<div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	
	<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnhthAtributoEncabezado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="19" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthAtributoFormato/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="20" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="21" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthAtributoFormato/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthAtributoFormato.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idAtributoEncabezado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.IdAtributoEncabezado"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idAtributoEncabezado" name="idAtributoEncabezado" value="${hthAtributoFormatoFiltro['hthAtributoEncabezado'].idAtributoEncabezado}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthAtributoEncabezado.nombreAtributo" name="hthAtributoEncabezado.nombreAtributo" value="${hthAtributoFormatoFiltro['hthAtributoEncabezado'].nombreAtributo}"  size="40"  readonly="readonly"/>		
				<button tabindex="2" id="btnhthAtributoEncabezado" class="sq" onClick="doForaneo('hthAtributoEncabezado', 'hthAtributoFormato/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idAtributoEncabezado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idAtributoDetalle']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.IdAtributoDetalle"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idAtributoDetalle" name="idAtributoDetalle" value="${hthAtributoFormatoFiltro['hthAtributoDetalle'].idAtributoDetalle}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthAtributoDetalle.nombreAtributo" name="hthAtributoDetalle.nombreAtributo" value="${hthAtributoFormatoFiltro['hthAtributoDetalle'].nombreAtributo}"  size="40"  readonly="readonly"/>		
				<button tabindex="3" id="btnhthAtributoDetalle" class="sq" onClick="doForaneo('hthAtributoDetalle', 'hthAtributoFormato/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idAtributoDetalle'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idFormato']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.IdFormato"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idFormato" name="idFormato" value="${hthAtributoFormatoFiltro['hthFormato'].idFormato}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthFormato.nombreFormato" name="hthFormato.nombreFormato" value="${hthAtributoFormatoFiltro['hthFormato'].nombreFormato}"  size="40"  readonly="readonly"/>		
				<button tabindex="4" id="btnhthFormato" class="sq" onClick="doForaneo('hthFormato', 'hthAtributoFormato/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idFormato'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['posicion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.Posicion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integerMin" type="text" id="posicionMin" name="posicionMin" value="${hthAtributoFormatoFiltro['posicionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="5" alt="integerMax" type="text" id="posicionMax" name="posicionMax" value="${hthAtributoFormatoFiltro['posicionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['posicion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['posicionIni']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.PosicionIni"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integerMin" type="text" id="posicionIniMin" name="posicionIniMin" value="${hthAtributoFormatoFiltro['posicionIniMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="6" alt="integerMax" type="text" id="posicionIniMax" name="posicionIniMax" value="${hthAtributoFormatoFiltro['posicionIniMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['posicionIni'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['posicionFin']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.PosicionFin"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="7" alt="integerMin" type="text" id="posicionFinMin" name="posicionFinMin" value="${hthAtributoFormatoFiltro['posicionFinMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="7" alt="integerMax" type="text" id="posicionFinMax" name="posicionFinMax" value="${hthAtributoFormatoFiltro['posicionFinMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['posicionFin'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoDatoAtributo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.TipoDatoAtributo"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="8" id="tipoDatoAtributo" name="tipoDatoAtributo" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="T"<c:if test="${hthAtributoFormatoFiltro['tipoDatoAtributo'] == 'T'}">selected="selected"</c:if>	><fmt:message key="hthAtributoFormato.formulario.comboLabel.Texto"/></option>
				    <option value="E"<c:if test="${hthAtributoFormatoFiltro['tipoDatoAtributo'] == 'E'}">selected="selected"</c:if>	><fmt:message key="hthAtributoFormato.formulario.comboLabel.Entero"/></option>
				    <option value="D"<c:if test="${hthAtributoFormatoFiltro['tipoDatoAtributo'] == 'D'}">selected="selected"</c:if>	><fmt:message key="hthAtributoFormato.formulario.comboLabel.Decimal"/></option>
				    <option value="F"<c:if test="${hthAtributoFormatoFiltro['tipoDatoAtributo'] == 'F'}">selected="selected"</c:if>	><fmt:message key="hthAtributoFormato.formulario.comboLabel.Fecha"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoDatoAtributo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['formatoAtributo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.FormatoAtributo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="formatoAtributo" name="formatoAtributo" value="${hthAtributoFormatoFiltro['formatoAtributo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['formatoAtributo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['opcional']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthAtributoFormato.filtro.etiqueta.Opcional"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="10" id="opcional" name="opcional" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="S"<c:if test="${hthAtributoFormatoFiltro['opcional'] == 'S'}">selected="selected"</c:if>	><fmt:message key="hthAtributoFormato.formulario.comboLabel.Si"/></option>
				    <option value="N"<c:if test="${hthAtributoFormatoFiltro['opcional'] == 'N'}">selected="selected"</c:if>	><fmt:message key="hthAtributoFormato.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['opcional'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>