<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty hthParametroSistemaEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${hthParametroSistemaEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty hthParametroSistemaEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthParametroSistemaEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${hthParametroSistemaEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${hthParametroSistemaEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty hthParametroSistemaSeg}">
	<c:set var="restriccion" value="${hthParametroSistemaSeg}"/>
</c:if>

<c:set var="nombre" value="hthParametroSistema"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthParametroSistema.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthParametroSistema.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="hthParametroSistema"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="idParametroSistema" name="idParametroSistema" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="tiempoMonitoreoDefault"><fmt:message key="hthParametroSistema.listado.etiqueta.TiempoMonitoreoDefault"/></option>
			<option value="horaInicioMonitoreo"><fmt:message key="hthParametroSistema.listado.etiqueta.HoraInicioMonitoreo"/></option>
			<option value="horaFinMonitoreo"><fmt:message key="hthParametroSistema.listado.etiqueta.HoraFinMonitoreo"/></option>
			<option value="carpetaLecturaDefault"><fmt:message key="hthParametroSistema.listado.etiqueta.CarpetaLecturaDefault"/></option>
			<option value="carpetaEscrituraDefault"><fmt:message key="hthParametroSistema.listado.etiqueta.CarpetaEscrituraDefault"/></option>
			<option value="rutaRaiz"><fmt:message key="hthParametroSistema.listado.etiqueta.RutaRaiz"/></option>
			<option value="carSeparadorDefault"><fmt:message key="hthParametroSistema.listado.etiqueta.CarSeparadorDefault"/></option>
			<option value="habilitarEnvioCorreo"ctype="combo"><fmt:message key="hthParametroSistema.listado.etiqueta.HabilitarEnvioCorreo"/></option>
			<option value="remitenteEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.RemitenteEnvioCorreo"/></option>
			<option value="asuntoEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.AsuntoEnvioCorreo"/></option>
			<option value="javamailJndi"><fmt:message key="hthParametroSistema.listado.etiqueta.JavamailJndi"/></option>
			<option value="listaEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.ListaEnvioCorreo"/></option>
			<option value="servidorEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.ServidorEnvioCorreo"/></option>
			<option value="usuarioEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.UsuarioEnvioCorreo"/></option>
			<option value="passwordEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.PasswordEnvioCorreo"/></option>
			<option value="puertoEnvioCorreo"><fmt:message key="hthParametroSistema.listado.etiqueta.PuertoEnvioCorreo"/></option>
			<option value="directorioLocalDestino"><fmt:message key="hthParametroSistema.listado.etiqueta.DirectorioLocalDestino"/></option>
			<option value="hostSftp"><fmt:message key="hthParametroSistema.listado.etiqueta.HostSftp"/></option>
			<option value="puertoSftp"><fmt:message key="hthParametroSistema.listado.etiqueta.PuertoSftp"/></option>
			<option value="usuarioSftp"><fmt:message key="hthParametroSistema.listado.etiqueta.UsuarioSftp"/></option>
			<option value="passwordSftp"><fmt:message key="hthParametroSistema.listado.etiqueta.PasswordSftp"/></option>
			<option value="llaveSftp"><fmt:message key="hthParametroSistema.listado.etiqueta.LlaveSftp"/></option>
			<option value="tiempoDrenadoBitacora"><fmt:message key="hthParametroSistema.listado.etiqueta.TiempoDrenadoBitacora"/></option>
		</select> &nbsp;
                <select  id="habilitarEnvioCorreo" name="habilitarEnvioCorreo" onchange="$('#busqueda').val(this.value); doFiltrarPor('hthParametroSistema');" class="ui-widget ui-state-default" style="display: none;">
				    <option value=""/>			
				    <option value="S"><fmt:message key="hthParametroSistema.formulario.comboLabel.Si"/></option>
				    <option value="N"><fmt:message key="hthParametroSistema.formulario.comboLabel.No"/></option>
                </select>
		<input onkeypress="doFiltrarPor('hthParametroSistema', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('tiempoMonitoreoDefault','${nombre}')" sort="${orden.mapaDeValores['tiempoMonitoreoDefault'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.TiempoMonitoreoDefault"/></th>
  			<th onclick="doSort('horaInicioMonitoreo','${nombre}')" sort="${orden.mapaDeValores['horaInicioMonitoreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.HoraInicioMonitoreo"/></th>
  			<th onclick="doSort('horaFinMonitoreo','${nombre}')" sort="${orden.mapaDeValores['horaFinMonitoreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.HoraFinMonitoreo"/></th>
  			<th onclick="doSort('carpetaLecturaDefault','${nombre}')" sort="${orden.mapaDeValores['carpetaLecturaDefault'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.CarpetaLecturaDefault"/></th>
  			<th onclick="doSort('carpetaEscrituraDefault','${nombre}')" sort="${orden.mapaDeValores['carpetaEscrituraDefault'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.CarpetaEscrituraDefault"/></th>
  			<th onclick="doSort('rutaRaiz','${nombre}')" sort="${orden.mapaDeValores['rutaRaiz'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.RutaRaiz"/></th>
  			<th onclick="doSort('carSeparadorDefault','${nombre}')" sort="${orden.mapaDeValores['carSeparadorDefault'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.CarSeparadorDefault"/></th>
  			<th onclick="doSort('habilitarEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['habilitarEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.HabilitarEnvioCorreo"/></th>
  			<th onclick="doSort('remitenteEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['remitenteEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.RemitenteEnvioCorreo"/></th>
  			<th onclick="doSort('asuntoEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['asuntoEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.AsuntoEnvioCorreo"/></th>
  			<th onclick="doSort('javamailJndi','${nombre}')" sort="${orden.mapaDeValores['javamailJndi'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.JavamailJndi"/></th>
  			<th onclick="doSort('listaEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['listaEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.ListaEnvioCorreo"/></th>
  			<th onclick="doSort('servidorEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['servidorEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.ServidorEnvioCorreo"/></th>
  			<th onclick="doSort('usuarioEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['usuarioEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.UsuarioEnvioCorreo"/></th>
  			<th onclick="doSort('passwordEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['passwordEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.PasswordEnvioCorreo"/></th>
  			<th onclick="doSort('puertoEnvioCorreo','${nombre}')" sort="${orden.mapaDeValores['puertoEnvioCorreo'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.PuertoEnvioCorreo"/></th>
  			<th onclick="doSort('directorioLocalDestino','${nombre}')" sort="${orden.mapaDeValores['directorioLocalDestino'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.DirectorioLocalDestino"/></th>
  			<th onclick="doSort('hostSftp','${nombre}')" sort="${orden.mapaDeValores['hostSftp'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.HostSftp"/></th>
  			<th onclick="doSort('puertoSftp','${nombre}')" sort="${orden.mapaDeValores['puertoSftp'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.PuertoSftp"/></th>
  			<th onclick="doSort('usuarioSftp','${nombre}')" sort="${orden.mapaDeValores['usuarioSftp'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.UsuarioSftp"/></th>
  			<th onclick="doSort('passwordSftp','${nombre}')" sort="${orden.mapaDeValores['passwordSftp'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.PasswordSftp"/></th>
  			<th onclick="doSort('llaveSftp','${nombre}')" sort="${orden.mapaDeValores['llaveSftp'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.LlaveSftp"/></th>
  			<th onclick="doSort('tiempoDrenadoBitacora','${nombre}')" sort="${orden.mapaDeValores['tiempoDrenadoBitacora'].strOrden}"><fmt:message key="hthParametroSistema.listado.etiqueta.TiempoDrenadoBitacora"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#idParametroSistema').val('${dto.idParametroSistema}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.tiempoMonitoreoDefault}"/></td>
		<td><c:out value="${dto.horaInicioMonitoreo}"/></td>
		<td><c:out value="${dto.horaFinMonitoreo}"/></td>
		<td><c:out value="${dto.carpetaLecturaDefault}"/></td>
		<td><c:out value="${dto.carpetaEscrituraDefault}"/></td>
		<td><c:out value="${dto.rutaRaiz}"/></td>
		<td><c:out value="${dto.carSeparadorDefault}"/></td>
		<td>
		<c:if test="${dto.habilitarEnvioCorreo == 'S'}"><fmt:message key="hthParametroSistema.formulario.comboLabel.Si"/></c:if>
		<c:if test="${dto.habilitarEnvioCorreo == 'N'}"><fmt:message key="hthParametroSistema.formulario.comboLabel.No"/></c:if>
	    </td>
		<td><c:out value="${dto.remitenteEnvioCorreo}"/></td>
		<td><c:out value="${dto.asuntoEnvioCorreo}"/></td>
		<td><c:out value="${dto.javamailJndi}"/></td>
		<td><c:out value="${dto.listaEnvioCorreo}"/></td>
		<td><c:out value="${dto.servidorEnvioCorreo}"/></td>
		<td><c:out value="${dto.usuarioEnvioCorreo}"/></td>
		<td><c:out value="${dto.passwordEnvioCorreo}"/></td>
		<td><c:out value="${dto.puertoEnvioCorreo}"/></td>
		<td><c:out value="${dto.directorioLocalDestino}"/></td>
		<td><c:out value="${dto.hostSftp}"/></td>
		<td><c:out value="${dto.puertoSftp}"/></td>
		<td><c:out value="${dto.usuarioSftp}"/></td>
		<td><c:out value="${dto.passwordSftp}"/></td>
		<td><c:out value="${dto.llaveSftp}"/></td>
		<td><c:out value="${dto.tiempoDrenadoBitacora}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>