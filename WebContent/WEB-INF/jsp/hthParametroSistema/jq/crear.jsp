<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${locale}" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
	
<c:if test="${ not empty hthParametroSistemaEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthParametroSistemaEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>

<c:set var="nombre" value="hthParametroSistema"/>
<c:set var="ubicacion" value="crear"/>
<c:set var="primerCampo" value="tiempoMonitoreoDefault"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthParametroSistema.bc.etiqueta.crear" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>	
	
	
<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthParametroSistema.formulario.etiqueta.crear.titulo"/>
</div> --%>
	<input type="hidden" id="pageLoc" name="pageLoc" value="crear"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="tiempoMonitoreoDefault"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
	
	


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div class="ui-state-error ui-corner-all" style="padding: 0 .7em; font-size: 0.8em;"> 
	<span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em; margin-top: .3em;"></span> 
	<strong><fmt:message key="globales.formulario.etiqueta.error"/></strong> <br /> 
	<ul>
		<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
     	</c:forEach>
	</ul>
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
    <button tabindex="24" id="btnGuardar" type="submit" name="ok" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/crearEjecutar');"><fmt:message key="globales.formulario.boton.crear.guardar"/></button>
	<button tabindex="25" id="btnCancelar" type="button" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/cancelarCrear');"><fmt:message key="globales.formulario.boton.crear.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div><span style="font-size: small;">-Los campos marcados con * son obligatorios</span></div>
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthParametroSistema.formulario.etiqueta.crear.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tiempoMonitoreoDefault']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.TiempoMonitoreoDefault"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="1" alt="integer" type="text" id="tiempoMonitoreoDefault" name="tiempoMonitoreoDefault"  value="${hthParametroSistema.tiempoMonitoreoDefault}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','tiempoMonitoreoDefault')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tiempoMonitoreoDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['horaInicioMonitoreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.HoraInicioMonitoreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="2" type="text" id="horaInicioMonitoreo" name="horaInicioMonitoreo"  value="${hthParametroSistema.horaInicioMonitoreo}" size="50"  maxlength="12"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','horaInicioMonitoreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['horaInicioMonitoreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['horaFinMonitoreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.HoraFinMonitoreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="3" type="text" id="horaFinMonitoreo" name="horaFinMonitoreo"  value="${hthParametroSistema.horaFinMonitoreo}" size="50"  maxlength="12"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','horaFinMonitoreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['horaFinMonitoreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['carpetaLecturaDefault']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.CarpetaLecturaDefault"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="4" type="text" id="carpetaLecturaDefault" name="carpetaLecturaDefault"  value="${hthParametroSistema.carpetaLecturaDefault}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','carpetaLecturaDefault')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['carpetaLecturaDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['carpetaEscrituraDefault']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.CarpetaEscrituraDefault"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="5" type="text" id="carpetaEscrituraDefault" name="carpetaEscrituraDefault"  value="${hthParametroSistema.carpetaEscrituraDefault}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','carpetaEscrituraDefault')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['carpetaEscrituraDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaRaiz']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.RutaRaiz"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="6" id="rutaRaiz" name="rutaRaiz" cols="45" rows="3"><c:out value="${hthParametroSistema.rutaRaiz}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','rutaRaiz')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaRaiz'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['carSeparadorDefault']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.CarSeparadorDefault"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="7" type="text" id="carSeparadorDefault" name="carSeparadorDefault"  value="${hthParametroSistema.carSeparadorDefault}" size="50"  maxlength="10"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','carSeparadorDefault')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['carSeparadorDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['habilitarEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.HabilitarEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
	            <select tabindex="8" id="habilitarEnvioCorreo" name="habilitarEnvioCorreo" class="ui-widget ui-state-default">
		            <option value="S" <c:if test="${hthParametroSistema.habilitarEnvioCorreo == 'S'}">selected</c:if>><fmt:message key="hthParametroSistema.formulario.comboLabel.Si"/></option>
		            <option value="N" <c:if test="${hthParametroSistema.habilitarEnvioCorreo == 'N'}">selected</c:if>><fmt:message key="hthParametroSistema.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','habilitarEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['habilitarEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['remitenteEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.RemitenteEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="9" type="text" id="remitenteEnvioCorreo" name="remitenteEnvioCorreo"  value="${hthParametroSistema.remitenteEnvioCorreo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','remitenteEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['remitenteEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['asuntoEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.AsuntoEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="10" id="asuntoEnvioCorreo" name="asuntoEnvioCorreo" cols="45" rows="3"><c:out value="${hthParametroSistema.asuntoEnvioCorreo}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','asuntoEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['asuntoEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['javamailJndi']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.JavamailJndi"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="11" type="text" id="javamailJndi" name="javamailJndi"  value="${hthParametroSistema.javamailJndi}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','javamailJndi')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['javamailJndi'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['listaEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.ListaEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="12" id="listaEnvioCorreo" name="listaEnvioCorreo" cols="45" rows="3"><c:out value="${hthParametroSistema.listaEnvioCorreo}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','listaEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['listaEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['servidorEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.ServidorEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="13" type="text" id="servidorEnvioCorreo" name="servidorEnvioCorreo"  value="${hthParametroSistema.servidorEnvioCorreo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','servidorEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['servidorEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.UsuarioEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="14" type="text" id="usuarioEnvioCorreo" name="usuarioEnvioCorreo"  value="${hthParametroSistema.usuarioEnvioCorreo}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','usuarioEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['passwordEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.PasswordEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="15" id="passwordEnvioCorreo" name="passwordEnvioCorreo" cols="45" rows="3"><c:out value="${hthParametroSistema.passwordEnvioCorreo}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','passwordEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['passwordEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['puertoEnvioCorreo']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.PuertoEnvioCorreo"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integer" type="text" id="puertoEnvioCorreo" name="puertoEnvioCorreo"  value="${hthParametroSistema.puertoEnvioCorreo}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','puertoEnvioCorreo')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['puertoEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['directorioLocalDestino']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.DirectorioLocalDestino"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="17" type="text" id="directorioLocalDestino" name="directorioLocalDestino"  value="${hthParametroSistema.directorioLocalDestino}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','directorioLocalDestino')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['directorioLocalDestino'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['hostSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.HostSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="18" type="text" id="hostSftp" name="hostSftp"  value="${hthParametroSistema.hostSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','hostSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['hostSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['puertoSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.PuertoSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="19" type="text" id="puertoSftp" name="puertoSftp"  value="${hthParametroSistema.puertoSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','puertoSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['puertoSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.UsuarioSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="20" type="text" id="usuarioSftp" name="usuarioSftp"  value="${hthParametroSistema.usuarioSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','usuarioSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['passwordSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.PasswordSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  			  <input tabindex="21" type="text" id="passwordSftp" name="passwordSftp"  value="${hthParametroSistema.passwordSftp}" size="50"  maxlength="100"/>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','passwordSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['passwordSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['llaveSftp']}">	
		<div class="row" >
			<span class="label" > 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.LlaveSftp"/>			</span>	 
			
			<span class="forminput" style="display: inline">
  		      <textarea tabindex="22" id="llaveSftp" name="llaveSftp" cols="45" rows="3"><c:out value="${hthParametroSistema.llaveSftp}"/></textarea>
  			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','llaveSftp')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['llaveSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tiempoDrenadoBitacora']}">	
		<div class="row" >
			<span class="label" style="color: #AA0707;"> 
                <fmt:message key="hthParametroSistema.formulario.etiqueta.TiempoDrenadoBitacora"/><fmt:message key="globales.formulario.etiqueta.requerido"/>			</span>	 
			
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integer" type="text" id="tiempoDrenadoBitacora" name="tiempoDrenadoBitacora"  value="${hthParametroSistema.tiempoDrenadoBitacora}" size="10" maxlength="10"/>
			</span>
			<button type="button" class="btnAyuda" onClick="doSubmitAyuda('hthParametroSistema/ayudaPropiedadJson','tiempoDrenadoBitacora')" ></button>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tiempoDrenadoBitacora'].mensaje}" var="mensaje">
			<span style="font-size: 90%; vertical-align: top;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
			
		</div>
</c:if>	
		</div>
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
