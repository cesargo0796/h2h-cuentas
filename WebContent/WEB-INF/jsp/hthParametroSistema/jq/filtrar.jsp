<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthParametroSistemaEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthParametroSistemaEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="hthParametroSistema"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="tiempoMonitoreoDefaultMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthParametroSistema.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthParametroSistema.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="tiempoMonitoreoDefaultMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="47" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="48" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="49" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthParametroSistema/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthParametroSistema.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tiempoMonitoreoDefault']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.TiempoMonitoreoDefault"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="2" alt="integerMin" type="text" id="tiempoMonitoreoDefaultMin" name="tiempoMonitoreoDefaultMin" value="${hthParametroSistemaFiltro['tiempoMonitoreoDefaultMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="2" alt="integerMax" type="text" id="tiempoMonitoreoDefaultMax" name="tiempoMonitoreoDefaultMax" value="${hthParametroSistemaFiltro['tiempoMonitoreoDefaultMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tiempoMonitoreoDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['horaInicioMonitoreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.HoraInicioMonitoreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="3" type="text" id="horaInicioMonitoreo" name="horaInicioMonitoreo" value="${hthParametroSistemaFiltro['horaInicioMonitoreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['horaInicioMonitoreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['horaFinMonitoreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.HoraFinMonitoreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="horaFinMonitoreo" name="horaFinMonitoreo" value="${hthParametroSistemaFiltro['horaFinMonitoreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['horaFinMonitoreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['carpetaLecturaDefault']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.CarpetaLecturaDefault"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="carpetaLecturaDefault" name="carpetaLecturaDefault" value="${hthParametroSistemaFiltro['carpetaLecturaDefault']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['carpetaLecturaDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['carpetaEscrituraDefault']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.CarpetaEscrituraDefault"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="carpetaEscrituraDefault" name="carpetaEscrituraDefault" value="${hthParametroSistemaFiltro['carpetaEscrituraDefault']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['carpetaEscrituraDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['rutaRaiz']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.RutaRaiz"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" type="text" id="rutaRaiz" name="rutaRaiz" value="${hthParametroSistemaFiltro['rutaRaiz']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['rutaRaiz'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['carSeparadorDefault']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.CarSeparadorDefault"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="carSeparadorDefault" name="carSeparadorDefault" value="${hthParametroSistemaFiltro['carSeparadorDefault']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['carSeparadorDefault'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['habilitarEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.HabilitarEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="9" id="habilitarEnvioCorreo" name="habilitarEnvioCorreo" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="S"<c:if test="${hthParametroSistemaFiltro['habilitarEnvioCorreo'] == 'S'}">selected="selected"</c:if>	><fmt:message key="hthParametroSistema.formulario.comboLabel.Si"/></option>
				    <option value="N"<c:if test="${hthParametroSistemaFiltro['habilitarEnvioCorreo'] == 'N'}">selected="selected"</c:if>	><fmt:message key="hthParametroSistema.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['habilitarEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['remitenteEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.RemitenteEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="10" type="text" id="remitenteEnvioCorreo" name="remitenteEnvioCorreo" value="${hthParametroSistemaFiltro['remitenteEnvioCorreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['remitenteEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['asuntoEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.AsuntoEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="11" type="text" id="asuntoEnvioCorreo" name="asuntoEnvioCorreo" value="${hthParametroSistemaFiltro['asuntoEnvioCorreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['asuntoEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['javamailJndi']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.JavamailJndi"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="12" type="text" id="javamailJndi" name="javamailJndi" value="${hthParametroSistemaFiltro['javamailJndi']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['javamailJndi'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['listaEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.ListaEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="13" type="text" id="listaEnvioCorreo" name="listaEnvioCorreo" value="${hthParametroSistemaFiltro['listaEnvioCorreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['listaEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['servidorEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.ServidorEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="14" type="text" id="servidorEnvioCorreo" name="servidorEnvioCorreo" value="${hthParametroSistemaFiltro['servidorEnvioCorreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['servidorEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.UsuarioEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="15" type="text" id="usuarioEnvioCorreo" name="usuarioEnvioCorreo" value="${hthParametroSistemaFiltro['usuarioEnvioCorreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['passwordEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.PasswordEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" type="text" id="passwordEnvioCorreo" name="passwordEnvioCorreo" value="${hthParametroSistemaFiltro['passwordEnvioCorreo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['passwordEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['puertoEnvioCorreo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.PuertoEnvioCorreo"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integerMin" type="text" id="puertoEnvioCorreoMin" name="puertoEnvioCorreoMin" value="${hthParametroSistemaFiltro['puertoEnvioCorreoMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="17" alt="integerMax" type="text" id="puertoEnvioCorreoMax" name="puertoEnvioCorreoMax" value="${hthParametroSistemaFiltro['puertoEnvioCorreoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['puertoEnvioCorreo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['directorioLocalDestino']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.DirectorioLocalDestino"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="18" type="text" id="directorioLocalDestino" name="directorioLocalDestino" value="${hthParametroSistemaFiltro['directorioLocalDestino']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['directorioLocalDestino'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['hostSftp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.HostSftp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" type="text" id="hostSftp" name="hostSftp" value="${hthParametroSistemaFiltro['hostSftp']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['hostSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['puertoSftp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.PuertoSftp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="20" type="text" id="puertoSftp" name="puertoSftp" value="${hthParametroSistemaFiltro['puertoSftp']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['puertoSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioSftp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.UsuarioSftp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="21" type="text" id="usuarioSftp" name="usuarioSftp" value="${hthParametroSistemaFiltro['usuarioSftp']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['passwordSftp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.PasswordSftp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="22" type="text" id="passwordSftp" name="passwordSftp" value="${hthParametroSistemaFiltro['passwordSftp']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['passwordSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['llaveSftp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.LlaveSftp"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="23" type="text" id="llaveSftp" name="llaveSftp" value="${hthParametroSistemaFiltro['llaveSftp']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['llaveSftp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tiempoDrenadoBitacora']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthParametroSistema.filtro.etiqueta.TiempoDrenadoBitacora"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="24" alt="integerMin" type="text" id="tiempoDrenadoBitacoraMin" name="tiempoDrenadoBitacoraMin" value="${hthParametroSistemaFiltro['tiempoDrenadoBitacoraMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="24" alt="integerMax" type="text" id="tiempoDrenadoBitacoraMax" name="tiempoDrenadoBitacoraMax" value="${hthParametroSistemaFiltro['tiempoDrenadoBitacoraMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tiempoDrenadoBitacora'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>