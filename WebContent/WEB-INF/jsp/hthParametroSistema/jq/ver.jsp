<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthParametroSistemaEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthParametroSistemaEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="hthParametroSistema"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="hthParametroSistema.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthParametroSistema.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('hthParametroSistema/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthParametroSistema.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.TiempoMonitoreoDefault"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.tiempoMonitoreoDefault}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.HoraInicioMonitoreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.horaInicioMonitoreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.HoraFinMonitoreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.horaFinMonitoreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.CarpetaLecturaDefault"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.carpetaLecturaDefault}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.CarpetaEscrituraDefault"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.carpetaEscrituraDefault}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.RutaRaiz"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.rutaRaiz}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.CarSeparadorDefault"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.carSeparadorDefault}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.HabilitarEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
			<c:if test="${hthParametroSistema.habilitarEnvioCorreo == 'S'}"><fmt:message key="hthParametroSistema.formulario.comboLabel.Si"/></c:if>	
			<c:if test="${hthParametroSistema.habilitarEnvioCorreo == 'N'}"><fmt:message key="hthParametroSistema.formulario.comboLabel.No"/></c:if>	
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.RemitenteEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.remitenteEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.AsuntoEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.asuntoEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.JavamailJndi"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.javamailJndi}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.ListaEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.listaEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.ServidorEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.servidorEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.UsuarioEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.usuarioEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.PasswordEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.passwordEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.PuertoEnvioCorreo"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.puertoEnvioCorreo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.DirectorioLocalDestino"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.directorioLocalDestino}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.HostSftp"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.hostSftp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.PuertoSftp"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.puertoSftp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.UsuarioSftp"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.usuarioSftp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.PasswordSftp"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.passwordSftp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.LlaveSftp"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.llaveSftp}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="hthParametroSistema.formulario.etiqueta.TiempoDrenadoBitacora"/>
			</span>	 
			
			<span class="formvalue">
                ${hthParametroSistema.tiempoDrenadoBitacora}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
