<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsLoteachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLoteachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsLoteach"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="convenioMin"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsLoteach.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLoteach.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="convenioMin"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="71" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="72" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="73" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsLoteach/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsLoteach.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['convenio']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Convenio"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="4" alt="integerMin" type="text" id="convenioMin" name="convenioMin" value="${bsLoteachFiltro['convenioMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="4" alt="integerMax" type="text" id="convenioMax" name="convenioMax" value="${bsLoteachFiltro['convenioMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['convenio'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['empresaodepto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Empresaodepto"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="empresaodepto" name="empresaodepto" value="${bsLoteachFiltro['bsConvenioempresaach'].empresaodepto}"  size="10"  readonly="readonly"/>
                <input type="text" id="bsConvenioempresaach.convenio" name="bsConvenioempresaach.convenio" value="${bsLoteachFiltro['bsConvenioempresaach'].convenio}"  size="10"  readonly="readonly"/>		
				<button tabindex="5" id="btnbsConvenioempresaach" class="sq" onClick="doForaneo('bsConvenioempresaach', 'bsLoteach/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['empresaodepto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipolote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Tipolote"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="6" alt="integerMin" type="text" id="tipoloteMin" name="tipoloteMin" value="${bsLoteachFiltro['tipoloteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="6" alt="integerMax" type="text" id="tipoloteMax" name="tipoloteMax" value="${bsLoteachFiltro['tipoloteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipolote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montototal']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Montototal"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" alt="numericMin" type="text" id="montototalMin" name="montototalMin" value="${bsLoteachFiltro['montototalMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="7" alt="numericMax" type="text" id="montototalMax" name="montototalMax" value="${bsLoteachFiltro['montototalMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montototal'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numoperaciones']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Numoperaciones"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="8" alt="integerMin" type="text" id="numoperacionesMin" name="numoperacionesMin" value="${bsLoteachFiltro['numoperacionesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="8" alt="integerMax" type="text" id="numoperacionesMax" name="numoperacionesMax" value="${bsLoteachFiltro['numoperacionesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numoperaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrelote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Nombrelote"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="nombrelote" name="nombrelote" value="${bsLoteachFiltro['nombrelote']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrelote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaenviado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Fechaenviado"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" type="text" cdate="fechaenviadoMax" cattr="minDate" id="fechaenviadoMin" name="fechaenviadoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaenviadoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="10" type="text" cdate="fechaenviadoMin" cattr="maxDate" id="fechaenviadoMax" name="fechaenviadoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaenviadoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaenviado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fecharecibido']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Fecharecibido"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" type="text" cdate="fecharecibidoMax" cattr="minDate" id="fecharecibidoMin" name="fecharecibidoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fecharecibidoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="11" type="text" cdate="fecharecibidoMin" cattr="maxDate" id="fecharecibidoMax" name="fecharecibidoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fecharecibidoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fecharecibido'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaaplicacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Fechaaplicacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" type="text" cdate="fechaaplicacionMax" cattr="minDate" id="fechaaplicacionMin" name="fechaaplicacionMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaaplicacionMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="12" type="text" cdate="fechaaplicacionMin" cattr="maxDate" id="fechaaplicacionMax" name="fechaaplicacionMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaaplicacionMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaaplicacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="13" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsLoteachFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsLoteachFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['usuarioingreso']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Usuarioingreso"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="15" type="text" id="usuarioingreso" name="usuarioingreso" value="${bsLoteachFiltro['usuarioingreso']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['usuarioingreso'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizaciones']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Autorizaciones"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integerMin" type="text" id="autorizacionesMin" name="autorizacionesMin" value="${bsLoteachFiltro['autorizacionesMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="16" alt="integerMax" type="text" id="autorizacionesMax" name="autorizacionesMax" value="${bsLoteachFiltro['autorizacionesMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizaciones'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['aplicaciondebitohost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Aplicaciondebitohost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="17" alt="integerMin" type="text" id="aplicaciondebitohostMin" name="aplicaciondebitohostMin" value="${bsLoteachFiltro['aplicaciondebitohostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="17" alt="integerMax" type="text" id="aplicaciondebitohostMax" name="aplicaciondebitohostMax" value="${bsLoteachFiltro['aplicaciondebitohostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['aplicaciondebitohost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysMarcatiempo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.SysMarcatiempo"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="18" type="text" cdate="sysMarcatiempoMax" cattr="minDate" id="sysMarcatiempoMin" name="sysMarcatiempoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['sysMarcatiempoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="18" type="text" cdate="sysMarcatiempoMin" cattr="maxDate" id="sysMarcatiempoMax" name="sysMarcatiempoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['sysMarcatiempoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysMarcatiempo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysProcessid']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.SysProcessid"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="19" alt="integerMin" type="text" id="sysProcessidMin" name="sysProcessidMin" value="${bsLoteachFiltro['sysProcessidMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="19" alt="integerMax" type="text" id="sysProcessidMax" name="sysProcessidMax" value="${bsLoteachFiltro['sysProcessidMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysProcessid'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysLowdatetime']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.SysLowdatetime"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integerMin" type="text" id="sysLowdatetimeMin" name="sysLowdatetimeMin" value="${bsLoteachFiltro['sysLowdatetimeMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="20" alt="integerMax" type="text" id="sysLowdatetimeMax" name="sysLowdatetimeMax" value="${bsLoteachFiltro['sysLowdatetimeMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysLowdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sysHighdatetime']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.SysHighdatetime"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="sysHighdatetimeMin" name="sysHighdatetimeMin" value="${bsLoteachFiltro['sysHighdatetimeMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="sysHighdatetimeMax" name="sysHighdatetimeMax" value="${bsLoteachFiltro['sysHighdatetimeMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sysHighdatetime'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ponderacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Ponderacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="22" alt="integerMin" type="text" id="ponderacionMin" name="ponderacionMin" value="${bsLoteachFiltro['ponderacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="22" alt="integerMax" type="text" id="ponderacionMax" name="ponderacionMax" value="${bsLoteachFiltro['ponderacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ponderacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['firmas']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Firmas"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integerMin" type="text" id="firmasMin" name="firmasMin" value="${bsLoteachFiltro['firmasMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="23" alt="integerMax" type="text" id="firmasMax" name="firmasMax" value="${bsLoteachFiltro['firmasMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['firmas'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaaplicacionhost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Fechaaplicacionhost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="24" type="text" cdate="fechaaplicacionhostMax" cattr="minDate" id="fechaaplicacionhostMin" name="fechaaplicacionhostMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaaplicacionhostMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="24" type="text" cdate="fechaaplicacionhostMin" cattr="maxDate" id="fechaaplicacionhostMax" name="fechaaplicacionhostMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechaaplicacionhostMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaaplicacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['listoparahost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Listoparahost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="25" type="text" cdate="listoparahostMax" cattr="minDate" id="listoparahostMin" name="listoparahostMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['listoparahostMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="25" type="text" cdate="listoparahostMin" cattr="maxDate" id="listoparahostMax" name="listoparahostMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['listoparahostMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['listoparahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechahoraprocesado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Fechahoraprocesado"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="26" type="text" cdate="fechahoraprocesadoMax" cattr="minDate" id="fechahoraprocesadoMin" name="fechahoraprocesadoMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechahoraprocesadoMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="26" type="text" cdate="fechahoraprocesadoMin" cattr="maxDate" id="fechahoraprocesadoMax" name="fechahoraprocesadoMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteachFiltro['fechahoraprocesadoMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechahoraprocesado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['token']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Token"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="27" alt="integerMin" type="text" id="tokenMin" name="tokenMin" value="${bsLoteachFiltro['tokenMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="27" alt="integerMax" type="text" id="tokenMax" name="tokenMax" value="${bsLoteachFiltro['tokenMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['token'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comentariorechazo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Comentariorechazo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="28" type="text" id="comentariorechazo" name="comentariorechazo" value="${bsLoteachFiltro['comentariorechazo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comentariorechazo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Descripcion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="29" type="text" id="descripcion" name="descripcion" value="${bsLoteachFiltro['descripcion']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatuspaybank']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Estatuspaybank"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="30" type="text" id="estatuspaybank" name="estatuspaybank" value="${bsLoteachFiltro['estatuspaybank']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatuspaybank'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['archivogenerado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Archivogenerado"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="31" type="text" id="archivogenerado" name="archivogenerado" value="${bsLoteachFiltro['archivogenerado']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['archivogenerado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisioncobrada']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Comisioncobrada"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="32" alt="numericMin" type="text" id="comisioncobradaMin" name="comisioncobradaMin" value="${bsLoteachFiltro['comisioncobradaMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="32" alt="numericMax" type="text" id="comisioncobradaMax" name="comisioncobradaMax" value="${bsLoteachFiltro['comisioncobradaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisioncobrada'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuentadebito']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Cuentadebito"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="33" type="text" id="cuentadebito" name="cuentadebito" value="${bsLoteachFiltro['cuentadebito']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuentadebito'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionhost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Autorizacionhost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="34" alt="integerMin" type="text" id="autorizacionhostMin" name="autorizacionhostMin" value="${bsLoteachFiltro['autorizacionhostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="34" alt="integerMax" type="text" id="autorizacionhostMax" name="autorizacionhostMax" value="${bsLoteachFiltro['autorizacionhostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Descestatus"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="35" type="text" id="descestatus" name="descestatus" value="${bsLoteachFiltro['descestatus']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idtransaccion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Idtransaccion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="36" alt="integerMin" type="text" id="idtransaccionMin" name="idtransaccionMin" value="${bsLoteachFiltro['idtransaccionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="36" alt="integerMax" type="text" id="idtransaccionMax" name="idtransaccionMax" value="${bsLoteachFiltro['idtransaccionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idtransaccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['intactualizacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Intactualizacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="37" alt="integerMin" type="text" id="intactualizacionMin" name="intactualizacionMin" value="${bsLoteachFiltro['intactualizacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="37" alt="integerMax" type="text" id="intactualizacionMax" name="intactualizacionMax" value="${bsLoteachFiltro['intactualizacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['intactualizacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoimpuesto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsLoteach.filtro.etiqueta.Montoimpuesto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="38" alt="numericMin" type="text" id="montoimpuestoMin" name="montoimpuestoMin" value="${bsLoteachFiltro['montoimpuestoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="38" alt="numericMax" type="text" id="montoimpuestoMax" name="montoimpuestoMax" value="${bsLoteachFiltro['montoimpuestoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoimpuesto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>