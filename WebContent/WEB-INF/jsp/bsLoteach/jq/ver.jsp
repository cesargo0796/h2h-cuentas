<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsLoteachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsLoteachEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsLoteach"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsLoteach.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsLoteach.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	
	<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsLoteach/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsLoteach.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Convenio"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.convenio}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Empresaodepto"/>
			</span>	 
			
			<span class="formvalue">
	            [${bsLoteach.empresaodepto}] - ${bsLoteach.bsConvenioempresaach.convenio}
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Tipolote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.tipolote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Montototal"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.montototal}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Numoperaciones"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.numoperaciones}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Nombrelote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.nombrelote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaenviado"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaenviado}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fecharecibido"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fecharecibido}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaaplicacion"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaaplicacion}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaestatus"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaestatus}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Estatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.estatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Usuarioingreso"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.usuarioingreso}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Autorizaciones"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.autorizaciones}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Aplicaciondebitohost"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.aplicaciondebitohost}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysMarcatiempo"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.sysMarcatiempo}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysProcessid"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.sysProcessid}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysLowdatetime"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.sysLowdatetime}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.SysHighdatetime"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.sysHighdatetime}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Ponderacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.ponderacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Firmas"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.firmas}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechaaplicacionhost"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechaaplicacionhost}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Listoparahost"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.listoparahost}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Fechahoraprocesado"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsLoteach.fechahoraprocesado}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Token"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.token}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Comentariorechazo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.comentariorechazo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Descripcion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.descripcion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Estatuspaybank"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.estatuspaybank}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Archivogenerado"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.archivogenerado}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Comisioncobrada"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.comisioncobrada}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Cuentadebito"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.cuentadebito}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Autorizacionhost"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.autorizacionhost}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Descestatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.descestatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Idtransaccion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.idtransaccion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Intactualizacion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.intactualizacion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsLoteach.formulario.etiqueta.Montoimpuesto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsLoteach.montoimpuesto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
