<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsClienteEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsClienteEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsCliente"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="nombre"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsCliente.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsCliente.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="nombre"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="77" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="78" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="79" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsCliente/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsCliente.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nombre"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="2" type="text" id="nombre" name="nombre" value="${bsClienteFiltro['nombre']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['modulos']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Modulos"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="3" alt="integerMin" type="text" id="modulosMin" name="modulosMin" value="${bsClienteFiltro['modulosMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="3" alt="integerMax" type="text" id="modulosMax" name="modulosMax" value="${bsClienteFiltro['modulosMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['modulos'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['direccion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Direccion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="direccion" name="direccion" value="${bsClienteFiltro['direccion']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['direccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['telefono']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Telefono"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="5" type="text" id="telefono" name="telefono" value="${bsClienteFiltro['telefono']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['telefono'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fax']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Fax"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" type="text" id="fax" name="fax" value="${bsClienteFiltro['fax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fax'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Email"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" type="text" id="email" name="email" value="${bsClienteFiltro['email']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecontacto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nombrecontacto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="nombrecontacto" name="nombrecontacto" value="${bsClienteFiltro['nombrecontacto']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['apellidocontacto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Apellidocontacto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="9" type="text" id="apellidocontacto" name="apellidocontacto" value="${bsClienteFiltro['apellidocontacto']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['apellidocontacto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['numinstalacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Numinstalacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="numinstalacionMin" name="numinstalacionMin" value="${bsClienteFiltro['numinstalacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="numinstalacionMax" name="numinstalacionMax" value="${bsClienteFiltro['numinstalacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['numinstalacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="11" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsClienteFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="11" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsClienteFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsClienteFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="12" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsClienteFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechacreacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Fechacreacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" type="text" cdate="fechacreacionMax" cattr="minDate" id="fechacreacionMin" name="fechacreacionMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsClienteFiltro['fechacreacionMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="13" type="text" cdate="fechacreacionMin" cattr="maxDate" id="fechacreacionMax" name="fechacreacionMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsClienteFiltro['fechacreacionMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechacreacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['solicitudpendiente']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Solicitudpendiente"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="14" alt="integerMin" type="text" id="solicitudpendienteMin" name="solicitudpendienteMin" value="${bsClienteFiltro['solicitudpendienteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="14" alt="integerMax" type="text" id="solicitudpendienteMax" name="solicitudpendienteMax" value="${bsClienteFiltro['solicitudpendienteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['solicitudpendiente'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadtex']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nivelseguridadtex"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="15" alt="integerMin" type="text" id="nivelseguridadtexMin" name="nivelseguridadtexMin" value="${bsClienteFiltro['nivelseguridadtexMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="15" alt="integerMax" type="text" id="nivelseguridadtexMax" name="nivelseguridadtexMax" value="${bsClienteFiltro['nivelseguridadtexMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadtex'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadPIMP']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.NivelseguridadPIMP"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="16" alt="integerMin" type="text" id="nivelseguridadPIMPMin" name="nivelseguridadPIMPMin" value="${bsClienteFiltro['nivelseguridadPIMPMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="16" alt="integerMax" type="text" id="nivelseguridadPIMPMax" name="nivelseguridadPIMPMax" value="${bsClienteFiltro['nivelseguridadPIMPMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadPIMP'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrerepresentante']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nombrerepresentante"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="17" type="text" id="nombrerepresentante" name="nombrerepresentante" value="${bsClienteFiltro['nombrerepresentante']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrerepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['apellidorepresentante']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Apellidorepresentante"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="18" type="text" id="apellidorepresentante" name="apellidorepresentante" value="${bsClienteFiltro['apellidorepresentante']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['apellidorepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['proveedorinternet']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Proveedorinternet"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" type="text" id="proveedorinternet" name="proveedorinternet" value="${bsClienteFiltro['proveedorinternet']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['proveedorinternet'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['banca']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Banca"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="20" alt="integerMin" type="text" id="bancaMin" name="bancaMin" value="${bsClienteFiltro['bancaMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="20" alt="integerMax" type="text" id="bancaMax" name="bancaMax" value="${bsClienteFiltro['bancaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['banca'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['politicacobrotex']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Politicacobrotex"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="politicacobrotexMin" name="politicacobrotexMin" value="${bsClienteFiltro['politicacobrotexMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="politicacobrotexMax" name="politicacobrotexMax" value="${bsClienteFiltro['politicacobrotexMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['politicacobrotex'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisiontransext']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Comisiontransext"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="22" alt="numericMin" type="text" id="comisiontransextMin" name="comisiontransextMin" value="${bsClienteFiltro['comisiontransextMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="22" alt="numericMax" type="text" id="comisiontransextMax" name="comisiontransextMax" value="${bsClienteFiltro['comisiontransextMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisiontransext'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigotablatransext']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Codigotablatransext"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integerMin" type="text" id="codigotablatransextMin" name="codigotablatransextMin" value="${bsClienteFiltro['codigotablatransextMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="23" alt="integerMax" type="text" id="codigotablatransextMax" name="codigotablatransextMax" value="${bsClienteFiltro['codigotablatransextMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigotablatransext'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadisss']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nivelseguridadisss"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="24" alt="integerMin" type="text" id="nivelseguridadisssMin" name="nivelseguridadisssMin" value="${bsClienteFiltro['nivelseguridadisssMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="24" alt="integerMax" type="text" id="nivelseguridadisssMax" name="nivelseguridadisssMax" value="${bsClienteFiltro['nivelseguridadisssMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadisss'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadafp']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nivelseguridadafp"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="25" alt="integerMin" type="text" id="nivelseguridadafpMin" name="nivelseguridadafpMin" value="${bsClienteFiltro['nivelseguridadafpMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="25" alt="integerMax" type="text" id="nivelseguridadafpMax" name="nivelseguridadafpMax" value="${bsClienteFiltro['nivelseguridadafpMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadafp'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadccre']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nivelseguridadccre"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="26" alt="integerMin" type="text" id="nivelseguridadccreMin" name="nivelseguridadccreMin" value="${bsClienteFiltro['nivelseguridadccreMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="26" alt="integerMax" type="text" id="nivelseguridadccreMax" name="nivelseguridadccreMax" value="${bsClienteFiltro['nivelseguridadccreMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadccre'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nivelseguridadppag']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Nivelseguridadppag"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="27" alt="integerMin" type="text" id="nivelseguridadppagMin" name="nivelseguridadppagMin" value="${bsClienteFiltro['nivelseguridadppagMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="27" alt="integerMax" type="text" id="nivelseguridadppagMax" name="nivelseguridadppagMax" value="${bsClienteFiltro['nivelseguridadppagMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nivelseguridadppag'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cargapersonalizable']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Cargapersonalizable"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="28" type="text" id="cargapersonalizable" name="cargapersonalizable" value="${bsClienteFiltro['cargapersonalizable']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cargapersonalizable'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipodoctorepresentante']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Tipodoctorepresentante"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="29" alt="integerMin" type="text" id="tipodoctorepresentanteMin" name="tipodoctorepresentanteMin" value="${bsClienteFiltro['tipodoctorepresentanteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="29" alt="integerMax" type="text" id="tipodoctorepresentanteMax" name="tipodoctorepresentanteMax" value="${bsClienteFiltro['tipodoctorepresentanteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipodoctorepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['doctorepresentante']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Doctorepresentante"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="30" type="text" id="doctorepresentante" name="doctorepresentante" value="${bsClienteFiltro['doctorepresentante']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['doctorepresentante'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ciudad']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Ciudad"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="31" type="text" id="ciudad" name="ciudad" value="${bsClienteFiltro['ciudad']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ciudad'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cargo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Cargo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="32" type="text" id="cargo" name="cargo" value="${bsClienteFiltro['cargo']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cargo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexarchivo']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Limitexarchivo"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="33" alt="numericMin" type="text" id="limitexarchivoMin" name="limitexarchivoMin" value="${bsClienteFiltro['limitexarchivoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="33" alt="numericMax" type="text" id="limitexarchivoMax" name="limitexarchivoMax" value="${bsClienteFiltro['limitexarchivoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexarchivo'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitexlote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Limitexlote"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="34" alt="numericMin" type="text" id="limitexloteMin" name="limitexloteMin" value="${bsClienteFiltro['limitexloteMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="34" alt="numericMax" type="text" id="limitexloteMax" name="limitexloteMax" value="${bsClienteFiltro['limitexloteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitexlote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitextransaccion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Limitextransaccion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="35" alt="numericMin" type="text" id="limitextransaccionMin" name="limitextransaccionMin" value="${bsClienteFiltro['limitextransaccionMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="35" alt="numericMax" type="text" id="limitextransaccionMax" name="limitextransaccionMax" value="${bsClienteFiltro['limitextransaccionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitextransaccion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['permitedebitos']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Permitedebitos"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="36" type="text" id="permitedebitos" name="permitedebitos" value="${bsClienteFiltro['permitedebitos']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['permitedebitos'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitecreditos']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Limitecreditos"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="37" alt="numericMin" type="text" id="limitecreditosMin" name="limitecreditosMin" value="${bsClienteFiltro['limitecreditosMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="37" alt="numericMax" type="text" id="limitecreditosMax" name="limitecreditosMax" value="${bsClienteFiltro['limitecreditosMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitecreditos'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['limitedebitos']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Limitedebitos"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="38" alt="numericMin" type="text" id="limitedebitosMin" name="limitedebitosMin" value="${bsClienteFiltro['limitedebitosMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="38" alt="numericMax" type="text" id="limitedebitosMax" name="limitedebitosMax" value="${bsClienteFiltro['limitedebitosMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['limitedebitos'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisiontrnach']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsCliente.filtro.etiqueta.Comisiontrnach"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="39" alt="numericMin" type="text" id="comisiontrnachMin" name="comisiontrnachMin" value="${bsClienteFiltro['comisiontrnachMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="39" alt="numericMax" type="text" id="comisiontrnachMax" name="comisiontrnachMax" value="${bsClienteFiltro['comisiontrnachMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisiontrnach'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>