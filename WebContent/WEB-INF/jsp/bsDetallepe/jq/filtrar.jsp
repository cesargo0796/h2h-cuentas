<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsDetallepeEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsDetallepeEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="bsDetallepe"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="cuenta"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="bsDetallepe.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsDetallepe.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="cuenta"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="49" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetallepe/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="50" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="51" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetallepe/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsDetallepe.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Cuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="cuenta" name="cuenta" value="${bsDetallepeFiltro['cuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipooperacion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Tipooperacion"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="5" alt="integerMin" type="text" id="tipooperacionMin" name="tipooperacionMin" value="${bsDetallepeFiltro['tipooperacionMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="5" alt="integerMax" type="text" id="tipooperacionMax" name="tipooperacionMax" value="${bsDetallepeFiltro['tipooperacionMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipooperacion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['monto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Monto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="6" alt="numericMin" type="text" id="montoMin" name="montoMin" value="${bsDetallepeFiltro['montoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="6" alt="numericMax" type="text" id="montoMax" name="montoMax" value="${bsDetallepeFiltro['montoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['monto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoaplicado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Montoaplicado"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="7" alt="numericMin" type="text" id="montoaplicadoMin" name="montoaplicadoMin" value="${bsDetallepeFiltro['montoaplicadoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="7" alt="numericMax" type="text" id="montoaplicadoMax" name="montoaplicadoMax" value="${bsDetallepeFiltro['montoaplicadoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoaplicado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['adenda']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Adenda"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="8" type="text" id="adenda" name="adenda" value="${bsDetallepeFiltro['adenda']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['adenda'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['fechaestatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Fechaestatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="9" type="text" cdate="fechaestatusMax" cattr="minDate" id="fechaestatusMin" name="fechaestatusMin" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetallepeFiltro['fechaestatusMin']}"/>"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="9" type="text" cdate="fechaestatusMin" cattr="maxDate" id="fechaestatusMax" name="fechaestatusMax" value="<fmt:formatDate pattern="dd-MM-yyyy" value="${bsDetallepeFiltro['fechaestatusMax']}"/>"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['fechaestatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['estatus']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Estatus"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="10" alt="integerMin" type="text" id="estatusMin" name="estatusMin" value="${bsDetallepeFiltro['estatusMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="10" alt="integerMax" type="text" id="estatusMax" name="estatusMax" value="${bsDetallepeFiltro['estatusMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['estatus'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idpago']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Idpago"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="11" type="text" id="idpago" name="idpago" value="${bsDetallepeFiltro['idpago']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idpago'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizacionhost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Autorizacionhost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="12" alt="integerMin" type="text" id="autorizacionhostMin" name="autorizacionhostMin" value="${bsDetallepeFiltro['autorizacionhostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="12" alt="integerMax" type="text" id="autorizacionhostMax" name="autorizacionhostMax" value="${bsDetallepeFiltro['autorizacionhostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizacionhost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['enviarahost']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Enviarahost"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="13" alt="integerMin" type="text" id="enviarahostMin" name="enviarahostMin" value="${bsDetallepeFiltro['enviarahostMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="13" alt="integerMax" type="text" id="enviarahostMax" name="enviarahostMax" value="${bsDetallepeFiltro['enviarahostMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['enviarahost'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['contrasena']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Contrasena"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="14" type="text" id="contrasena" name="contrasena" value="${bsDetallepeFiltro['contrasena']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['contrasena'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrebenef']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Nombrebenef"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="15" type="text" id="nombrebenef" name="nombrebenef" value="${bsDetallepeFiltro['nombrebenef']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrebenef'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['direccionbenef']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Direccionbenef"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="16" type="text" id="direccionbenef" name="direccionbenef" value="${bsDetallepeFiltro['direccionbenef']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['direccionbenef'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['email']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Email"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="17" type="text" id="email" name="email" value="${bsDetallepeFiltro['email']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['email'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['comisionaplicada']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Comisionaplicada"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="18" alt="numericMin" type="text" id="comisionaplicadaMin" name="comisionaplicadaMin" value="${bsDetallepeFiltro['comisionaplicadaMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="18" alt="numericMax" type="text" id="comisionaplicadaMax" name="comisionaplicadaMax" value="${bsDetallepeFiltro['comisionaplicadaMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['comisionaplicada'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['ivaaplicado']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Ivaaplicado"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="19" alt="numericMin" type="text" id="ivaaplicadoMin" name="ivaaplicadoMin" value="${bsDetallepeFiltro['ivaaplicadoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="19" alt="numericMax" type="text" id="ivaaplicadoMax" name="ivaaplicadoMax" value="${bsDetallepeFiltro['ivaaplicadoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['ivaaplicado'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombrecuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Nombrecuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="20" type="text" id="nombrecuenta" name="nombrecuenta" value="${bsDetallepeFiltro['nombrecuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombrecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['niu']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Niu"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="21" alt="integerMin" type="text" id="niuMin" name="niuMin" value="${bsDetallepeFiltro['niuMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="21" alt="integerMax" type="text" id="niuMax" name="niuMax" value="${bsDetallepeFiltro['niuMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['niu'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['autorizador']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Autorizador"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="22" type="text" id="autorizador" name="autorizador" value="${bsDetallepeFiltro['autorizador']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['autorizador'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['sublote']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Sublote"/>
			</span>
			<span class="forminput" style="display: inline">
		        <input tabindex="23" alt="integerMin" type="text" id="subloteMin" name="subloteMin" value="${bsDetallepeFiltro['subloteMin']}"/>
			    &nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
			    <input tabindex="23" alt="integerMax" type="text" id="subloteMax" name="subloteMax" value="${bsDetallepeFiltro['subloteMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['sublote'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['cuentadebito']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Cuentadebito"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="24" type="text" id="cuentadebito" name="cuentadebito" value="${bsDetallepeFiltro['cuentadebito']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['cuentadebito'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombredecuenta']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Nombredecuenta"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="25" type="text" id="nombredecuenta" name="nombredecuenta" value="${bsDetallepeFiltro['nombredecuenta']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombredecuenta'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['montoimpuesto']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Montoimpuesto"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="26" alt="numericMin" type="text" id="montoimpuestoMin" name="montoimpuestoMin" value="${bsDetallepeFiltro['montoimpuestoMin']}"/>
				&nbsp;<fmt:message key="globales.filtro.etiqueta.rangoHasta"/>&nbsp;
				<input tabindex="26" alt="numericMax" type="text" id="montoimpuestoMax" name="montoimpuestoMax" value="${bsDetallepeFiltro['montoimpuestoMax']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['montoimpuesto'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['codigoorefint']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="bsDetallepe.filtro.etiqueta.Codigoorefint"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="27" type="text" id="codigoorefint" name="codigoorefint" value="${bsDetallepeFiltro['codigoorefint']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['codigoorefint'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>