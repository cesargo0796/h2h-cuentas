<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty bsEmpresaodeptoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsEmpresaodeptoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>		

<c:set var="nombre" value="bsEmpresaodepto"/>
<c:set var="ubicacion" value="ver"/>

<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsEmpresaodepto.bc.etiqueta.ver" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsEmpresaodepto.formulario.etiqueta.ver.titulo"/>
</div>--%>

    <div id="iProcessing" style="visibility: hidden" align="center">
    	<img src="${ctx}/include/images/design/processing_request.gif"></img>
    </div>
	<input type="hidden" id="pageLoc" name="pageLoc" value="ver"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
	
	

<div id="action-buttons">
<div id="action-bar">
	<button id="btnCancelar" type="button" name="regresar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera}">Async</c:if>('bsEmpresaodepto/cancelarVer');"><fmt:message key="globales.formulario.boton.regresar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="bsEmpresaodepto.formulario.etiqueta.ver.titulo"/> 
		</div>
		
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Cliente"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.cliente}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Nombre"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.nombre}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Direccion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.direccion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Telefono"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.telefono}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Fax"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.fax}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Email"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.email}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Nombrecontacto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.nombrecontacto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Apellidocontacto"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.apellidocontacto}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Proveedor"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.proveedor}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Nivelseguridad"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.nivelseguridad}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Fechaestatus"/>
			</span>	 
			
			<span class="formvalue">
                <fmt:formatDate pattern="dd-MM-yyyy" value="${bsEmpresaodepto.fechaestatus}" />
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Estatus"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.estatus}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Origenlotes"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.origenlotes}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Registrofiscal"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.registrofiscal}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Numpatronal"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.numpatronal}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Requierecompro"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.requierecompro}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Esconsumidorfinal"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.esconsumidorfinal}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Giroempresa"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.giroempresa}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Ciudad"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.ciudad}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Cargo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.cargo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Limitexarchivo"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.limitexarchivo}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Limitexlote"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.limitexlote}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
	
		<div class="row">
			<span class="label ui-state-default"> 
				<fmt:message key="bsEmpresaodepto.formulario.etiqueta.Limitextransaccion"/>
			</span>	 
			
			<span class="formvalue">
                ${bsEmpresaodepto.limitextransaccion}		    
			</span>
			
			<span class="clearer"></span>
		</div>
	
		
</div>		
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
