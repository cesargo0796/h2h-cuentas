<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsEmpresaodeptoEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsEmpresaodeptoEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsEmpresaodeptoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsEmpresaodeptoEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsEmpresaodeptoEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsEmpresaodeptoEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsEmpresaodeptoSeg}">
	<c:set var="restriccion" value="${bsEmpresaodeptoSeg}"/>
</c:if>

<c:set var="nombre" value="bsEmpresaodepto"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsEmpresaodepto.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsEmpresaodepto.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsEmpresaodepto"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="empresaodepto" name="empresaodepto" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	<button id="btnDetalle" type="button" class="ui-button ui-corner-all ui-button-color" name="detalle" onClick="if(isValueSelected()){ doSubmit('bsEmpresaodepto/detalle'); }"><fmt:message key="globales.listado.boton.detalle"/></button>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsEmpresaodepto/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="cliente"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Cliente"/></option>
			<option value="nombre"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Nombre"/></option>
			<option value="direccion"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Direccion"/></option>
			<option value="telefono"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Telefono"/></option>
			<option value="fax"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Fax"/></option>
			<option value="email"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Email"/></option>
			<option value="nombrecontacto"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Nombrecontacto"/></option>
			<option value="apellidocontacto"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Apellidocontacto"/></option>
			<option value="proveedor"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Proveedor"/></option>
			<option value="nivelseguridad"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Nivelseguridad"/></option>
			<option value="estatus"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Estatus"/></option>
			<option value="origenlotes"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Origenlotes"/></option>
			<option value="registrofiscal"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Registrofiscal"/></option>
			<option value="requierecompro"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Requierecompro"/></option>
			<option value="esconsumidorfinal"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Esconsumidorfinal"/></option>
			<option value="giroempresa"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Giroempresa"/></option>
			<option value="ciudad"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Ciudad"/></option>
			<option value="cargo"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Cargo"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsEmpresaodepto', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('cliente','${nombre}')" sort="${orden.mapaDeValores['cliente'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Cliente"/></th>
  			<th onclick="doSort('nombre','${nombre}')" sort="${orden.mapaDeValores['nombre'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Nombre"/></th>
  			<th onclick="doSort('direccion','${nombre}')" sort="${orden.mapaDeValores['direccion'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Direccion"/></th>
  			<th onclick="doSort('telefono','${nombre}')" sort="${orden.mapaDeValores['telefono'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Telefono"/></th>
  			<th onclick="doSort('fax','${nombre}')" sort="${orden.mapaDeValores['fax'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Fax"/></th>
  			<th onclick="doSort('email','${nombre}')" sort="${orden.mapaDeValores['email'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Email"/></th>
  			<th onclick="doSort('nombrecontacto','${nombre}')" sort="${orden.mapaDeValores['nombrecontacto'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Nombrecontacto"/></th>
  			<th onclick="doSort('apellidocontacto','${nombre}')" sort="${orden.mapaDeValores['apellidocontacto'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Apellidocontacto"/></th>
  			<th onclick="doSort('proveedor','${nombre}')" sort="${orden.mapaDeValores['proveedor'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Proveedor"/></th>
  			<th onclick="doSort('nivelseguridad','${nombre}')" sort="${orden.mapaDeValores['nivelseguridad'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Nivelseguridad"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Estatus"/></th>
  			<th onclick="doSort('origenlotes','${nombre}')" sort="${orden.mapaDeValores['origenlotes'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Origenlotes"/></th>
  			<th onclick="doSort('registrofiscal','${nombre}')" sort="${orden.mapaDeValores['registrofiscal'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Registrofiscal"/></th>
  			<th onclick="doSort('numpatronal','${nombre}')" sort="${orden.mapaDeValores['numpatronal'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Numpatronal"/></th>
  			<th onclick="doSort('requierecompro','${nombre}')" sort="${orden.mapaDeValores['requierecompro'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Requierecompro"/></th>
  			<th onclick="doSort('esconsumidorfinal','${nombre}')" sort="${orden.mapaDeValores['esconsumidorfinal'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Esconsumidorfinal"/></th>
  			<th onclick="doSort('giroempresa','${nombre}')" sort="${orden.mapaDeValores['giroempresa'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Giroempresa"/></th>
  			<th onclick="doSort('ciudad','${nombre}')" sort="${orden.mapaDeValores['ciudad'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Ciudad"/></th>
  			<th onclick="doSort('cargo','${nombre}')" sort="${orden.mapaDeValores['cargo'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Cargo"/></th>
  			<th onclick="doSort('limitexarchivo','${nombre}')" sort="${orden.mapaDeValores['limitexarchivo'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Limitexarchivo"/></th>
  			<th onclick="doSort('limitexlote','${nombre}')" sort="${orden.mapaDeValores['limitexlote'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Limitexlote"/></th>
  			<th onclick="doSort('limitextransaccion','${nombre}')" sort="${orden.mapaDeValores['limitextransaccion'].strOrden}"><fmt:message key="bsEmpresaodepto.listado.etiqueta.Limitextransaccion"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#empresaodepto').val('${dto.empresaodepto}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.cliente}"/></td>
		<td><c:out value="${dto.nombre}"/></td>
		<td><c:out value="${dto.direccion}"/></td>
		<td><c:out value="${dto.telefono}"/></td>
		<td><c:out value="${dto.fax}"/></td>
		<td><c:out value="${dto.email}"/></td>
		<td><c:out value="${dto.nombrecontacto}"/></td>
		<td><c:out value="${dto.apellidocontacto}"/></td>
		<td><c:out value="${dto.proveedor}"/></td>
		<td><c:out value="${dto.nivelseguridad}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.estatus}"/></td>
		<td><c:out value="${dto.origenlotes}"/></td>
		<td><c:out value="${dto.registrofiscal}"/></td>
		<td><c:out value="${dto.numpatronal}"/></td>
		<td><c:out value="${dto.requierecompro}"/></td>
		<td><c:out value="${dto.esconsumidorfinal}"/></td>
		<td><c:out value="${dto.giroempresa}"/></td>
		<td><c:out value="${dto.ciudad}"/></td>
		<td><c:out value="${dto.cargo}"/></td>
		<td><c:out value="${dto.limitexarchivo}"/></td>
		<td><c:out value="${dto.limitexlote}"/></td>
		<td><c:out value="${dto.limitextransaccion}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>