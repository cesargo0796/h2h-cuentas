<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">

<c:if test="${ not empty bsDetalleachEstado.enlaceBusqueda.enllavador}">
	<c:set var="busquedaEnllavador" value="${bsDetalleachEstado.enlaceBusqueda.enllavador}"/>	
</c:if>	
<c:if test="${ not empty bsDetalleachEstado.enlaceDetalle}">
	<c:set var="detalle" value="${bsDetalleachEstado.enlaceDetalle}"/>	
</c:if>	
<c:if test="${bsDetalleachEstado.ordenamientoAplicado}">
	<c:set var="orden" value="${bsDetalleachEstado.ordenamiento}"/>	
</c:if>		
<c:if test="${not empty bsDetalleachSeg}">
	<c:set var="restriccion" value="${bsDetalleachSeg}"/>
</c:if>

<c:set var="nombre" value="bsDetalleach"/>
<c:set var="ubicacion" value="listado"/>

<% int counter = 0; %>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>
<!-- Elementos no visibles -->	
	
<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">

<div id="mainContent">

<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
<div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
<ul>
	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
<c:if test="${ not empty ubicacionAplicacion.ruta }">
	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
	</c:forEach>			
</c:if>
	<li><fmt:message key="bsDetalleach.bc.etiqueta.principal" /></li>
</ul>
</div>
	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
</div>

<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>

<%-- <div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="bsDetalleach.listado.etiqueta.titulo"/>
</div>--%>
	<input type="hidden" id="maxRowNum" name="maxRowNum" value="<c:out value="${navegacion.registroFinal - 1}"/>">
	<input type="hidden" id="pageLoc" name="pageLoc" value="listado"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
	<c:if test="${ not empty busquedaEnllavador}">
	<input type="hidden" id="listadoLov" value="bsDetalleach"/>
	</c:if>
	<c:if test="${ not empty esAsync}">
	<input type="hidden" id="esAsync" value="${esAsync}"/>
	</c:if>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<!-- valores de llaves -->
    <input type="hidden" alt="notForSubmit" id="iddetalle" name="iddetalle" value=""/>
    <input type="hidden" alt="notForSubmit" id="instalacion" name="instalacion" value=""/>
    <input type="hidden" alt="notForSubmit" id="lote" name="lote" value=""/>
    <input type="hidden" alt="notForSubmit" id="operacion" name="operacion" value=""/>
	 <input type="hidden" id="expo" name="expo" value=""/>  



<%@ include file="/WEB-INF/global/jsp/mensajes-globales.jsp" %>

<c:if test="${ not empty busquedaEnllavador}">
	<div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em; font-size: 85%;"> 
	<p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
 	<strong><fmt:message key="globales.listado.etiqueta.tipLov.titulo"/></strong>&nbsp;<fmt:message key="globales.listado.etiqueta.tipLov.mensaje"/>
	</p>
</div>
</c:if>
<!-- botones de acciones basicas -->
<div id="action-buttons">
<div id="action-bar">
<c:if test="${ empty busquedaEnllavador}">
	<c:if test="${restriccion.verDisponible}">
	<button id="btnVer" type="button" class="ui-button ui-corner-all ui-button-color" name="ver" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/ver'); }"><fmt:message key="globales.listado.boton.ver"/></button>
	</c:if>
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<c:if test="${restriccion.actualizarDisponible}">	
	<button id="btnActualizar" type="button" class="ui-button ui-corner-all ui-button-color" name="actualizar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/actualizar'); }"><fmt:message key="globales.listado.boton.actualizar"/></button>
	</c:if>
	<c:if test="${restriccion.eliminarDisponible}">
	<button id="btnEliminar" type="button" class="ui-button ui-corner-all ui-button-color" name="eliminar" onClick="if(isValueSelected()){$('#dialog-confirm').dialog('open');}"><fmt:message key="globales.listado.boton.eliminar"/></button>
	</c:if>
	</c:if>
<c:if test="${ not empty busquedaEnllavador}">
	<c:if test="${restriccion.crearDisponible}">
	<button id="btnCrear" type="button" class="ui-button ui-corner-all ui-button-color" name="crear" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/crear');"><fmt:message key="globales.listado.boton.crear"/></button>
	</c:if>
	<button id="btnSeleccionar" type="button" class="ui-button ui-corner-all ui-button-color" name="seleccionar" onClick="if(isValueSelected()){ doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/buscarSeleccionar'); }"><fmt:message key="globales.listado.boton.seleccionar"/></button>
	<button id="btnCancelar" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/cancelarBuscar');"><fmt:message key="globales.listado.boton.cancelar"/></button>
</c:if>
	<c:if test="${restriccion.filtrarDisponible}">	
	<button id="btnFiltrar" type="button" class="ui-button ui-corner-all ui-button-color" name="filter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/filtrar');"><fmt:message key="globales.listado.boton.filtrar"/></button>
	</c:if>	
	
</div>
<div id="navigation-bar">	
<c:if test="${navegacion.paginaAnterior}">				
	<button id="btnAnterior" type="button" name="anterior" onClick="$('#movimientoPagina').val('anterior');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/listado')"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${!navegacion.paginaAnterior}">			
	<button id="btnAnterior" type="button" name="anterior" disabled="disabled"><fmt:message key="globales.listado.boton.anterior"/></button>
</c:if>
<c:if test="${not empty navegacion.listadoPaginas}">
<c:forEach items="${navegacion.listadoPaginas}" var="p">
	<c:if test="${p == navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />" class="ui-button-text ui-state-highlight" type="button" name="pag<c:out value="${p}" />" disabled="disabled"><c:out value="${p}" /></button>
	</c:if>
	<c:if test="${p != navegacion.noPaginaActual}">
	<button id="pag<c:out value="${p}" />"  onClick="$('#movimientoPagina').val('${p}');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/listado')" type="button" name="pag<c:out value="${p}"/>"><c:out value="${p}" /></button>	
	</c:if> 
</c:forEach>
</c:if>
<c:if test="${navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" onClick="$('#movimientoPagina').val('siguiente');doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/listado')" ><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
<c:if test="${!navegacion.paginaSiguiente}">			
	<button id="btnSiguiente" type="button" name="siguiente" disabled="disabled"><fmt:message key="globales.listado.boton.siguiente"/></button>
</c:if>
	
</div>
</div>
<br/>

<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-tl ui-corner-tr ui-helper-clearfix">

<c:if test="${ not empty orden}">
	<div style="display:block; float: left;">
		<button id="btnRemoverOrden" type="button" class="ui-button ui-corner-all ui-button-color" name="cancel" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/removerOrdenamiento');"><fmt:message key="globales.listado.boton.removerOrden"/></button>
	</div>
</c:if>
<c:if test="${navegacion.filtroAplicado}">
	<div style="display:block; float: left;">
		<button id="btnRemoverFiltro" type="button" class="ui-button ui-corner-all ui-button-color" name="nofilter" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('bsDetalleach/removerFiltrar');"><fmt:message key="globales.listado.boton.removerFiltro"/></button>
	</div>			
</c:if>
	<div id="crudTable_filter" style="display: block; text-align: right; float: right; margin-top: 6px; margin-bottom: 6px;"> 
		<fmt:message key="globales.listado.etiqueta.busquedaRapida"/>
		<select id="campoBusqueda" name="campoBusqueda" onchange="doBusquedaRapida('#busqueda', this.value, $('option:selected', this));" class="ui-widget ui-widget-content ui-state-default">
			<option value="cuenta"><fmt:message key="bsDetalleach.listado.etiqueta.Cuenta"/></option>
			<option value="tipooperacion"><fmt:message key="bsDetalleach.listado.etiqueta.Tipooperacion"/></option>
			<option value="adenda"><fmt:message key="bsDetalleach.listado.etiqueta.Adenda"/></option>
			<option value="estatus"><fmt:message key="bsDetalleach.listado.etiqueta.Estatus"/></option>
			<option value="idpago"><fmt:message key="bsDetalleach.listado.etiqueta.Idpago"/></option>
			<option value="autorizacionhost"><fmt:message key="bsDetalleach.listado.etiqueta.Autorizacionhost"/></option>
			<option value="email"><fmt:message key="bsDetalleach.listado.etiqueta.Email"/></option>
			<option value="nombrecuenta"><fmt:message key="bsDetalleach.listado.etiqueta.Nombrecuenta"/></option>
			<option value="autorizador"><fmt:message key="bsDetalleach.listado.etiqueta.Autorizador"/></option>
			<option value="estatuspaybank"><fmt:message key="bsDetalleach.listado.etiqueta.Estatuspaybank"/></option>
			<option value="cuentadebito"><fmt:message key="bsDetalleach.listado.etiqueta.Cuentadebito"/></option>
			<option value="nombredecuenta"><fmt:message key="bsDetalleach.listado.etiqueta.Nombredecuenta"/></option>
			<option value="codbanco"><fmt:message key="bsDetalleach.listado.etiqueta.Codbanco"/></option>
			<option value="tipocuenta"><fmt:message key="bsDetalleach.listado.etiqueta.Tipocuenta"/></option>
			<option value="descestatuspaybank"><fmt:message key="bsDetalleach.listado.etiqueta.Descestatuspaybank"/></option>
			<option value="enviarahost"><fmt:message key="bsDetalleach.listado.etiqueta.Enviarahost"/></option>
		</select> &nbsp;
		<input onkeypress="doFiltrarPor('bsDetalleach', event)" type="text" id="busqueda" name="busqueda" class="ui-widget ui-widget-content " style="width:150px; height: 18px;"/>
	</div>
</div>
<c:if test="${restriccion.listadoDisponible or not empty busquedaEnllavador}">
<table id="crudTable" width="100%">
	<thead>
		<tr>
  			<th onclick="doSort('cuenta','${nombre}')" sort="${orden.mapaDeValores['cuenta'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Cuenta"/></th>
  			<th onclick="doSort('tipooperacion','${nombre}')" sort="${orden.mapaDeValores['tipooperacion'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Tipooperacion"/></th>
  			<th onclick="doSort('monto','${nombre}')" sort="${orden.mapaDeValores['monto'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Monto"/></th>
  			<th onclick="doSort('adenda','${nombre}')" sort="${orden.mapaDeValores['adenda'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Adenda"/></th>
  			<th onclick="doSort('fechaestatus','${nombre}')" sort="${orden.mapaDeValores['fechaestatus'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Fechaestatus"/></th>
  			<th onclick="doSort('estatus','${nombre}')" sort="${orden.mapaDeValores['estatus'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Estatus"/></th>
  			<th onclick="doSort('idpago','${nombre}')" sort="${orden.mapaDeValores['idpago'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Idpago"/></th>
  			<th onclick="doSort('autorizacionhost','${nombre}')" sort="${orden.mapaDeValores['autorizacionhost'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Autorizacionhost"/></th>
  			<th onclick="doSort('email','${nombre}')" sort="${orden.mapaDeValores['email'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Email"/></th>
  			<th onclick="doSort('comisionaplicada','${nombre}')" sort="${orden.mapaDeValores['comisionaplicada'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Comisionaplicada"/></th>
  			<th onclick="doSort('nombrecuenta','${nombre}')" sort="${orden.mapaDeValores['nombrecuenta'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Nombrecuenta"/></th>
  			<th onclick="doSort('autorizador','${nombre}')" sort="${orden.mapaDeValores['autorizador'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Autorizador"/></th>
  			<th onclick="doSort('estatuspaybank','${nombre}')" sort="${orden.mapaDeValores['estatuspaybank'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Estatuspaybank"/></th>
  			<th onclick="doSort('fechahorahost','${nombre}')" sort="${orden.mapaDeValores['fechahorahost'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Fechahorahost"/></th>
  			<th onclick="doSort('cuentadebito','${nombre}')" sort="${orden.mapaDeValores['cuentadebito'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Cuentadebito"/></th>
  			<th onclick="doSort('nombredecuenta','${nombre}')" sort="${orden.mapaDeValores['nombredecuenta'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Nombredecuenta"/></th>
  			<th onclick="doSort('codbanco','${nombre}')" sort="${orden.mapaDeValores['codbanco'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Codbanco"/></th>
  			<th onclick="doSort('tipocuenta','${nombre}')" sort="${orden.mapaDeValores['tipocuenta'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Tipocuenta"/></th>
  			<th onclick="doSort('descestatuspaybank','${nombre}')" sort="${orden.mapaDeValores['descestatuspaybank'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Descestatuspaybank"/></th>
  			<th onclick="doSort('enviarahost','${nombre}')" sort="${orden.mapaDeValores['enviarahost'].strOrden}"><fmt:message key="bsDetalleach.listado.etiqueta.Enviarahost"/></th>
		</tr>
	</thead>	
	<tbody>
<c:forEach items="${coleccionResultados}" var="dto">
		<tr onclick="$('#iddetalle').val('${dto.iddetalle}');$('#instalacion').val('${dto.instalacion}');$('#lote').val('${dto.lote}');$('#operacion').val('${dto.operacion}'); selectDeSelectObject(this, 'ui-state-active'); sel = true; currentRow = $(this).parent().children().index($(this));">  
		<td><c:out value="${dto.cuenta}"/></td>
		<td><c:out value="${dto.tipooperacion}"/></td>
		<td><c:out value="${dto.monto}"/></td>
		<td><c:out value="${dto.adenda}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechaestatus}" /></td>
		<td><c:out value="${dto.estatus}"/></td>
		<td><c:out value="${dto.idpago}"/></td>
		<td><c:out value="${dto.autorizacionhost}"/></td>
		<td><c:out value="${dto.email}"/></td>
		<td><c:out value="${dto.comisionaplicada}"/></td>
		<td><c:out value="${dto.nombrecuenta}"/></td>
		<td><c:out value="${dto.autorizador}"/></td>
		<td><c:out value="${dto.estatuspaybank}"/></td>
		<td><fmt:formatDate pattern="dd-MM-yyyy" value="${dto.fechahorahost}" /></td>
		<td><c:out value="${dto.cuentadebito}"/></td>
		<td><c:out value="${dto.nombredecuenta}"/></td>
		<td><c:out value="${dto.codbanco}"/></td>
		<td><c:out value="${dto.tipocuenta}"/></td>
		<td><c:out value="${dto.descestatuspaybank}"/></td>
		<td><c:out value="${dto.enviarahost}"/></td>
		</tr>
</c:forEach>
	</tbody>
</table>
</c:if>
<div class="ui-widget fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">
	<c:if test="${navegacion.totalRegistros <= 0}">
		<fmt:message key="globales.listado.etiqueta.sinRegistros" />
    </c:if>
    <c:if test="${navegacion.totalRegistros > 0}">
    	<fmt:message key="globales.listado.etiqueta.numRegistros">
    		<fmt:param><c:out value="${navegacion.registroInicial}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.registroFinal}"/></fmt:param>
    		<fmt:param><c:out value="${navegacion.totalRegistros}"/></fmt:param>
    	</fmt:message>
    </c:if>	
</div>
<br/>
</div>
</form>
<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>