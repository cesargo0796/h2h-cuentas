<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<input type="hidden" id="nombreDetalle" name="nombreDetalle" value=""/>
<!-- valores de llaves del padre -->
  <input type="hidden" id="idColumnaDestino" name="idColumnaDestino" value="${hthColumnaDestinoDtoCabeza.idColumnaDestino}"/>

<c:set var="ubicacion" value="cabecera"/>
<c:if test="${hthColumnaDestinoEnlaceDetalle == 'hthAtributoEncabezado'}">
	<c:set var="nombre" value="hthAtributoEncabezado"/>
</c:if>
<c:if test="${hthColumnaDestinoEnlaceDetalle == 'hthAtributoDetalle'}">
	<c:set var="nombre" value="hthAtributoDetalle"/>
</c:if>

<script>
$(document).ready(function(){
	$('#iHeader').accordion({collapsible: true, active: 0});
	$('#backLink').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});;
	$('#backLinkMod').button().removeClass('ui-button-text-only').css({'padding':'0.2em 0.5em', 'margin-right':'20px'}).bind("click", function(e) {e.stopPropagation();});
});
</script>  

<!-- datos de formulario -->
<div id="iHeader">
	<h3><a id="headerInfo" href="#">
			<span id="backLink" onclick="doSubmit('hthColumnaDestino/cancelarDetalle');"><fmt:message key="globales.cabecera.boton.regresar"/></span>
			<span id="backLinkMod" onclick="doSubmit('hthColumnaDestino/actualizar');"><fmt:message key="globales.cabecera.boton.actualizar"/></span>	
			 <fmt:message key="hthColumnaDestino.encabezado.etiqueta.titulo"/> ${hthColumnaDestinoDtoCabeza.idColumnaDestino} &nbsp;|			<span style="font-weight: normal;"><fmt:message key="hthColumnaDestino.encabezado.etiqueta.IdTablaDestino"/>:</span>
	        [${hthColumnaDestinoDtoCabeza.idTablaDestino}] - ${hthColumnaDestinoDtoCabeza.hthTablaDestino.nombreTabla}
 &nbsp;| 			<span style="font-weight: normal;"><fmt:message key="hthColumnaDestino.encabezado.etiqueta.NombreColumna"/>:</span>
		    ${hthColumnaDestinoDtoCabeza.nombreColumna} 
 &nbsp;|        	</a></h3>
	<div>
	    <div>
		<strong><fmt:message key="hthColumnaDestino.encabezado.etiqueta.IdTablaDestino"/></strong>&nbsp;
		[${hthColumnaDestinoDtoCabeza.idTablaDestino}] - ${hthColumnaDestinoDtoCabeza.hthTablaDestino.nombreTabla}
		</div>
		
	    <div>
		<strong><fmt:message key="hthColumnaDestino.encabezado.etiqueta.NombreColumna"/></strong>&nbsp;
		${hthColumnaDestinoDtoCabeza.nombreColumna}
		</div>
		
	</div>
</div>

<div id="iTab" class="ui-tabs ui-widget ui-widget-content ui-corner-top" >
 	<ul id="detailTabs" class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
	<c:if test="${hthColumnaDestinoEnlaceDetalle == 'hthAtributoEncabezado'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthAtributoEncabezado');doSubmit('hthColumnaDestino/detalle')" class="ui-selected"><fmt:message key="hthColumnaDestino.encabezado.etiqueta.hthAtributoEncabezadoDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthColumnaDestinoEnlaceDetalle != 'hthAtributoEncabezado'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthAtributoEncabezado');doSubmit('hthColumnaDestino/detalle')"><fmt:message key="hthColumnaDestino.encabezado.etiqueta.hthAtributoEncabezadoDrillDown"/></a>
		</li>
	</c:if>		
	<c:if test="${hthColumnaDestinoEnlaceDetalle == 'hthAtributoDetalle'}">
		<li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active">
			<a onClick="$('#nombreDetalle').val('hthAtributoDetalle');doSubmit('hthColumnaDestino/detalle')" class="ui-selected"><fmt:message key="hthColumnaDestino.encabezado.etiqueta.hthAtributoDetalleDrillDown"/></a>
		</li>
	</c:if> 			
	<c:if test="${hthColumnaDestinoEnlaceDetalle != 'hthAtributoDetalle'}">
		<li>
			<a onClick="$('#nombreDetalle').val('hthAtributoDetalle');doSubmit('hthColumnaDestino/detalle')"><fmt:message key="hthColumnaDestino.encabezado.etiqueta.hthAtributoDetalleDrillDown"/></a>
		</li>
	</c:if>		
	</ul>
</div>
<div id="iTabline"></div>
</fmt:bundle>