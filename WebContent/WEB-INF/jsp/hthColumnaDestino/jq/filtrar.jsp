<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:if test="${ not empty locale}">
	<fmt:setLocale value="${ locale }" />
</c:if>
<fmt:bundle basename="com.ofbox.davivienda.h2h.resourceBundle.MessagesResources">
<c:if test="${ not empty hthColumnaDestinoEstado.enlaceDetalle}">
	<c:set var="detalle" value="${hthColumnaDestinoEstado.enlaceDetalle}"/>	
	<c:set var="detalleEnllavador" value="${detalle.enllavador}"/>
</c:if>	

<c:set var="nombre" value="hthColumnaDestino"/>
<c:set var="ubicacion" value="filtrar"/>
<c:set var="primerCampo" value="btnhthTablaDestino"/>
<%@ include file="/WEB-INF/global/jsp/jq-header.jsp" %>

<form  name="mainForm" id="mainForm" action="${NewtPageRenderData.actionUrl}" method="post">
<c:if test="${ not empty detalle.cabecera}">
    <jsp:include page="/WEB-INF/jsp/${detalle.cabecera.locacion}/jq/cabecera.jsp" />
</c:if>	
<div id="mainContent">
	<div class="breadCrumbHolder module" style="margin-bottom: 3px;">
    <div id="breadcrumb" class="breadCrumb module ui-widget ui-widget-content" style="width: 99%;">
    <ul>
    	<li><a href="${ctx}/inicio/blank/ria"> HOME </a> </li>
    <c:if test="${ not empty ubicacionAplicacion.ruta }">
    	<input type="hidden" name="idUbicacionAplicacionActual" id="idUbicacionAplicacionActual" value="${ ubicacionAplicacion.idUbicacionAplicacionActual }"/>
    	<c:forEach items="${ ubicacionAplicacion.ruta }" var="ub">
    		<li><a href="javascript: doSubmitBreadcrumb('<c:out value="${ ub.submit }"/>', '<c:out value="${ ub.id }"/>')"><c:out value="${ ub.label }" escapeXml="false" /></a>  </li> 
    	</c:forEach>			
    </c:if>
    	<li><fmt:message key="hthColumnaDestino.bc.etiqueta.filtrar" /></li>
    </ul>
    </div>
    	<input type="hidden" name="id_ubicacion_retorno" id="id_ubicacion_retorno" value=""/>
    </div>
	
<%--<div id="iTitle" class="ui-state-default ui-widget-content ui-corner-all">
	<fmt:message key="hthColumnaDestino.formulario.etiqueta.filtrar.titulo"/>
</div>--%>
<input type="hidden" id="pageLoc" name="pageLoc" value="filtrar"/>
	<input type="hidden" id="primerCampo" name="primerCampo" value="btnhthTablaDestino"/>
    <input type="hidden" id="eag" name="eag" value="${eag}"/>
    <input type="hidden" id="movimientoPagina" name="movimientoPagina"/>
    <input type="hidden" id="columna" name="columna"/>
    <input type="hidden" id="fenixf31seguridad" name="fenixf31seguridad" value="<c:out value='${fenixf31seguridad}' />" />
	<c:if test="${not empty scrollTo }">
    <input type="hidden" id="scrollSmooth" value="<c:out value='${scrollTo}' />" />
    </c:if>
	<!-- valores de llaves -->
<input type="hidden" id="nombreBusqueda" name="nombreBusqueda"/>


<!-- mensajes de error-->
<c:if test="${mensajesUsuario.erroresGlobales}">
<div id="iError">
	<b class="ui-round-top"> 
	<b class="ui-line-one"></b><b class="ui-line-two"></b><b class="ui-line-three"></b><b class="ui-line-four"></b> 
    </b> 
  	<table>
     	<tr>
       		<td>
         	<ol>
	     	<c:forEach items="${mensajesUsuario.mensajesDeError}" var="mensaje">
	        	<li><c:out value="${mensaje}" escapeXml="false"/></li>
	     	</c:forEach>
	     	</ol>
       		</td>
     	</tr>
  	</table>
  	<b class="ui-round-bottom"> 
	<b class="ui-line-four"></b><b class="ui-line-three"></b><b class="ui-line-two"></b><b class="ui-line-one"></b> 
  	</b> 
</div>
</c:if>

<div id="action-buttons">
<div id="action-bar">
	<button tabindex="11" id="btnAplicarFiltro" type="submit" name="Filtrar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthColumnaDestino/filtrarEjecutar');"><fmt:message key="globales.filtro.boton.aplicarFiltro"/></button>
	<button tabindex="12" id="btnLimpiar" type="button" name="Limpiar" onClick="limpiarCampos('mainForm');"><fmt:message key="globales.filtro.boton.limpiar"/></button>
	<button tabindex="13" id="btnCancelar" type="button" name="Cancelar" onClick="doSubmit<c:if test="${ not empty detalle.cabecera || not empty esAsync}">Async</c:if>('hthColumnaDestino/cancelarFiltrar');"><fmt:message key="globales.filtro.boton.cancelar"/></button>
</div>
</div>
<br />

<!-- datos de formulario -->
<div class="ui-widget ui-widget-content ui-corner-all" style="width: 99%">
		<div id="formInputHeader" class="ui-widget ui-widget-header ui-corner-top" style="padding-left: 5px;"> 
			<fmt:message key="hthColumnaDestino.formulario.etiqueta.filtrar.titulo"/> 
		</div>
		<div style="display: inline;" id="formInput">
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['idTablaDestino']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthColumnaDestino.filtro.etiqueta.IdTablaDestino"/>
			</span>
			<span class="forminput" style="display: inline">
	            	            <input type="text" id="idTablaDestino" name="idTablaDestino" value="${hthColumnaDestinoFiltro['hthTablaDestino'].idTablaDestino}"  size="10"  readonly="readonly"/>
                <input type="text" id="hthTablaDestino.nombreTabla" name="hthTablaDestino.nombreTabla" value="${hthColumnaDestinoFiltro['hthTablaDestino'].nombreTabla}"  size="40"  readonly="readonly"/>		
				<button tabindex="2" id="btnhthTablaDestino" class="sq" onClick="doForaneo('hthTablaDestino', 'hthColumnaDestino/buscar');"><fmt:message key='globales.formulario.boton.lov'/></button>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['idTablaDestino'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['nombreColumna']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthColumnaDestino.filtro.etiqueta.NombreColumna"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="3" type="text" id="nombreColumna" name="nombreColumna" value="${hthColumnaDestinoFiltro['nombreColumna']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['nombreColumna'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['descripcion']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthColumnaDestino.filtro.etiqueta.Descripcion"/>
			</span>
			<span class="forminput" style="display: inline">
	            <input tabindex="4" type="text" id="descripcion" name="descripcion" value="${hthColumnaDestinoFiltro['descripcion']}"/>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['descripcion'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['esNull']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthColumnaDestino.filtro.etiqueta.EsNull"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="5" id="esNull" name="esNull" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="S"<c:if test="${hthColumnaDestinoFiltro['esNull'] == 'S'}">selected="selected"</c:if>	><fmt:message key="hthColumnaDestino.formulario.comboLabel.Si"/></option>
				    <option value="N"<c:if test="${hthColumnaDestinoFiltro['esNull'] == 'N'}">selected="selected"</c:if>	><fmt:message key="hthColumnaDestino.formulario.comboLabel.No"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['esNull'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
<c:if test="${ empty detalleEnllavador.valoresEnllavador or empty detalleEnllavador.valoresEnllavador['tipoDato']}">
		<div class="row" >
			<span class="label"> 
				<fmt:message key="hthColumnaDestino.filtro.etiqueta.TipoDato"/>
			</span>
			<span class="forminput" style="display: inline">
                <select  tabindex="6" id="tipoDato" name="tipoDato" class="ui-widget ui-state-default">
				    <option value="" display=""/>			
				    <option value="S"<c:if test="${hthColumnaDestinoFiltro['tipoDato'] == 'S'}">selected="selected"</c:if>	><fmt:message key="hthColumnaDestino.formulario.comboLabel.Caracter"/></option>
				    <option value="N"<c:if test="${hthColumnaDestinoFiltro['tipoDato'] == 'N'}">selected="selected"</c:if>	><fmt:message key="hthColumnaDestino.formulario.comboLabel.Num&eacute;rica"/></option>
				    <option value="F"<c:if test="${hthColumnaDestinoFiltro['tipoDato'] == 'F'}">selected="selected"</c:if>	><fmt:message key="hthColumnaDestino.formulario.comboLabel.Fecha"/></option>
			    </select>
			</span>
			<c:forEach items="${mensajesUsuario.mapaDeErroresEnPropiedades['tipoDato'].mensaje}" var="mensaje">
			<span style="font-size: 90%;" class="ui-state-error"> <c:out value="${mensaje}" escapeXml="false"/> </span>
			</c:forEach>
		</div>	
</c:if>
</div>	


</div>
</div>
</form>

<%@ include file="/WEB-INF/global/jsp/jq-footer.jsp" %>
</fmt:bundle>
</body>