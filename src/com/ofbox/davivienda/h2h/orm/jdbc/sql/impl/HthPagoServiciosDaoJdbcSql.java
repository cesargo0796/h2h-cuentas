package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.ofbox.davivienda.h2h.abstraccion.dao.BsClienteDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthPagoServiciosDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthPagoServicios;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;

public class HthPagoServiciosDaoJdbcSql extends SQLImplementacionBasica implements HthPagoServiciosDao {
	
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("Hth_Pago_Servicios", new SQLColumnaMeta[]{
	        new SQLColumnaMeta(5, "Hth_Pago_Servicios", "E5", 5001, "IdPagoServicio", "C5001", Types.BIGINT ,true)
	     ,  new SQLColumnaMeta(5, "Hth_Pago_Servicios", "E5", 5002, "IdCliente", "C5002", Types.BIGINT )
	     ,  new SQLColumnaMeta(5, "Hth_Pago_Servicios", "E5", 5003, "NumCuenta", "C5003", Types.VARCHAR )	    
	     ,  new SQLColumnaMeta(5, "Hth_Pago_Servicios", "E5", 5004, "Estado", "C5004", Types.VARCHAR )
	     ,  new SQLColumnaMeta(5, "Hth_Pago_servicios", "E5", 5005, "Nit", "C5005", Types.VARCHAR)
	     
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 1, "Cliente", "C1", Types.BIGINT ,true)
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 2, "Nombre", "C2", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 3, "Modulos", "C3", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 4, "Direccion", "C4", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 5, "Telefono", "C5", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 6, "Fax", "C6", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 7, "Email", "C7", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 8, "NombreContacto", "C8", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 9, "ApellidoContacto", "C9", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 10, "NumInstalacion", "C10", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 11, "FechaEstatus", "C11", Types.TIMESTAMP )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 12, "Estatus", "C12", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 13, "FechaCreacion", "C13", Types.TIMESTAMP )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 14, "SolicitudPendiente", "C14", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 15, "NivelSeguridadTEx", "C15", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 16, "NivelSeguridadPImp", "C16", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 17, "nombreRepresentante", "C17", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 18, "apellidoRepresentante", "C18", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 19, "proveedorInternet", "C19", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 20, "Banca", "C20", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 21, "PoliticaCobroTEx", "C21", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 22, "ComisionTransExt", "C22", Types.NUMERIC )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 23, "CodigoTablaTransExt", "C23", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 24, "NivelSeguridadISSS", "C24", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 25, "NivelSeguridadAFP", "C25", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 26, "NivelSeguridadCCre", "C26", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 27, "NivelSeguridadPPag", "C27", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 28, "CargaPersonalizable", "C28", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 29, "TipoDoctoRepresentante", "C29", Types.BIGINT )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 30, "DoctoRepresentante", "C30", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 31, "Ciudad", "C31", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 32, "Cargo", "C32", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 33, "LimiteXArchivo", "C33", Types.NUMERIC )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 34, "LimiteXLote", "C34", Types.NUMERIC )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 35, "LimiteXTransaccion", "C35", Types.NUMERIC )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 36, "PermiteDebitos", "C36", Types.VARCHAR )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 37, "LimiteCreditos", "C37", Types.NUMERIC )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 38, "LimiteDebitos", "C38", Types.NUMERIC )
	      ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 39, "ComisionTrnACH", "C39", Types.NUMERIC )
		});
	
	
	
	
	public BsCliente getBsCliente(HthPagoServicios hthPagoServicios) throws ExcepcionEnDAO {
		return getBsCliente(hthPagoServicios, null);
	}

	public BsCliente getBsCliente(HthPagoServicios hthPagoServicios, Transaccion tx) throws ExcepcionEnDAO {	     
		BsCliente bsClienteRel = hthPagoServicios.getBsCliente();
		if(bsClienteRel==null){
		       PeticionDeDatos peticionDeDatosParaBsCliente = SQLImplementacionBasica.crearPeticionDeDatosST();
		       peticionDeDatosParaBsCliente.agregarRestriccion(PeticionDeDatos.IGUAL,
		    		   BsClienteDao.CLIENTE, 
		       							hthPagoServicios.getIdCliente()
			                              );
		       TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
		       bsClienteRel = (BsCliente)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsCliente, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsClienteDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
		       hthPagoServicios.setBsCliente(bsClienteRel);										
		}										
		return 	bsClienteRel;	
	}
	
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
			"SELECT E5.IdPagoServicio as C5001, E5.IdCliente as C5002, E5.NumCuenta as C5003, E5.Estado as C5004, E5.Nit as C5005, " +
			"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
			"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
			"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
			"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
			"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
			"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
			"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
			"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
			"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
			"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39 " +
					"FROM Hth_Pago_Servicios E5 " + 
					"INNER JOIN BS_Cliente E0 on E0.Cliente = E5.IdCliente ";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F =  
			"SELECT E5.IdPagoServicio as C5001, E5.IdCliente as C5002, E5.NumCuenta as C5003, E5.Estado as C5004, E5.Nit as C5005, " +
					"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
					"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
					"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
					"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
					"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
					"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
					"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
					"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
					"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
					"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39 " +	
					"FROM Hth_Pago_Servicios E5 " +
					"INNER JOIN BS_Cliente E0 on E0.Cliente = E5.IdCliente " +
					"WHERE E5.IdPagoServicio= ?";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
			"SELECT E5.IdPagoServicio as C5001, E5.IdCliente as C5002, E5.NumCuenta as C5003, E5.Estado as C5004, E5.Nit as C5005, " +
					"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
					"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
					"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
					"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
					"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
					"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
					"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
					"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
					"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
					"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39 " +
			        "FROM Hth_Pago_Servicios E5 " +
	                "INNER JOIN BS_Cliente E0 on E0.Cliente = E5.IdCliente ";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de c&oacute;digos de banco.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM Hth_Pago_Servicios";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO Hth_Pago_Servicios " +
		"(IdCliente, NumCuenta, Estado, Nit) "+
		"VALUES (?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO Hth_Pago_Servicios " +
		"(IdPagoServicio, IdCliente, NumCuenta, Estado, Nit) "+
		"VALUES (?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE Hth_Pago_Servicios SET IdCliente=?, NumCuenta=?, Estado=?, Nit=? "
		+ "WHERE IdPagoServicio=?";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL = 
		"UPDATE Hth_Pago_Servicios SET Estado= ? "+
		"WHERE IdPagoServicio=?";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E5.IdPagoServicio as C5001, E5.IdCliente as C5002, E5.NumCuenta as C5003, E5.Estado as C5004, E5.Nit as C5005, " +
				"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
				"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
				"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
				"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
				"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
				"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
				"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
				"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
				"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
				"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39 " +
				"FROM Hth_Pago_Servicios E5 " +
				"INNER JOIN BS_Cliente E0 on E0.Cliente = E5.IdCliente " +
				"WHERE E5.IdPagoServicio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE" + 
		"    TABLE BancaEmpresaPlus.dbo.HTH_PAGO_SERVICIOS" + 
		"    (" + 
		"        IdPagoServicio INT NOT NULL IDENTITY," + 
		"        IdCliente INT NOT NULL," + 
		"        NumCuenta VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," + 
		"        Nit VARCHAR(50) NOT NULL," + 
		"        Estado VARCHAR(10) NOT NULL DEFAULT 'ACTIVO'," + 
		"        CONSTRAINT PK_ID_TH_PAGOSERVICIOS PRIMARY KEY (IdPagoServicio)," + 
		"     	 CONSTRAINT FK_BS_Cliente_PS FOREIGN KEY (IdCliente) REFERENCES BS_Cliente (IdCliente),"+
		"        CONSTRAINT IX_NUM_CUENTA UNIQUE (NumCuenta)" + 
		"    );";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
		HthPagoServicios hthPagoServiciosDto = new HthPagoServicios();	
		hthPagoServiciosDto.setIdPagoServicio(SQLUtils.getLongFromResultSet(rst, "C5001"));
		hthPagoServiciosDto.setIdCliente(SQLUtils.getLongFromResultSet(rst,"C5002"));
		hthPagoServiciosDto.setNumCuenta(rst.getString("C5003"));
		hthPagoServiciosDto.setEstado(rst.getString("C5004"));
		hthPagoServiciosDto.setNit(rst.getString("C5005"));
		return hthPagoServiciosDto;
	}
	

	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static HthPagoServicios crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		HthPagoServicios hthPagoServiciosDto = (HthPagoServicios) crearDtoST(rst);
        BsCliente bsClienteRel  = (BsCliente) BsClienteDaoJdbcSql.crearDtoST(rst);
        hthPagoServiciosDto.setBsCliente(bsClienteRel);        
     
		return hthPagoServiciosDto;
	}

	
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static HthPagoServicios crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn ) throws SQLException, ExcepcionEnDAO{	
		HthPagoServicios hthPagoServicios = (HthPagoServicios) crearDtoST(rst);
		BsCliente bsClienteRell = (BsCliente) SQLImplementacionOperacionesComunes.buscarPorIDST(
				hthPagoServicios.getIdCliente(), conn, 
				BsClienteDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthPagoServicios.setBsCliente(bsClienteRell);
      
        return hthPagoServicios;
	}


	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {	
		HthPagoServicios hthPagoServiciosDto = (HthPagoServicios)theDtoUntyped;	
 	    Long idCliente=hthPagoServiciosDto.getIdCliente(); 	   
 	    String numCuenta=hthPagoServiciosDto.getNumCuenta(); 	   
 	    String estado=hthPagoServiciosDto.getEstado();
 	    String nit=hthPagoServiciosDto.getNit();
 	    
 	    SQLUtils.setIntoStatement( 1, idCliente, smt);         		     
		SQLUtils.setIntoStatement( 2, numCuenta, smt);      		     
		SQLUtils.setIntoStatement( 3, estado, smt); 
		SQLUtils.setIntoStatement( 4, nit, smt); 
		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
		HthPagoServicios hthPagoServiciosDto = (HthPagoServicios)theDtoUntyped;
 	    Long idPagoServicio=hthPagoServiciosDto.getIdPagoServicio();
 	    Long idCliente=hthPagoServiciosDto.getIdCliente();
 	    String numCuenta=hthPagoServiciosDto.getNumCuenta();
 	    String estado=hthPagoServiciosDto.getEstado(); 
	    String nit=hthPagoServiciosDto.getNit(); 
 	   

 	    SQLUtils.setIntoStatement( 1, idPagoServicio, smt); 
 	    SQLUtils.setIntoStatement( 2, idCliente, smt); 
 	    SQLUtils.setIntoStatement( 3, numCuenta, smt);         		     
		SQLUtils.setIntoStatement( 4, estado, smt);  
		SQLUtils.setIntoStatement( 5, nit, smt);  
		
	}
		

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
 	    Long idPagoServicio=(Long)theIdUntyped;
 	    String estado= "INACTIVO";   
 	    
		SQLUtils.setIntoStatement( 1, estado, smt);     		     
		SQLUtils.setIntoStatement( 2, idPagoServicio, smt);   		 		 
		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
 	    Long idPagoServicio=(Long)theIdUntyped;
	     
 	    SQLUtils.setIntoStatement( 1, idPagoServicio, smt); 
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
		HthPagoServicios hthPagoServicios = (HthPagoServicios)theDtoUntyped;	
 	    Long idPagoServicio=hthPagoServicios.getIdPagoServicio();
 	    Long idCliente=hthPagoServicios.getIdCliente();
 	    String numCuenta=hthPagoServicios.getNumCuenta();
 	    String estado=hthPagoServicios.getEstado();
 	    String nit=hthPagoServicios.getNit();
 	    
 	    
 	    SQLUtils.setIntoStatement( 1, idCliente, smt);         		     
		SQLUtils.setIntoStatement( 2, numCuenta, smt);      		     
		SQLUtils.setIntoStatement( 3, estado, smt);   	    
 	    SQLUtils.setIntoStatement( 4, nit, smt);
 	    SQLUtils.setIntoStatement( 5, idPagoServicio, smt);   
    		     
	     			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}	

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>BsBancoAch</code> objeto del tipo <code>BsBancoAch</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthPagoServicios HthPagoServicios) throws ExcepcionEnDAO {
	    actualizar(HthPagoServicios, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>BsBancoAchID</code> objeto del tipo <code>BsBancoAchID</code>.
	 * @return <code>BsBancoAch</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthPagoServicios buscarPorID(HthPagoServicios HthPagoServicios)
			throws ExcepcionEnDAO {
		return (HthPagoServicios)buscarPorID(HthPagoServicios, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>BsBancoAch</code> objeto del tipo <code>BsBancoAch</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthPagoServicios HthPagoServicios) throws ExcepcionEnDAO {
	    crear(HthPagoServicios, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>BsBancoAchID</code> objeto del tipo <code>BsBancoAchID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(HthPagoServicios HthPagoServicios) throws ExcepcionEnDAO {
	    eliminar(HthPagoServicios, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsBancoAch</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthPagoServicios resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthPagoServicios)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
		
		public int getManejoForaneosPorDefecto() {
			return Dao.FORANEOS_JOIN;
		}
			
			
		public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
				PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
		}
		
		public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
				PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
		}
		
		public void operacionesParaEliminar(Object theId, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaEliminarST(theId, smt);
		}
		
		public void operacionesParaCrear(Object theDto, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaCrearST(theDto, smt);
		}
		
		public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
		        throws SQLException, ExcepcionEnDAO {
			HthPagoServiciosDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
        }
		
		public void operacionesParaConteo(PeticionDeDatos request,
				PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaConteoST(request, smt);				
		}
		
		public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
		}
		
		public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
				throws SQLException, ExcepcionEnDAO {
		HthPagoServiciosDaoJdbcSql.operacionesParaActualizarST(theDto, smt);			
		}
		
		public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
				throws SQLException, ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);	
		}
		
		public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
				throws SQLException, ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
		}
		
		public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
				ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.getSqlDeConteoST(request);
		}
		
		public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.SQL_ENTIDAD_META;
		}
		
		public String getEliminarSql() {
			return  HthPagoServiciosDaoJdbcSql.DELETE_SQL;
		}
		
		public String getCrearSql() {
			return  HthPagoServiciosDaoJdbcSql.CREATE_SQL;
		}
		
		public String getCrearSqlConId(){
		    return HthPagoServiciosDaoJdbcSql.CREATE_SQL_CON_ID;
		}			
		
		public String getBuscarPorIdSqlF() {
			return HthPagoServiciosDaoJdbcSql.FIND_BY_ID_SQL_F;
		}
		
		public String getBuscarPorIdSql() {				
			return HthPagoServiciosDaoJdbcSql.FIND_BY_ID_SQL;
		}
		
		public String getActualizarSql() {				
			return HthPagoServiciosDaoJdbcSql.UPDATE_SQL;
		}
		
		public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.crearDtoST(rst);
		}
        
        public Object crearDataConValoresForaneos(ResultSet rst)
				throws SQLException, ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.crearDataConValoresForaneosST(rst);
		}
		
        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
				throws SQLException, ExcepcionEnDAO {
			return HthPagoServiciosDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
		}
		
		
        public boolean isForaneosDisponible(){
			return true;
        
        }
        
        public SQLLenguaje getSQLLenguaje() {
            return HthPagoServiciosDaoJdbcSql.getLenguaje();
        }
        
        public void setIdEnDto(Object theDto, Object dtoId) {
            setIdEnDtoST(theDto, dtoId);
        }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthPagoServicios</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		HthPagoServicios dto = (HthPagoServicios)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdPagoServicio(dtoId);	
	}
	
}
