package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoFormato;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthAtributoFormatoDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoEncabezado;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoDetalle;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthAtributoEncabezadoDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthAtributoEncabezadoDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthAtributoDetalleDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthAtributoDetalleDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthFormatoDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthFormatoDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Atributo Formato</b>
 * asociado a la tabla <b>HTH_Atributo_Formato</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthAtributoFormatoDaoJdbcSql extends SQLImplementacionBasica implements HthAtributoFormatoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Atributo_Formato", new SQLColumnaMeta[]{
        new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13001, "id_atributo_formato", "C13001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13012, "id_atributo_encabezado", "C13012", Types.BIGINT )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13013, "id_atributo_detalle", "C13013", Types.BIGINT )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13014, "id_formato", "C13014", Types.BIGINT )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13002, "posicion", "C13002", Types.BIGINT )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13003, "posicion_ini", "C13003", Types.BIGINT )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13004, "posicion_fin", "C13004", Types.BIGINT )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13005, "tipo_dato_atributo", "C13005", Types.VARCHAR )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13006, "formato_atributo", "C13006", Types.VARCHAR )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13007, "opcional", "C13007", Types.VARCHAR )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13008, "usuario_creacion", "C13008", Types.VARCHAR )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13009, "fecha_creacion", "C13009", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13010, "usuario_ult_act", "C13010", Types.VARCHAR )
     ,  new SQLColumnaMeta(13, "HTH_Atributo_Formato", "E13", 13011, "fecha_ult_act", "C13011", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12001, "id_atributo_encabezado", "C12001", Types.BIGINT)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12002, "nombre_atributo", "C12002", Types.VARCHAR)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12003, "descripcion", "C12003", Types.VARCHAR)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12004, "tabla", "C12004", Types.VARCHAR)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12005, "columna", "C12005", Types.VARCHAR)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12006, "usuario_creacion", "C12006", Types.VARCHAR)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12007, "fecha_creacion", "C12007", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12008, "usuario_ult_act", "C12008", Types.VARCHAR)
     ,  new SQLColumnaMeta(12, "HTH_Atributo_Encabezado", "E12", 12009, "fecha_ult_act", "C12009", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11001, "id_atributo_detalle", "C11001", Types.BIGINT)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11002, "nombre_atributo", "C11002", Types.VARCHAR)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11003, "descripcion", "C11003", Types.VARCHAR)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11004, "tabla", "C11004", Types.VARCHAR)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11005, "columna", "C11005", Types.VARCHAR)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11006, "usuario_creacion", "C11006", Types.VARCHAR)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11007, "fecha_creacion", "C11007", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11008, "usuario_ult_act", "C11008", Types.VARCHAR)
     ,  new SQLColumnaMeta(11, "HTH_Atributo_Detalle", "E11", 11009, "fecha_ult_act", "C11009", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16001, "id_formato", "C16001", Types.BIGINT)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16002, "nombre_formato", "C16002", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16003, "tipo_formato", "C16003", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16004, "tipo_separador", "C16004", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16005, "caracter_separador", "C16005", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16006, "lote_destino", "C16006", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16007, "usuario_creacion", "C16007", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16008, "fecha_creacion", "C16008", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16009, "usuario_ult_act", "C16009", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16010, "fecha_ult_act", "C16010", Types.TIMESTAMP)
	});
	
    
	public HthAtributoEncabezado getHthAtributoEncabezado(HthAtributoFormato hthAtributoFormato)throws ExcepcionEnDAO{
		return getHthAtributoEncabezado(hthAtributoFormato, null);
	}

    public HthAtributoEncabezado getHthAtributoEncabezado(HthAtributoFormato hthAtributoFormato,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     HthAtributoEncabezado hthAtributoEncabezadoRel = hthAtributoFormato.getHthAtributoEncabezado();
	     if(hthAtributoEncabezadoRel==null){
                PeticionDeDatos peticionDeDatosParaHthAtributoEncabezado = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaHthAtributoEncabezado.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              HthAtributoEncabezadoDao.IDATRIBUTOENCABEZADO, 
	     	                              hthAtributoFormato.getIdAtributoEncabezado()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		hthAtributoEncabezadoRel = (HthAtributoEncabezado)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaHthAtributoEncabezado, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										HthAtributoEncabezadoDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthAtributoFormato.setHthAtributoEncabezado(hthAtributoEncabezadoRel);										
		}
										
		return 	hthAtributoEncabezadoRel;									
    }
    
    
	public HthAtributoDetalle getHthAtributoDetalle(HthAtributoFormato hthAtributoFormato)throws ExcepcionEnDAO{
		return getHthAtributoDetalle(hthAtributoFormato, null);
	}

    public HthAtributoDetalle getHthAtributoDetalle(HthAtributoFormato hthAtributoFormato,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     HthAtributoDetalle hthAtributoDetalleRel = hthAtributoFormato.getHthAtributoDetalle();
	     if(hthAtributoDetalleRel==null){
                PeticionDeDatos peticionDeDatosParaHthAtributoDetalle = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaHthAtributoDetalle.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              HthAtributoDetalleDao.IDATRIBUTODETALLE, 
	     	                              hthAtributoFormato.getIdAtributoDetalle()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		hthAtributoDetalleRel = (HthAtributoDetalle)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaHthAtributoDetalle, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										HthAtributoDetalleDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthAtributoFormato.setHthAtributoDetalle(hthAtributoDetalleRel);										
		}
										
		return 	hthAtributoDetalleRel;									
    }
    
    
	public HthFormato getHthFormato(HthAtributoFormato hthAtributoFormato)throws ExcepcionEnDAO{
		return getHthFormato(hthAtributoFormato, null);
	}

    public HthFormato getHthFormato(HthAtributoFormato hthAtributoFormato,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     HthFormato hthFormatoRel = hthAtributoFormato.getHthFormato();
	     if(hthFormatoRel==null){
                PeticionDeDatos peticionDeDatosParaHthFormato = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaHthFormato.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              HthFormatoDao.IDFORMATO, 
	     	                              hthAtributoFormato.getIdFormato()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		hthFormatoRel = (HthFormato)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaHthFormato, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										HthFormatoDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthAtributoFormato.setHthFormato(hthFormatoRel);										
		}
										
		return 	hthFormatoRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E13.id_atributo_formato AS C13001 , E13.id_atributo_encabezado AS C13012 , E13.id_atributo_detalle AS C13013 , E13.id_formato AS C13014 , " +
		"       E13.posicion AS C13002 , E13.posicion_ini AS C13003 , E13.posicion_fin AS C13004 , E13.tipo_dato_atributo AS C13005 , " +
		"       E13.formato_atributo AS C13006 , E13.opcional AS C13007 , E13.usuario_creacion AS C13008 , E13.fecha_creacion AS C13009 , " +
		"       E13.usuario_ult_act AS C13010 , E13.fecha_ult_act AS C13011 , " +
		"       E12.id_atributo_encabezado AS C12001 , E12.nombre_atributo AS C12002 , E12.descripcion AS C12003 , E12.tabla AS C12004 , " +
		"       E12.columna AS C12005 , E12.usuario_creacion AS C12006 , E12.fecha_creacion AS C12007 , E12.usuario_ult_act AS C12008 , " +
		"       E12.fecha_ult_act AS C12009 , " +
		"       E11.id_atributo_detalle AS C11001 , E11.nombre_atributo AS C11002 , E11.descripcion AS C11003 , E11.tabla AS C11004 , " +
		"       E11.columna AS C11005 , E11.usuario_creacion AS C11006 , E11.fecha_creacion AS C11007 , E11.usuario_ult_act AS C11008 , " +
		"       E11.fecha_ult_act AS C11009 , " +
		"       E16.id_formato AS C16001 , E16.nombre_formato AS C16002 , E16.tipo_formato AS C16003 , E16.tipo_separador AS C16004 , " +
		"       E16.caracter_separador AS C16005 , E16.lote_destino AS C16006 , E16.usuario_creacion AS C16007 , E16.fecha_creacion AS C16008 , " +
		"       E16.usuario_ult_act AS C16009 , E16.fecha_ult_act AS C16010" +
		"  FROM HTH_Atributo_Formato E13 " +
		"  LEFT OUTER JOIN HTH_Atributo_Encabezado E12 ON  E12.id_atributo_encabezado = E13.id_atributo_encabezado" +
		"  LEFT OUTER JOIN HTH_Atributo_Detalle E11 ON E11.id_atributo_detalle = E13.id_atributo_detalle " +
		"  LEFT OUTER JOIN HTH_Formato E16 ON E16.id_formato = E13.id_formato" +
		" WHERE E13.id_atributo_formato = E13.id_atributo_formato ";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E13.id_atributo_formato AS C13001 , E13.id_atributo_encabezado AS C13012 , E13.id_atributo_detalle AS C13013 , E13.id_formato AS C13014 , " +
		"       E13.posicion AS C13002 , E13.posicion_ini AS C13003 , E13.posicion_fin AS C13004 , E13.tipo_dato_atributo AS C13005 , " +
		"       E13.formato_atributo AS C13006 , E13.opcional AS C13007 , E13.usuario_creacion AS C13008 , E13.fecha_creacion AS C13009 , " +
		"       E13.usuario_ult_act AS C13010 , E13.fecha_ult_act AS C13011 , " +
		"       E12.id_atributo_encabezado AS C12001 , E12.nombre_atributo AS C12002 , E12.descripcion AS C12003 , E12.tabla AS C12004 , " +
		"       E12.columna AS C12005 , E12.usuario_creacion AS C12006 , E12.fecha_creacion AS C12007 , E12.usuario_ult_act AS C12008 , " +
		"       E12.fecha_ult_act AS C12009 , " +
		"       E11.id_atributo_detalle AS C11001 , E11.nombre_atributo AS C11002 , E11.descripcion AS C11003 , E11.tabla AS C11004 , " +
		"       E11.columna AS C11005 , E11.usuario_creacion AS C11006 , E11.fecha_creacion AS C11007 , E11.usuario_ult_act AS C11008 , " +
		"       E11.fecha_ult_act AS C11009 , " +
		"       E16.id_formato AS C16001 , E16.nombre_formato AS C16002 , E16.tipo_formato AS C16003 , E16.tipo_separador AS C16004 , " +
		"       E16.caracter_separador AS C16005 , E16.lote_destino AS C16006 , E16.usuario_creacion AS C16007 , E16.fecha_creacion AS C16008 , " +
		"       E16.usuario_ult_act AS C16009 , E16.fecha_ult_act AS C16010" +
		"  FROM HTH_Atributo_Formato E13 " +
		"  LEFT OUTER JOIN HTH_Atributo_Encabezado E12 ON  E12.id_atributo_encabezado = E13.id_atributo_encabezado" +
		"  LEFT OUTER JOIN HTH_Atributo_Detalle E11 ON E11.id_atributo_detalle = E13.id_atributo_detalle " +
		"  LEFT OUTER JOIN HTH_Formato E16 ON E16.id_formato = E13.id_formato" +
		"   AND E13.id_atributo_formato= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E13.id_atributo_formato AS C13001 , E13.id_atributo_encabezado AS C13012 , E13.id_atributo_detalle AS C13013 , E13.id_formato AS C13014 , " +
		"       E13.posicion AS C13002 , E13.posicion_ini AS C13003 , E13.posicion_fin AS C13004 , E13.tipo_dato_atributo AS C13005 , " +
		"       E13.formato_atributo AS C13006 , E13.opcional AS C13007 , E13.usuario_creacion AS C13008 , E13.fecha_creacion AS C13009 , " +
		"       E13.usuario_ult_act AS C13010 , E13.fecha_ult_act AS C13011" +
		"  FROM HTH_Atributo_Formato E13";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Atributo_Formato";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Atributo_Formato (" +
		"id_atributo_encabezado,id_atributo_detalle,id_formato,posicion,posicion_ini" +
		",posicion_fin,tipo_dato_atributo,formato_atributo,opcional" +
		",usuario_creacion,fecha_creacion,usuario_ult_act,fecha_ult_act" +
		"" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Atributo_Formato (" +
		"id_atributo_formato,id_atributo_encabezado,id_atributo_detalle,id_formato,posicion" +
		",posicion_ini,posicion_fin,tipo_dato_atributo,formato_atributo" +
		",opcional,usuario_creacion,fecha_creacion,usuario_ult_act" +
		",fecha_ult_act" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Atributo_Formato" +
		" SET id_atributo_encabezado= ?  , id_atributo_detalle= ?  , id_formato= ?  , posicion= ?  , " +
		"posicion_ini= ?  , posicion_fin= ?  , tipo_dato_atributo= ?  , formato_atributo= ?  , " +
		"opcional= ?  , usuario_creacion= ?  , fecha_creacion= ?  , usuario_ult_act= ?  , " +
		"fecha_ult_act= ? " +
		" WHERE id_atributo_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Atributo_Formato" +
		" WHERE id_atributo_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E13.id_atributo_formato AS C13001 , E13.id_atributo_encabezado AS C13012 , E13.id_atributo_detalle AS C13013 , E13.id_formato AS C13014 , " +
		"       E13.posicion AS C13002 , E13.posicion_ini AS C13003 , E13.posicion_fin AS C13004 , E13.tipo_dato_atributo AS C13005 , " +
		"       E13.formato_atributo AS C13006 , E13.opcional AS C13007 , E13.usuario_creacion AS C13008 , E13.fecha_creacion AS C13009 , " +
		"       E13.usuario_ult_act AS C13010 , E13.fecha_ult_act AS C13011" +
		"  FROM HTH_Atributo_Formato E13" +
		" WHERE E13.id_atributo_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Atributo_Formato (" +
		"    id_atributo_formato LONG NOT NULL," +
		"    id_atributo_encabezado LONG," +
		"    id_atributo_detalle LONG," +
		"    id_formato LONG NOT NULL," +
		"    posicion LONG NOT NULL," +
		"    posicion_ini LONG NOT NULL," +
		"    posicion_fin LONG NOT NULL," +
		"    tipo_dato_atributo VARCHAR(1)  NOT NULL," +
		"    formato_atributo VARCHAR(25) ," +
		"    opcional VARCHAR(1)  NOT NULL," +
		"    usuario_creacion VARCHAR(100)  NOT NULL," +
		"    fecha_creacion TIMESTAMP NOT NULL," +
		"    usuario_ult_act VARCHAR(100)  NOT NULL," +
		"    fecha_ult_act TIMESTAMP NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthAtributoFormato theDto = new HthAtributoFormato();
     	theDto.setIdAtributoFormato(SQLUtils.getLongFromResultSet(rst, "C13001"));
     	theDto.setIdAtributoEncabezado(SQLUtils.getLongFromResultSet(rst, "C13012"));
     	theDto.setIdAtributoDetalle(SQLUtils.getLongFromResultSet(rst, "C13013"));
     	theDto.setIdFormato(SQLUtils.getLongFromResultSet(rst, "C13014"));
     	theDto.setPosicion(SQLUtils.getLongFromResultSet(rst, "C13002"));
     	theDto.setPosicionIni(SQLUtils.getLongFromResultSet(rst, "C13003"));
     	theDto.setPosicionFin(SQLUtils.getLongFromResultSet(rst, "C13004"));
		theDto.setTipoDatoAtributo(rst.getString("C13005"));
		theDto.setFormatoAtributo(rst.getString("C13006"));
		theDto.setOpcional(rst.getString("C13007"));
		theDto.setUsuarioCreacion(rst.getString("C13008"));
		theDto.setFechaCreacin(rst.getTimestamp("C13009"));
		theDto.setUsuarioUltAct(rst.getString("C13010"));
		theDto.setFechaUltAct(rst.getTimestamp("C13011"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthAtributoFormato</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static HthAtributoFormato crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		HthAtributoFormato hthAtributoFormato = (HthAtributoFormato) crearDtoST(rst);
        HthAtributoEncabezado hthAtributoEncabezadoRel  = (HthAtributoEncabezado) HthAtributoEncabezadoDaoJdbcSql.crearDtoST(rst);
        hthAtributoFormato.setHthAtributoEncabezado(hthAtributoEncabezadoRel);
        HthAtributoDetalle hthAtributoDetalleRel  = (HthAtributoDetalle) HthAtributoDetalleDaoJdbcSql.crearDtoST(rst);
        hthAtributoFormato.setHthAtributoDetalle(hthAtributoDetalleRel);
        HthFormato hthFormatoRel  = (HthFormato) HthFormatoDaoJdbcSql.crearDtoST(rst);
        hthAtributoFormato.setHthFormato(hthFormatoRel);
		return hthAtributoFormato;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthAtributoFormato</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static HthAtributoFormato crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		HthAtributoFormato hthAtributoFormato = (HthAtributoFormato) crearDtoST(rst);
        HthAtributoEncabezado idAtributoEncabezadoRelhthAtributoEncabezado  = (HthAtributoEncabezado) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthAtributoFormato.getIdAtributoEncabezado(), conn, 
				HthAtributoEncabezadoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthAtributoFormato.setHthAtributoEncabezado(idAtributoEncabezadoRelhthAtributoEncabezado);
        HthAtributoDetalle idAtributoDetalleRelhthAtributoDetalle  = (HthAtributoDetalle) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthAtributoFormato.getIdAtributoDetalle(), conn, 
				HthAtributoDetalleDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthAtributoFormato.setHthAtributoDetalle(idAtributoDetalleRelhthAtributoDetalle);
        HthFormato idFormatoRelhthFormato  = (HthFormato) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthAtributoFormato.getIdFormato(), conn, 
				HthFormatoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthAtributoFormato.setHthFormato(idFormatoRelhthFormato);
		return hthAtributoFormato;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthAtributoFormato theDto = (HthAtributoFormato)theDtoUntyped;
				Long idAtributoEncabezado = theDto.getIdAtributoEncabezado();     
		Long idAtributoDetalle = theDto.getIdAtributoDetalle();     
		Long idFormato = theDto.getIdFormato();     
		Long posicion = theDto.getPosicion();     
		Long posicionIni = theDto.getPosicionIni();     
		Long posicionFin = theDto.getPosicionFin();     
		String tipoDatoAtributo = theDto.getTipoDatoAtributo();     
		String formatoAtributo = theDto.getFormatoAtributo();     
		String opcional = theDto.getOpcional();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacin = theDto.getFechaCreacin();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, idAtributoEncabezado, smt);  
    		     
		SQLUtils.setIntoStatement( 2, idAtributoDetalle, smt);  
    		     
		SQLUtils.setIntoStatement( 3, idFormato, smt);  
    		     
		SQLUtils.setIntoStatement( 4, posicion, smt);  
    		     
		SQLUtils.setIntoStatement( 5, posicionIni, smt);  
    		     
		SQLUtils.setIntoStatement( 6, posicionFin, smt);  
    		     
		SQLUtils.setIntoStatement( 7, tipoDatoAtributo, smt);  
    		     
		SQLUtils.setIntoStatement( 8, formatoAtributo, smt);  
    		     
		SQLUtils.setIntoStatement( 9, opcional, smt);  
    		     
		SQLUtils.setIntoStatement( 10, usuarioCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 11, fechaCreacin, smt);  
    		     
		SQLUtils.setIntoStatement( 12, usuarioUltAct, smt);  
    		     
		SQLUtils.setIntoStatement( 13, fechaUltAct, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthAtributoFormato theDto = (HthAtributoFormato)theDtoUntyped;
				Long idAtributoFormato = theDto.getIdAtributoFormato();     
		Long idAtributoEncabezado = theDto.getIdAtributoEncabezado();     
		Long idAtributoDetalle = theDto.getIdAtributoDetalle();     
		Long idFormato = theDto.getIdFormato();     
		Long posicion = theDto.getPosicion();     
		Long posicionIni = theDto.getPosicionIni();     
		Long posicionFin = theDto.getPosicionFin();     
		String tipoDatoAtributo = theDto.getTipoDatoAtributo();     
		String formatoAtributo = theDto.getFormatoAtributo();     
		String opcional = theDto.getOpcional();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacin = theDto.getFechaCreacin();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
	
		SQLUtils.setIntoStatement( 1, idAtributoFormato, smt);
    	
		SQLUtils.setIntoStatement( 2, idAtributoEncabezado, smt);
    	
		SQLUtils.setIntoStatement( 3, idAtributoDetalle, smt);
    	
		SQLUtils.setIntoStatement( 4, idFormato, smt);
    	
		SQLUtils.setIntoStatement( 5, posicion, smt);
    	
		SQLUtils.setIntoStatement( 6, posicionIni, smt);
    	
		SQLUtils.setIntoStatement( 7, posicionFin, smt);
    	
		SQLUtils.setIntoStatement( 8, tipoDatoAtributo, smt);
    	
		SQLUtils.setIntoStatement( 9, formatoAtributo, smt);
    	
		SQLUtils.setIntoStatement( 10, opcional, smt);
    	
		SQLUtils.setIntoStatement( 11, usuarioCreacion, smt);
    	
		SQLUtils.setIntoStatement( 12, fechaCreacin, smt);
    	
		SQLUtils.setIntoStatement( 13, usuarioUltAct, smt);
    	
		SQLUtils.setIntoStatement( 14, fechaUltAct, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthAtributoFormato theDto = (HthAtributoFormato)theDtoUntyped;
				Long idAtributoFormato = theDto.getIdAtributoFormato();     
		Long idAtributoEncabezado = theDto.getIdAtributoEncabezado();     
		Long idAtributoDetalle = theDto.getIdAtributoDetalle();     
		Long idFormato = theDto.getIdFormato();     
		Long posicion = theDto.getPosicion();     
		Long posicionIni = theDto.getPosicionIni();     
		Long posicionFin = theDto.getPosicionFin();     
		String tipoDatoAtributo = theDto.getTipoDatoAtributo();     
		String formatoAtributo = theDto.getFormatoAtributo();     
		String opcional = theDto.getOpcional();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacin = theDto.getFechaCreacin();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, idAtributoEncabezado, smt);  
        	     
		SQLUtils.setIntoStatement( 2, idAtributoDetalle, smt);  
        	     
		SQLUtils.setIntoStatement( 3, idFormato, smt);  
        	     
		SQLUtils.setIntoStatement( 4, posicion, smt);  
        	     
		SQLUtils.setIntoStatement( 5, posicionIni, smt);  
        	     
		SQLUtils.setIntoStatement( 6, posicionFin, smt);  
        	     
		SQLUtils.setIntoStatement( 7, tipoDatoAtributo, smt);  
        	     
		SQLUtils.setIntoStatement( 8, formatoAtributo, smt);  
        	     
		SQLUtils.setIntoStatement( 9, opcional, smt);  
        	     
		SQLUtils.setIntoStatement( 10, usuarioCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 11, fechaCreacin, smt);  
        	     
		SQLUtils.setIntoStatement( 12, usuarioUltAct, smt);  
        	     
		SQLUtils.setIntoStatement( 13, fechaUltAct, smt);  
        		     
		SQLUtils.setIntoStatement( 14, idAtributoFormato, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthAtributoFormato</code> objeto del tipo <code>HthAtributoFormato</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthAtributoFormato hthAtributoFormato) throws ExcepcionEnDAO {
	    actualizar(hthAtributoFormato, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idAtributoFormatoId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthAtributoFormato</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthAtributoFormato buscarPorID(Long idAtributoFormatoId)
			throws ExcepcionEnDAO {
		return (HthAtributoFormato)buscarPorID(idAtributoFormatoId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthAtributoFormato</code> objeto del tipo <code>HthAtributoFormato</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthAtributoFormato hthAtributoFormato) throws ExcepcionEnDAO {
	    crear(hthAtributoFormato, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idAtributoFormatoId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idAtributoFormatoId) throws ExcepcionEnDAO {
	    eliminar(idAtributoFormatoId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthAtributoFormato</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthAtributoFormato resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthAtributoFormato)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthAtributoFormatoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthAtributoFormatoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return HthAtributoFormatoDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthAtributoFormatoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthAtributoFormatoDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthAtributoFormatoDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthAtributoFormatoDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthAtributoFormatoDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthAtributoFormatoDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return HthAtributoFormatoDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return HthAtributoFormatoDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthAtributoFormatoDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthAtributoFormatoDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return HthAtributoFormatoDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return HthAtributoFormatoDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthAtributoFormatoDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthAtributoFormato</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthAtributoFormato dto = (HthAtributoFormato)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdAtributoFormato(dtoId);

	}	
	
	
}
