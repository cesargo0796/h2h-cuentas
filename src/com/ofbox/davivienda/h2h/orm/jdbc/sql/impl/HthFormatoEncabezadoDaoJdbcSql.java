package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormatoEncabezado;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthFormatoEncabezadoDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Formato Encabezado</b>
 * asociado a la tabla <b>HTH_Formato</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthFormatoEncabezadoDaoJdbcSql extends SQLImplementacionBasica implements HthFormatoEncabezadoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Formato", new SQLColumnaMeta[]{
        new SQLColumnaMeta(15, "HTH_Formato", "E15", 15001, "id_formato", "C15001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15002, "nombre_formato", "C15002", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15003, "tipo_formato", "C15003", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15004, "tipo_separador", "C15004", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15005, "caracter_separador", "C15005", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15006, "lote_destino", "C15006", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15007, "usuario_creacion", "C15007", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15008, "fecha_creacion", "C15008", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15009, "usuario_ult_act", "C15009", Types.VARCHAR )
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15010, "fecha_ult_act", "C15010", Types.TIMESTAMP )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E15.id_formato AS C15001 , E15.nombre_formato AS C15002 , E15.tipo_formato AS C15003 , E15.tipo_separador AS C15004 , " +
		"       E15.caracter_separador AS C15005 , E15.lote_destino AS C15006 , E15.usuario_creacion AS C15007 , E15.fecha_creacion AS C15008 , " +
		"       E15.usuario_ult_act AS C15009 , E15.fecha_ult_act AS C15010" +
		"  FROM HTH_Formato E15";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Formato";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Formato (" +
		"nombre_formato,tipo_formato,tipo_separador,caracter_separador,lote_destino" +
		",usuario_creacion,fecha_creacion,usuario_ult_act,fecha_ult_act" +
		"" +
		") VALUES (?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Formato (" +
		"id_formato,nombre_formato,tipo_formato,tipo_separador,caracter_separador" +
		",lote_destino,usuario_creacion,fecha_creacion,usuario_ult_act" +
		",fecha_ult_act" +
		") VALUES (?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Formato" +
		" SET nombre_formato= ?  , tipo_formato= ?  , tipo_separador= ?  , caracter_separador= ?  , " +
		"lote_destino= ?  , usuario_creacion= ?  , fecha_creacion= ?  , usuario_ult_act= ?  , " +
		"fecha_ult_act= ? " +
		" WHERE id_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Formato" +
		" WHERE id_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E15.id_formato AS C15001 , E15.nombre_formato AS C15002 , E15.tipo_formato AS C15003 , E15.tipo_separador AS C15004 , " +
		"       E15.caracter_separador AS C15005 , E15.lote_destino AS C15006 , E15.usuario_creacion AS C15007 , E15.fecha_creacion AS C15008 , " +
		"       E15.usuario_ult_act AS C15009 , E15.fecha_ult_act AS C15010" +
		"  FROM HTH_Formato E15" +
		" WHERE E15.id_formato= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Formato (" +
		"    id_formato LONG NOT NULL," +
		"    nombre_formato VARCHAR(100)  NOT NULL," +
		"    tipo_formato VARCHAR(1)  NOT NULL," +
		"    tipo_separador VARCHAR(1)  NOT NULL," +
		"    caracter_separador VARCHAR(10) ," +
		"    lote_destino VARCHAR(3)  NOT NULL," +
		"    usuario_creacion VARCHAR(100)  NOT NULL," +
		"    fecha_creacion TIMESTAMP NOT NULL," +
		"    usuario_ult_act VARCHAR(100)  NOT NULL," +
		"    fecha_ult_act TIMESTAMP NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthFormatoEncabezado theDto = new HthFormatoEncabezado();
     	theDto.setIdFormato(SQLUtils.getLongFromResultSet(rst, "C15001"));
		theDto.setNombreFormato(rst.getString("C15002"));
		theDto.setTipoFormato(rst.getString("C15003"));
		theDto.setTipoSeparador(rst.getString("C15004"));
		theDto.setCaracterSeparador(rst.getString("C15005"));
		theDto.setLoteDestino(rst.getString("C15006"));
		theDto.setUsuarioCreacion(rst.getString("C15007"));
		theDto.setFechaCreacion(rst.getTimestamp("C15008"));
		theDto.setUsuarioUltAct(rst.getString("C15009"));
		theDto.setFechaUltAct(rst.getTimestamp("C15010"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthFormatoEncabezado theDto = (HthFormatoEncabezado)theDtoUntyped;
				String nombreFormato = theDto.getNombreFormato();     
		String tipoFormato = theDto.getTipoFormato();     
		String tipoSeparador = theDto.getTipoSeparador();     
		String caracterSeparador = theDto.getCaracterSeparador();     
		String loteDestino = theDto.getLoteDestino();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, nombreFormato, smt);  
    		     
		SQLUtils.setIntoStatement( 2, tipoFormato, smt);  
    		     
		SQLUtils.setIntoStatement( 3, tipoSeparador, smt);  
    		     
		SQLUtils.setIntoStatement( 4, caracterSeparador, smt);  
    		     
		SQLUtils.setIntoStatement( 5, loteDestino, smt);  
    		     
		SQLUtils.setIntoStatement( 6, usuarioCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 7, fechaCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 8, usuarioUltAct, smt);  
    		     
		SQLUtils.setIntoStatement( 9, fechaUltAct, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthFormatoEncabezado theDto = (HthFormatoEncabezado)theDtoUntyped;
				Long idFormato = theDto.getIdFormato();     
		String nombreFormato = theDto.getNombreFormato();     
		String tipoFormato = theDto.getTipoFormato();     
		String tipoSeparador = theDto.getTipoSeparador();     
		String caracterSeparador = theDto.getCaracterSeparador();     
		String loteDestino = theDto.getLoteDestino();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
	
		SQLUtils.setIntoStatement( 1, idFormato, smt);
    	
		SQLUtils.setIntoStatement( 2, nombreFormato, smt);
    	
		SQLUtils.setIntoStatement( 3, tipoFormato, smt);
    	
		SQLUtils.setIntoStatement( 4, tipoSeparador, smt);
    	
		SQLUtils.setIntoStatement( 5, caracterSeparador, smt);
    	
		SQLUtils.setIntoStatement( 6, loteDestino, smt);
    	
		SQLUtils.setIntoStatement( 7, usuarioCreacion, smt);
    	
		SQLUtils.setIntoStatement( 8, fechaCreacion, smt);
    	
		SQLUtils.setIntoStatement( 9, usuarioUltAct, smt);
    	
		SQLUtils.setIntoStatement( 10, fechaUltAct, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthFormatoEncabezado theDto = (HthFormatoEncabezado)theDtoUntyped;
				Long idFormato = theDto.getIdFormato();     
		String nombreFormato = theDto.getNombreFormato();     
		String tipoFormato = theDto.getTipoFormato();     
		String tipoSeparador = theDto.getTipoSeparador();     
		String caracterSeparador = theDto.getCaracterSeparador();     
		String loteDestino = theDto.getLoteDestino();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		     
		SQLUtils.setIntoStatement( 1, nombreFormato, smt);  
        	     
		SQLUtils.setIntoStatement( 2, tipoFormato, smt);  
        	     
		SQLUtils.setIntoStatement( 3, tipoSeparador, smt);  
        	     
		SQLUtils.setIntoStatement( 4, caracterSeparador, smt);  
        	     
		SQLUtils.setIntoStatement( 5, loteDestino, smt);  
        	     
		SQLUtils.setIntoStatement( 6, usuarioCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 7, fechaCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 8, usuarioUltAct, smt);  
        	     
		SQLUtils.setIntoStatement( 9, fechaUltAct, smt);  
        		     
		SQLUtils.setIntoStatement( 10, idFormato, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthFormatoEncabezado</code> objeto del tipo <code>HthFormatoEncabezado</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthFormatoEncabezado hthFormatoEncabezado) throws ExcepcionEnDAO {
	    actualizar(hthFormatoEncabezado, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idFormatoId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthFormatoEncabezado</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthFormatoEncabezado buscarPorID(Long idFormatoId)
			throws ExcepcionEnDAO {
		return (HthFormatoEncabezado)buscarPorID(idFormatoId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthFormatoEncabezado</code> objeto del tipo <code>HthFormatoEncabezado</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthFormatoEncabezado hthFormatoEncabezado) throws ExcepcionEnDAO {
	    crear(hthFormatoEncabezado, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idFormatoId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idFormatoId) throws ExcepcionEnDAO {
	    eliminar(idFormatoId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthFormatoEncabezado</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthFormatoEncabezado resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthFormatoEncabezado)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthFormatoEncabezadoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthFormatoEncabezadoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthFormatoEncabezadoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthFormatoEncabezadoDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthFormatoEncabezadoDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthFormatoEncabezadoDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthFormatoEncabezadoDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthFormatoEncabezadoDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return HthFormatoEncabezadoDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthFormatoEncabezadoDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthFormatoEncabezadoDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthFormatoEncabezadoDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthFormatoEncabezado</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthFormatoEncabezado dto = (HthFormatoEncabezado)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdFormato(dtoId);

	}	
	
	
}
