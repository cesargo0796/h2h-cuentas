package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.HthProcesoMonitoreo;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthProcesoMonitoreoDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthVistaConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsEmpresaodepto;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormatoEncabezado;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsClienteDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsClienteDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthVistaConvenioDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthVistaConvenioDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsEmpresaodeptoDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsEmpresaodeptoDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthFormatoEncabezadoDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthFormatoEncabezadoDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.HthFormatoDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthFormatoDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Proceso Monitoreo</b>
 * asociado a la tabla <b>HTH_Proceso_Monitoreo</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class HthProcesoMonitoreoDaoJdbcSql extends SQLImplementacionBasica implements HthProcesoMonitoreoDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("HTH_Proceso_Monitoreo", new SQLColumnaMeta[]{
        new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19001, "id_proceso_monitoreo", "C19001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19021, "Cliente", "C19021", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19022, "Id_Vista_Convenio", "C19022", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19002, "Convenio", "C19002", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19003, "TipoConvenio", "C19003", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19023, "EmpresaODepto", "C19023", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19004, "convenio_PE_ACH", "C19004", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19005, "convenio_nombre", "C19005", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19006, "nombre_archivo", "C19006", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19007, "ruta_sftp", "C19007", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19008, "ruta_archivos_in", "C19008", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19009, "ruta_archivos_out", "C19009", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19010, "ruta_procesados", "C19010", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19011, "tiempo_monitoreo", "C19011", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19012, "linea_encabezado", "C19012", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19024, "id_formato_encabezado", "C19024", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19013, "procesamiento_superusuario", "C19013", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19025, "id_formato_detalle", "C19025", Types.BIGINT )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19014, "monto_maximo_procesar", "C19014", Types.NUMERIC )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19015, "estado", "C19015", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19016, "usuario_creacion", "C19016", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19017, "fecha_creacion", "C19017", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19018, "usuario_ult_act", "C19018", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19019, "fecha_ult_act", "C19019", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19020, "email_cliente", "C19020", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19026, "pm_host_sftp", "C19026", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19027, "pm_puerto_sftp", "C19027", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19028, "pm_usuario_sftp", "C19028", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19029, "pm_password_sftp", "C19029", Types.VARCHAR )
     ,  new SQLColumnaMeta(19, "HTH_Proceso_Monitoreo", "E19", 19030, "pm_llave_sftp", "C19030", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 1, "Cliente", "C1", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 2, "Nombre", "C2", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 3, "Modulos", "C3", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 4, "Direccion", "C4", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 5, "Telefono", "C5", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 6, "Fax", "C6", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 7, "Email", "C7", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 8, "NombreContacto", "C8", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 9, "ApellidoContacto", "C9", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 10, "NumInstalacion", "C10", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 11, "FechaEstatus", "C11", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 12, "Estatus", "C12", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 13, "FechaCreacion", "C13", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 14, "SolicitudPendiente", "C14", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 15, "NivelSeguridadTEx", "C15", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 16, "NivelSeguridadPImp", "C16", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 17, "nombreRepresentante", "C17", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 18, "apellidoRepresentante", "C18", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 19, "proveedorInternet", "C19", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 20, "Banca", "C20", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 21, "PoliticaCobroTEx", "C21", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 22, "ComisionTransExt", "C22", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 23, "CodigoTablaTransExt", "C23", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 24, "NivelSeguridadISSS", "C24", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 25, "NivelSeguridadAFP", "C25", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 26, "NivelSeguridadCCre", "C26", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 27, "NivelSeguridadPPag", "C27", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 28, "CargaPersonalizable", "C28", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 29, "TipoDoctoRepresentante", "C29", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 30, "DoctoRepresentante", "C30", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 31, "Ciudad", "C31", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 32, "Cargo", "C32", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 33, "LimiteXArchivo", "C33", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 34, "LimiteXLote", "C34", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 35, "LimiteXTransaccion", "C35", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 36, "PermiteDebitos", "C36", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 37, "LimiteCreditos", "C37", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 38, "LimiteDebitos", "C38", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 39, "ComisionTrnACH", "C39", Types.NUMERIC)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20001, "Id_Vista_Convenio", "C20001", Types.BIGINT)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20002, "TipoConvenio", "C20002", Types.BIGINT)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20003, "Convenio", "C20003", Types.BIGINT)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20004, "Cliente", "C20004", Types.BIGINT)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20005, "Nombre", "C20005", Types.VARCHAR)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20006, "Cuenta", "C20006", Types.VARCHAR)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20007, "NombreCta", "C20007", Types.VARCHAR)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20008, "TipoMoneda", "C20008", Types.BIGINT)
     ,  new SQLColumnaMeta(20, "HTH_Vista_Convenio", "E20", 20009, "Descripcion", "C20009", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7001, "EmpresaODepto", "C7001", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7002, "Cliente", "C7002", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7003, "Nombre", "C7003", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7004, "Direccion", "C7004", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7005, "Telefono", "C7005", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7006, "Fax", "C7006", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7007, "Email", "C7007", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7008, "NombreContacto", "C7008", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7009, "ApellidoContacto", "C7009", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7010, "Proveedor", "C7010", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7011, "NivelSeguridad", "C7011", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7012, "FechaEstatus", "C7012", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7013, "Estatus", "C7013", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7014, "OrigenLotes", "C7014", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7015, "RegistroFiscal", "C7015", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7016, "NumPatronal", "C7016", Types.NUMERIC)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7017, "RequiereCompro", "C7017", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7018, "EsConsumidorFinal", "C7018", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7019, "GiroEmpresa", "C7019", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7020, "Ciudad", "C7020", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7021, "Cargo", "C7021", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7022, "LimiteXArchivo", "C7022", Types.NUMERIC)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7023, "LimiteXLote", "C7023", Types.NUMERIC)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7024, "LimiteXTransaccion", "C7024", Types.NUMERIC)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15001, "id_formato", "C15001", Types.BIGINT)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15002, "nombre_formato", "C15002", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15003, "tipo_formato", "C15003", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15004, "tipo_separador", "C15004", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15005, "caracter_separador", "C15005", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15006, "lote_destino", "C15006", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15007, "usuario_creacion", "C15007", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15008, "fecha_creacion", "C15008", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15009, "usuario_ult_act", "C15009", Types.VARCHAR)
     ,  new SQLColumnaMeta(15, "HTH_Formato", "E15", 15010, "fecha_ult_act", "C15010", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16001, "id_formato", "C16001", Types.BIGINT)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16002, "nombre_formato", "C16002", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16003, "tipo_formato", "C16003", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16004, "tipo_separador", "C16004", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16005, "caracter_separador", "C16005", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16006, "lote_destino", "C16006", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16007, "usuario_creacion", "C16007", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16008, "fecha_creacion", "C16008", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16009, "usuario_ult_act", "C16009", Types.VARCHAR)
     ,  new SQLColumnaMeta(16, "HTH_Formato", "E16", 16010, "fecha_ult_act", "C16010", Types.TIMESTAMP)
	});
	
    
	public BsCliente getBsCliente(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO{
		return getBsCliente(hthProcesoMonitoreo, null);
	}

    public BsCliente getBsCliente(HthProcesoMonitoreo hthProcesoMonitoreo,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsCliente bsClienteRel = hthProcesoMonitoreo.getBsCliente();
	     if(bsClienteRel==null){
                PeticionDeDatos peticionDeDatosParaBsCliente = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsCliente.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsClienteDao.CLIENTE, 
	     	                              hthProcesoMonitoreo.getCliente()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsClienteRel = (BsCliente)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsCliente, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsClienteDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthProcesoMonitoreo.setBsCliente(bsClienteRel);										
		}
										
		return 	bsClienteRel;									
    }
    
    
	public HthVistaConvenio getHthVistaConvenio(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO{
		return getHthVistaConvenio(hthProcesoMonitoreo, null);
	}

    public HthVistaConvenio getHthVistaConvenio(HthProcesoMonitoreo hthProcesoMonitoreo,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     HthVistaConvenio hthVistaConvenioRel = hthProcesoMonitoreo.getHthVistaConvenio();
	     if(hthVistaConvenioRel==null){
                PeticionDeDatos peticionDeDatosParaHthVistaConvenio = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaHthVistaConvenio.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              HthVistaConvenioDao.IDVISTACONVENIO, 
	     	                              hthProcesoMonitoreo.getIdVistaConvenio()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		hthVistaConvenioRel = (HthVistaConvenio)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaHthVistaConvenio, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										HthVistaConvenioDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthProcesoMonitoreo.setHthVistaConvenio(hthVistaConvenioRel);										
		}
										
		return 	hthVistaConvenioRel;									
    }
    
    
	public BsEmpresaodepto getBsEmpresaodepto(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO{
		return getBsEmpresaodepto(hthProcesoMonitoreo, null);
	}

    public BsEmpresaodepto getBsEmpresaodepto(HthProcesoMonitoreo hthProcesoMonitoreo,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsEmpresaodepto bsEmpresaodeptoRel = hthProcesoMonitoreo.getBsEmpresaodepto();
	     if(bsEmpresaodeptoRel==null){
                PeticionDeDatos peticionDeDatosParaBsEmpresaodepto = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsEmpresaodepto.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsEmpresaodeptoDao.EMPRESAODEPTO, 
	     	                              hthProcesoMonitoreo.getEmpresaodepto()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsEmpresaodeptoRel = (BsEmpresaodepto)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsEmpresaodepto, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsEmpresaodeptoDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthProcesoMonitoreo.setBsEmpresaodepto(bsEmpresaodeptoRel);										
		}
										
		return 	bsEmpresaodeptoRel;									
    }
    
    
	public HthFormatoEncabezado getHthFormatoEncabezado(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO{
		return getHthFormatoEncabezado(hthProcesoMonitoreo, null);
	}

    public HthFormatoEncabezado getHthFormatoEncabezado(HthProcesoMonitoreo hthProcesoMonitoreo,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     HthFormatoEncabezado hthFormatoEncabezadoRel = hthProcesoMonitoreo.getHthFormatoEncabezado();
	     if(hthFormatoEncabezadoRel==null){
                PeticionDeDatos peticionDeDatosParaHthFormatoEncabezado = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaHthFormatoEncabezado.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              HthFormatoEncabezadoDao.IDFORMATO, 
	     	                              hthProcesoMonitoreo.getIdFormatoEncabezado()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		hthFormatoEncabezadoRel = (HthFormatoEncabezado)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaHthFormatoEncabezado, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										HthFormatoEncabezadoDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthProcesoMonitoreo.setHthFormatoEncabezado(hthFormatoEncabezadoRel);										
		}
										
		return 	hthFormatoEncabezadoRel;									
    }
    
    
	public HthFormato getHthFormato(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO{
		return getHthFormato(hthProcesoMonitoreo, null);
	}

    public HthFormato getHthFormato(HthProcesoMonitoreo hthProcesoMonitoreo,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     HthFormato hthFormatoRel = hthProcesoMonitoreo.getHthFormato();
	     if(hthFormatoRel==null){
                PeticionDeDatos peticionDeDatosParaHthFormato = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaHthFormato.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              HthFormatoDao.IDFORMATO, 
	     	                              hthProcesoMonitoreo.getIdFormatoDetalle()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		hthFormatoRel = (HthFormato)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaHthFormato, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										HthFormatoDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				hthProcesoMonitoreo.setHthFormato(hthFormatoRel);										
		}
										
		return 	hthFormatoRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E19.id_proceso_monitoreo AS C19001 , E19.Cliente AS C19021 , E19.Id_Vista_Convenio AS C19022 , E19.Convenio AS C19002 , " +
		"       E19.TipoConvenio AS C19003 , E19.EmpresaODepto AS C19023 , E19.convenio_PE_ACH AS C19004 , E19.convenio_nombre AS C19005 , " +
		"       E19.nombre_archivo AS C19006 , E19.ruta_sftp AS C19007 , E19.ruta_archivos_in AS C19008 , E19.ruta_archivos_out AS C19009 , " +
		"       E19.ruta_procesados AS C19010 , E19.tiempo_monitoreo AS C19011 , E19.linea_encabezado AS C19012 , E19.id_formato_encabezado AS C19024 , " +
		"       E19.procesamiento_superusuario AS C19013 , E19.id_formato_detalle AS C19025 , E19.monto_maximo_procesar AS C19014 , E19.estado AS C19015 , " +
		"       E19.usuario_creacion AS C19016 , E19.fecha_creacion AS C19017 , E19.usuario_ult_act AS C19018 , E19.fecha_ult_act AS C19019 , " +
		"       E19.email_cliente AS C19020 , E19.pm_host_sftp AS C19026 , E19.pm_puerto_sftp AS C19027 , E19.pm_usuario_sftp AS C19028 , " +
		" 		E19.pm_password_sftp AS C19029 , E19.pm_llave_sftp AS C19030 ," +
		"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
		"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
		"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
		"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
		"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
		"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
		"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
		"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
		"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
		"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39 , " +
		"       E20.Id_Vista_Convenio AS C20001 , E20.TipoConvenio AS C20002 , E20.Convenio AS C20003 , E20.Cliente AS C20004 , " +
		"       E20.Nombre AS C20005 , E20.Cuenta AS C20006 , E20.NombreCta AS C20007 , E20.TipoMoneda AS C20008 , " +
		"       E20.Descripcion AS C20009 , " +
		"       E7.EmpresaODepto AS C7001 , E7.Cliente AS C7002 , E7.Nombre AS C7003 , E7.Direccion AS C7004 , " +
		"       E7.Telefono AS C7005 , E7.Fax AS C7006 , E7.Email AS C7007 , E7.NombreContacto AS C7008 , " +
		"       E7.ApellidoContacto AS C7009 , E7.Proveedor AS C7010 , E7.NivelSeguridad AS C7011 , E7.FechaEstatus AS C7012 , " +
		"       E7.Estatus AS C7013 , E7.OrigenLotes AS C7014 , E7.RegistroFiscal AS C7015 , E7.NumPatronal AS C7016 , " +
		"       E7.RequiereCompro AS C7017 , E7.EsConsumidorFinal AS C7018 , E7.GiroEmpresa AS C7019 , E7.Ciudad AS C7020 , " +
		"       E7.Cargo AS C7021 , E7.LimiteXArchivo AS C7022 , E7.LimiteXLote AS C7023 , E7.LimiteXTransaccion AS C7024 , " +
		"       E15.id_formato AS C15001 , E15.nombre_formato AS C15002 , E15.tipo_formato AS C15003 , E15.tipo_separador AS C15004 , " +
		"       E15.caracter_separador AS C15005 , E15.lote_destino AS C15006 , E15.usuario_creacion AS C15007 , E15.fecha_creacion AS C15008 , " +
		"       E15.usuario_ult_act AS C15009 , E15.fecha_ult_act AS C15010 , " +
		"       E16.id_formato AS C16001 , E16.nombre_formato AS C16002 , E16.tipo_formato AS C16003 , E16.tipo_separador AS C16004 , " +
		"       E16.caracter_separador AS C16005 , E16.lote_destino AS C16006 , E16.usuario_creacion AS C16007 , E16.fecha_creacion AS C16008 , " +
		"       E16.usuario_ult_act AS C16009 , E16.fecha_ult_act AS C16010" +
		"  FROM HTH_Proceso_Monitoreo E19  "
		+ "LEFT OUTER JOIN BS_Cliente E0 ON E0.Cliente = E19.Cliente "
		+ "LEFT OUTER JOIN HTH_Vista_Convenio E20 ON E20.Id_Vista_Convenio = E19.Id_Vista_Convenio " 
		+ "LEFT OUTER JOIN BS_EmpresaODepto E7 ON E7.EmpresaODepto = E19.EmpresaODepto "
		+ "LEFT OUTER JOIN HTH_Formato E15 ON E15.id_formato = E19.id_formato_encabezado "
		+ "LEFT OUTER JOIN HTH_Formato E16 ON E16.id_formato = E19.id_formato_detalle " +
		" WHERE E19.id_proceso_monitoreo = E19.id_proceso_monitoreo ";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E19.id_proceso_monitoreo AS C19001 , E19.Cliente AS C19021 , E19.Id_Vista_Convenio AS C19022 , E19.Convenio AS C19002 , " +
		"       E19.TipoConvenio AS C19003 , E19.EmpresaODepto AS C19023 , E19.convenio_PE_ACH AS C19004 , E19.convenio_nombre AS C19005 , " +
		"       E19.nombre_archivo AS C19006 , E19.ruta_sftp AS C19007 , E19.ruta_archivos_in AS C19008 , E19.ruta_archivos_out AS C19009 , " +
		"       E19.ruta_procesados AS C19010 , E19.tiempo_monitoreo AS C19011 , E19.linea_encabezado AS C19012 , E19.id_formato_encabezado AS C19024 , " +
		"       E19.procesamiento_superusuario AS C19013 , E19.id_formato_detalle AS C19025 , E19.monto_maximo_procesar AS C19014 , E19.estado AS C19015 , " +
		"       E19.usuario_creacion AS C19016 , E19.fecha_creacion AS C19017 , E19.usuario_ult_act AS C19018 , E19.fecha_ult_act AS C19019 , " +
		"       E19.email_cliente AS C19020 , E19.pm_host_sftp AS C19026 , E19.pm_puerto_sftp AS C19027 , E19.pm_usuario_sftp AS C19028 , " + 
		" 		E19.pm_password_sftp AS C19029 , E19.pm_llave_sftp AS C19030 ,"  +
		"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
		"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
		"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
		"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
		"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
		"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
		"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
		"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
		"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
		"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39 , " +
		"       E20.Id_Vista_Convenio AS C20001 , E20.TipoConvenio AS C20002 , E20.Convenio AS C20003 , E20.Cliente AS C20004 , " +
		"       E20.Nombre AS C20005 , E20.Cuenta AS C20006 , E20.NombreCta AS C20007 , E20.TipoMoneda AS C20008 , " +
		"       E20.Descripcion AS C20009 , " +
		"       E7.EmpresaODepto AS C7001 , E7.Cliente AS C7002 , E7.Nombre AS C7003 , E7.Direccion AS C7004 , " +
		"       E7.Telefono AS C7005 , E7.Fax AS C7006 , E7.Email AS C7007 , E7.NombreContacto AS C7008 , " +
		"       E7.ApellidoContacto AS C7009 , E7.Proveedor AS C7010 , E7.NivelSeguridad AS C7011 , E7.FechaEstatus AS C7012 , " +
		"       E7.Estatus AS C7013 , E7.OrigenLotes AS C7014 , E7.RegistroFiscal AS C7015 , E7.NumPatronal AS C7016 , " +
		"       E7.RequiereCompro AS C7017 , E7.EsConsumidorFinal AS C7018 , E7.GiroEmpresa AS C7019 , E7.Ciudad AS C7020 , " +
		"       E7.Cargo AS C7021 , E7.LimiteXArchivo AS C7022 , E7.LimiteXLote AS C7023 , E7.LimiteXTransaccion AS C7024 , " +
		"       E15.id_formato AS C15001 , E15.nombre_formato AS C15002 , E15.tipo_formato AS C15003 , E15.tipo_separador AS C15004 , " +
		"       E15.caracter_separador AS C15005 , E15.lote_destino AS C15006 , E15.usuario_creacion AS C15007 , E15.fecha_creacion AS C15008 , " +
		"       E15.usuario_ult_act AS C15009 , E15.fecha_ult_act AS C15010 , " +
		"       E16.id_formato AS C16001 , E16.nombre_formato AS C16002 , E16.tipo_formato AS C16003 , E16.tipo_separador AS C16004 , " +
		"       E16.caracter_separador AS C16005 , E16.lote_destino AS C16006 , E16.usuario_creacion AS C16007 , E16.fecha_creacion AS C16008 , " +
		"       E16.usuario_ult_act AS C16009 , E16.fecha_ult_act AS C16010" +
		"  FROM HTH_Proceso_Monitoreo E19  "
		+ "LEFT OUTER JOIN BS_Cliente E0 ON E0.Cliente = E19.Cliente "
		+ "LEFT OUTER JOIN HTH_Vista_Convenio E20 ON E20.Id_Vista_Convenio = E19.Id_Vista_Convenio " 
		+ "LEFT OUTER JOIN BS_EmpresaODepto E7 ON E7.EmpresaODepto = E19.EmpresaODepto "
		+ "LEFT OUTER JOIN HTH_Formato E15 ON E15.id_formato = E19.id_formato_encabezado "
		+ "LEFT OUTER JOIN HTH_Formato E16 ON E16.id_formato = E19.id_formato_detalle " +
		" WHERE E19.id_proceso_monitoreo = E19.id_proceso_monitoreo " +
		"   AND E19.id_proceso_monitoreo= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E19.id_proceso_monitoreo AS C19001 , E19.Cliente AS C19021 , E19.Id_Vista_Convenio AS C19022 , E19.Convenio AS C19002 , " +
		"       E19.TipoConvenio AS C19003 , E19.EmpresaODepto AS C19023 , E19.convenio_PE_ACH AS C19004 , E19.convenio_nombre AS C19005 , " +
		"       E19.nombre_archivo AS C19006 , E19.ruta_sftp AS C19007 , E19.ruta_archivos_in AS C19008 , E19.ruta_archivos_out AS C19009 , " +
		"       E19.ruta_procesados AS C19010 , E19.tiempo_monitoreo AS C19011 , E19.linea_encabezado AS C19012 , E19.id_formato_encabezado AS C19024 , " +
		"       E19.procesamiento_superusuario AS C19013 , E19.id_formato_detalle AS C19025 , E19.monto_maximo_procesar AS C19014 , E19.estado AS C19015 , " +
		"       E19.usuario_creacion AS C19016 , E19.fecha_creacion AS C19017 , E19.usuario_ult_act AS C19018 , E19.fecha_ult_act AS C19019 , " +
		"       E19.email_cliente AS C19020 , E19.pm_host_sftp AS C19026 , E19.pm_puerto_sftp AS C19027 , E19.pm_usuario_sftp AS C19028 , " + 
		" 		E19.pm_password_sftp AS C19029 , E19.pm_llave_sftp AS C19030"  +
		"  FROM HTH_Proceso_Monitoreo E19";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM HTH_Proceso_Monitoreo";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO HTH_Proceso_Monitoreo (" +
		"Cliente,Id_Vista_Convenio,Convenio,TipoConvenio,EmpresaODepto" +
		",convenio_PE_ACH,convenio_nombre,nombre_archivo,ruta_sftp" +
		",ruta_archivos_in,ruta_archivos_out,ruta_procesados,tiempo_monitoreo" +
		",linea_encabezado,id_formato_encabezado,procesamiento_superusuario,id_formato_detalle" +
		",monto_maximo_procesar,estado,usuario_creacion,fecha_creacion" +
		",usuario_ult_act,fecha_ult_act,email_cliente,pm_host_sftp,pm_puerto_sftp,pm_usuario_sftp,pm_password_sftp,pm_llave_sftp" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO HTH_Proceso_Monitoreo (" +
		"id_proceso_monitoreo,Cliente,Id_Vista_Convenio,Convenio,TipoConvenio" +
		",EmpresaODepto,convenio_PE_ACH,convenio_nombre,nombre_archivo" +
		",ruta_sftp,ruta_archivos_in,ruta_archivos_out,ruta_procesados" +
		",tiempo_monitoreo,linea_encabezado,id_formato_encabezado,procesamiento_superusuario" +
		",id_formato_detalle,monto_maximo_procesar,estado,usuario_creacion" +
		",fecha_creacion,usuario_ult_act,fecha_ult_act,email_cliente" +
		",pm_host_sftp,pm_puerto_sftp,pm_usuario_sftp,pm_password_sftp,pm_llave_sftp" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE HTH_Proceso_Monitoreo" +
		" SET Cliente= ?  , Id_Vista_Convenio= ?  , Convenio= ?  , TipoConvenio= ?  , " +
		"EmpresaODepto= ?  , convenio_PE_ACH= ?  , convenio_nombre= ?  , nombre_archivo= ?  , " +
		"ruta_sftp= ?  , ruta_archivos_in= ?  , ruta_archivos_out= ?  , ruta_procesados= ?  , " +
		"tiempo_monitoreo= ?  , linea_encabezado= ?  , id_formato_encabezado= ?  , procesamiento_superusuario= ?  , " +
		"id_formato_detalle= ?  , monto_maximo_procesar= ?  , estado= ?  , usuario_creacion= ?  , " +
		"fecha_creacion= ?  , usuario_ult_act= ?  , fecha_ult_act= ?  , email_cliente= ?  ," +
		"pm_host_sftp = ? , pm_puerto_sftp = ?, pm_usuario_sftp = ?, pm_password_sftp = ?, pm_llave_sftp = ?" +
		" WHERE id_proceso_monitoreo= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM HTH_Proceso_Monitoreo" +
		" WHERE id_proceso_monitoreo= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E19.id_proceso_monitoreo AS C19001 , E19.Cliente AS C19021 , E19.Id_Vista_Convenio AS C19022 , E19.Convenio AS C19002 , " +
		"       E19.TipoConvenio AS C19003 , E19.EmpresaODepto AS C19023 , E19.convenio_PE_ACH AS C19004 , E19.convenio_nombre AS C19005 , " +
		"       E19.nombre_archivo AS C19006 , E19.ruta_sftp AS C19007 , E19.ruta_archivos_in AS C19008 , E19.ruta_archivos_out AS C19009 , " +
		"       E19.ruta_procesados AS C19010 , E19.tiempo_monitoreo AS C19011 , E19.linea_encabezado AS C19012 , E19.id_formato_encabezado AS C19024 , " +
		"       E19.procesamiento_superusuario AS C19013 , E19.id_formato_detalle AS C19025 , E19.monto_maximo_procesar AS C19014 , E19.estado AS C19015 , " +
		"       E19.usuario_creacion AS C19016 , E19.fecha_creacion AS C19017 , E19.usuario_ult_act AS C19018 , E19.fecha_ult_act AS C19019 , " +
		"       E19.email_cliente AS C19020 , E19.pm_host_sftp AS C19026 , E19.pm_puerto_sftp AS C19027 , E19.pm_usuario_sftp AS C19028 , " + 
		"		E19.pm_password_sftp AS C19029 , E19.pm_llave_sftp AS C19030"  +
		"  FROM HTH_Proceso_Monitoreo E19" +
		" WHERE E19.id_proceso_monitoreo= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE HTH_Proceso_Monitoreo (" +
		"    id_proceso_monitoreo LONG NOT NULL," +
		"    Cliente LONG NOT NULL," +
		"    Id_Vista_Convenio LONG," +
		"    Convenio LONG," +
		"    TipoConvenio LONG," +
		"    EmpresaODepto LONG NOT NULL," +
		"    convenio_PE_ACH VARCHAR(5)  NOT NULL," +
		"    convenio_nombre VARCHAR(25)  NOT NULL," +
		"    nombre_archivo VARCHAR(100)  NOT NULL," +
		"    ruta_sftp VARCHAR(200)  NOT NULL," +
		"    ruta_archivos_in VARCHAR(100)  NOT NULL," +
		"    ruta_archivos_out VARCHAR(100)  NOT NULL," +
		"    ruta_procesados VARCHAR(100)  NOT NULL," +
		"    tiempo_monitoreo LONG NOT NULL," +
		"    linea_encabezado VARCHAR(1)  NOT NULL," +
		"    id_formato_encabezado LONG," +
		"    procesamiento_superusuario VARCHAR(1)  NOT NULL," +
		"    id_formato_detalle LONG NOT NULL," +
		"    monto_maximo_procesar NUMERIC(19, 0) ," +
		"    estado VARCHAR(1)  NOT NULL," +
		"    usuario_creacion VARCHAR(100)  NOT NULL," +
		"    fecha_creacion TIMESTAMP NOT NULL," +
		"    usuario_ult_act VARCHAR(100)  NOT NULL," +
		"    fecha_ult_act TIMESTAMP NOT NULL," +
		"    email_cliente VARCHAR(250)  NOT NULL," +
		"    pm_host_sftp VARCHAR(100)," +
	    "    pm_puerto_sftp VARCHAR(100)," +
	    "    pm_usuario_sftp VARCHAR(100)," +
	    "    pm_password_sftp VARCHAR(100)," +
	    "    pm_llave_sftp VARCHAR(200)" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    HthProcesoMonitoreo theDto = new HthProcesoMonitoreo();
     	theDto.setIdProcesoMonitoreo(SQLUtils.getLongFromResultSet(rst, "C19001"));
     	theDto.setCliente(SQLUtils.getLongFromResultSet(rst, "C19021"));
     	theDto.setIdVistaConvenio(SQLUtils.getLongFromResultSet(rst, "C19022"));
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C19002"));
     	theDto.setTipoconvenio(SQLUtils.getLongFromResultSet(rst, "C19003"));
     	theDto.setEmpresaodepto(SQLUtils.getLongFromResultSet(rst, "C19023"));
		theDto.setConvenioPEACH(rst.getString("C19004"));
		theDto.setConvenioNombre(rst.getString("C19005"));
		theDto.setNombreArchivo(rst.getString("C19006"));
		theDto.setRutaSftp(rst.getString("C19007"));
		theDto.setRutaArchivosIn(rst.getString("C19008"));
		theDto.setRutaArchivosOut(rst.getString("C19009"));
		theDto.setRutaProcesados(rst.getString("C19010"));
     	theDto.setTiempoMonitoreo(SQLUtils.getLongFromResultSet(rst, "C19011"));
		theDto.setLineaEncabezado(rst.getString("C19012"));
     	theDto.setIdFormatoEncabezado(SQLUtils.getLongFromResultSet(rst, "C19024"));
		theDto.setProcesamientoSuperusuario(rst.getString("C19013"));
     	theDto.setIdFormatoDetalle(SQLUtils.getLongFromResultSet(rst, "C19025"));
		theDto.setMontoMaximoProcesar(rst.getBigDecimal("C19014"));
		theDto.setEstado(rst.getString("C19015"));
		theDto.setUsuarioCreacion(rst.getString("C19016"));
		theDto.setFechaCreacion(rst.getTimestamp("C19017"));
		theDto.setUsuarioUltAct(rst.getString("C19018"));
		theDto.setFechaUltAct(rst.getTimestamp("C19019"));
		theDto.setEmailCliente(rst.getString("C19020"));
		theDto.setPmHostSftp(rst.getString("C19026"));
		theDto.setPmPuertoSftp(rst.getString("C19027"));
		theDto.setPmUsuarioSftp(rst.getString("C19028"));
		theDto.setPmPasswordSftp(rst.getString("C19029"));
		theDto.setPmLlaveSftp(rst.getString("C19030"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static HthProcesoMonitoreo crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		HthProcesoMonitoreo hthProcesoMonitoreo = (HthProcesoMonitoreo) crearDtoST(rst);
        BsCliente bsClienteRel  = (BsCliente) BsClienteDaoJdbcSql.crearDtoST(rst);
        hthProcesoMonitoreo.setBsCliente(bsClienteRel);
        HthVistaConvenio hthVistaConvenioRel  = (HthVistaConvenio) HthVistaConvenioDaoJdbcSql.crearDtoST(rst);
        hthProcesoMonitoreo.setHthVistaConvenio(hthVistaConvenioRel);
        BsEmpresaodepto bsEmpresaodeptoRel  = (BsEmpresaodepto) BsEmpresaodeptoDaoJdbcSql.crearDtoST(rst);
        hthProcesoMonitoreo.setBsEmpresaodepto(bsEmpresaodeptoRel);
        HthFormatoEncabezado hthFormatoEncabezadoRel  = (HthFormatoEncabezado) HthFormatoEncabezadoDaoJdbcSql.crearDtoST(rst);
        hthProcesoMonitoreo.setHthFormatoEncabezado(hthFormatoEncabezadoRel);
        HthFormato hthFormatoRel  = (HthFormato) HthFormatoDaoJdbcSql.crearDtoST(rst);
        hthProcesoMonitoreo.setHthFormato(hthFormatoRel);
		return hthProcesoMonitoreo;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static HthProcesoMonitoreo crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		HthProcesoMonitoreo hthProcesoMonitoreo = (HthProcesoMonitoreo) crearDtoST(rst);
        BsCliente clienteRelbsCliente  = (BsCliente) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthProcesoMonitoreo.getCliente(), conn, 
				BsClienteDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthProcesoMonitoreo.setBsCliente(clienteRelbsCliente);
        HthVistaConvenio idVistaConvenioRelhthVistaConvenio  = (HthVistaConvenio) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthProcesoMonitoreo.getIdVistaConvenio(), conn, 
				HthVistaConvenioDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthProcesoMonitoreo.setHthVistaConvenio(idVistaConvenioRelhthVistaConvenio);
        BsEmpresaodepto empresaodeptoRelbsEmpresaodepto  = (BsEmpresaodepto) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthProcesoMonitoreo.getEmpresaodepto(), conn, 
				BsEmpresaodeptoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthProcesoMonitoreo.setBsEmpresaodepto(empresaodeptoRelbsEmpresaodepto);
        HthFormatoEncabezado idFormatoEncabezadoRelhthFormatoEncabezado  = (HthFormatoEncabezado) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthProcesoMonitoreo.getIdFormatoEncabezado(), conn, 
				HthFormatoEncabezadoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthProcesoMonitoreo.setHthFormatoEncabezado(idFormatoEncabezadoRelhthFormatoEncabezado);
        HthFormato idFormatoDetalleRelhthFormato  = (HthFormato) SQLImplementacionOperacionesComunes.buscarPorIDST(
                hthProcesoMonitoreo.getIdFormatoDetalle(), conn, 
				HthFormatoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        hthProcesoMonitoreo.setHthFormato(idFormatoDetalleRelhthFormato);
		return hthProcesoMonitoreo;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthProcesoMonitoreo theDto = (HthProcesoMonitoreo)theDtoUntyped;
				Long cliente = theDto.getCliente();     
		Long idVistaConvenio = theDto.getIdVistaConvenio();     
		Long convenio = theDto.getConvenio();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		String convenioPEACH = theDto.getConvenioPEACH();     
		String convenioNombre = theDto.getConvenioNombre();     
		String nombreArchivo = theDto.getNombreArchivo();     
		String rutaSftp = theDto.getRutaSftp();     
		String rutaArchivosIn = theDto.getRutaArchivosIn();     
		String rutaArchivosOut = theDto.getRutaArchivosOut();     
		String rutaProcesados = theDto.getRutaProcesados();     
		Long tiempoMonitoreo = theDto.getTiempoMonitoreo();     
		String lineaEncabezado = theDto.getLineaEncabezado();     
		Long idFormatoEncabezado = theDto.getIdFormatoEncabezado();     
		String procesamientoSuperusuario = theDto.getProcesamientoSuperusuario();     
		Long idFormatoDetalle = theDto.getIdFormatoDetalle();     
		java.math.BigDecimal montoMaximoProcesar = theDto.getMontoMaximoProcesar();     
		String estado = theDto.getEstado();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		String emailCliente = theDto.getEmailCliente();  
		String pmUsuarioSftp = theDto.getPmUsuarioSftp();
		String pmHostSftp = theDto.getPmHostSftp();
		String pmPuertoSftp = theDto.getPmPuertoSftp();
		String pmPassswordSftp = theDto.getPmPasswordSftp();
		String pmLlaveSftp = theDto.getPmLlaveSftp();
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
    		     
		SQLUtils.setIntoStatement( 2, idVistaConvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 3, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 4, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 5, empresaodepto, smt);  
    		     
		SQLUtils.setIntoStatement( 6, convenioPEACH, smt);  
    		     
		SQLUtils.setIntoStatement( 7, convenioNombre, smt);  
    		     
		SQLUtils.setIntoStatement( 8, nombreArchivo, smt);  
    		     
		SQLUtils.setIntoStatement( 9, rutaSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 10, rutaArchivosIn, smt);  
    		     
		SQLUtils.setIntoStatement( 11, rutaArchivosOut, smt);  
    		     
		SQLUtils.setIntoStatement( 12, rutaProcesados, smt);  
    		     
		SQLUtils.setIntoStatement( 13, tiempoMonitoreo, smt);  
    		     
		SQLUtils.setIntoStatement( 14, lineaEncabezado, smt);  
    		     
		SQLUtils.setIntoStatement( 15, idFormatoEncabezado, smt);  
    		     
		SQLUtils.setIntoStatement( 16, procesamientoSuperusuario, smt);  
    		     
		SQLUtils.setIntoStatement( 17, idFormatoDetalle, smt);  
    		     
		SQLUtils.setIntoStatement( 18, montoMaximoProcesar, smt);  
    		     
		SQLUtils.setIntoStatement( 19, estado, smt);  
    		     
		SQLUtils.setIntoStatement( 20, usuarioCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 21, fechaCreacion, smt);  
    		     
		SQLUtils.setIntoStatement( 22, usuarioUltAct, smt);  
    		     
		SQLUtils.setIntoStatement( 23, fechaUltAct, smt);  
    		     
		SQLUtils.setIntoStatement( 24, emailCliente, smt);  
		
		SQLUtils.setIntoStatement( 25, pmHostSftp, smt);  
	     
		SQLUtils.setIntoStatement( 26, pmPuertoSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 27, pmUsuarioSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 28, pmPassswordSftp, smt);  
		
		SQLUtils.setIntoStatement( 29, pmLlaveSftp, smt);  

    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        HthProcesoMonitoreo theDto = (HthProcesoMonitoreo)theDtoUntyped;
				Long idProcesoMonitoreo = theDto.getIdProcesoMonitoreo();     
		Long cliente = theDto.getCliente();     
		Long idVistaConvenio = theDto.getIdVistaConvenio();     
		Long convenio = theDto.getConvenio();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		String convenioPEACH = theDto.getConvenioPEACH();     
		String convenioNombre = theDto.getConvenioNombre();     
		String nombreArchivo = theDto.getNombreArchivo();     
		String rutaSftp = theDto.getRutaSftp();     
		String rutaArchivosIn = theDto.getRutaArchivosIn();     
		String rutaArchivosOut = theDto.getRutaArchivosOut();     
		String rutaProcesados = theDto.getRutaProcesados();     
		Long tiempoMonitoreo = theDto.getTiempoMonitoreo();     
		String lineaEncabezado = theDto.getLineaEncabezado();     
		Long idFormatoEncabezado = theDto.getIdFormatoEncabezado();     
		String procesamientoSuperusuario = theDto.getProcesamientoSuperusuario();     
		Long idFormatoDetalle = theDto.getIdFormatoDetalle();     
		java.math.BigDecimal montoMaximoProcesar = theDto.getMontoMaximoProcesar();     
		String estado = theDto.getEstado();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		String emailCliente = theDto.getEmailCliente();  
		String pmUsuarioSftp = theDto.getPmUsuarioSftp();
		String pmHostSftp = theDto.getPmHostSftp();
		String pmPuertoSftp = theDto.getPmPuertoSftp();
		String pmPassswordSftp = theDto.getPmPasswordSftp();
		String pmLlaveSftp = theDto.getPmLlaveSftp();
	
		SQLUtils.setIntoStatement( 1, idProcesoMonitoreo, smt);
    	
		SQLUtils.setIntoStatement( 2, cliente, smt);
    	
		SQLUtils.setIntoStatement( 3, idVistaConvenio, smt);
    	
		SQLUtils.setIntoStatement( 4, convenio, smt);
    	
		SQLUtils.setIntoStatement( 5, tipoconvenio, smt);
    	
		SQLUtils.setIntoStatement( 6, empresaodepto, smt);
    	
		SQLUtils.setIntoStatement( 7, convenioPEACH, smt);
    	
		SQLUtils.setIntoStatement( 8, convenioNombre, smt);
    	
		SQLUtils.setIntoStatement( 9, nombreArchivo, smt);
    	
		SQLUtils.setIntoStatement( 10, rutaSftp, smt);
    	
		SQLUtils.setIntoStatement( 11, rutaArchivosIn, smt);
    	
		SQLUtils.setIntoStatement( 12, rutaArchivosOut, smt);
    	
		SQLUtils.setIntoStatement( 13, rutaProcesados, smt);
    	
		SQLUtils.setIntoStatement( 14, tiempoMonitoreo, smt);
    	
		SQLUtils.setIntoStatement( 15, lineaEncabezado, smt);
    	
		SQLUtils.setIntoStatement( 16, idFormatoEncabezado, smt);
    	
		SQLUtils.setIntoStatement( 17, procesamientoSuperusuario, smt);
    	
		SQLUtils.setIntoStatement( 18, idFormatoDetalle, smt);
    	
		SQLUtils.setIntoStatement( 19, montoMaximoProcesar, smt);
    	
		SQLUtils.setIntoStatement( 20, estado, smt);
    	
		SQLUtils.setIntoStatement( 21, usuarioCreacion, smt);
    	
		SQLUtils.setIntoStatement( 22, fechaCreacion, smt);
    	
		SQLUtils.setIntoStatement( 23, usuarioUltAct, smt);
    	
		SQLUtils.setIntoStatement( 24, fechaUltAct, smt);
    	
		SQLUtils.setIntoStatement( 25, emailCliente, smt);
		
		SQLUtils.setIntoStatement( 26, pmHostSftp, smt);  
	     
		SQLUtils.setIntoStatement( 27, pmPuertoSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 28, pmUsuarioSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 29, pmPassswordSftp, smt);  
		
		SQLUtils.setIntoStatement( 30, pmLlaveSftp, smt); 
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			HthProcesoMonitoreo theDto = (HthProcesoMonitoreo)theDtoUntyped;
				Long idProcesoMonitoreo = theDto.getIdProcesoMonitoreo();     
		Long cliente = theDto.getCliente();     
		Long idVistaConvenio = theDto.getIdVistaConvenio();     
		Long convenio = theDto.getConvenio();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
		String convenioPEACH = theDto.getConvenioPEACH();     
		String convenioNombre = theDto.getConvenioNombre();     
		String nombreArchivo = theDto.getNombreArchivo();     
		String rutaSftp = theDto.getRutaSftp();     
		String rutaArchivosIn = theDto.getRutaArchivosIn();     
		String rutaArchivosOut = theDto.getRutaArchivosOut();     
		String rutaProcesados = theDto.getRutaProcesados();     
		Long tiempoMonitoreo = theDto.getTiempoMonitoreo();     
		String lineaEncabezado = theDto.getLineaEncabezado();     
		Long idFormatoEncabezado = theDto.getIdFormatoEncabezado();     
		String procesamientoSuperusuario = theDto.getProcesamientoSuperusuario();     
		Long idFormatoDetalle = theDto.getIdFormatoDetalle();     
		java.math.BigDecimal montoMaximoProcesar = theDto.getMontoMaximoProcesar();     
		String estado = theDto.getEstado();     
		String usuarioCreacion = theDto.getUsuarioCreacion();     
		java.util.Date fechaCreacion = theDto.getFechaCreacion();     
		String usuarioUltAct = theDto.getUsuarioUltAct();     
		java.util.Date fechaUltAct = theDto.getFechaUltAct();     
		String emailCliente = theDto.getEmailCliente(); 
		String pmUsuarioSftp = theDto.getPmUsuarioSftp();
		String pmHostSftp = theDto.getPmHostSftp();
		String pmPuertoSftp = theDto.getPmPuertoSftp();
		String pmPassswordSftp = theDto.getPmPasswordSftp();
		String pmLlaveSftp = theDto.getPmLlaveSftp();
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
        	     
		SQLUtils.setIntoStatement( 2, idVistaConvenio, smt);  
        	     
		SQLUtils.setIntoStatement( 3, convenio, smt);  
        	     
		SQLUtils.setIntoStatement( 4, tipoconvenio, smt);  
        	     
		SQLUtils.setIntoStatement( 5, empresaodepto, smt);  
        	     
		SQLUtils.setIntoStatement( 6, convenioPEACH, smt);  
        	     
		SQLUtils.setIntoStatement( 7, convenioNombre, smt);  
        	     
		SQLUtils.setIntoStatement( 8, nombreArchivo, smt);  
        	     
		SQLUtils.setIntoStatement( 9, rutaSftp, smt);  
        	     
		SQLUtils.setIntoStatement( 10, rutaArchivosIn, smt);  
        	     
		SQLUtils.setIntoStatement( 11, rutaArchivosOut, smt);  
        	     
		SQLUtils.setIntoStatement( 12, rutaProcesados, smt);  
        	     
		SQLUtils.setIntoStatement( 13, tiempoMonitoreo, smt);  
        	     
		SQLUtils.setIntoStatement( 14, lineaEncabezado, smt);  
        	     
		SQLUtils.setIntoStatement( 15, idFormatoEncabezado, smt);  
        	     
		SQLUtils.setIntoStatement( 16, procesamientoSuperusuario, smt);  
        	     
		SQLUtils.setIntoStatement( 17, idFormatoDetalle, smt);  
        	     
		SQLUtils.setIntoStatement( 18, montoMaximoProcesar, smt);  
        	     
		SQLUtils.setIntoStatement( 19, estado, smt);  
        	     
		SQLUtils.setIntoStatement( 20, usuarioCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 21, fechaCreacion, smt);  
        	     
		SQLUtils.setIntoStatement( 22, usuarioUltAct, smt);  
        	     
		SQLUtils.setIntoStatement( 23, fechaUltAct, smt);  
        	     
		SQLUtils.setIntoStatement( 24, emailCliente, smt);  
        		     
		SQLUtils.setIntoStatement( 25, pmHostSftp, smt);  
	     
		SQLUtils.setIntoStatement( 26, pmPuertoSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 27, pmUsuarioSftp, smt);  
    		     
		SQLUtils.setIntoStatement( 28, pmPassswordSftp, smt);  
		
		SQLUtils.setIntoStatement( 29, pmLlaveSftp, smt);
		
		SQLUtils.setIntoStatement( 30, idProcesoMonitoreo, smt);

    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>hthProcesoMonitoreo</code> objeto del tipo <code>HthProcesoMonitoreo</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(HthProcesoMonitoreo hthProcesoMonitoreo) throws ExcepcionEnDAO {
	    actualizar(hthProcesoMonitoreo, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>idProcesoMonitoreoId</code> objeto del tipo <code>Long</code>.
	 * @return <code>HthProcesoMonitoreo</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthProcesoMonitoreo buscarPorID(Long idProcesoMonitoreoId)
			throws ExcepcionEnDAO {
		return (HthProcesoMonitoreo)buscarPorID(idProcesoMonitoreoId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>hthProcesoMonitoreo</code> objeto del tipo <code>HthProcesoMonitoreo</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(HthProcesoMonitoreo hthProcesoMonitoreo) throws ExcepcionEnDAO {
	    crear(hthProcesoMonitoreo, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>idProcesoMonitoreoId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long idProcesoMonitoreoId) throws ExcepcionEnDAO {
	    eliminar(idProcesoMonitoreoId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>HthProcesoMonitoreo</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public HthProcesoMonitoreo resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (HthProcesoMonitoreo)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    HthProcesoMonitoreoDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				HthProcesoMonitoreoDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return HthProcesoMonitoreoDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return HthProcesoMonitoreoDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return HthProcesoMonitoreoDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return HthProcesoMonitoreoDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  HthProcesoMonitoreoDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  HthProcesoMonitoreoDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  HthProcesoMonitoreoDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return HthProcesoMonitoreoDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return HthProcesoMonitoreoDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return HthProcesoMonitoreoDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return HthProcesoMonitoreoDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return HthProcesoMonitoreoDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return HthProcesoMonitoreoDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return HthProcesoMonitoreoDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>HthProcesoMonitoreo</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    HthProcesoMonitoreo dto = (HthProcesoMonitoreo)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setIdProcesoMonitoreo(dtoId);

	}	
	
	
}
