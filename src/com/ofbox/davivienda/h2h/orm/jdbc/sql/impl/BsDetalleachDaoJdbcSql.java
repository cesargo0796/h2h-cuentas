package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetalleach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetalleachID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsDetalleachDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Detalle ACH</b>
 * asociado a la tabla <b>BS_DetalleACH</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsDetalleachDaoJdbcSql extends SQLImplementacionBasica implements BsDetalleachDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_DetalleACH", new SQLColumnaMeta[]{
        new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5001, "idDetalle", "C5001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5002, "Instalacion", "C5002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5003, "Lote", "C5003", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5004, "Operacion", "C5004", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5005, "Cuenta", "C5005", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5006, "TipoOperacion", "C5006", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5007, "Monto", "C5007", Types.NUMERIC )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5008, "Adenda", "C5008", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5009, "FechaEstatus", "C5009", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5010, "Estatus", "C5010", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5011, "IDPago", "C5011", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5012, "AutorizacionHost", "C5012", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5013, "Email", "C5013", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5014, "ComisionAplicada", "C5014", Types.NUMERIC )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5015, "NombreCuenta", "C5015", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5016, "Autorizador", "C5016", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5017, "EstatusPayBank", "C5017", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5018, "FechaHoraHOST", "C5018", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5019, "CuentaDebito", "C5019", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5020, "NombreDeCuenta", "C5020", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5021, "CodBanco", "C5021", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5022, "TipoCuenta", "C5022", Types.BIGINT )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5023, "DescEstatusPayBank", "C5023", Types.VARCHAR )
     ,  new SQLColumnaMeta(5, "BS_DetalleACH", "E5", 5024, "EnviarAHost", "C5024", Types.BIGINT )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E5.idDetalle AS C5001 , E5.Instalacion AS C5002 , E5.Lote AS C5003 , E5.Operacion AS C5004 , " +
		"       E5.Cuenta AS C5005 , E5.TipoOperacion AS C5006 , E5.Monto AS C5007 , E5.Adenda AS C5008 , " +
		"       E5.FechaEstatus AS C5009 , E5.Estatus AS C5010 , E5.IDPago AS C5011 , E5.AutorizacionHost AS C5012 , " +
		"       E5.Email AS C5013 , E5.ComisionAplicada AS C5014 , E5.NombreCuenta AS C5015 , E5.Autorizador AS C5016 , " +
		"       E5.EstatusPayBank AS C5017 , E5.FechaHoraHOST AS C5018 , E5.CuentaDebito AS C5019 , E5.NombreDeCuenta AS C5020 , " +
		"       E5.CodBanco AS C5021 , E5.TipoCuenta AS C5022 , E5.DescEstatusPayBank AS C5023 , E5.EnviarAHost AS C5024" +
		"  FROM dbo.BS_DetalleACH E5";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_DetalleACH";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO dbo.BS_DetalleACH (" +
		"Cuenta,TipoOperacion,Monto,Adenda,FechaEstatus" +
		",Estatus,IDPago,AutorizacionHost,Email" +
		",ComisionAplicada,NombreCuenta,Autorizador,EstatusPayBank" +
		",FechaHoraHOST,CuentaDebito,NombreDeCuenta,CodBanco" +
		",TipoCuenta,DescEstatusPayBank,EnviarAHost" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO dbo.BS_DetalleACH (" +
		"idDetalle,Instalacion,Lote,Operacion,Cuenta" +
		",TipoOperacion,Monto,Adenda,FechaEstatus" +
		",Estatus,IDPago,AutorizacionHost,Email" +
		",ComisionAplicada,NombreCuenta,Autorizador,EstatusPayBank" +
		",FechaHoraHOST,CuentaDebito,NombreDeCuenta,CodBanco" +
		",TipoCuenta,DescEstatusPayBank,EnviarAHost" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE dbo.BS_DetalleACH" +
		" SET Cuenta= ?  , TipoOperacion= ?  , Monto= ?  , Adenda= ?  , " +
		"FechaEstatus= ?  , Estatus= ?  , IDPago= ?  , AutorizacionHost= ?  , " +
		"Email= ?  , ComisionAplicada= ?  , NombreCuenta= ?  , Autorizador= ?  , " +
		"EstatusPayBank= ?  , FechaHoraHOST= ?  , CuentaDebito= ?  , NombreDeCuenta= ?  , " +
		"CodBanco= ?  , TipoCuenta= ?  , DescEstatusPayBank= ?  , EnviarAHost= ? " +
		" WHERE idDetalle= ?    AND Instalacion= ?    AND Lote= ?    AND Operacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM dbo.BS_DetalleACH" +
		" WHERE idDetalle= ?    AND Instalacion= ?    AND Lote= ?    AND Operacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E5.idDetalle AS C5001 , E5.Instalacion AS C5002 , E5.Lote AS C5003 , E5.Operacion AS C5004 , " +
		"       E5.Cuenta AS C5005 , E5.TipoOperacion AS C5006 , E5.Monto AS C5007 , E5.Adenda AS C5008 , " +
		"       E5.FechaEstatus AS C5009 , E5.Estatus AS C5010 , E5.IDPago AS C5011 , E5.AutorizacionHost AS C5012 , " +
		"       E5.Email AS C5013 , E5.ComisionAplicada AS C5014 , E5.NombreCuenta AS C5015 , E5.Autorizador AS C5016 , " +
		"       E5.EstatusPayBank AS C5017 , E5.FechaHoraHOST AS C5018 , E5.CuentaDebito AS C5019 , E5.NombreDeCuenta AS C5020 , " +
		"       E5.CodBanco AS C5021 , E5.TipoCuenta AS C5022 , E5.DescEstatusPayBank AS C5023 , E5.EnviarAHost AS C5024" +
		"  FROM dbo.BS_DetalleACH E5" +
		" WHERE E5.idDetalle= ? " +
		"   AND E5.Instalacion= ? " +
		"   AND E5.Lote= ? " +
		"   AND E5.Operacion= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE dbo.BS_DetalleACH (" +
		"    idDetalle LONG NOT NULL," +
		"    Instalacion LONG NOT NULL," +
		"    Lote LONG NOT NULL," +
		"    Operacion LONG NOT NULL," +
		"    Cuenta VARCHAR(16) ," +
		"    TipoOperacion LONG," +
		"    Monto NUMERIC(19, 0)  NOT NULL," +
		"    Adenda VARCHAR(500) ," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    IDPago VARCHAR(50) ," +
		"    AutorizacionHost LONG," +
		"    Email VARCHAR(75) ," +
		"    ComisionAplicada NUMERIC(19, 0)  NOT NULL," +
		"    NombreCuenta VARCHAR(50) ," +
		"    Autorizador VARCHAR(10) ," +
		"    EstatusPayBank VARCHAR(10) ," +
		"    FechaHoraHOST TIMESTAMP," +
		"    CuentaDebito VARCHAR(16) ," +
		"    NombreDeCuenta VARCHAR(50) ," +
		"    CodBanco LONG NOT NULL," +
		"    TipoCuenta LONG NOT NULL," +
		"    DescEstatusPayBank VARCHAR(100) ," +
		"    EnviarAHost LONG" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsDetalleach theDto = new BsDetalleach();
     	theDto.setIddetalle(SQLUtils.getLongFromResultSet(rst, "C5001"));
     	theDto.setInstalacion(SQLUtils.getLongFromResultSet(rst, "C5002"));
     	theDto.setLote(SQLUtils.getLongFromResultSet(rst, "C5003"));
     	theDto.setOperacion(SQLUtils.getLongFromResultSet(rst, "C5004"));
		theDto.setCuenta(rst.getString("C5005"));
     	theDto.setTipooperacion(SQLUtils.getLongFromResultSet(rst, "C5006"));
		theDto.setMonto(rst.getBigDecimal("C5007"));
		theDto.setAdenda(rst.getString("C5008"));
		theDto.setFechaestatus(rst.getTimestamp("C5009"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C5010"));
		theDto.setIdpago(rst.getString("C5011"));
     	theDto.setAutorizacionhost(SQLUtils.getLongFromResultSet(rst, "C5012"));
		theDto.setEmail(rst.getString("C5013"));
		theDto.setComisionaplicada(rst.getBigDecimal("C5014"));
		theDto.setNombrecuenta(rst.getString("C5015"));
		theDto.setAutorizador(rst.getString("C5016"));
		theDto.setEstatuspaybank(rst.getString("C5017"));
		theDto.setFechahorahost(rst.getTimestamp("C5018"));
		theDto.setCuentadebito(rst.getString("C5019"));
		theDto.setNombredecuenta(rst.getString("C5020"));
     	theDto.setCodbanco(SQLUtils.getLongFromResultSet(rst, "C5021"));
     	theDto.setTipocuenta(SQLUtils.getLongFromResultSet(rst, "C5022"));
		theDto.setDescestatuspaybank(rst.getString("C5023"));
     	theDto.setEnviarahost(SQLUtils.getLongFromResultSet(rst, "C5024"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsDetalleach theDto = (BsDetalleach)theDtoUntyped;
				String cuenta = theDto.getCuenta();     
		Long tipooperacion = theDto.getTipooperacion();     
		java.math.BigDecimal monto = theDto.getMonto();     
		String adenda = theDto.getAdenda();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String idpago = theDto.getIdpago();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		String email = theDto.getEmail();     
		java.math.BigDecimal comisionaplicada = theDto.getComisionaplicada();     
		String nombrecuenta = theDto.getNombrecuenta();     
		String autorizador = theDto.getAutorizador();     
		String estatuspaybank = theDto.getEstatuspaybank();     
		java.util.Date fechahorahost = theDto.getFechahorahost();     
		String cuentadebito = theDto.getCuentadebito();     
		String nombredecuenta = theDto.getNombredecuenta();     
		Long codbanco = theDto.getCodbanco();     
		Long tipocuenta = theDto.getTipocuenta();     
		String descestatuspaybank = theDto.getDescestatuspaybank();     
		Long enviarahost = theDto.getEnviarahost();     
		     
		SQLUtils.setIntoStatement( 1, cuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 2, tipooperacion, smt);  
    		     
		SQLUtils.setIntoStatement( 3, monto, smt);  
    		     
		SQLUtils.setIntoStatement( 4, adenda, smt);  
    		     
		SQLUtils.setIntoStatement( 5, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 6, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 7, idpago, smt);  
    		     
		SQLUtils.setIntoStatement( 8, autorizacionhost, smt);  
    		     
		SQLUtils.setIntoStatement( 9, email, smt);  
    		     
		SQLUtils.setIntoStatement( 10, comisionaplicada, smt);  
    		     
		SQLUtils.setIntoStatement( 11, nombrecuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 12, autorizador, smt);  
    		     
		SQLUtils.setIntoStatement( 13, estatuspaybank, smt);  
    		     
		SQLUtils.setIntoStatement( 14, fechahorahost, smt);  
    		     
		SQLUtils.setIntoStatement( 15, cuentadebito, smt);  
    		     
		SQLUtils.setIntoStatement( 16, nombredecuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 17, codbanco, smt);  
    		     
		SQLUtils.setIntoStatement( 18, tipocuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 19, descestatuspaybank, smt);  
    		     
		SQLUtils.setIntoStatement( 20, enviarahost, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsDetalleach theDto = (BsDetalleach)theDtoUntyped;
				Long iddetalle = theDto.getIddetalle();     
		Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long operacion = theDto.getOperacion();     
		String cuenta = theDto.getCuenta();     
		Long tipooperacion = theDto.getTipooperacion();     
		java.math.BigDecimal monto = theDto.getMonto();     
		String adenda = theDto.getAdenda();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String idpago = theDto.getIdpago();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		String email = theDto.getEmail();     
		java.math.BigDecimal comisionaplicada = theDto.getComisionaplicada();     
		String nombrecuenta = theDto.getNombrecuenta();     
		String autorizador = theDto.getAutorizador();     
		String estatuspaybank = theDto.getEstatuspaybank();     
		java.util.Date fechahorahost = theDto.getFechahorahost();     
		String cuentadebito = theDto.getCuentadebito();     
		String nombredecuenta = theDto.getNombredecuenta();     
		Long codbanco = theDto.getCodbanco();     
		Long tipocuenta = theDto.getTipocuenta();     
		String descestatuspaybank = theDto.getDescestatuspaybank();     
		Long enviarahost = theDto.getEnviarahost();     
	
		SQLUtils.setIntoStatement( 1, iddetalle, smt);
    	
		SQLUtils.setIntoStatement( 2, instalacion, smt);
    	
		SQLUtils.setIntoStatement( 3, lote, smt);
    	
		SQLUtils.setIntoStatement( 4, operacion, smt);
    	
		SQLUtils.setIntoStatement( 5, cuenta, smt);
    	
		SQLUtils.setIntoStatement( 6, tipooperacion, smt);
    	
		SQLUtils.setIntoStatement( 7, monto, smt);
    	
		SQLUtils.setIntoStatement( 8, adenda, smt);
    	
		SQLUtils.setIntoStatement( 9, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 10, estatus, smt);
    	
		SQLUtils.setIntoStatement( 11, idpago, smt);
    	
		SQLUtils.setIntoStatement( 12, autorizacionhost, smt);
    	
		SQLUtils.setIntoStatement( 13, email, smt);
    	
		SQLUtils.setIntoStatement( 14, comisionaplicada, smt);
    	
		SQLUtils.setIntoStatement( 15, nombrecuenta, smt);
    	
		SQLUtils.setIntoStatement( 16, autorizador, smt);
    	
		SQLUtils.setIntoStatement( 17, estatuspaybank, smt);
    	
		SQLUtils.setIntoStatement( 18, fechahorahost, smt);
    	
		SQLUtils.setIntoStatement( 19, cuentadebito, smt);
    	
		SQLUtils.setIntoStatement( 20, nombredecuenta, smt);
    	
		SQLUtils.setIntoStatement( 21, codbanco, smt);
    	
		SQLUtils.setIntoStatement( 22, tipocuenta, smt);
    	
		SQLUtils.setIntoStatement( 23, descestatuspaybank, smt);
    	
		SQLUtils.setIntoStatement( 24, enviarahost, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsDetalleachID theId = (BsDetalleachID)theIdUntyped;
			Long iddetalle = theId.getIddetalle();     
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		Long operacion = theId.getOperacion();     
		     
		SQLUtils.setIntoStatement( 1, iddetalle, smt);  
    		     
		SQLUtils.setIntoStatement( 2, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 3, lote, smt);  
    		     
		SQLUtils.setIntoStatement( 4, operacion, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsDetalleachID theId = (BsDetalleachID)theIdUntyped;
				Long iddetalle = theId.getIddetalle();     
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		Long operacion = theId.getOperacion();     
		     
		SQLUtils.setIntoStatement( 1, iddetalle, smt);  
    		     
		SQLUtils.setIntoStatement( 2, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 3, lote, smt);  
    		     
		SQLUtils.setIntoStatement( 4, operacion, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsDetalleach theDto = (BsDetalleach)theDtoUntyped;
				Long iddetalle = theDto.getIddetalle();     
		Long instalacion = theDto.getInstalacion();     
		Long lote = theDto.getLote();     
		Long operacion = theDto.getOperacion();     
		String cuenta = theDto.getCuenta();     
		Long tipooperacion = theDto.getTipooperacion();     
		java.math.BigDecimal monto = theDto.getMonto();     
		String adenda = theDto.getAdenda();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long estatus = theDto.getEstatus();     
		String idpago = theDto.getIdpago();     
		Long autorizacionhost = theDto.getAutorizacionhost();     
		String email = theDto.getEmail();     
		java.math.BigDecimal comisionaplicada = theDto.getComisionaplicada();     
		String nombrecuenta = theDto.getNombrecuenta();     
		String autorizador = theDto.getAutorizador();     
		String estatuspaybank = theDto.getEstatuspaybank();     
		java.util.Date fechahorahost = theDto.getFechahorahost();     
		String cuentadebito = theDto.getCuentadebito();     
		String nombredecuenta = theDto.getNombredecuenta();     
		Long codbanco = theDto.getCodbanco();     
		Long tipocuenta = theDto.getTipocuenta();     
		String descestatuspaybank = theDto.getDescestatuspaybank();     
		Long enviarahost = theDto.getEnviarahost();     
		     
		SQLUtils.setIntoStatement( 1, cuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 2, tipooperacion, smt);  
        	     
		SQLUtils.setIntoStatement( 3, monto, smt);  
        	     
		SQLUtils.setIntoStatement( 4, adenda, smt);  
        	     
		SQLUtils.setIntoStatement( 5, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 6, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 7, idpago, smt);  
        	     
		SQLUtils.setIntoStatement( 8, autorizacionhost, smt);  
        	     
		SQLUtils.setIntoStatement( 9, email, smt);  
        	     
		SQLUtils.setIntoStatement( 10, comisionaplicada, smt);  
        	     
		SQLUtils.setIntoStatement( 11, nombrecuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 12, autorizador, smt);  
        	     
		SQLUtils.setIntoStatement( 13, estatuspaybank, smt);  
        	     
		SQLUtils.setIntoStatement( 14, fechahorahost, smt);  
        	     
		SQLUtils.setIntoStatement( 15, cuentadebito, smt);  
        	     
		SQLUtils.setIntoStatement( 16, nombredecuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 17, codbanco, smt);  
        	     
		SQLUtils.setIntoStatement( 18, tipocuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 19, descestatuspaybank, smt);  
        	     
		SQLUtils.setIntoStatement( 20, enviarahost, smt);  
        		     
		SQLUtils.setIntoStatement( 21, iddetalle, smt);  
    		     
		SQLUtils.setIntoStatement( 22, instalacion, smt);  
    		     
		SQLUtils.setIntoStatement( 23, lote, smt);  
    		     
		SQLUtils.setIntoStatement( 24, operacion, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsDetalleach</code> objeto del tipo <code>BsDetalleach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsDetalleach bsDetalleach) throws ExcepcionEnDAO {
	    actualizar(bsDetalleach, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsDetalleachID</code> objeto del tipo <code>BsDetalleachID</code>.
	 * @return <code>BsDetalleach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsDetalleach buscarPorID(BsDetalleachID bsDetalleachID)
			throws ExcepcionEnDAO {
		return (BsDetalleach)buscarPorID(bsDetalleachID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsDetalleach</code> objeto del tipo <code>BsDetalleach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsDetalleach bsDetalleach) throws ExcepcionEnDAO {
	    crear(bsDetalleach, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsDetalleachID</code> objeto del tipo <code>BsDetalleachID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsDetalleachID bsDetalleachID) throws ExcepcionEnDAO {
	    eliminar(bsDetalleachID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsDetalleach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsDetalleach resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsDetalleach)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsDetalleachDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsDetalleachDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsDetalleachDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsDetalleachDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsDetalleachDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsDetalleachDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsDetalleachDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsDetalleachDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return BsDetalleachDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsDetalleachDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsDetalleachDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsDetalleachDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsDetalleach</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsDetalleach dto = (BsDetalleach)theDtoUntyped;
		BsDetalleachID theId = (BsDetalleachID)dtoIdUntyped;
		Long iddetalle = theId.getIddetalle();     
		Long instalacion = theId.getInstalacion();     
		Long lote = theId.getLote();     
		Long operacion = theId.getOperacion();     
		dto.setIddetalle(iddetalle);
		dto.setInstalacion(instalacion);
		dto.setLote(lote);
		dto.setOperacion(operacion);

	}	
	
	
}
