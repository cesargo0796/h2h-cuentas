package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresa;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioempresaDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsEmpresaodepto;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioDao;
import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsEmpresaodeptoDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsEmpresaodeptoDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>Convenio Empresa</b>
 * asociado a la tabla <b>BS_ConvenioEmpresa</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsConvenioempresaDaoJdbcSql extends SQLImplementacionBasica implements BsConvenioempresaDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_ConvenioEmpresa", new SQLColumnaMeta[]{
        new SQLColumnaMeta(3, "BS_ConvenioEmpresa", "E3", 3001, "TipoConvenio", "C3001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(3, "BS_ConvenioEmpresa", "E3", 3002, "Convenio", "C3002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(3, "BS_ConvenioEmpresa", "E3", 3003, "EmpresaODepto", "C3003", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1001, "TipoConvenio", "C1001", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1002, "Convenio", "C1002", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1003, "Cliente", "C1003", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1004, "Nombre", "C1004", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1005, "Cuenta", "C1005", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1006, "NombreCta", "C1006", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1007, "TipoMoneda", "C1007", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1008, "Categoria", "C1008", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1009, "Descripcion", "C1009", Types.VARCHAR)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1010, "DiaAplicacion", "C1010", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1011, "NumReintentos", "C1011", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1012, "PolParticipante", "C1012", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1013, "PolDiaNoExistente", "C1013", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1014, "Estatus", "C1014", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1015, "FechaEstatus", "C1015", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1016, "IntervaloReintento", "C1016", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1017, "PolMontoFijo", "C1017", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1018, "MontoFijo", "C1018", Types.NUMERIC)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1019, "IntervaloAplica", "C1019", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1020, "CreditosParciales", "C1020", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1021, "AutorizacionWeb", "C1021", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1022, "Clasificacion", "C1022", Types.BIGINT)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1023, "ComisionXOperacion", "C1023", Types.NUMERIC)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1024, "ComisionXOperacionPP", "C1024", Types.NUMERIC)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1025, "MultiplesCuentas", "C1025", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7001, "EmpresaODepto", "C7001", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7002, "Cliente", "C7002", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7003, "Nombre", "C7003", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7004, "Direccion", "C7004", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7005, "Telefono", "C7005", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7006, "Fax", "C7006", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7007, "Email", "C7007", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7008, "NombreContacto", "C7008", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7009, "ApellidoContacto", "C7009", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7010, "Proveedor", "C7010", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7011, "NivelSeguridad", "C7011", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7012, "FechaEstatus", "C7012", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7013, "Estatus", "C7013", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7014, "OrigenLotes", "C7014", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7015, "RegistroFiscal", "C7015", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7016, "NumPatronal", "C7016", Types.NUMERIC)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7017, "RequiereCompro", "C7017", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7018, "EsConsumidorFinal", "C7018", Types.BIGINT)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7019, "GiroEmpresa", "C7019", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7020, "Ciudad", "C7020", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7021, "Cargo", "C7021", Types.VARCHAR)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7022, "LimiteXArchivo", "C7022", Types.NUMERIC)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7023, "LimiteXLote", "C7023", Types.NUMERIC)
     ,  new SQLColumnaMeta(7, "BS_EmpresaODepto", "E7", 7024, "LimiteXTransaccion", "C7024", Types.NUMERIC)
	});
	
    
	public BsConvenio getBsConvenio(BsConvenioempresa bsConvenioempresa)throws ExcepcionEnDAO{
		return getBsConvenio(bsConvenioempresa, null);
	}

    public BsConvenio getBsConvenio(BsConvenioempresa bsConvenioempresa,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsConvenio bsConvenioRel = bsConvenioempresa.getBsConvenio();
	     if(bsConvenioRel==null){
                PeticionDeDatos peticionDeDatosParaBsConvenio = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsConvenio.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsConvenioDao.TIPOCONVENIO, 
	     	                              bsConvenioempresa.getTipoconvenio()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsConvenioRel = (BsConvenio)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsConvenio, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsConvenioDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsConvenioempresa.setBsConvenio(bsConvenioRel);										
		}
										
		return 	bsConvenioRel;									
    }
    
    
	public BsEmpresaodepto getBsEmpresaodepto(BsConvenioempresa bsConvenioempresa)throws ExcepcionEnDAO{
		return getBsEmpresaodepto(bsConvenioempresa, null);
	}

    public BsEmpresaodepto getBsEmpresaodepto(BsConvenioempresa bsConvenioempresa,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsEmpresaodepto bsEmpresaodeptoRel = bsConvenioempresa.getBsEmpresaodepto();
	     if(bsEmpresaodeptoRel==null){
                PeticionDeDatos peticionDeDatosParaBsEmpresaodepto = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsEmpresaodepto.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsEmpresaodeptoDao.EMPRESAODEPTO, 
	     	                              bsConvenioempresa.getEmpresaodepto()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsEmpresaodeptoRel = (BsEmpresaodepto)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsEmpresaodepto, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsEmpresaodeptoDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsConvenioempresa.setBsEmpresaodepto(bsEmpresaodeptoRel);										
		}
										
		return 	bsEmpresaodeptoRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E3.TipoConvenio AS C3001 , E3.Convenio AS C3002 , E3.EmpresaODepto AS C3003 , " +
		"       E1.TipoConvenio AS C1001 , E1.Convenio AS C1002 , E1.Cliente AS C1003 , E1.Nombre AS C1004 , " +
		"       E1.Cuenta AS C1005 , E1.NombreCta AS C1006 , E1.TipoMoneda AS C1007 , E1.Categoria AS C1008 , " +
		"       E1.Descripcion AS C1009 , E1.DiaAplicacion AS C1010 , E1.NumReintentos AS C1011 , E1.PolParticipante AS C1012 , " +
		"       E1.PolDiaNoExistente AS C1013 , E1.Estatus AS C1014 , E1.FechaEstatus AS C1015 , E1.IntervaloReintento AS C1016 , " +
		"       E1.PolMontoFijo AS C1017 , E1.MontoFijo AS C1018 , E1.IntervaloAplica AS C1019 , E1.CreditosParciales AS C1020 , " +
		"       E1.AutorizacionWeb AS C1021 , E1.Clasificacion AS C1022 , E1.ComisionXOperacion AS C1023 , E1.ComisionXOperacionPP AS C1024 , " +
		"       E1.MultiplesCuentas AS C1025 , " +
		"       E7.EmpresaODepto AS C7001 , E7.Cliente AS C7002 , E7.Nombre AS C7003 , E7.Direccion AS C7004 , " +
		"       E7.Telefono AS C7005 , E7.Fax AS C7006 , E7.Email AS C7007 , E7.NombreContacto AS C7008 , " +
		"       E7.ApellidoContacto AS C7009 , E7.Proveedor AS C7010 , E7.NivelSeguridad AS C7011 , E7.FechaEstatus AS C7012 , " +
		"       E7.Estatus AS C7013 , E7.OrigenLotes AS C7014 , E7.RegistroFiscal AS C7015 , E7.NumPatronal AS C7016 , " +
		"       E7.RequiereCompro AS C7017 , E7.EsConsumidorFinal AS C7018 , E7.GiroEmpresa AS C7019 , E7.Ciudad AS C7020 , " +
		"       E7.Cargo AS C7021 , E7.LimiteXArchivo AS C7022 , E7.LimiteXLote AS C7023 , E7.LimiteXTransaccion AS C7024" +
		"  FROM BS_ConvenioEmpresa E3 , BS_Convenio E1 , BS_EmpresaODepto E7" +
		" WHERE E1.TipoConvenio = E3.TipoConvenio" +
		"   AND E7.EmpresaODepto = E3.EmpresaODepto";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E3.TipoConvenio AS C3001 , E3.Convenio AS C3002 , E3.EmpresaODepto AS C3003 , " +
		"       E1.TipoConvenio AS C1001 , E1.Convenio AS C1002 , E1.Cliente AS C1003 , E1.Nombre AS C1004 , " +
		"       E1.Cuenta AS C1005 , E1.NombreCta AS C1006 , E1.TipoMoneda AS C1007 , E1.Categoria AS C1008 , " +
		"       E1.Descripcion AS C1009 , E1.DiaAplicacion AS C1010 , E1.NumReintentos AS C1011 , E1.PolParticipante AS C1012 , " +
		"       E1.PolDiaNoExistente AS C1013 , E1.Estatus AS C1014 , E1.FechaEstatus AS C1015 , E1.IntervaloReintento AS C1016 , " +
		"       E1.PolMontoFijo AS C1017 , E1.MontoFijo AS C1018 , E1.IntervaloAplica AS C1019 , E1.CreditosParciales AS C1020 , " +
		"       E1.AutorizacionWeb AS C1021 , E1.Clasificacion AS C1022 , E1.ComisionXOperacion AS C1023 , E1.ComisionXOperacionPP AS C1024 , " +
		"       E1.MultiplesCuentas AS C1025 , " +
		"       E7.EmpresaODepto AS C7001 , E7.Cliente AS C7002 , E7.Nombre AS C7003 , E7.Direccion AS C7004 , " +
		"       E7.Telefono AS C7005 , E7.Fax AS C7006 , E7.Email AS C7007 , E7.NombreContacto AS C7008 , " +
		"       E7.ApellidoContacto AS C7009 , E7.Proveedor AS C7010 , E7.NivelSeguridad AS C7011 , E7.FechaEstatus AS C7012 , " +
		"       E7.Estatus AS C7013 , E7.OrigenLotes AS C7014 , E7.RegistroFiscal AS C7015 , E7.NumPatronal AS C7016 , " +
		"       E7.RequiereCompro AS C7017 , E7.EsConsumidorFinal AS C7018 , E7.GiroEmpresa AS C7019 , E7.Ciudad AS C7020 , " +
		"       E7.Cargo AS C7021 , E7.LimiteXArchivo AS C7022 , E7.LimiteXLote AS C7023 , E7.LimiteXTransaccion AS C7024" +
		"  FROM BS_ConvenioEmpresa E3 , BS_Convenio E1 , BS_EmpresaODepto E7" +
		" WHERE E1.TipoConvenio = E3.TipoConvenio" +
		"   AND E7.EmpresaODepto = E3.EmpresaODepto" +
		"   AND E3.TipoConvenio= ? " +
		"   AND E3.Convenio= ? " +
		"   AND E3.EmpresaODepto= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E3.TipoConvenio AS C3001 , E3.Convenio AS C3002 , E3.EmpresaODepto AS C3003" +
		"  FROM BS_ConvenioEmpresa E3";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_ConvenioEmpresa";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_ConvenioEmpresa (" +
		"" +
		") VALUES ()";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_ConvenioEmpresa (" +
		"TipoConvenio,Convenio,EmpresaODepto" +
		") VALUES (?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_ConvenioEmpresa" +
		" SET " +
		" WHERE TipoConvenio= ?    AND Convenio= ?    AND EmpresaODepto= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_ConvenioEmpresa" +
		" WHERE TipoConvenio= ?    AND Convenio= ?    AND EmpresaODepto= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E3.TipoConvenio AS C3001 , E3.Convenio AS C3002 , E3.EmpresaODepto AS C3003" +
		"  FROM BS_ConvenioEmpresa E3" +
		" WHERE E3.TipoConvenio= ? " +
		"   AND E3.Convenio= ? " +
		"   AND E3.EmpresaODepto= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_ConvenioEmpresa (" +
		"    TipoConvenio LONG NOT NULL," +
		"    Convenio LONG NOT NULL," +
		"    EmpresaODepto LONG NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsConvenioempresa theDto = new BsConvenioempresa();
     	theDto.setTipoconvenio(SQLUtils.getLongFromResultSet(rst, "C3001"));
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C3002"));
     	theDto.setEmpresaodepto(SQLUtils.getLongFromResultSet(rst, "C3003"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsConvenioempresa</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsConvenioempresa crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsConvenioempresa bsConvenioempresa = (BsConvenioempresa) crearDtoST(rst);
        BsConvenio bsConvenioRel  = (BsConvenio) BsConvenioDaoJdbcSql.crearDtoST(rst);
        bsConvenioempresa.setBsConvenio(bsConvenioRel);
        BsEmpresaodepto bsEmpresaodeptoRel  = (BsEmpresaodepto) BsEmpresaodeptoDaoJdbcSql.crearDtoST(rst);
        bsConvenioempresa.setBsEmpresaodepto(bsEmpresaodeptoRel);
		return bsConvenioempresa;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsConvenioempresa</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsConvenioempresa crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsConvenioempresa bsConvenioempresa = (BsConvenioempresa) crearDtoST(rst);
        BsConvenio tipoconvenioRelbsConvenio  = (BsConvenio) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsConvenioempresa.getTipoconvenio(), conn, 
				BsConvenioDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsConvenioempresa.setBsConvenio(tipoconvenioRelbsConvenio);
        BsEmpresaodepto empresaodeptoRelbsEmpresaodepto  = (BsEmpresaodepto) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsConvenioempresa.getEmpresaodepto(), conn, 
				BsEmpresaodeptoDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsConvenioempresa.setBsEmpresaodepto(empresaodeptoRelbsEmpresaodepto);
		return bsConvenioempresa;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenioempresa theDto = (BsConvenioempresa)theDtoUntyped;
				
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenioempresa theDto = (BsConvenioempresa)theDtoUntyped;
				Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
	
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);
    	
		SQLUtils.setIntoStatement( 2, convenio, smt);
    	
		SQLUtils.setIntoStatement( 3, empresaodepto, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioempresaID theId = (BsConvenioempresaID)theIdUntyped;
			Long tipoconvenio = theId.getTipoconvenio();     
		Long convenio = theId.getConvenio();     
		Long empresaodepto = theId.getEmpresaodepto();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 3, empresaodepto, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioempresaID theId = (BsConvenioempresaID)theIdUntyped;
				Long tipoconvenio = theId.getTipoconvenio();     
		Long convenio = theId.getConvenio();     
		Long empresaodepto = theId.getEmpresaodepto();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 3, empresaodepto, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioempresa theDto = (BsConvenioempresa)theDtoUntyped;
				Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long empresaodepto = theDto.getEmpresaodepto();     
			     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    		     
		SQLUtils.setIntoStatement( 3, empresaodepto, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenioempresa</code> objeto del tipo <code>BsConvenioempresa</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsConvenioempresa bsConvenioempresa) throws ExcepcionEnDAO {
	    actualizar(bsConvenioempresa, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsConvenioempresaID</code> objeto del tipo <code>BsConvenioempresaID</code>.
	 * @return <code>BsConvenioempresa</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenioempresa buscarPorID(BsConvenioempresaID bsConvenioempresaID)
			throws ExcepcionEnDAO {
		return (BsConvenioempresa)buscarPorID(bsConvenioempresaID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsConvenioempresa</code> objeto del tipo <code>BsConvenioempresa</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsConvenioempresa bsConvenioempresa) throws ExcepcionEnDAO {
	    crear(bsConvenioempresa, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenioempresaID</code> objeto del tipo <code>BsConvenioempresaID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsConvenioempresaID bsConvenioempresaID) throws ExcepcionEnDAO {
	    eliminar(bsConvenioempresaID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsConvenioempresa</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenioempresa resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsConvenioempresa)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsConvenioempresaDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return BsConvenioempresaDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsConvenioempresaDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsConvenioempresaDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsConvenioempresaDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsConvenioempresaDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsConvenioempresaDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return BsConvenioempresaDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return BsConvenioempresaDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsConvenioempresaDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsConvenioempresaDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsConvenioempresa</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsConvenioempresa dto = (BsConvenioempresa)theDtoUntyped;
		BsConvenioempresaID theId = (BsConvenioempresaID)dtoIdUntyped;
		Long tipoconvenio = theId.getTipoconvenio();     
		Long convenio = theId.getConvenio();     
		Long empresaodepto = theId.getEmpresaodepto();     
		dto.setTipoconvenio(tipoconvenio);
		dto.setConvenio(convenio);
		dto.setEmpresaodepto(empresaodepto);

	}	
	
	
}
