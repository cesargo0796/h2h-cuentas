package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioDao;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Convenio</b>
 * asociado a la tabla <b>BS_Convenio</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsConvenioDaoJdbcSql extends SQLImplementacionBasica implements BsConvenioDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_Convenio", new SQLColumnaMeta[]{
        new SQLColumnaMeta(1, "BS_Convenio", "E1", 1001, "TipoConvenio", "C1001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1002, "Convenio", "C1002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1003, "Cliente", "C1003", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1004, "Nombre", "C1004", Types.VARCHAR )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1005, "Cuenta", "C1005", Types.VARCHAR )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1006, "NombreCta", "C1006", Types.VARCHAR )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1007, "TipoMoneda", "C1007", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1008, "Categoria", "C1008", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1009, "Descripcion", "C1009", Types.VARCHAR )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1010, "DiaAplicacion", "C1010", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1011, "NumReintentos", "C1011", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1012, "PolParticipante", "C1012", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1013, "PolDiaNoExistente", "C1013", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1014, "Estatus", "C1014", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1015, "FechaEstatus", "C1015", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1016, "IntervaloReintento", "C1016", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1017, "PolMontoFijo", "C1017", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1018, "MontoFijo", "C1018", Types.NUMERIC )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1019, "IntervaloAplica", "C1019", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1020, "CreditosParciales", "C1020", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1021, "AutorizacionWeb", "C1021", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1022, "Clasificacion", "C1022", Types.BIGINT )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1023, "ComisionXOperacion", "C1023", Types.NUMERIC )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1024, "ComisionXOperacionPP", "C1024", Types.NUMERIC )
     ,  new SQLColumnaMeta(1, "BS_Convenio", "E1", 1025, "MultiplesCuentas", "C1025", Types.VARCHAR )
	});
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E1.TipoConvenio AS C1001 , E1.Convenio AS C1002 , E1.Cliente AS C1003 , E1.Nombre AS C1004 , " +
		"       E1.Cuenta AS C1005 , E1.NombreCta AS C1006 , E1.TipoMoneda AS C1007 , E1.Categoria AS C1008 , " +
		"       E1.Descripcion AS C1009 , E1.DiaAplicacion AS C1010 , E1.NumReintentos AS C1011 , E1.PolParticipante AS C1012 , " +
		"       E1.PolDiaNoExistente AS C1013 , E1.Estatus AS C1014 , E1.FechaEstatus AS C1015 , E1.IntervaloReintento AS C1016 , " +
		"       E1.PolMontoFijo AS C1017 , E1.MontoFijo AS C1018 , E1.IntervaloAplica AS C1019 , E1.CreditosParciales AS C1020 , " +
		"       E1.AutorizacionWeb AS C1021 , E1.Clasificacion AS C1022 , E1.ComisionXOperacion AS C1023 , E1.ComisionXOperacionPP AS C1024 , " +
		"       E1.MultiplesCuentas AS C1025" +
		"  FROM BS_Convenio E1";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_Convenio";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_Convenio (" +
		"Cliente,Nombre,Cuenta,NombreCta,TipoMoneda" +
		",Categoria,Descripcion,DiaAplicacion,NumReintentos" +
		",PolParticipante,PolDiaNoExistente,Estatus,FechaEstatus" +
		",IntervaloReintento,PolMontoFijo,MontoFijo,IntervaloAplica" +
		",CreditosParciales,AutorizacionWeb,Clasificacion,ComisionXOperacion" +
		",ComisionXOperacionPP,MultiplesCuentas" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_Convenio (" +
		"TipoConvenio,Convenio,Cliente,Nombre,Cuenta" +
		",NombreCta,TipoMoneda,Categoria,Descripcion" +
		",DiaAplicacion,NumReintentos,PolParticipante,PolDiaNoExistente" +
		",Estatus,FechaEstatus,IntervaloReintento,PolMontoFijo" +
		",MontoFijo,IntervaloAplica,CreditosParciales,AutorizacionWeb" +
		",Clasificacion,ComisionXOperacion,ComisionXOperacionPP,MultiplesCuentas" +
		"" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_Convenio" +
		" SET Cliente= ?  , Nombre= ?  , Cuenta= ?  , NombreCta= ?  , " +
		"TipoMoneda= ?  , Categoria= ?  , Descripcion= ?  , DiaAplicacion= ?  , " +
		"NumReintentos= ?  , PolParticipante= ?  , PolDiaNoExistente= ?  , Estatus= ?  , " +
		"FechaEstatus= ?  , IntervaloReintento= ?  , PolMontoFijo= ?  , MontoFijo= ?  , " +
		"IntervaloAplica= ?  , CreditosParciales= ?  , AutorizacionWeb= ?  , Clasificacion= ?  , " +
		"ComisionXOperacion= ?  , ComisionXOperacionPP= ?  , MultiplesCuentas= ? " +
		" WHERE TipoConvenio= ?    AND Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_Convenio" +
		" WHERE TipoConvenio= ?    AND Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E1.TipoConvenio AS C1001 , E1.Convenio AS C1002 , E1.Cliente AS C1003 , E1.Nombre AS C1004 , " +
		"       E1.Cuenta AS C1005 , E1.NombreCta AS C1006 , E1.TipoMoneda AS C1007 , E1.Categoria AS C1008 , " +
		"       E1.Descripcion AS C1009 , E1.DiaAplicacion AS C1010 , E1.NumReintentos AS C1011 , E1.PolParticipante AS C1012 , " +
		"       E1.PolDiaNoExistente AS C1013 , E1.Estatus AS C1014 , E1.FechaEstatus AS C1015 , E1.IntervaloReintento AS C1016 , " +
		"       E1.PolMontoFijo AS C1017 , E1.MontoFijo AS C1018 , E1.IntervaloAplica AS C1019 , E1.CreditosParciales AS C1020 , " +
		"       E1.AutorizacionWeb AS C1021 , E1.Clasificacion AS C1022 , E1.ComisionXOperacion AS C1023 , E1.ComisionXOperacionPP AS C1024 , " +
		"       E1.MultiplesCuentas AS C1025" +
		"  FROM BS_Convenio E1" +
		" WHERE E1.TipoConvenio= ? " +
		"   AND E1.Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_Convenio (" +
		"    TipoConvenio LONG NOT NULL," +
		"    Convenio LONG NOT NULL," +
		"    Cliente LONG NOT NULL," +
		"    Nombre VARCHAR(50)  NOT NULL," +
		"    Cuenta VARCHAR(16)  NOT NULL," +
		"    NombreCta VARCHAR(50)  NOT NULL," +
		"    TipoMoneda LONG NOT NULL," +
		"    Categoria LONG NOT NULL," +
		"    Descripcion VARCHAR(50)  NOT NULL," +
		"    DiaAplicacion LONG NOT NULL," +
		"    NumReintentos LONG NOT NULL," +
		"    PolParticipante LONG NOT NULL," +
		"    PolDiaNoExistente LONG NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    IntervaloReintento LONG," +
		"    PolMontoFijo LONG," +
		"    MontoFijo NUMERIC(19, 0) ," +
		"    IntervaloAplica LONG," +
		"    CreditosParciales LONG," +
		"    AutorizacionWeb LONG," +
		"    Clasificacion LONG NOT NULL," +
		"    ComisionXOperacion NUMERIC(19, 0)  NOT NULL," +
		"    ComisionXOperacionPP NUMERIC(19, 0) ," +
		"    MultiplesCuentas VARCHAR(1) " +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsConvenio theDto = new BsConvenio();
     	theDto.setTipoconvenio(SQLUtils.getLongFromResultSet(rst, "C1001"));
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C1002"));
     	theDto.setCliente(SQLUtils.getLongFromResultSet(rst, "C1003"));
		theDto.setNombre(rst.getString("C1004"));
		theDto.setCuenta(rst.getString("C1005"));
		theDto.setNombrecta(rst.getString("C1006"));
     	theDto.setTipomoneda(SQLUtils.getLongFromResultSet(rst, "C1007"));
     	theDto.setCategoria(SQLUtils.getLongFromResultSet(rst, "C1008"));
		theDto.setDescripcion(rst.getString("C1009"));
     	theDto.setDiaaplicacion(SQLUtils.getLongFromResultSet(rst, "C1010"));
     	theDto.setNumreintentos(SQLUtils.getLongFromResultSet(rst, "C1011"));
     	theDto.setPolparticipante(SQLUtils.getLongFromResultSet(rst, "C1012"));
     	theDto.setPoldianoexistente(SQLUtils.getLongFromResultSet(rst, "C1013"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C1014"));
		theDto.setFechaestatus(rst.getTimestamp("C1015"));
     	theDto.setIntervaloreintento(SQLUtils.getLongFromResultSet(rst, "C1016"));
     	theDto.setPolmontofijo(SQLUtils.getLongFromResultSet(rst, "C1017"));
		theDto.setMontofijo(rst.getBigDecimal("C1018"));
     	theDto.setIntervaloaplica(SQLUtils.getLongFromResultSet(rst, "C1019"));
     	theDto.setCreditosparciales(SQLUtils.getLongFromResultSet(rst, "C1020"));
     	theDto.setAutorizacionweb(SQLUtils.getLongFromResultSet(rst, "C1021"));
     	theDto.setClasificacion(SQLUtils.getLongFromResultSet(rst, "C1022"));
		theDto.setComisionxoperacion(rst.getBigDecimal("C1023"));
		theDto.setComisionxoperacionpp(rst.getBigDecimal("C1024"));
		theDto.setMultiplescuentas(rst.getString("C1025"));
		return theDto;
	}
	
	







	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenio theDto = (BsConvenio)theDtoUntyped;
				Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String cuenta = theDto.getCuenta();     
		String nombrecta = theDto.getNombrecta();     
		Long tipomoneda = theDto.getTipomoneda();     
		Long categoria = theDto.getCategoria();     
		String descripcion = theDto.getDescripcion();     
		Long diaaplicacion = theDto.getDiaaplicacion();     
		Long numreintentos = theDto.getNumreintentos();     
		Long polparticipante = theDto.getPolparticipante();     
		Long poldianoexistente = theDto.getPoldianoexistente();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long intervaloreintento = theDto.getIntervaloreintento();     
		Long polmontofijo = theDto.getPolmontofijo();     
		java.math.BigDecimal montofijo = theDto.getMontofijo();     
		Long intervaloaplica = theDto.getIntervaloaplica();     
		Long creditosparciales = theDto.getCreditosparciales();     
		Long autorizacionweb = theDto.getAutorizacionweb();     
		Long clasificacion = theDto.getClasificacion();     
		java.math.BigDecimal comisionxoperacion = theDto.getComisionxoperacion();     
		java.math.BigDecimal comisionxoperacionpp = theDto.getComisionxoperacionpp();     
		String multiplescuentas = theDto.getMultiplescuentas();     
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
    		     
		SQLUtils.setIntoStatement( 2, nombre, smt);  
    		     
		SQLUtils.setIntoStatement( 3, cuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 4, nombrecta, smt);  
    		     
		SQLUtils.setIntoStatement( 5, tipomoneda, smt);  
    		     
		SQLUtils.setIntoStatement( 6, categoria, smt);  
    		     
		SQLUtils.setIntoStatement( 7, descripcion, smt);  
    		     
		SQLUtils.setIntoStatement( 8, diaaplicacion, smt);  
    		     
		SQLUtils.setIntoStatement( 9, numreintentos, smt);  
    		     
		SQLUtils.setIntoStatement( 10, polparticipante, smt);  
    		     
		SQLUtils.setIntoStatement( 11, poldianoexistente, smt);  
    		     
		SQLUtils.setIntoStatement( 12, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 13, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 14, intervaloreintento, smt);  
    		     
		SQLUtils.setIntoStatement( 15, polmontofijo, smt);  
    		     
		SQLUtils.setIntoStatement( 16, montofijo, smt);  
    		     
		SQLUtils.setIntoStatement( 17, intervaloaplica, smt);  
    		     
		SQLUtils.setIntoStatement( 18, creditosparciales, smt);  
    		     
		SQLUtils.setIntoStatement( 19, autorizacionweb, smt);  
    		     
		SQLUtils.setIntoStatement( 20, clasificacion, smt);  
    		     
		SQLUtils.setIntoStatement( 21, comisionxoperacion, smt);  
    		     
		SQLUtils.setIntoStatement( 22, comisionxoperacionpp, smt);  
    		     
		SQLUtils.setIntoStatement( 23, multiplescuentas, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenio theDto = (BsConvenio)theDtoUntyped;
				Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String cuenta = theDto.getCuenta();     
		String nombrecta = theDto.getNombrecta();     
		Long tipomoneda = theDto.getTipomoneda();     
		Long categoria = theDto.getCategoria();     
		String descripcion = theDto.getDescripcion();     
		Long diaaplicacion = theDto.getDiaaplicacion();     
		Long numreintentos = theDto.getNumreintentos();     
		Long polparticipante = theDto.getPolparticipante();     
		Long poldianoexistente = theDto.getPoldianoexistente();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long intervaloreintento = theDto.getIntervaloreintento();     
		Long polmontofijo = theDto.getPolmontofijo();     
		java.math.BigDecimal montofijo = theDto.getMontofijo();     
		Long intervaloaplica = theDto.getIntervaloaplica();     
		Long creditosparciales = theDto.getCreditosparciales();     
		Long autorizacionweb = theDto.getAutorizacionweb();     
		Long clasificacion = theDto.getClasificacion();     
		java.math.BigDecimal comisionxoperacion = theDto.getComisionxoperacion();     
		java.math.BigDecimal comisionxoperacionpp = theDto.getComisionxoperacionpp();     
		String multiplescuentas = theDto.getMultiplescuentas();     
	
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);
    	
		SQLUtils.setIntoStatement( 2, convenio, smt);
    	
		SQLUtils.setIntoStatement( 3, cliente, smt);
    	
		SQLUtils.setIntoStatement( 4, nombre, smt);
    	
		SQLUtils.setIntoStatement( 5, cuenta, smt);
    	
		SQLUtils.setIntoStatement( 6, nombrecta, smt);
    	
		SQLUtils.setIntoStatement( 7, tipomoneda, smt);
    	
		SQLUtils.setIntoStatement( 8, categoria, smt);
    	
		SQLUtils.setIntoStatement( 9, descripcion, smt);
    	
		SQLUtils.setIntoStatement( 10, diaaplicacion, smt);
    	
		SQLUtils.setIntoStatement( 11, numreintentos, smt);
    	
		SQLUtils.setIntoStatement( 12, polparticipante, smt);
    	
		SQLUtils.setIntoStatement( 13, poldianoexistente, smt);
    	
		SQLUtils.setIntoStatement( 14, estatus, smt);
    	
		SQLUtils.setIntoStatement( 15, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 16, intervaloreintento, smt);
    	
		SQLUtils.setIntoStatement( 17, polmontofijo, smt);
    	
		SQLUtils.setIntoStatement( 18, montofijo, smt);
    	
		SQLUtils.setIntoStatement( 19, intervaloaplica, smt);
    	
		SQLUtils.setIntoStatement( 20, creditosparciales, smt);
    	
		SQLUtils.setIntoStatement( 21, autorizacionweb, smt);
    	
		SQLUtils.setIntoStatement( 22, clasificacion, smt);
    	
		SQLUtils.setIntoStatement( 23, comisionxoperacion, smt);
    	
		SQLUtils.setIntoStatement( 24, comisionxoperacionpp, smt);
    	
		SQLUtils.setIntoStatement( 25, multiplescuentas, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioID theId = (BsConvenioID)theIdUntyped;
			Long tipoconvenio = theId.getTipoconvenio();     
		Long convenio = theId.getConvenio();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioID theId = (BsConvenioID)theIdUntyped;
				Long tipoconvenio = theId.getTipoconvenio();     
		Long convenio = theId.getConvenio();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenio theDto = (BsConvenio)theDtoUntyped;
				Long tipoconvenio = theDto.getTipoconvenio();     
		Long convenio = theDto.getConvenio();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String cuenta = theDto.getCuenta();     
		String nombrecta = theDto.getNombrecta();     
		Long tipomoneda = theDto.getTipomoneda();     
		Long categoria = theDto.getCategoria();     
		String descripcion = theDto.getDescripcion();     
		Long diaaplicacion = theDto.getDiaaplicacion();     
		Long numreintentos = theDto.getNumreintentos();     
		Long polparticipante = theDto.getPolparticipante();     
		Long poldianoexistente = theDto.getPoldianoexistente();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long intervaloreintento = theDto.getIntervaloreintento();     
		Long polmontofijo = theDto.getPolmontofijo();     
		java.math.BigDecimal montofijo = theDto.getMontofijo();     
		Long intervaloaplica = theDto.getIntervaloaplica();     
		Long creditosparciales = theDto.getCreditosparciales();     
		Long autorizacionweb = theDto.getAutorizacionweb();     
		Long clasificacion = theDto.getClasificacion();     
		java.math.BigDecimal comisionxoperacion = theDto.getComisionxoperacion();     
		java.math.BigDecimal comisionxoperacionpp = theDto.getComisionxoperacionpp();     
		String multiplescuentas = theDto.getMultiplescuentas();     
		     
		SQLUtils.setIntoStatement( 1, cliente, smt);  
        	     
		SQLUtils.setIntoStatement( 2, nombre, smt);  
        	     
		SQLUtils.setIntoStatement( 3, cuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 4, nombrecta, smt);  
        	     
		SQLUtils.setIntoStatement( 5, tipomoneda, smt);  
        	     
		SQLUtils.setIntoStatement( 6, categoria, smt);  
        	     
		SQLUtils.setIntoStatement( 7, descripcion, smt);  
        	     
		SQLUtils.setIntoStatement( 8, diaaplicacion, smt);  
        	     
		SQLUtils.setIntoStatement( 9, numreintentos, smt);  
        	     
		SQLUtils.setIntoStatement( 10, polparticipante, smt);  
        	     
		SQLUtils.setIntoStatement( 11, poldianoexistente, smt);  
        	     
		SQLUtils.setIntoStatement( 12, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 13, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 14, intervaloreintento, smt);  
        	     
		SQLUtils.setIntoStatement( 15, polmontofijo, smt);  
        	     
		SQLUtils.setIntoStatement( 16, montofijo, smt);  
        	     
		SQLUtils.setIntoStatement( 17, intervaloaplica, smt);  
        	     
		SQLUtils.setIntoStatement( 18, creditosparciales, smt);  
        	     
		SQLUtils.setIntoStatement( 19, autorizacionweb, smt);  
        	     
		SQLUtils.setIntoStatement( 20, clasificacion, smt);  
        	     
		SQLUtils.setIntoStatement( 21, comisionxoperacion, smt);  
        	     
		SQLUtils.setIntoStatement( 22, comisionxoperacionpp, smt);  
        	     
		SQLUtils.setIntoStatement( 23, multiplescuentas, smt);  
        		     
		SQLUtils.setIntoStatement( 24, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 25, convenio, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenio</code> objeto del tipo <code>BsConvenio</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsConvenio bsConvenio) throws ExcepcionEnDAO {
	    actualizar(bsConvenio, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsConvenioID</code> objeto del tipo <code>BsConvenioID</code>.
	 * @return <code>BsConvenio</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenio buscarPorID(BsConvenioID bsConvenioID)
			throws ExcepcionEnDAO {
		return (BsConvenio)buscarPorID(bsConvenioID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsConvenio</code> objeto del tipo <code>BsConvenio</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsConvenio bsConvenio) throws ExcepcionEnDAO {
	    crear(bsConvenio, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenioID</code> objeto del tipo <code>BsConvenioID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsConvenioID bsConvenioID) throws ExcepcionEnDAO {
	    eliminar(bsConvenioID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsConvenio</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenio resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsConvenio)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsConvenioDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsConvenioDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsConvenioDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsConvenioDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsConvenioDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsConvenioDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return BsConvenioDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsConvenioDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsConvenioDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsConvenioDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsConvenio</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsConvenio dto = (BsConvenio)theDtoUntyped;
		BsConvenioID theId = (BsConvenioID)dtoIdUntyped;
		Long tipoconvenio = theId.getTipoconvenio();     
		Long convenio = theId.getConvenio();     
		dto.setTipoconvenio(tipoconvenio);
		dto.setConvenio(convenio);

	}	
	
	
}
