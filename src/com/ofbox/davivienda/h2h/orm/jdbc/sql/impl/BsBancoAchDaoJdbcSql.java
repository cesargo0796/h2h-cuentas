package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import com.ofbox.davivienda.h2h.abstraccion.dao.BsBancoAchDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsBancoAch;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;




/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Detalle ACH</b>
 * asociado a la tabla <b>BS_BancoACH</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsBancoAchDaoJdbcSql extends SQLImplementacionBasica implements BsBancoAchDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_BancoACH", new SQLColumnaMeta[]{
        new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6001, "codBanco", "C6001", Types.BIGINT ,true)    
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6002, "codPayBank", "C6002", Types.VARCHAR )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6003, "digitoChequeo", "C6003", Types.SMALLINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6004, "enlinea", "C6004", Types.BIGINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6005, "esActivo", "C6005", Types.BOOLEAN )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6006, "longitudCtaAhorro", "C6006", Types.SMALLINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6007, "longitudCtaCorriente", "C6007", Types.SMALLINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6008, "longitudCtaPrestamo", "C6008", Types.SMALLINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6009, "longitudTjCredito", "C6009", Types.SMALLINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6010, "idAs400", "C6010", Types.BIGINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6011, "moneda", "C6011", Types.BIGINT )   
     ,  new SQLColumnaMeta(6, "BS_BancoACH", "E6", 6012, "nombre", "C6012", Types.VARCHAR )
	});
     
	
	


	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E6.codBanco as C6001, E6.codPayBank as C6002, E6.digitoChequeo as C6003, E6.enlinea as C6004, "+
		"E6.esActivo as C6005, E6.longitudCtaAhorro as C6006, E6.longitudCtaCorriente as C6007, E6.longitudCtaPrestamo as C6008, "+
		"E6.longitudTjCredito as C6009, E6.idAs400 as C6010, E6.moneda as C6011, E6.nombre as C6012 "+
		"FROM BS_BancoACH E6 ";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de c&oacute;digos de banco.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_BancoACH";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_BancoACH(codPayBank, digitoChequeo, enlinea, "+
		"esActivo, longitudCtaAhorro, longitudCtaCorriente, longitudCtaPrestamo, "+
		"longitudTjCredito, idAs400, moneda, nombre) " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID =
		"INSERT INTO BS_BancoACH(codBanco, codPayBank, digitoChequeo, enlinea, "+
		"esActivo, longitudCtaAhorro, longitudCtaCorriente, longitudCtaPrestamo, "+
		"longitudTjCredito, idAs400, moneda, nombre) " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_BancoACH SET codBanco=?, codPayBank=?, digitoChequeo=?, enlinea=?, "+
		"esActivo=?, longitudCtaAhorro=?, longitudCtaCorriente=?, longitudCtaPrestamo=?, "+
		"longitudTjCredito=?, idAs400=?, moneda=?, nombre=? "+
		"UPDATE BS_BancoACH SET interpretacion=? "+
		"WHERE codBanco=?";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL = 
		"DELETE FROM BS_BancoACH WHERE codBanco=?";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E6.codBanco as C6001, E6.codPayBank as C6002, E6.digitoChequeo as C6003, E6.enlinea as C6004, "+
		"E6.esActivo as C6005, E6.longitudCtaAhorro as C6006, E6.longitudCtaCorriente as C6007, E6.longitudCtaPrestamo as C6008, "+
		"E6.longitudTjCredito as C6009, E6.idAs400 as C6010, E6.moneda as C6011, E6.nombre as C6012 "+
		"FROM BS_BancoACH E6 "+		
		"WHERE E6.codBanco= ?";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BancaEmpresaPlus.dbo.BS_BancoACH (" + 
		"    (" + 
		"        CodBanco INT NOT NULL," + 
		"        Nombre VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," + 
		"        Moneda INT NOT NULL," + 
		"        EsActivo BIT NOT NULL," + 
		"        CodPayBank VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," + 
		"        DigitoChequeo SMALLINT," + 
		"        LongitudCtaAhorro SMALLINT DEFAULT 0 NOT NULL," + 
		"        LongitudCtaCorriente SMALLINT DEFAULT 0 NOT NULL," + 
		"        LongitudCtaPrestamo SMALLINT DEFAULT 0 NOT NULL," + 
		"        LongitudTjCredito SMALLINT DEFAULT 0 NOT NULL," + 
		"        idAS400 INT," + 
		"        Enlinea INT DEFAULT 0," + 
		"        CONSTRAINT PK_ACH_Banco PRIMARY KEY (CodBanco)," + 
		"        CONSTRAINT IX_ACH_Banco UNIQUE (CodPayBank, Moneda)" + 
		"    )";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {	
		BsBancoAch bsBancoAchDto = new BsBancoAch();
		bsBancoAchDto.setCodBanco(SQLUtils.getLongFromResultSet(rst, "C6001"));
		bsBancoAchDto.setCodPayBank(rst.getString("C6002"));
		bsBancoAchDto.setDigitoChequeo(SQLUtils.getLongFromResultSet(rst, "C6003"));
		bsBancoAchDto.setEnlinea(SQLUtils.getLongFromResultSet(rst, "C6004"));
		bsBancoAchDto.setEsActivo(rst.getString("C6005"));
		bsBancoAchDto.setLongitudCtaAhorro(SQLUtils.getLongFromResultSet(rst, "C6006"));
		bsBancoAchDto.setLongitudCtaCorriente(SQLUtils.getLongFromResultSet(rst, "C6007"));
		bsBancoAchDto.setLongitudCtaPrestamo(SQLUtils.getLongFromResultSet(rst, "C6008"));
		bsBancoAchDto.setLongitudTjCredito(SQLUtils.getLongFromResultSet(rst, "C6009"));
		bsBancoAchDto.setIdAs400(SQLUtils.getLongFromResultSet(rst, "C6010"));
		bsBancoAchDto.setMoneda(SQLUtils.getLongFromResultSet(rst, "C6011"));
		bsBancoAchDto.setNombre(rst.getString("C6012"));
		return bsBancoAchDto;
	}


	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {	
		BsBancoAch bsBancoAchDto = (BsBancoAch)theDtoUntyped;
		String codPayBank=bsBancoAchDto.getCodPayBank();
		Long digitoChequeo=bsBancoAchDto.getDigitoChequeo();
		Long enlinea=bsBancoAchDto.getEnlinea();
		String esActivo=bsBancoAchDto.getEsActivo();
		Long longitudCtaAhorro=bsBancoAchDto.getLongitudCtaAhorro();
		Long longitudCtaCorriente=bsBancoAchDto.getLongitudCtaCorriente();
		Long longitudCtaPrestamo=bsBancoAchDto.getLongitudCtaPrestamo();
		Long longitudTjCredito=bsBancoAchDto.getLongitudTjCredito();
		Long idAS400=bsBancoAchDto.getIdAs400();
		Long moneda=bsBancoAchDto.getMoneda();
		String nombre=bsBancoAchDto.getNombre();
		
 	    SQLUtils.setIntoStatement( 1, codPayBank, smt); 
 	    SQLUtils.setIntoStatement( 2, digitoChequeo, smt); 
 	    SQLUtils.setIntoStatement( 3, enlinea, smt); 
 	    SQLUtils.setIntoStatement( 4, esActivo, smt); 
 	    SQLUtils.setIntoStatement( 5, longitudCtaAhorro, smt); 
 	    SQLUtils.setIntoStatement( 6, longitudCtaCorriente, smt); 
 	    SQLUtils.setIntoStatement( 7, longitudCtaPrestamo, smt); 
 	    SQLUtils.setIntoStatement( 8, longitudTjCredito, smt); 
 	    SQLUtils.setIntoStatement( 9, idAS400, smt); 
 	    SQLUtils.setIntoStatement( 10, moneda, smt); 
 	    SQLUtils.setIntoStatement( 11, nombre, smt);      
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
		BsBancoAch bsBancoAchDto = (BsBancoAch)theDtoUntyped;
		Long codBanco=bsBancoAchDto.getCodBanco();
		String codPayBank=bsBancoAchDto.getCodPayBank();
		Long digitoChequeo=bsBancoAchDto.getDigitoChequeo();
		Long enlinea=bsBancoAchDto.getEnlinea();
		String esActivo=bsBancoAchDto.getEsActivo();
		Long longitudCtaAhorro=bsBancoAchDto.getLongitudCtaAhorro();
		Long longitudCtaCorriente=bsBancoAchDto.getLongitudCtaCorriente();
		Long longitudCtaPrestamo=bsBancoAchDto.getLongitudCtaPrestamo();
		Long longitudTjCredito=bsBancoAchDto.getLongitudTjCredito();
		Long idAS400=bsBancoAchDto.getIdAs400();
		Long moneda=bsBancoAchDto.getMoneda();
		String nombre=bsBancoAchDto.getNombre();
		
 	    SQLUtils.setIntoStatement( 1, codBanco, smt); 
 	    SQLUtils.setIntoStatement( 2, codPayBank, smt); 
 	    SQLUtils.setIntoStatement( 3, digitoChequeo, smt); 
 	    SQLUtils.setIntoStatement( 4, enlinea, smt); 
 	    SQLUtils.setIntoStatement( 5, esActivo, smt); 
 	    SQLUtils.setIntoStatement( 6, longitudCtaAhorro, smt); 
 	    SQLUtils.setIntoStatement( 7, longitudCtaCorriente, smt); 
 	    SQLUtils.setIntoStatement( 8, longitudCtaPrestamo, smt); 
 	    SQLUtils.setIntoStatement( 9, longitudTjCredito, smt); 
 	    SQLUtils.setIntoStatement( 10, idAS400, smt); 
 	    SQLUtils.setIntoStatement( 11, moneda, smt); 
 	    SQLUtils.setIntoStatement( 12, nombre, smt);   
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
 	    Long codBanco=(Long)theIdUntyped;
 	    
		SQLUtils.setIntoStatement( 1, codBanco, smt);  
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
 	    Long codBanco=(Long)theIdUntyped;
 	    SQLUtils.setIntoStatement( 1, codBanco, smt); 
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
		BsBancoAch bsBancoAchDto = (BsBancoAch)theDtoUntyped;
		Long codBanco=bsBancoAchDto.getCodBanco();
		String codPayBank=bsBancoAchDto.getCodPayBank();
		Long digitoChequeo=bsBancoAchDto.getDigitoChequeo();
		Long enlinea=bsBancoAchDto.getEnlinea();
		String esActivo=bsBancoAchDto.getEsActivo();
		Long longitudCtaAhorro=bsBancoAchDto.getLongitudCtaAhorro();
		Long longitudCtaCorriente=bsBancoAchDto.getLongitudCtaCorriente();
		Long longitudCtaPrestamo=bsBancoAchDto.getLongitudCtaPrestamo();
		Long longitudTjCredito=bsBancoAchDto.getLongitudTjCredito();
		Long idAS400=bsBancoAchDto.getIdAs400();
		Long moneda=bsBancoAchDto.getMoneda();
		String nombre=bsBancoAchDto.getNombre();
		
 	    SQLUtils.setIntoStatement( 1, codBanco, smt); 
 	    SQLUtils.setIntoStatement( 2, codPayBank, smt); 
 	    SQLUtils.setIntoStatement( 3, digitoChequeo, smt); 
 	    SQLUtils.setIntoStatement( 4, enlinea, smt); 
 	    SQLUtils.setIntoStatement( 5, esActivo, smt); 
 	    SQLUtils.setIntoStatement( 6, longitudCtaAhorro, smt); 
 	    SQLUtils.setIntoStatement( 7, longitudCtaCorriente, smt); 
 	    SQLUtils.setIntoStatement( 8, longitudCtaPrestamo, smt); 
 	    SQLUtils.setIntoStatement( 9, longitudTjCredito, smt); 
 	    SQLUtils.setIntoStatement( 10, idAS400, smt); 
 	    SQLUtils.setIntoStatement( 11, moneda, smt); 
 	    SQLUtils.setIntoStatement( 12, nombre, smt);             			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>BsBancoAch</code> objeto del tipo <code>BsBancoAch</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsBancoAch BsBancoAch) throws ExcepcionEnDAO {
	    actualizar(BsBancoAch, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>BsBancoAchID</code> objeto del tipo <code>BsBancoAchID</code>.
	 * @return <code>BsBancoAch</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsBancoAch buscarPorID(BsBancoAch BsBancoAch)
			throws ExcepcionEnDAO {
		return (BsBancoAch)buscarPorID(BsBancoAch, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>BsBancoAch</code> objeto del tipo <code>BsBancoAch</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsBancoAch BsBancoAch) throws ExcepcionEnDAO {
	    crear(BsBancoAch, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>BsBancoAchID</code> objeto del tipo <code>BsBancoAchID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsBancoAch BsBancoAch) throws ExcepcionEnDAO {
	    eliminar(BsBancoAch, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsBancoAch</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsBancoAch resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsBancoAch)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.SIN_FORANEOS;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsBancoAchDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsBancoAchDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return null;
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsBancoAchDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsBancoAchDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsBancoAchDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsBancoAchDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsBancoAchDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsBancoAchDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return null;
			}
			
			public String getBuscarPorIdSql() {				
				return BsBancoAchDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsBancoAchDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsBancoAchDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return null;
			}
			
			
            public boolean isForaneosDisponible(){
				return false;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsBancoAchDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}	
}
