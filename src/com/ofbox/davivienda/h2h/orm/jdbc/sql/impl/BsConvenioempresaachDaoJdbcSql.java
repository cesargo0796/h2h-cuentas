package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaach;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaachID;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioempresaachDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioach;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsConvenioachDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioachDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Convenio Empresa ACH</b>
 * asociado a la tabla <b>BS_ConvenioEmpresaACH</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsConvenioempresaachDaoJdbcSql extends SQLImplementacionBasica implements BsConvenioempresaachDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_ConvenioEmpresaACH", new SQLColumnaMeta[]{
        new SQLColumnaMeta(4, "BS_ConvenioEmpresaACH", "E4", 4001, "EmpresaODepto", "C4001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(4, "BS_ConvenioEmpresaACH", "E4", 4002, "Convenio", "C4002", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2001, "Convenio", "C2001", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2002, "TipoConvenio", "C2002", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2020, "Cliente", "C2020", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2003, "Nombre", "C2003", Types.VARCHAR)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2004, "Cuenta", "C2004", Types.VARCHAR)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2005, "NombreCta", "C2005", Types.VARCHAR)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2006, "TipoMoneda", "C2006", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2007, "Descripcion", "C2007", Types.VARCHAR)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2008, "PolParticipante", "C2008", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2009, "Estatus", "C2009", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2010, "FechaEstatus", "C2010", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2011, "AutorizacionWeb", "C2011", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2012, "Clasificacion", "C2012", Types.BIGINT)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2013, "ComisionXOperacionPropiaOK", "C2013", Types.NUMERIC)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2014, "ComisionXOperacionPropiaError", "C2014", Types.NUMERIC)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2015, "ComisionXOperacionNoPropiaOK", "C2015", Types.NUMERIC)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2016, "ComisionXOperacionNoPropiaError", "C2016", Types.NUMERIC)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2017, "MultiplesCuentas", "C2017", Types.VARCHAR)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2018, "Activo", "C2018", Types.VARCHAR)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2019, "bitBorrado", "C2019", Types.VARCHAR)
	});
	
    
	public BsConvenioach getBsConvenioach(BsConvenioempresaach bsConvenioempresaach)throws ExcepcionEnDAO{
		return getBsConvenioach(bsConvenioempresaach, null);
	}

    public BsConvenioach getBsConvenioach(BsConvenioempresaach bsConvenioempresaach,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsConvenioach bsConvenioachRel = bsConvenioempresaach.getBsConvenioach();
	     if(bsConvenioachRel==null){
                PeticionDeDatos peticionDeDatosParaBsConvenioach = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsConvenioach.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsConvenioachDao.CONVENIO, 
	     	                              bsConvenioempresaach.getConvenio()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsConvenioachRel = (BsConvenioach)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsConvenioach, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsConvenioachDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsConvenioempresaach.setBsConvenioach(bsConvenioachRel);										
		}
										
		return 	bsConvenioachRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E4.EmpresaODepto AS C4001 , E4.Convenio AS C4002 , " +
		"       E2.Convenio AS C2001 , E2.TipoConvenio AS C2002 , E2.Cliente AS C2020 , E2.Nombre AS C2003 , " +
		"       E2.Cuenta AS C2004 , E2.NombreCta AS C2005 , E2.TipoMoneda AS C2006 , E2.Descripcion AS C2007 , " +
		"       E2.PolParticipante AS C2008 , E2.Estatus AS C2009 , E2.FechaEstatus AS C2010 , E2.AutorizacionWeb AS C2011 , " +
		"       E2.Clasificacion AS C2012 , E2.ComisionXOperacionPropiaOK AS C2013 , E2.ComisionXOperacionPropiaError AS C2014 , E2.ComisionXOperacionNoPropiaOK AS C2015 , " +
		"       E2.ComisionXOperacionNoPropiaError AS C2016 , E2.MultiplesCuentas AS C2017 , E2.Activo AS C2018 , E2.bitBorrado AS C2019" +
		"  FROM BS_ConvenioEmpresaACH E4 , BS_ConvenioACH E2" +
		" WHERE E2.Convenio = E4.Convenio";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E4.EmpresaODepto AS C4001 , E4.Convenio AS C4002 , " +
		"       E2.Convenio AS C2001 , E2.TipoConvenio AS C2002 , E2.Cliente AS C2020 , E2.Nombre AS C2003 , " +
		"       E2.Cuenta AS C2004 , E2.NombreCta AS C2005 , E2.TipoMoneda AS C2006 , E2.Descripcion AS C2007 , " +
		"       E2.PolParticipante AS C2008 , E2.Estatus AS C2009 , E2.FechaEstatus AS C2010 , E2.AutorizacionWeb AS C2011 , " +
		"       E2.Clasificacion AS C2012 , E2.ComisionXOperacionPropiaOK AS C2013 , E2.ComisionXOperacionPropiaError AS C2014 , E2.ComisionXOperacionNoPropiaOK AS C2015 , " +
		"       E2.ComisionXOperacionNoPropiaError AS C2016 , E2.MultiplesCuentas AS C2017 , E2.Activo AS C2018 , E2.bitBorrado AS C2019" +
		"  FROM BS_ConvenioEmpresaACH E4 , BS_ConvenioACH E2" +
		" WHERE E2.Convenio = E4.Convenio" +
		"   AND E4.EmpresaODepto= ? " +
		"   AND E4.Convenio= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E4.EmpresaODepto AS C4001 , E4.Convenio AS C4002" +
		"  FROM BS_ConvenioEmpresaACH E4";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_ConvenioEmpresaACH";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_ConvenioEmpresaACH (" +
		"" +
		") VALUES ()";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_ConvenioEmpresaACH (" +
		"EmpresaODepto,Convenio" +
		") VALUES (?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_ConvenioEmpresaACH" +
		" SET " +
		" WHERE EmpresaODepto= ?    AND Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_ConvenioEmpresaACH" +
		" WHERE EmpresaODepto= ?    AND Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E4.EmpresaODepto AS C4001 , E4.Convenio AS C4002" +
		"  FROM BS_ConvenioEmpresaACH E4" +
		" WHERE E4.EmpresaODepto= ? " +
		"   AND E4.Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_ConvenioEmpresaACH (" +
		"    EmpresaODepto LONG NOT NULL," +
		"    Convenio LONG NOT NULL" +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsConvenioempresaach theDto = new BsConvenioempresaach();
     	theDto.setEmpresaodepto(SQLUtils.getLongFromResultSet(rst, "C4001"));
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C4002"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsConvenioempresaach</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsConvenioempresaach crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsConvenioempresaach bsConvenioempresaach = (BsConvenioempresaach) crearDtoST(rst);
        BsConvenioach bsConvenioachRel  = (BsConvenioach) BsConvenioachDaoJdbcSql.crearDtoST(rst);
        bsConvenioempresaach.setBsConvenioach(bsConvenioachRel);
		return bsConvenioempresaach;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsConvenioempresaach</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsConvenioempresaach crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsConvenioempresaach bsConvenioempresaach = (BsConvenioempresaach) crearDtoST(rst);
        BsConvenioach convenioRelbsConvenioach  = (BsConvenioach) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsConvenioempresaach.getConvenio(), conn, 
				BsConvenioachDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsConvenioempresaach.setBsConvenioach(convenioRelbsConvenioach);
		return bsConvenioempresaach;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenioempresaach theDto = (BsConvenioempresaach)theDtoUntyped;
				
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenioempresaach theDto = (BsConvenioempresaach)theDtoUntyped;
				Long empresaodepto = theDto.getEmpresaodepto();     
		Long convenio = theDto.getConvenio();     
	
		SQLUtils.setIntoStatement( 1, empresaodepto, smt);
    	
		SQLUtils.setIntoStatement( 2, convenio, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioempresaachID theId = (BsConvenioempresaachID)theIdUntyped;
			Long empresaodepto = theId.getEmpresaodepto();     
		Long convenio = theId.getConvenio();     
		     
		SQLUtils.setIntoStatement( 1, empresaodepto, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    		}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioempresaachID theId = (BsConvenioempresaachID)theIdUntyped;
				Long empresaodepto = theId.getEmpresaodepto();     
		Long convenio = theId.getConvenio();     
		     
		SQLUtils.setIntoStatement( 1, empresaodepto, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    			}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioempresaach theDto = (BsConvenioempresaach)theDtoUntyped;
				Long empresaodepto = theDto.getEmpresaodepto();     
		Long convenio = theDto.getConvenio();     
			     
		SQLUtils.setIntoStatement( 1, empresaodepto, smt);  
    		     
		SQLUtils.setIntoStatement( 2, convenio, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenioempresaach</code> objeto del tipo <code>BsConvenioempresaach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsConvenioempresaach bsConvenioempresaach) throws ExcepcionEnDAO {
	    actualizar(bsConvenioempresaach, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>bsConvenioempresaachID</code> objeto del tipo <code>BsConvenioempresaachID</code>.
	 * @return <code>BsConvenioempresaach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenioempresaach buscarPorID(BsConvenioempresaachID bsConvenioempresaachID)
			throws ExcepcionEnDAO {
		return (BsConvenioempresaach)buscarPorID(bsConvenioempresaachID, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsConvenioempresaach</code> objeto del tipo <code>BsConvenioempresaach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsConvenioempresaach bsConvenioempresaach) throws ExcepcionEnDAO {
	    crear(bsConvenioempresaach, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenioempresaachID</code> objeto del tipo <code>BsConvenioempresaachID</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(BsConvenioempresaachID bsConvenioempresaachID) throws ExcepcionEnDAO {
	    eliminar(bsConvenioempresaachID, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsConvenioempresaach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenioempresaach resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsConvenioempresaach)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsConvenioempresaachDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioempresaachDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return BsConvenioempresaachDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaachDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsConvenioempresaachDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsConvenioempresaachDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsConvenioempresaachDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsConvenioempresaachDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsConvenioempresaachDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return BsConvenioempresaachDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return BsConvenioempresaachDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsConvenioempresaachDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaachDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaachDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioempresaachDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsConvenioempresaachDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsConvenioempresaach</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
		BsConvenioempresaach dto = (BsConvenioempresaach)theDtoUntyped;
		BsConvenioempresaachID theId = (BsConvenioempresaachID)dtoIdUntyped;
		Long empresaodepto = theId.getEmpresaodepto();     
		Long convenio = theId.getConvenio();     
		dto.setEmpresaodepto(empresaodepto);
		dto.setConvenio(convenio);

	}	
	
	
}
