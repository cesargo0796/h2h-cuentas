package com.ofbox.davivienda.h2h.orm.jdbc.sql.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.sql.Connection;
import java.util.Collection;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLColumnaMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionBasica;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLImplementacionOperacionesComunes;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLLenguaje;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLOperaciones;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLUtils;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.TransaccionJdbc;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.LocalizadorDeServicio;


import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioach;
import java.lang.Long;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsConvenioachDao;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.impl.DataConValoresForaneosImpl;   
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;


import com.ofbox.davivienda.h2h.orm.jdbc.sql.impl.BsClienteDaoJdbcSql;
import com.ofbox.davivienda.h2h.abstraccion.dao.BsClienteDao;

/**
 * Clase que hereda comportamientos de {@link SQLImplementacionBasica} e implementa el comportamiento definido en la capa DAO.<br/>
 * Esta clase es una implementaci&oacute;n intermedia para el DAO de <b>BS Convenio ACH</b>
 * asociado a la tabla <b>BS_ConvenioACH</b>, en esta clase se construyen todos los sql para las operaciones de listado,
 * creaci&oacute;n, consulta, actualizaci&oacute;n y elminaci&oacute;n.<br/>
 * @author Systems Out of the Box
 * @version f3.1
 *
 */
public class BsConvenioachDaoJdbcSql extends SQLImplementacionBasica implements BsConvenioachDao{

	/**
	 * Atributo del tipo {@link SQLEntidadMeta}, que almacena la descripci&oacute;n del negocio como una entidad.<br/>
	 * Entre sus caracter&iacute;sticas tiene el nombre de la tabla, nombre de la secuencia, posici&oacute;n de las columnas,
	 * nombre de las columnas y tipos de datos para la columna.<br/>
	 * El arreglo que recibe son objetos de tipo {@link SQLColumnaMeta}, y definen informaci&oacute;n para obtener las columnas.
	 */
	public static final SQLEntidadMeta SQL_ENTIDAD_META = new SQLEntidadMeta("BS_ConvenioACH", new SQLColumnaMeta[]{
        new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2001, "Convenio", "C2001", Types.BIGINT ,true)
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2002, "TipoConvenio", "C2002", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2020, "Cliente", "C2020", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2003, "Nombre", "C2003", Types.VARCHAR )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2004, "Cuenta", "C2004", Types.VARCHAR )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2005, "NombreCta", "C2005", Types.VARCHAR )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2006, "TipoMoneda", "C2006", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2007, "Descripcion", "C2007", Types.VARCHAR )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2008, "PolParticipante", "C2008", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2009, "Estatus", "C2009", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2010, "FechaEstatus", "C2010", Types.TIMESTAMP )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2011, "AutorizacionWeb", "C2011", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2012, "Clasificacion", "C2012", Types.BIGINT )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2013, "ComisionXOperacionPropiaOK", "C2013", Types.NUMERIC )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2014, "ComisionXOperacionPropiaError", "C2014", Types.NUMERIC )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2015, "ComisionXOperacionNoPropiaOK", "C2015", Types.NUMERIC )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2016, "ComisionXOperacionNoPropiaError", "C2016", Types.NUMERIC )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2017, "MultiplesCuentas", "C2017", Types.VARCHAR )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2018, "Activo", "C2018", Types.VARCHAR )
     ,  new SQLColumnaMeta(2, "BS_ConvenioACH", "E2", 2019, "bitBorrado", "C2019", Types.VARCHAR )
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 1, "Cliente", "C1", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 2, "Nombre", "C2", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 3, "Modulos", "C3", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 4, "Direccion", "C4", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 5, "Telefono", "C5", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 6, "Fax", "C6", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 7, "Email", "C7", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 8, "NombreContacto", "C8", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 9, "ApellidoContacto", "C9", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 10, "NumInstalacion", "C10", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 11, "FechaEstatus", "C11", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 12, "Estatus", "C12", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 13, "FechaCreacion", "C13", Types.TIMESTAMP)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 14, "SolicitudPendiente", "C14", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 15, "NivelSeguridadTEx", "C15", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 16, "NivelSeguridadPImp", "C16", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 17, "nombreRepresentante", "C17", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 18, "apellidoRepresentante", "C18", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 19, "proveedorInternet", "C19", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 20, "Banca", "C20", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 21, "PoliticaCobroTEx", "C21", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 22, "ComisionTransExt", "C22", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 23, "CodigoTablaTransExt", "C23", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 24, "NivelSeguridadISSS", "C24", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 25, "NivelSeguridadAFP", "C25", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 26, "NivelSeguridadCCre", "C26", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 27, "NivelSeguridadPPag", "C27", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 28, "CargaPersonalizable", "C28", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 29, "TipoDoctoRepresentante", "C29", Types.BIGINT)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 30, "DoctoRepresentante", "C30", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 31, "Ciudad", "C31", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 32, "Cargo", "C32", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 33, "LimiteXArchivo", "C33", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 34, "LimiteXLote", "C34", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 35, "LimiteXTransaccion", "C35", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 36, "PermiteDebitos", "C36", Types.VARCHAR)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 37, "LimiteCreditos", "C37", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 38, "LimiteDebitos", "C38", Types.NUMERIC)
     ,  new SQLColumnaMeta(0, "BS_Cliente", "E0", 39, "ComisionTrnACH", "C39", Types.NUMERIC)
	});
	
    
	public BsCliente getBsCliente(BsConvenioach bsConvenioach)throws ExcepcionEnDAO{
		return getBsCliente(bsConvenioach, null);
	}

    public BsCliente getBsCliente(BsConvenioach bsConvenioach,Transaccion tx)throws ExcepcionEnDAO{
         	     
	     BsCliente bsClienteRel = bsConvenioach.getBsCliente();
	     if(bsClienteRel==null){
                PeticionDeDatos peticionDeDatosParaBsCliente = SQLImplementacionBasica.crearPeticionDeDatosST();
                peticionDeDatosParaBsCliente.agregarRestriccion(PeticionDeDatos.IGUAL,
	     	                              BsClienteDao.CLIENTE, 
	     	                              bsConvenioach.getCliente()
	     	                              );
	            TransaccionJdbc tjdbc = (TransaccionJdbc)tx;
	     		bsClienteRel = (BsCliente)SQLImplementacionOperacionesComunes.resultadoUnicoST(
							            peticionDeDatosParaBsCliente, 
										tjdbc!=null? tjdbc.getConn(): getConnection(), 
										BsClienteDaoJdbcSql.SQL_OPERACIONES, 
										tjdbc==null, 
										getLog());
				bsConvenioach.setBsCliente(bsClienteRel);										
		}
										
		return 	bsClienteRel;									
    }
    

	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
	 * junto con otras tablas foraneas, realiza un JOIN.
	 */
	public static final String SELECT_HEADER_SQL_F =  
		"SELECT E2.Convenio AS C2001 , E2.TipoConvenio AS C2002 , E2.Cliente AS C2020 , E2.Nombre AS C2003 , " +
		"       E2.Cuenta AS C2004 , E2.NombreCta AS C2005 , E2.TipoMoneda AS C2006 , E2.Descripcion AS C2007 , " +
		"       E2.PolParticipante AS C2008 , E2.Estatus AS C2009 , E2.FechaEstatus AS C2010 , E2.AutorizacionWeb AS C2011 , " +
		"       E2.Clasificacion AS C2012 , E2.ComisionXOperacionPropiaOK AS C2013 , E2.ComisionXOperacionPropiaError AS C2014 , E2.ComisionXOperacionNoPropiaOK AS C2015 , " +
		"       E2.ComisionXOperacionNoPropiaError AS C2016 , E2.MultiplesCuentas AS C2017 , E2.Activo AS C2018 , E2.bitBorrado AS C2019 , " +
		"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
		"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
		"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
		"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
		"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
		"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
		"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
		"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
		"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
		"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39" +
		"  FROM BS_ConvenioACH E2 , BS_Cliente E0" +
		" WHERE E0.Cliente = E2.Cliente";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros de la tabla principal
     * junto con otras tablas foraneas, realiza un JOIN utilizando la <code>primary key</code> de la tabla principal.
	 */
	public static final String FIND_BY_ID_SQL_F = 
		"SELECT E2.Convenio AS C2001 , E2.TipoConvenio AS C2002 , E2.Cliente AS C2020 , E2.Nombre AS C2003 , " +
		"       E2.Cuenta AS C2004 , E2.NombreCta AS C2005 , E2.TipoMoneda AS C2006 , E2.Descripcion AS C2007 , " +
		"       E2.PolParticipante AS C2008 , E2.Estatus AS C2009 , E2.FechaEstatus AS C2010 , E2.AutorizacionWeb AS C2011 , " +
		"       E2.Clasificacion AS C2012 , E2.ComisionXOperacionPropiaOK AS C2013 , E2.ComisionXOperacionPropiaError AS C2014 , E2.ComisionXOperacionNoPropiaOK AS C2015 , " +
		"       E2.ComisionXOperacionNoPropiaError AS C2016 , E2.MultiplesCuentas AS C2017 , E2.Activo AS C2018 , E2.bitBorrado AS C2019 , " +
		"       E0.Cliente AS C1 , E0.Nombre AS C2 , E0.Modulos AS C3 , E0.Direccion AS C4 , " +
		"       E0.Telefono AS C5 , E0.Fax AS C6 , E0.Email AS C7 , E0.NombreContacto AS C8 , " +
		"       E0.ApellidoContacto AS C9 , E0.NumInstalacion AS C10 , E0.FechaEstatus AS C11 , E0.Estatus AS C12 , " +
		"       E0.FechaCreacion AS C13 , E0.SolicitudPendiente AS C14 , E0.NivelSeguridadTEx AS C15 , E0.NivelSeguridadPImp AS C16 , " +
		"       E0.nombreRepresentante AS C17 , E0.apellidoRepresentante AS C18 , E0.proveedorInternet AS C19 , E0.Banca AS C20 , " +
		"       E0.PoliticaCobroTEx AS C21 , E0.ComisionTransExt AS C22 , E0.CodigoTablaTransExt AS C23 , E0.NivelSeguridadISSS AS C24 , " +
		"       E0.NivelSeguridadAFP AS C25 , E0.NivelSeguridadCCre AS C26 , E0.NivelSeguridadPPag AS C27 , E0.CargaPersonalizable AS C28 , " +
		"       E0.TipoDoctoRepresentante AS C29 , E0.DoctoRepresentante AS C30 , E0.Ciudad AS C31 , E0.Cargo AS C32 , " +
		"       E0.LimiteXArchivo AS C33 , E0.LimiteXLote AS C34 , E0.LimiteXTransaccion AS C35 , E0.PermiteDebitos AS C36 , " +
		"       E0.LimiteCreditos AS C37 , E0.LimiteDebitos AS C38 , E0.ComisionTrnACH AS C39" +
		"  FROM BS_ConvenioACH E2 , BS_Cliente E0" +
		" WHERE E0.Cliente = E2.Cliente" +
		"   AND E2.Convenio= ? ";
	

	/**
	 * Atributo que almacena el <code>String</code> para obtener el listado de registros del negocio.
	 */
	public static final String SELECT_HEADER_SQL =  
		"SELECT E2.Convenio AS C2001 , E2.TipoConvenio AS C2002 , E2.Cliente AS C2020 , E2.Nombre AS C2003 , " +
		"       E2.Cuenta AS C2004 , E2.NombreCta AS C2005 , E2.TipoMoneda AS C2006 , E2.Descripcion AS C2007 , " +
		"       E2.PolParticipante AS C2008 , E2.Estatus AS C2009 , E2.FechaEstatus AS C2010 , E2.AutorizacionWeb AS C2011 , " +
		"       E2.Clasificacion AS C2012 , E2.ComisionXOperacionPropiaOK AS C2013 , E2.ComisionXOperacionPropiaError AS C2014 , E2.ComisionXOperacionNoPropiaOK AS C2015 , " +
		"       E2.ComisionXOperacionNoPropiaError AS C2016 , E2.MultiplesCuentas AS C2017 , E2.Activo AS C2018 , E2.bitBorrado AS C2019" +
		"  FROM BS_ConvenioACH E2";
	/**
	 * Atributo que almacena el <code>String</code> para obtener el n&uacute;mero de registros presentes en el negocio.
	 */
	public static final String COUNT_HEADER_SQL =   "SELECT COUNT(*) FROM BS_ConvenioACH";    												
	/**
	 * Atributo que almacena el <code>String</code> para la inserci&oacute;n, sin id, de un nuevo registro en la tabla.
	 */
	public static final String CREATE_SQL = 
		"INSERT INTO BS_ConvenioACH (" +
		"TipoConvenio,Cliente,Nombre,Cuenta,NombreCta" +
		",TipoMoneda,Descripcion,PolParticipante,Estatus" +
		",FechaEstatus,AutorizacionWeb,Clasificacion,ComisionXOperacionPropiaOK" +
		",ComisionXOperacionPropiaError,ComisionXOperacionNoPropiaOK,ComisionXOperacionNoPropiaError,MultiplesCuentas" +
		",Activo,bitBorrado" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para insertar, con id, un registro en la table.
	 */
	public static final String CREATE_SQL_CON_ID = 
		"INSERT INTO BS_ConvenioACH (" +
		"Convenio,TipoConvenio,Cliente,Nombre,Cuenta" +
		",NombreCta,TipoMoneda,Descripcion,PolParticipante" +
		",Estatus,FechaEstatus,AutorizacionWeb,Clasificacion" +
		",ComisionXOperacionPropiaOK,ComisionXOperacionPropiaError,ComisionXOperacionNoPropiaOK,ComisionXOperacionNoPropiaError" +
		",MultiplesCuentas,Activo,bitBorrado" +
		") VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	/**
	 * Atributo que almacena el <code>String</code> para la atualizaci&oacute; de un registro en la tabla.
	 */
	public static final String UPDATE_SQL = 
		"UPDATE BS_ConvenioACH" +
		" SET TipoConvenio= ?  , Cliente= ?  , Nombre= ?  , Cuenta= ?  , " +
		"NombreCta= ?  , TipoMoneda= ?  , Descripcion= ?  , PolParticipante= ?  , " +
		"Estatus= ?  , FechaEstatus= ?  , AutorizacionWeb= ?  , Clasificacion= ?  , " +
		"ComisionXOperacionPropiaOK= ?  , ComisionXOperacionPropiaError= ?  , ComisionXOperacionNoPropiaOK= ?  , ComisionXOperacionNoPropiaError= ?  , " +
		"MultiplesCuentas= ?  , Activo= ?  , bitBorrado= ? " +
		" WHERE Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la eliminaci&oacute; de un registro en la tabla.
	 */
	public static final String DELETE_SQL =  
		"DELETE FROM BS_ConvenioACH" +
		" WHERE Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la b&uacute;squeda por id de un registro de la tabla.
	 */
	public static final String FIND_BY_ID_SQL = 
		"SELECT E2.Convenio AS C2001 , E2.TipoConvenio AS C2002 , E2.Cliente AS C2020 , E2.Nombre AS C2003 , " +
		"       E2.Cuenta AS C2004 , E2.NombreCta AS C2005 , E2.TipoMoneda AS C2006 , E2.Descripcion AS C2007 , " +
		"       E2.PolParticipante AS C2008 , E2.Estatus AS C2009 , E2.FechaEstatus AS C2010 , E2.AutorizacionWeb AS C2011 , " +
		"       E2.Clasificacion AS C2012 , E2.ComisionXOperacionPropiaOK AS C2013 , E2.ComisionXOperacionPropiaError AS C2014 , E2.ComisionXOperacionNoPropiaOK AS C2015 , " +
		"       E2.ComisionXOperacionNoPropiaError AS C2016 , E2.MultiplesCuentas AS C2017 , E2.Activo AS C2018 , E2.bitBorrado AS C2019" +
		"  FROM BS_ConvenioACH E2" +
		" WHERE E2.Convenio= ? ";
	/**
	 * Atributo que almacena el <code>String</code> para la creacion de la tabla en la base de datos.
	 */
	public static final String CREATE_TABLE_DDL = 
		"CREATE TABLE BS_ConvenioACH (" +
		"    Convenio LONG NOT NULL," +
		"    TipoConvenio LONG NOT NULL," +
		"    Cliente LONG NOT NULL," +
		"    Nombre VARCHAR(50)  NOT NULL," +
		"    Cuenta VARCHAR(16)  NOT NULL," +
		"    NombreCta VARCHAR(50)  NOT NULL," +
		"    TipoMoneda LONG NOT NULL," +
		"    Descripcion VARCHAR(50)  NOT NULL," +
		"    PolParticipante LONG NOT NULL," +
		"    Estatus LONG NOT NULL," +
		"    FechaEstatus TIMESTAMP NOT NULL," +
		"    AutorizacionWeb LONG," +
		"    Clasificacion LONG NOT NULL," +
		"    ComisionXOperacionPropiaOK NUMERIC(19, 0)  NOT NULL," +
		"    ComisionXOperacionPropiaError NUMERIC(19, 0) ," +
		"    ComisionXOperacionNoPropiaOK NUMERIC(19, 0) ," +
		"    ComisionXOperacionNoPropiaError NUMERIC(19, 0) ," +
		"    MultiplesCuentas VARCHAR(1) ," +
		"    Activo VARCHAR(1) ," +
		"    bitBorrado VARCHAR(1) " +
		")";
	/**
	 * Atributo que almacena un objeto del tipo {@link SQLLenguaje}.
	 */
	private static SQLLenguaje lenguaje;
	
	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>Object</code> - representa al DTO creado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static Object crearDtoST(ResultSet rst)
			throws SQLException, ExcepcionEnDAO {
 	    BsConvenioach theDto = new BsConvenioach();
     	theDto.setConvenio(SQLUtils.getLongFromResultSet(rst, "C2001"));
     	theDto.setTipoconvenio(SQLUtils.getLongFromResultSet(rst, "C2002"));
     	theDto.setCliente(SQLUtils.getLongFromResultSet(rst, "C2020"));
		theDto.setNombre(rst.getString("C2003"));
		theDto.setCuenta(rst.getString("C2004"));
		theDto.setNombrecta(rst.getString("C2005"));
     	theDto.setTipomoneda(SQLUtils.getLongFromResultSet(rst, "C2006"));
		theDto.setDescripcion(rst.getString("C2007"));
     	theDto.setPolparticipante(SQLUtils.getLongFromResultSet(rst, "C2008"));
     	theDto.setEstatus(SQLUtils.getLongFromResultSet(rst, "C2009"));
		theDto.setFechaestatus(rst.getTimestamp("C2010"));
     	theDto.setAutorizacionweb(SQLUtils.getLongFromResultSet(rst, "C2011"));
     	theDto.setClasificacion(SQLUtils.getLongFromResultSet(rst, "C2012"));
		theDto.setComisionxoperacionpropiaok(rst.getBigDecimal("C2013"));
		theDto.setComisionxoperacionpropiaerror(rst.getBigDecimal("C2014"));
		theDto.setComisionxoperacionnopropiaok(rst.getBigDecimal("C2015"));
		theDto.setComisionxoperacionnopropiaerror(rst.getBigDecimal("C2016"));
		theDto.setMultiplescuentas(rst.getString("C2017"));
		theDto.setActivo(rst.getString("C2018"));
		theDto.setBitborrado(rst.getString("C2019"));
		return theDto;
	}
	
	


	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir del resultado de alguna consulta.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsConvenioach</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsConvenioach crearDataConValoresForaneosST(ResultSet rst)throws SQLException, ExcepcionEnDAO{
		BsConvenioach bsConvenioach = (BsConvenioach) crearDtoST(rst);
        BsCliente bsClienteRel  = (BsCliente) BsClienteDaoJdbcSql.crearDtoST(rst);
        bsConvenioach.setBsCliente(bsClienteRel);
		return bsConvenioach;
	}




	/**
	 * M&eacute;todo que realiza la creaci&oacute;n de un DTO foraneo, del negocio, a partir de la b&uacute;squeda por <code>id</code>.
	 * @param <code>rst</code> objeto de tipo <code>Resultset</code>, que contiene los resultados de la consulta.
	 * @return <code>BsConvenioach</code> - representa al DTO del negocio con el nuevo DTO foraneo asociado.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static BsConvenioach crearDataConValoresForaneosNoRelST(ResultSet rst, Connection conn)throws SQLException, ExcepcionEnDAO{	
		BsConvenioach bsConvenioach = (BsConvenioach) crearDtoST(rst);
        BsCliente clienteRelbsCliente  = (BsCliente) SQLImplementacionOperacionesComunes.buscarPorIDST(
                bsConvenioach.getCliente(), conn, 
				BsClienteDaoJdbcSql.SQL_OPERACIONES,
	       		false, 
	       		null);
        bsConvenioach.setBsCliente(clienteRelbsCliente);
		return bsConvenioach;
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * no toma en cuenta el valor del <code>id</code>, puesto que se asume que es un valor secuencial
	 * autonumerico o serial propio de la base de datos.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenioach theDto = (BsConvenioach)theDtoUntyped;
				Long tipoconvenio = theDto.getTipoconvenio();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String cuenta = theDto.getCuenta();     
		String nombrecta = theDto.getNombrecta();     
		Long tipomoneda = theDto.getTipomoneda();     
		String descripcion = theDto.getDescripcion();     
		Long polparticipante = theDto.getPolparticipante();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long autorizacionweb = theDto.getAutorizacionweb();     
		Long clasificacion = theDto.getClasificacion();     
		java.math.BigDecimal comisionxoperacionpropiaok = theDto.getComisionxoperacionpropiaok();     
		java.math.BigDecimal comisionxoperacionpropiaerror = theDto.getComisionxoperacionpropiaerror();     
		java.math.BigDecimal comisionxoperacionnopropiaok = theDto.getComisionxoperacionnopropiaok();     
		java.math.BigDecimal comisionxoperacionnopropiaerror = theDto.getComisionxoperacionnopropiaerror();     
		String multiplescuentas = theDto.getMultiplescuentas();     
		String activo = theDto.getActivo();     
		String bitborrado = theDto.getBitborrado();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
    		     
		SQLUtils.setIntoStatement( 2, cliente, smt);  
    		     
		SQLUtils.setIntoStatement( 3, nombre, smt);  
    		     
		SQLUtils.setIntoStatement( 4, cuenta, smt);  
    		     
		SQLUtils.setIntoStatement( 5, nombrecta, smt);  
    		     
		SQLUtils.setIntoStatement( 6, tipomoneda, smt);  
    		     
		SQLUtils.setIntoStatement( 7, descripcion, smt);  
    		     
		SQLUtils.setIntoStatement( 8, polparticipante, smt);  
    		     
		SQLUtils.setIntoStatement( 9, estatus, smt);  
    		     
		SQLUtils.setIntoStatement( 10, fechaestatus, smt);  
    		     
		SQLUtils.setIntoStatement( 11, autorizacionweb, smt);  
    		     
		SQLUtils.setIntoStatement( 12, clasificacion, smt);  
    		     
		SQLUtils.setIntoStatement( 13, comisionxoperacionpropiaok, smt);  
    		     
		SQLUtils.setIntoStatement( 14, comisionxoperacionpropiaerror, smt);  
    		     
		SQLUtils.setIntoStatement( 15, comisionxoperacionnopropiaok, smt);  
    		     
		SQLUtils.setIntoStatement( 16, comisionxoperacionnopropiaerror, smt);  
    		     
		SQLUtils.setIntoStatement( 17, multiplescuentas, smt);  
    		     
		SQLUtils.setIntoStatement( 18, activo, smt);  
    		     
		SQLUtils.setIntoStatement( 19, bitborrado, smt);  
    		
	}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones}, este m&eacute;todo
	 * toma en cuenta el <code>id</code>, lo que quiere decir que se usa al momento de  crear registros
	 * en donde el identificador no es un valor secuencial autonumerico o serial propio de la base de datos
	 *
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaCrearConIdST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {			
	        BsConvenioach theDto = (BsConvenioach)theDtoUntyped;
				Long convenio = theDto.getConvenio();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String cuenta = theDto.getCuenta();     
		String nombrecta = theDto.getNombrecta();     
		Long tipomoneda = theDto.getTipomoneda();     
		String descripcion = theDto.getDescripcion();     
		Long polparticipante = theDto.getPolparticipante();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long autorizacionweb = theDto.getAutorizacionweb();     
		Long clasificacion = theDto.getClasificacion();     
		java.math.BigDecimal comisionxoperacionpropiaok = theDto.getComisionxoperacionpropiaok();     
		java.math.BigDecimal comisionxoperacionpropiaerror = theDto.getComisionxoperacionpropiaerror();     
		java.math.BigDecimal comisionxoperacionnopropiaok = theDto.getComisionxoperacionnopropiaok();     
		java.math.BigDecimal comisionxoperacionnopropiaerror = theDto.getComisionxoperacionnopropiaerror();     
		String multiplescuentas = theDto.getMultiplescuentas();     
		String activo = theDto.getActivo();     
		String bitborrado = theDto.getBitborrado();     
	
		SQLUtils.setIntoStatement( 1, convenio, smt);
    	
		SQLUtils.setIntoStatement( 2, tipoconvenio, smt);
    	
		SQLUtils.setIntoStatement( 3, cliente, smt);
    	
		SQLUtils.setIntoStatement( 4, nombre, smt);
    	
		SQLUtils.setIntoStatement( 5, cuenta, smt);
    	
		SQLUtils.setIntoStatement( 6, nombrecta, smt);
    	
		SQLUtils.setIntoStatement( 7, tipomoneda, smt);
    	
		SQLUtils.setIntoStatement( 8, descripcion, smt);
    	
		SQLUtils.setIntoStatement( 9, polparticipante, smt);
    	
		SQLUtils.setIntoStatement( 10, estatus, smt);
    	
		SQLUtils.setIntoStatement( 11, fechaestatus, smt);
    	
		SQLUtils.setIntoStatement( 12, autorizacionweb, smt);
    	
		SQLUtils.setIntoStatement( 13, clasificacion, smt);
    	
		SQLUtils.setIntoStatement( 14, comisionxoperacionpropiaok, smt);
    	
		SQLUtils.setIntoStatement( 15, comisionxoperacionpropiaerror, smt);
    	
		SQLUtils.setIntoStatement( 16, comisionxoperacionnopropiaok, smt);
    	
		SQLUtils.setIntoStatement( 17, comisionxoperacionnopropiaerror, smt);
    	
		SQLUtils.setIntoStatement( 18, multiplescuentas, smt);
    	
		SQLUtils.setIntoStatement( 19, activo, smt);
    	
		SQLUtils.setIntoStatement( 20, bitborrado, smt);
    		
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido en el objeto PreparedStatement, para poder ser eliminado.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaEliminarST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
		SQLUtils.setIntoStatement( 1, theId, smt);
	}

	/**
	 * M&eacute;todo que coloca el identificador recibido, en el objeto PreparedStatement para poder hacer la consulta de busqueda.
	 * @param <code>theIdUntyped</code> objeto sin tipo que identifica al <code>id</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaBuscarPorIdST(Object theIdUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			Long theId = (Long)theIdUntyped;
			SQLUtils.setIntoStatement( 1, theId, smt);
		}

	/**
	 * M&eacute;todo que coloca los valores provenientes del objeto Dto en el objeto PreparedStatement,
	 * el cual proviene de la llamada a la implementacion de {@link SQLOperaciones} para poder actualizar
     * un registro en espec&iacute;fico.
	 * 
	 * @param <code>theDtoUntyped</code> objeto sin tipo que almacena al DTO del negocio.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaActualizarST(Object theDtoUntyped, PreparedStatement smt)
			    throws SQLException, ExcepcionEnDAO {
			BsConvenioach theDto = (BsConvenioach)theDtoUntyped;
				Long convenio = theDto.getConvenio();     
		Long tipoconvenio = theDto.getTipoconvenio();     
		Long cliente = theDto.getCliente();     
		String nombre = theDto.getNombre();     
		String cuenta = theDto.getCuenta();     
		String nombrecta = theDto.getNombrecta();     
		Long tipomoneda = theDto.getTipomoneda();     
		String descripcion = theDto.getDescripcion();     
		Long polparticipante = theDto.getPolparticipante();     
		Long estatus = theDto.getEstatus();     
		java.util.Date fechaestatus = theDto.getFechaestatus();     
		Long autorizacionweb = theDto.getAutorizacionweb();     
		Long clasificacion = theDto.getClasificacion();     
		java.math.BigDecimal comisionxoperacionpropiaok = theDto.getComisionxoperacionpropiaok();     
		java.math.BigDecimal comisionxoperacionpropiaerror = theDto.getComisionxoperacionpropiaerror();     
		java.math.BigDecimal comisionxoperacionnopropiaok = theDto.getComisionxoperacionnopropiaok();     
		java.math.BigDecimal comisionxoperacionnopropiaerror = theDto.getComisionxoperacionnopropiaerror();     
		String multiplescuentas = theDto.getMultiplescuentas();     
		String activo = theDto.getActivo();     
		String bitborrado = theDto.getBitborrado();     
		     
		SQLUtils.setIntoStatement( 1, tipoconvenio, smt);  
        	     
		SQLUtils.setIntoStatement( 2, cliente, smt);  
        	     
		SQLUtils.setIntoStatement( 3, nombre, smt);  
        	     
		SQLUtils.setIntoStatement( 4, cuenta, smt);  
        	     
		SQLUtils.setIntoStatement( 5, nombrecta, smt);  
        	     
		SQLUtils.setIntoStatement( 6, tipomoneda, smt);  
        	     
		SQLUtils.setIntoStatement( 7, descripcion, smt);  
        	     
		SQLUtils.setIntoStatement( 8, polparticipante, smt);  
        	     
		SQLUtils.setIntoStatement( 9, estatus, smt);  
        	     
		SQLUtils.setIntoStatement( 10, fechaestatus, smt);  
        	     
		SQLUtils.setIntoStatement( 11, autorizacionweb, smt);  
        	     
		SQLUtils.setIntoStatement( 12, clasificacion, smt);  
        	     
		SQLUtils.setIntoStatement( 13, comisionxoperacionpropiaok, smt);  
        	     
		SQLUtils.setIntoStatement( 14, comisionxoperacionpropiaerror, smt);  
        	     
		SQLUtils.setIntoStatement( 15, comisionxoperacionnopropiaok, smt);  
        	     
		SQLUtils.setIntoStatement( 16, comisionxoperacionnopropiaerror, smt);  
        	     
		SQLUtils.setIntoStatement( 17, multiplescuentas, smt);  
        	     
		SQLUtils.setIntoStatement( 18, activo, smt);  
        	     
		SQLUtils.setIntoStatement( 19, bitborrado, smt);  
        		     
		SQLUtils.setIntoStatement( 20, convenio, smt);  
    			
	}


	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
	 * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
	 * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	
	
	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de datos a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaPeticionDeDatosFST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);	
	}	

	/**
	 * M&eacute;todo que ejecuta una petici&oacute;n de conteo a trav&eacute;s de un objeto {@link PreparedStatement},
     * y un objeto del tipo {@link PeticionDeDatos}, el cual en su implementaci&oacute;n define una serie de restricciones
     * usadas para hacer la petici&oacute;n.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>.
	 * @param <code>smt</code> objeto de tipo <code>PreparedStatement</code>.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static void operacionesParaConteoST(PeticionDeDatos peticion,
			PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
		SQLImplementacionOperacionesComunes.operacionesParaPeticionDeDatosST( peticion, smt, SQL_OPERACIONES, null);
	}

	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_WHERE);
	}
	
	
	
	/**
	 * M&eacute;todo que agrega al atributo {@link #SELECT_HEADER_SQL_F} las condiciones de b&uacute;squeda
     * que vienen en el objeto del tipo {@link PeticionDeDatos}
     *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlParaPeticionDeDatosFST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return SELECT_HEADER_SQL_F + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_ALIAS_ORDEN_AND);
	}
	

	

	/**
	 * M&eacute;todo que agrega al atributo {@link #COUNT_HEADER_SQL} las condiciones de conteo
	 * que vienen en el objeto de tipo {@link PeticionDeDatos}.
	 *
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>String</code> - la consulta condicionada como cadena de caracteres.
	 * @throws SQLException
	 * @throws ExcepcionEnDAO
	 */
	public static String getSqlDeConteoST(PeticionDeDatos peticion)
			throws SQLException, ExcepcionEnDAO {
		return COUNT_HEADER_SQL + SQLImplementacionOperacionesComunes.crearCondicionesSQLST( peticion, SQL_OPERACIONES, SQLImplementacionOperacionesComunes.COND_SQL_WHERE);
	}
	
	/**
	 * M&eacute;todo que se utiliza para actualizar un registro a nivel de tabla, recibe como par&aacute;metro un DTO del negocio
	 * que contiene la informaci&oacute;n a actualizar, e invoca la funci&oacute;n <code>actualizar</code> de la clase de la que hereda.
	 *
	 * @param <code>bsConvenioach</code> objeto del tipo <code>BsConvenioach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void actualizar(BsConvenioach bsConvenioach) throws ExcepcionEnDAO {
	    actualizar(bsConvenioach, AUTO_COMMIT);
	
	}
	
	/**
	 * M&eacute;todo utilizado para realizar una b&uacute;squeda de un registro espec&iacute;fico, utilizando un identificador.
	 *
	 * @param <code>convenioId</code> objeto del tipo <code>Long</code>.
	 * @return <code>BsConvenioach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenioach buscarPorID(Long convenioId)
			throws ExcepcionEnDAO {
		return (BsConvenioach)buscarPorID(convenioId, AUTO_COMMIT);				 
	}

	/**
	 * M&eacute;todo utilizado para la creaci&oacute;n de un registro, a nivel de tabla, a partir de la informaci&oacute;n
	 * recibida en un DTO del objeto de Negocio, e invoca la funci&oacute;n <code>crear</code> de la clase de la que hereda.
	 * 
	 * @param <code>bsConvenioach</code> objeto del tipo <code>BsConvenioach</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void crear(BsConvenioach bsConvenioach) throws ExcepcionEnDAO {
	    crear(bsConvenioach, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para la eliminaci&oacute;n de un registro, a nivel de tabla,
	 * que se busca a trav&eacute;s del identificador recibido. Se invoca la funci&oacute;n <code>eliminar</code> de la clase de la que hereda.
	 *
	 * @param <code>convenioId</code> objeto del tipo <code>Long</code>.
	 * @throws ExcepcionEnDAO
	 * @see {@link SQLImplementacionBasica}
	 */
	public void eliminar(Long convenioId) throws ExcepcionEnDAO {
	    eliminar(convenioId, AUTO_COMMIT);		
	}

	/**
	 * M&eacute;todo utilizado para devolver un &uacute;nico resultado de la petici&oacute;n recibida,
	 * la cual se realiza a trav&eacuate;s del objeto de tipo {@link PeticionDeDatos}.
	 * 
	 * @param <code>peticion</code> objeto de tipo <code>PeticionDeDatos</code>, que contiene las condiciones de la consulta.
	 * @return <code>BsConvenioach</code> - un objeto DTO.
	 * @throws ExcepcionEnDAO
	 * @throws ExcepcionPorDatoNoEncontrado
	 * @see {@link SQLImplementacionBasica}
	 */
	public BsConvenioach resultadoUnico(PeticionDeDatos peticion)
			throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado {
		return (BsConvenioach)resultadoUnico(peticion, AUTO_COMMIT);
	}
	
	/**
	 * Atributo del tipo {@link SQLOperaciones}, almacena la operaciones disponibles para esta implementaci&oacute;n.<br/>
	 * Algunas de las llamadas a los m&eacute;todos se realiza a trav&eacute;s de la invocaci&oacute;n de estas implementaciones.
	 */
    public static final SQLOperaciones	SQL_OPERACIONES =  new SQLOperaciones() {
			
			public int getManejoForaneosPorDefecto() {
				return Dao.FORANEOS_JOIN;
			}
			
			
			public void operacionesParaPeticionDeDatosF(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaPeticionDeDatosFST(request, smt);
			}
			
			public void operacionesParaPeticionDeDatos(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaPeticionDeDatosST(request, smt);
			}
			
			public void operacionesParaEliminar(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaEliminarST(theId, smt);
			}
			
			public void operacionesParaCrear(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaCrearST(theDto, smt);
			}
			
			public void operacionesParaCrearConId(Object theDto, PreparedStatement smt) 
			        throws SQLException, ExcepcionEnDAO {
			    BsConvenioachDaoJdbcSql.operacionesParaCrearConIdST(theDto, smt);
            }
			
			public void operacionesParaConteo(PeticionDeDatos request,
					PreparedStatement smt) throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaConteoST(request, smt);				
			}
			
			public void operacionesParaBuscarPorId(Object theId, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaBuscarPorIdST(theId, smt);
			}
			
			public void operacionesParaActualizar(Object theDto, PreparedStatement smt)
					throws SQLException, ExcepcionEnDAO {
				BsConvenioachDaoJdbcSql.operacionesParaActualizarST(theDto, smt);
				
			}
			
			public String getSqlParaPeticionDeDatosF(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {	
				return BsConvenioachDaoJdbcSql.getSqlParaPeticionDeDatosFST(request);
			}
			
			public String getSqlParaPeticionDeDatos(PeticionDeDatos request)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioachDaoJdbcSql.getSqlParaPeticionDeDatosST(request);
			}
			
			public String getSqlDeConteo(PeticionDeDatos request) throws SQLException,
					ExcepcionEnDAO {
				return BsConvenioachDaoJdbcSql.getSqlDeConteoST(request);
			}
			
			public SQLEntidadMeta getSQLEntidadMeta() throws ExcepcionEnDAO {
				return BsConvenioachDaoJdbcSql.SQL_ENTIDAD_META;
			}
			
			public String getEliminarSql() {
				return  BsConvenioachDaoJdbcSql.DELETE_SQL;
			}
			
			public String getCrearSql() {
				return  BsConvenioachDaoJdbcSql.CREATE_SQL;
			}
			
			public String getCrearSqlConId(){
			    return  BsConvenioachDaoJdbcSql.CREATE_SQL_CON_ID;
			}			
			
			public String getBuscarPorIdSqlF() {
				return BsConvenioachDaoJdbcSql.FIND_BY_ID_SQL_F;
			}
			
			public String getBuscarPorIdSql() {				
				return BsConvenioachDaoJdbcSql.FIND_BY_ID_SQL;
			}
			
			public String getActualizarSql() {				
				return BsConvenioachDaoJdbcSql.UPDATE_SQL;
			}
			
			public Object crearDto(ResultSet rst) throws SQLException, ExcepcionEnDAO {
				return BsConvenioachDaoJdbcSql.crearDtoST(rst);
			}
	        
	        public Object crearDataConValoresForaneos(ResultSet rst)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioachDaoJdbcSql.crearDataConValoresForaneosST(rst);
			}
			
	        public Object crearDataConValoresForaneosNoRel(ResultSet rst, Connection conn)
					throws SQLException, ExcepcionEnDAO {
				return BsConvenioachDaoJdbcSql.crearDataConValoresForaneosNoRelST(rst, conn);
			}
			
			
            public boolean isForaneosDisponible(){
				return true;
            
            }
            
            public SQLLenguaje getSQLLenguaje() {
                return BsConvenioachDaoJdbcSql.getLenguaje();
            }
            
            public void setIdEnDto(Object theDto, Object dtoId) {
                setIdEnDtoST(theDto, dtoId);
            }
	};
	
	
	
	
	
	/**
	 * Devuelve el atributo {@link #SQL_OPERACIONES}.
	 * @return <code>SQLOperaciones</code> - objeto del tipo {@link SQLOperaciones}
	 */
    public SQLOperaciones getSqlOperaciones() {
       return SQL_OPERACIONES;
	}
	
	/**
	 * Devuelve el atributo {@link #lenguaje}
	 * @return <code>SQLLenguaje</code> - objeto del tipo {@link SQLLenguaje}
	 */	
	public static SQLLenguaje getLenguaje() {
		return lenguaje;
	}
	
	/**
	 * M&eacute;todo que permite obtener a trav&eacute;s de un Servicio la implementaci&oacute;n del objeto de Lenguaje,
	 * y coloca las reglas de negocio almacenadas en el atributo {@link #SQL_ENTIDAD_META}.
	 * @param <code>localizadorLenguaje</code> objeto del tipo {@link LocalizadorDeServicio} que posee informaci&oacute;n para trabajar con la base de datos.
	 */
	public static void setLocalizadorLenguaje(LocalizadorDeServicio localizadorLenguaje) {
		lenguaje = (SQLLenguaje) localizadorLenguaje.getServicio();
		lenguaje.setSqlEntidadMeta(SQL_ENTIDAD_META);
	}

	/**
	 * M&eacute;todo que agrega un identificador a un DTO del tipo <code>BsConvenioach</code>.
	 * @param <code>theDtoUntyped</code> objeto gen&eacute;rico, que representa a un <code>DTO</code>.
	 * @param <code>dtoIdUntyped</code> objeto gen&eacute;rico, que representa al identificador.
	 */
	public static void setIdEnDtoST(Object theDtoUntyped, Object dtoIdUntyped){
	    BsConvenioach dto = (BsConvenioach)theDtoUntyped;
        Long dtoId = (Long) dtoIdUntyped;;
        dto.setConvenio(dtoId);

	}	
	
	
}
