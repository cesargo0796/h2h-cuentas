   package com.ofbox.davivienda.h2h.abstraccion.negocio;

import com.ofbox.davivienda.h2h.abstraccion.dao.HthPagoServiciosDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthPagoServicios;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadForaneoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.flujo.ProximaEjecucion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.ExcepcionEnNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.EnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;

public class HthPagoServiciosNegocio extends NegocioDesacoplado{
	protected Log log = LogServiceLocator.getDefaultImplementation().getLog(HthPagoServiciosNegocio.class);
	public static final DtoMeta DTO_META = new DtoMeta(H2hResolusor.getSingleton().getContenedorDeDtoMeta(), "hthPagoServicios",  HthPagoServicios.class,	
			new DtoPropiedadMeta[]{
        new DtoPropiedadMeta(HthPagoServiciosDao.IDPAGOSERVICIO,"idPagoServicio",DtoPropiedadMeta.DATO_LLAVE , 10) 
        ,  new DtoPropiedadMeta(HthPagoServiciosDao.IDCLIENTE,"idCliente",DtoPropiedadMeta.DATO_BUSQUEDA,
        		new DtoPropiedadForaneoMeta( "bsCliente","cliente","nombre") , false,10)
        ,  new DtoPropiedadMeta(HthPagoServiciosDao.NUMCUENTA,"numCuenta",DtoPropiedadMeta.NO_NULL , 50) 
        ,  new DtoPropiedadMeta(HthPagoServiciosDao.NIT,"nit",DtoPropiedadMeta.NO_NULL , 50)  
        ,  new DtoPropiedadMeta(HthPagoServiciosDao.ESTADO,"estado",DtoPropiedadMeta.NO_NULL , 10)       

	       },
            DtoMeta.OPERACIONES_BUSQUEDAS_COMPLETAS
    );
	
	public Log getLogNegocio() {
		return log;
	}
	
	public static final CreadorDeEnlaceDetalle[] CREADORES_DETALLE = new CreadorDeEnlaceDetalle[] {
				
	};
	
	public static final  CreadorDeEnlaceBusqueda[] CREADORES_BUSQUEDA = new CreadorDeEnlaceBusqueda[]{
	        new CreadorDeEnlaceBusquedaDinImpl(DTO_META, BsClienteNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "cliente",
	                      "nombre",
	                      "modulos",
	                      "direccion",
	                      " telefono",
	                      "fax",
	                      "email",
	                      "nombrecontacto",
	                      "apellidocontacto",
	                      "numinstalacion",
	                      "fechaestatus",
	                      "estatus",
	                      "fechacreacion",
	                      "solicitudpendiente",
	                      "nivelseguridadtex",
	                      "nivelseguridadPIMP",
	                      "nombrerepresentante",
	                      "apellidorepresentante",
	                      "proveedorinternet",
	                      "banca",
	                      "politicacobrotex",
	                      "comisiontransext",
	                      "codigotablatransext",
	                      "nivelseguridadisss",
	                      "nivelseguridadafp",
	                      "nivelseguridadccre",
	                      "nivelseguridadppag",
	                      "cargapersonalizable",
	                      "tipodoctorepresentante",
	                      "doctorepresentante",
	                      "ciudad",
	                      "cargo",
	                      "limitexarchivo",
	                      "limitexlote",
	                      "limitextransaccion",
	                      "permitedebitos",
	                      "limitecreditos",
	                      "limitedebitos",
	                      "comisiontrnach",                       
                };	
                }
		     }      
	   };  
	
	private HthPagoServiciosDao hthPagoServicioDao;
    
	public HthPagoServiciosDao getHthPagoServiciosDao() {
		if(hthPagoServicioDao == null){
			hthPagoServicioDao = (HthPagoServiciosDao)getDao();
		}
		return hthPagoServicioDao;
	}  
	
	
    private ValidadorDeDatos validadorDeDatos = new ValidadorDeDatosSobreDtoMeta(DTO_META);
	
	private CreadorDeFiltro creadorDeFiltro = new CreadorDeFiltroSobreDtoMeta(DTO_META);
	
	private CreadorDeOrdenamiento creadorDeOrdenamiento = new CreadorDeOrdenamientoSobreDtoMeta(DTO_META);
	
	private NegocioOperaciones negocioOperaciones = new NegocioOperacionesBase(CREADORES_DETALLE, CREADORES_BUSQUEDA, DTO_META, H2hResolusor.getSingleton());
	
     
	
	
	public NegocioOperaciones getNegocioOperaciones() {
		return negocioOperaciones;
	}	
	
	
	public ValidadorDeDatos getValidadorDeDatos() {
		return  validadorDeDatos;
	}
	
	
	public CreadorDeFiltro getCreadorDeFiltro(){
	    return creadorDeFiltro;
	}
	
	
	public CreadorDeOrdenamiento getCreadorDeOrdenamiento(){
		return creadorDeOrdenamiento;
	}
	
	
	public LocalizadorDeServiciosAplicacion getLocalizadorDeServiciosAplicacion(){
		return H2hResolusor.getSingleton();
	}
	
	@Override
	public ProximaEjecucion doCrear(DatosAplicacion datosAplicacion)
			throws Exception {
		HthPagoServicios dto = new HthPagoServicios();
		guardarDtoSiguienteCapa(datosAplicacion, dto);
		
		return super.doCrear(datosAplicacion);
	}
	
	
	@Override
	public void ambientarBusqueda(Object dto, String nombreBusqueda,
			EstadoAplicacion estadoAplicacion) throws ExcepcionEnNegocio {
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		CreadorDeEnlaceBusqueda cemd = negocioOperaciones.getCreadorDeEnlaceBusqueda(nombreBusqueda);
		EnlaceBusqueda enlace = cemd.crearEnlaceBusqueda(dto);
		EstadoNegocio estadoNegocio =  negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		enlace.setEstadoNegocio(estadoNegocio.getEstado());
		LocalizadorDeServiciosAplicacion locApp =  negocioOperaciones.getLocalizadorDeServiciosAplicacion();
		NegocioDesacoplado negocioBusqueda = (NegocioDesacoplado)locApp.getLocMultiNegocio().getServicioONull(nombreBusqueda + "Negocio");
		
	    estadoNegocio.putValorSiguienteCapa(negocioOperaciones.getEspacioDeNombre() + "EnlaceBusquedaAsociado", enlace.getNombreBusqueda());	    
	    estadoNegocio.putValorSiguienteCapa(negocioOperaciones.getEspacioDeNombre() + "DtoCabezaAsociado", dto);
	    negocioBusqueda.aplicarEnlaceBusqueda(enlace, estadoAplicacion);
	}
	
	
	@Override
	public void aplicarRetornoEnlaceBusqueda(EnlaceBusqueda enlaceBusqueda,
			EstadoAplicacion estadoAplicacion) {
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		String nombreBusqueda = enlaceBusqueda.getNombreBusqueda();
		CreadorDeEnlaceBusqueda cemd = negocioOperaciones.getCreadorDeEnlaceBusqueda(nombreBusqueda);
		cemd.operacionesPostBusqueda(enlaceBusqueda);
	}
}
	
	
	
	
	
	

