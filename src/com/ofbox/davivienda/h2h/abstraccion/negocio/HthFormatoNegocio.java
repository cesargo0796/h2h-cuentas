package com.ofbox.davivienda.h2h.abstraccion.negocio;

import java.util.Collection;
import java.util.LinkedList;

import org.apache.commons.beanutils.BeanUtils;

import com.ofbox.davivienda.h2h.abstraccion.dao.HthAtributoFormatoDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthFormatoDao;
import com.ofbox.davivienda.h2h.abstraccion.dao.HthProcesoMonitoreoDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoFormato;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DatoEnllavado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.impl.DatoEnllavadoImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.flujo.ProximaEjecucion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.ExcepcionEnNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceDetalleDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.AmbienteDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.ExcepcionDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.CreadorDeMensajes;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;
/**
 * Clase que hereda de {@link NegocioDesacoplado}. Esta clase se encarga de tener toda la informaci&oacute;n
 * y operaciones que pertenecen a un negocio en espec&iacute;fico, y permite modificar comportamientos de cada negocio.<br/>
 *
 * DTO_META: Atributo que almacena todas las reglas que rigen a las propiedades de un objeto de Negocio.
 * En esta secci&oacute;n se definen las reglas que condicionan a cada propiedad de este Negocio.
 * @see {@link DtoMeta}
 *
 * CREADORES_DETALLE: Atributo que alamcena objetos de tipo {@link CreadorDeEnlaceDetalle}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; a este negocio. <br/>
 * La relaci&oacute;n que surge con este atributo es la denominado maestro-detalle, este Negocio es el maestro u objeto for&aacute;neo
 * de cada uno de los objetos que se definen en el atributo.
 * 
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl} que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio y el DTO_META definido en el objeto negocio asociado,
 * conocido como  detalle, al mismo tiempo se definen los datos que enlazar&aacute;n al objeto maestro con el detalle.
 * @see {@link CreadorDeEnlaceDetalle} 
 *
 * CREADORES_BUSQUEDA: Atributo que almacena objetos de tipo {@link CreadorDeEnlaceBusqueda}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; con los objetos de negocio foraneos, a los cuales se les conoce como maestros.<br/>
 * La relaci&oacute;n que surge con este atributo es la denominado detalle-maestro, cada uno de los objetos definidos en este atributo son objetos
 * de Negocio maestro o foraneos.  
 *
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl}, que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio, y el DTO_META definido en el negocio asociado, conocido como maestro,
 * al mismo tiempo se definen las propiedades del maestro que ser&aacute;n visibles al momento de desplegar el listado de datos en modo lista de valores.
 *
 * validadorDeDatos: Atributo que almacena un objeto que contiene la informaci&oacute;n de validaci&oacute;n de datos para este Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>ValidadorDeDatosSobreDtoMeta</code>, que implementa un objeto del tipo {@link ValidadorDeDatos}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para validar datos, y de esta manera personalizar la validaci&oacute;n de las
 * propiedades del objeto de Negocio.
 *
 * creadorDeFiltro: Atributo que almacena un objeto que contiene la informaci&oacute;n del filtro aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeFiltroSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeFiltro}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar filtro, y de esta manera personalizar la aplicaci&oacute;n del
 * filtro.
 *
 * creadorDeOrdenamiento: Atributo que almacena un objeto que contiene la informaci&oacute;n del ordenamiento aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeOrdenamientoSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeOrdenamiento}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar ordenamiento, y de esta manera personalizar la aplicaci&oacute;n del
 * ordenamiento.
 *
 * negocioOperaciones: Atributo que almacena un objeto que contiene la informaci&oacute;n de la operaciones b&aacute;sicas para esta clase Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>NegocioOperacionesBase</code>, que implementa un objeto del tipo {@link NegocioOperaciones}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar operaciones de llenado de informaci&oacute;n. Entre las operaciones
 * b&aacute;sicas se encuentra el manejo de las colecciones de objetos detalle, objetos maestros y las reglas de negocio para cada propiedad.
 *
 * @author Systems Out of the Box
 * @version f3.1
 */

  public class HthFormatoNegocio extends NegocioDesacoplado {
	protected Log log = LogServiceLocator.getDefaultImplementation().getLog(HthFormatoNegocio.class);
	
	public static final DtoMeta DTO_META = new DtoMeta(H2hResolusor.getSingleton().getContenedorDeDtoMeta(), "hthFormato",  HthFormato.class, 	
			new DtoPropiedadMeta[]{
        new DtoPropiedadMeta(HthFormatoDao.IDFORMATO,"idFormato",DtoPropiedadMeta.DATO_LLAVE , 10) 
     ,  new DtoPropiedadMeta(HthFormatoDao.NOMBREFORMATO,"nombreFormato",DtoPropiedadMeta.NO_NULL , 100) 
     ,  new DtoPropiedadMeta(HthFormatoDao.TIPOFORMATO,"tipoFormato",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthFormatoDao.TIPOSEPARADOR,"tipoSeparador",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthFormatoDao.CARACTERSEPARADOR,"caracterSeparador",DtoPropiedadMeta.NULL , 10) 
     ,  new DtoPropiedadMeta(HthFormatoDao.LOTEDESTINO,"loteDestino",DtoPropiedadMeta.NO_NULL , 3) 
     ,  new DtoPropiedadMeta(HthFormatoDao.USUARIOCREACION,"usuarioCreacion",DtoPropiedadMeta.AUDITORIA_USUARIO_CREAR , 100) 
     ,  new DtoPropiedadMeta(HthFormatoDao.FECHACREACION,"fechaCreacion",DtoPropiedadMeta.AUDITORIA_FECHA_CREAR , 23) 
     ,  new DtoPropiedadMeta(HthFormatoDao.USUARIOULTACT,"usuarioUltAct",DtoPropiedadMeta.AUDITORIA_USUARIO_ACTUALIZAR , 100) 
     ,  new DtoPropiedadMeta(HthFormatoDao.FECHAULTACT,"fechaUltAct",DtoPropiedadMeta.AUDITORIA_FECHA_ACTUALIZAR , 23) 
	       },
            DtoMeta.OPERACIONES_BUSQUEDAS_CAMPOS_PRIOPIOS
    );
	
	
	
	public Log getLogNegocio(){
		return log;
	}
    
    
    
    
    
    
	public static final CreadorDeEnlaceDetalle[] CREADORES_DETALLE = new CreadorDeEnlaceDetalle[]{
        new CreadorDeEnlaceDetalleDinImpl(DTO_META,HthAtributoFormatoNegocio.DTO_META){
			public DatoEnllavado[] getDatosEnllavados(Object dto) {
                HthFormato hthFormato = (HthFormato)dto;
                return new DatoEnllavado[]{
                          new DatoEnllavadoImpl("idFormato", HthAtributoFormatoDao.IDFORMATO, hthFormato.getIdFormato())
                };
			}
	     }			
     ,  new CreadorDeEnlaceDetalleDinImpl(DTO_META,HthProcesoMonitoreoNegocio.DTO_META){
			public DatoEnllavado[] getDatosEnllavados(Object dto) {
                HthFormato hthFormato = (HthFormato)dto;
                return new DatoEnllavado[]{
                          new DatoEnllavadoImpl("idFormatoDetalle", HthProcesoMonitoreoDao.IDFORMATODETALLE, hthFormato.getIdFormato())
                       ,  new DatoEnllavadoImpl("idFormatoEncabezado", HthProcesoMonitoreoDao.IDFORMATOENCABEZADO, hthFormato.getIdFormato())
                };
			}
	     }			
     };
     
     
     
	public static final CreadorDeEnlaceBusqueda[] CREADORES_BUSQUEDA = new CreadorDeEnlaceBusqueda[]{
     };
     
	    
    /**
     * Atributo que almacena el objeto que permite acceder a la capa DAO para este negocio.
     * @see {@link Dao}
     */
    private  HthFormatoDao hthFormatoDao;
    /**
     * Devuelve el atributo que permite acceder a la capa DAO.
     * @return <code>Objeto</code> - objeto de tipo Dao.
     */
	public HthFormatoDao getHthFormatoDao() {
		if(hthFormatoDao == null){
			hthFormatoDao = (HthFormatoDao)getDao();
		}
		return hthFormatoDao;
	}    

	private ValidadorDeDatos validadorDeDatos = new ValidadorDeDatosSobreDtoMeta(DTO_META);
	
	private CreadorDeFiltro creadorDeFiltro = new CreadorDeFiltroSobreDtoMeta(DTO_META);
	
	private CreadorDeOrdenamiento creadorDeOrdenamiento = new CreadorDeOrdenamientoSobreDtoMeta(DTO_META);
	
	private NegocioOperaciones negocioOperaciones = new NegocioOperacionesBase(CREADORES_DETALLE, CREADORES_BUSQUEDA, DTO_META, H2hResolusor.getSingleton());
	
	/**
	 * Devuelve un objeto de tipo {@link NegocioOperaciones}
	 * @return <code>NegocioOperaciones</code>
	 */
	public NegocioOperaciones getNegocioOperaciones() {
		return negocioOperaciones;
	}	
	/**
	 * Devuelve un objeto de tipo {@link #ValidadorDeDatos}
	 * @return <code>ValidadorDeDatos</code>
	 */
	public ValidadorDeDatos getValidadorDeDatos() {
		return validadorDeDatos;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeFiltro}
	 * @return <code>CreadorDeFiltro</code>
	 */
	public CreadorDeFiltro getCreadorDeFiltro(){
	    return creadorDeFiltro;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeOrdenamiento}
	 * @return <code>CreadorDeOrdenamiento</code>
	 */
	public CreadorDeOrdenamiento getCreadorDeOrdenamiento(){
		return creadorDeOrdenamiento;
	}
	/**
	 * Devuelve un objeto Resolusor.
	 * @return <code>LocalizadorDeServiciosAplicacion</code>
	 */
	public LocalizadorDeServiciosAplicacion getLocalizadorDeServiciosAplicacion(){
		return H2hResolusor.getSingleton();
	}

	public ProximaEjecucion doCopiarFormato(DatosAplicacion datosAplicacion) throws Exception{
		
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		estadoNegocio.setEstado(EstadoNegocio.ESTADO_ACTUALIZACION);		
		ValidadorDeDatos validadorDeDatos = getValidadorDeDatos();
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		AmbienteDeSeguridad seg = getAmbienteDeSeguridad(datosAplicacion);
		
		if(!getSeguridadNegocio(datosAplicacion).isActualizarDisponible()){
			throw new ExcepcionDeSeguridad("No tiene permiso para actualizar registros en " + getNegocioOperaciones().getEspacioDeNombre());
		}
		
		
		Object dtoId = validadorDeDatos.leerDtoId(datosAplicacion, cmv, ValidadorDeDatos.SIN_ENLLAVADOR, ValidadorDeDatos.SIN_ESPACIO_NOMBRE);
		HthFormato dto =  null;
		Transaccion tx = null;
		try{
			dto = (HthFormato) cargarDtoParaActualizar(dtoId,seg);
			HthFormato copia = new HthFormato();
			BeanUtils.copyProperties(copia, dto);
			
			
			HthFormatoDao dao = (HthFormatoDao) getDao();
			HthAtributoFormatoDao atributoFormatoDao = (HthAtributoFormatoDao) H2hResolusor.getSingleton().getInstancia("hthAtributoFormatoDao");
			PeticionDeDatos peticion = atributoFormatoDao.crearPeticionDeDatos();
			peticion.agregarRestriccion(PeticionDeDatos.IGUAL, HthAtributoFormatoDao.IDFORMATO, dto.getIdFormato());
			Paginador paginador = atributoFormatoDao.ejecutarPeticionDeDatos(peticion);
			Collection<HthAtributoFormato> atributos = paginador.cargarTodosLosDatosEnMemoria();
			LinkedList<HthAtributoFormato> atributosCopiados = new LinkedList<HthAtributoFormato>();
			for(HthAtributoFormato atributo : atributos){
				HthAtributoFormato atributoCopia = new HthAtributoFormato();
				BeanUtils.copyProperties(atributoCopia, atributo);
				if(atributo.getIdAtributoDetalle() <= 0l) atributoCopia.setIdAtributoDetalle(null);
				if(atributo.getIdAtributoEncabezado() <= 0l) atributoCopia.setIdAtributoEncabezado(null);
				atributoCopia.setIdFormato(null);
				atributosCopiados.add(atributoCopia);
			}
			
			
			tx = dao.crearTransaccion();
			copia.setNombreFormato(dto.getNombreFormato() + " (COPIA)");
			dao.crear(copia, tx);
			for(HthAtributoFormato atributoCopia : atributosCopiados){
				atributoCopia.setIdFormato(copia.getIdFormato());
				atributoFormatoDao.crear(atributoCopia, tx);
			}
			tx.commit();
			
			cmv.agregarMensajeSatisfactorio("Se ha creado el registro del Formato [" + copia.getNombreFormato() + "] correctamente");
			
		}catch(ExcepcionEnDAO edao){
			cmv.agregarMensajeDeError("No es posible crear la copia del formato [" + dto.getNombreFormato() + "]");
			if(tx != null) try{ tx.rollback(); }catch(Exception ignored){}
		}catch(ExcepcionEnNegocio eneg){
			cmv.agregarExcepcionEnNegocio(eneg);			
		}
		
		
		
		
		return doListado(datosAplicacion);
	}


}