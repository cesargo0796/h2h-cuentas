package com.ofbox.davivienda.h2h.abstraccion.negocio;

import java.util.Iterator;
import java.util.Map;

import com.ofbox.davivienda.h2h.abstraccion.dao.HthAtributoFormatoDao;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoFormato;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadForaneoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.meta.DtoPropiedadMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.ValorHaciaPeticion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.flujo.ProximaEjecucion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.ExcepcionEnNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioDesacoplado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.NegocioOperaciones;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.LocalizadorDeServiciosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.dependencias.RecursosBundle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceBusqueda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.CreadorDeEnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.EnlaceDetalle;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.enlaces.impl.CreadorDeEnlaceBusquedaDinImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.CreadorDeFiltro;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.filtro.impl.CreadorDeFiltroSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.impl.NegocioOperacionesBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.CreadorDeOrdenamiento;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.negocio.orden.impl.CreadorDeOrdenamientoSobreDtoMeta;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.AmbienteDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.ExcepcionDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.SeguridadDeNegocio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.CreadorDeMensajes;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.ValidadorDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.impl.ValidadorDeDatosSobreDtoMeta;
/**
 * Clase que hereda de {@link NegocioDesacoplado}. Esta clase se encarga de tener toda la informaci&oacute;n
 * y operaciones que pertenecen a un negocio en espec&iacute;fico, y permite modificar comportamientos de cada negocio.<br/>
 *
 * DTO_META: Atributo que almacena todas las reglas que rigen a las propiedades de un objeto de Negocio.
 * En esta secci&oacute;n se definen las reglas que condicionan a cada propiedad de este Negocio.
 * @see {@link DtoMeta}
 *
 * CREADORES_DETALLE: Atributo que alamcena objetos de tipo {@link CreadorDeEnlaceDetalle}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; a este negocio. <br/>
 * La relaci&oacute;n que surge con este atributo es la denominado maestro-detalle, este Negocio es el maestro u objeto for&aacute;neo
 * de cada uno de los objetos que se definen en el atributo.
 * 
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl} que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio y el DTO_META definido en el objeto negocio asociado,
 * conocido como  detalle, al mismo tiempo se definen los datos que enlazar&aacute;n al objeto maestro con el detalle.
 * @see {@link CreadorDeEnlaceDetalle} 
 *
 * CREADORES_BUSQUEDA: Atributo que almacena objetos de tipo {@link CreadorDeEnlaceBusqueda}, cada uno de estos objetos define la informaci&oacute;n
 * que se enlazar&aacute; con los objetos de negocio foraneos, a los cuales se les conoce como maestros.<br/>
 * La relaci&oacute;n que surge con este atributo es la denominado detalle-maestro, cada uno de los objetos definidos en este atributo son objetos
 * de Negocio maestro o foraneos.  
 *
 * Este arreglo tiene implementaciones del tipo {@link CreadorDeEnlaceBusquedaDinImpl}, que son instanciados al momento de la creaci&oacute;n del atributo,
 * recibiendo en el constructor el atributo {@link #DTO_META} definido en este negocio, y el DTO_META definido en el negocio asociado, conocido como maestro,
 * al mismo tiempo se definen las propiedades del maestro que ser&aacute;n visibles al momento de desplegar el listado de datos en modo lista de valores.
 *
 * validadorDeDatos: Atributo que almacena un objeto que contiene la informaci&oacute;n de validaci&oacute;n de datos para este Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>ValidadorDeDatosSobreDtoMeta</code>, que implementa un objeto del tipo {@link ValidadorDeDatos}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para validar datos, y de esta manera personalizar la validaci&oacute;n de las
 * propiedades del objeto de Negocio.
 *
 * creadorDeFiltro: Atributo que almacena un objeto que contiene la informaci&oacute;n del filtro aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeFiltroSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeFiltro}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar filtro, y de esta manera personalizar la aplicaci&oacute;n del
 * filtro.
 *
 * creadorDeOrdenamiento: Atributo que almacena un objeto que contiene la informaci&oacute;n del ordenamiento aplicado.<br/>
 *
 * Al crear la instancia de la clase <code>CreadorDeOrdenamientoSobreDtoMeta</code>, que implementa un objeto del tipo {@link CreadorDeOrdenamiento}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar ordenamiento, y de esta manera personalizar la aplicaci&oacute;n del
 * ordenamiento.
 *
 * negocioOperaciones: Atributo que almacena un objeto que contiene la informaci&oacute;n de la operaciones b&aacute;sicas para esta clase Negocio.<br/>
 *
 * Al crear la instancia de la clase <code>NegocioOperacionesBase</code>, que implementa un objeto del tipo {@link NegocioOperaciones}, 
 * permite sobreescribir cada uno de los m&eacute;todos que posee para aplicar operaciones de llenado de informaci&oacute;n. Entre las operaciones
 * b&aacute;sicas se encuentra el manejo de las colecciones de objetos detalle, objetos maestros y las reglas de negocio para cada propiedad.
 *
 * @author Systems Out of the Box
 * @version f3.1
 */

  public class HthAtributoFormatoNegocio extends NegocioDesacoplado {
	protected Log log = LogServiceLocator.getDefaultImplementation().getLog(HthAtributoFormatoNegocio.class);
	
	public static final DtoMeta DTO_META = new DtoMeta(H2hResolusor.getSingleton().getContenedorDeDtoMeta(), "hthAtributoFormato",  HthAtributoFormato.class, 	
			new DtoPropiedadMeta[]{
        new DtoPropiedadMeta(HthAtributoFormatoDao.IDATRIBUTOFORMATO,"idAtributoFormato",DtoPropiedadMeta.DATO_LLAVE , 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.IDATRIBUTOENCABEZADO,"idAtributoEncabezado",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "hthAtributoEncabezado","idAtributoEncabezado","nombreAtributo") , true, 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.IDATRIBUTODETALLE,"idAtributoDetalle",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "hthAtributoDetalle","idAtributoDetalle","nombreAtributo") , true, 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.IDFORMATO,"idFormato",DtoPropiedadMeta.DATO_BUSQUEDA, new DtoPropiedadForaneoMeta( "hthFormato","idFormato","nombreFormato") , false, 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.POSICION,"posicion",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.POSICIONINI,"posicionIni",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.POSICIONFIN,"posicionFin",DtoPropiedadMeta.NO_NULL , 10) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.TIPODATOATRIBUTO,"tipoDatoAtributo",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.FORMATOATRIBUTO,"formatoAtributo",DtoPropiedadMeta.NULL , 25) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.OPCIONAL,"opcional",DtoPropiedadMeta.NO_NULL , 1) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.USUARIOCREACION,"usuarioCreacion",DtoPropiedadMeta.AUDITORIA_USUARIO_CREAR , 100) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.FECHACREACIN,"fechaCreacin",DtoPropiedadMeta.AUDITORIA_FECHA_CREAR , 23) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.USUARIOULTACT,"usuarioUltAct",DtoPropiedadMeta.AUDITORIA_USUARIO_ACTUALIZAR , 100) 
     ,  new DtoPropiedadMeta(HthAtributoFormatoDao.FECHAULTACT,"fechaUltAct",DtoPropiedadMeta.AUDITORIA_FECHA_ACTUALIZAR , 23) 
	       },
    	   	DtoMeta.OPERACIONES_BUSQUEDAS_COMPLETAS	 
    );
	
	
	
	public Log getLogNegocio(){
		return log;
	}
    
    
    
    
    
    
	public static final CreadorDeEnlaceDetalle[] CREADORES_DETALLE = new CreadorDeEnlaceDetalle[]{
     };
     
     
     
	public static final CreadorDeEnlaceBusqueda[] CREADORES_BUSQUEDA = new CreadorDeEnlaceBusqueda[]{
        new CreadorDeEnlaceBusquedaDinImpl(DTO_META, HthAtributoEncabezadoNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "idAtributoEncabezado"
                       ,  "nombreAtributo"
                       ,  "descripcion"
                       ,  "usuarioCreacion"
                       ,  "fechaCreacion"
                       ,  "fechaUltAct"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, HthAtributoDetalleNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "idAtributoDetalle"
                       ,  "nombreAtributo"
                       ,  "descripcion"
                       ,  "usuarioCreacion"
                       ,  "fechaCreacion"
                       ,  "fechaUltAct"
                };
			}
	     }			
     ,  new CreadorDeEnlaceBusquedaDinImpl(DTO_META, HthFormatoNegocio.DTO_META){
			public String[] getPropiedadesVisibles() {
                return new String[]{
                          "idFormato"
                       ,  "nombreFormato"
                       ,  "tipoFormato"
                       ,  "tipoSeparador"
                       ,  "loteDestino"
                       ,  "caracterSeparador"
                       ,  "usuarioCreacion"
                       ,  "fechaCreacion"
                       ,  "fechaUltAct"
                };
			}
	     }			
     };
     
	    
    /**
     * Atributo que almacena el objeto que permite acceder a la capa DAO para este negocio.
     * @see {@link Dao}
     */
    private  HthAtributoFormatoDao hthAtributoFormatoDao;
    /**
     * Devuelve el atributo que permite acceder a la capa DAO.
     * @return <code>Objeto</code> - objeto de tipo Dao.
     */
	public HthAtributoFormatoDao getHthAtributoFormatoDao() {
		if(hthAtributoFormatoDao == null){
			hthAtributoFormatoDao = (HthAtributoFormatoDao)getDao();
		}
		return hthAtributoFormatoDao;
	}    

	private ValidadorDeDatos validadorDeDatos = new ValidadorDeDatosSobreDtoMeta(DTO_META);
	
	private CreadorDeFiltro creadorDeFiltro = new CreadorDeFiltroSobreDtoMeta(DTO_META);
	
	private CreadorDeOrdenamiento creadorDeOrdenamiento = new CreadorDeOrdenamientoSobreDtoMeta(DTO_META);
	
	private NegocioOperaciones negocioOperaciones = new NegocioOperacionesBase(CREADORES_DETALLE, CREADORES_BUSQUEDA, DTO_META, H2hResolusor.getSingleton());
	
	/**
	 * Devuelve un objeto de tipo {@link NegocioOperaciones}
	 * @return <code>NegocioOperaciones</code>
	 */
	public NegocioOperaciones getNegocioOperaciones() {
		return negocioOperaciones;
	}	
	/**
	 * Devuelve un objeto de tipo {@link #ValidadorDeDatos}
	 * @return <code>ValidadorDeDatos</code>
	 */
	public ValidadorDeDatos getValidadorDeDatos() {
		return validadorDeDatos;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeFiltro}
	 * @return <code>CreadorDeFiltro</code>
	 */
	public CreadorDeFiltro getCreadorDeFiltro(){
	    return creadorDeFiltro;
	}
	/**
	 * Devuelve un objeto de tipo {@link #CreadorDeOrdenamiento}
	 * @return <code>CreadorDeOrdenamiento</code>
	 */
	public CreadorDeOrdenamiento getCreadorDeOrdenamiento(){
		return creadorDeOrdenamiento;
	}
	/**
	 * Devuelve un objeto Resolusor.
	 * @return <code>LocalizadorDeServiciosAplicacion</code>
	 */
	public LocalizadorDeServiciosAplicacion getLocalizadorDeServiciosAplicacion(){
		return H2hResolusor.getSingleton();
	}



	@Override
	public ProximaEjecucion doCrear(DatosAplicacion datosAplicacion)
			throws Exception {
		
		AmbienteDeSeguridad seg = getAmbienteDeSeguridad(datosAplicacion);
		if(!getSeguridadNegocio(datosAplicacion).isCrearDisponible()){
			throw new ExcepcionDeSeguridad("No tiene permiso para crear registros en " + getNegocioOperaciones().getEspacioDeNombre());
		}
		
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		RecursosBundle recursos = negocioOperaciones.getLocalizadorDeServiciosAplicacion().getRecursos();
		
		if(negocioOperaciones.existeCreadoresBusquedaCombo()){
			ambientarCombosLov(datosAplicacion);
		}
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		estadoNegocio.setEstado(EstadoNegocio.ESTADO_CREACION);
		estadoNegocio.limpiarCache();
		
		EnlaceDetalle enlaceDetalle = estadoNegocio.getEnlaceDetalle();
		
		NegocioDesacoplado negocioMaestro = (NegocioDesacoplado)  H2hResolusor.getSingleton().getLocMultiNegocio().getServicio(enlaceDetalle.getCabecera().getLocacion() + "Negocio");
		EstadoNegocio estadoNegocioMaestro = negocioMaestro.getNegocioOperaciones().getEstadoNegocio(estadoAplicacion);
		ValorHaciaPeticion valorSiguienteCapa = estadoNegocioMaestro.getValorSiguienteCapa(negocioMaestro.getNegocioOperaciones().getEspacioDeNombre() + "DtoCabeza");
		HthFormato formatoMaestro = (HthFormato)valorSiguienteCapa.getObjetoEnPeticion();
		
		datosAplicacion.setDatoPeticion("tipoFormato", formatoMaestro.getTipoFormato());
		
		
		estadoAplicacion.addUbicacion(negocioOperaciones.getEspacioDeNombre(), 
				"cancelarCrear",
				recursos.getMessage(negocioOperaciones.getEspacioDeNombre() + ".bc.etiqueta.principal"), 
				getIdUbicacionActual(datosAplicacion));
		return proximaEjecucion("crear", datosAplicacion);
	}
	
	@Override
	public ProximaEjecucion doVer(DatosAplicacion datosAplicacion) throws Exception {
		ValidadorDeDatos validadorDeDatos = getValidadorDeDatos();
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		AmbienteDeSeguridad seg = getAmbienteDeSeguridad(datosAplicacion);
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		SeguridadDeNegocio segneg = negocioOperaciones.getSeguridadNegocio(seg);
		RecursosBundle recursos = negocioOperaciones.getLocalizadorDeServiciosAplicacion().getRecursos();
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(datosAplicacion.getEstadoAplicacion());
		estadoNegocio.setEstado(EstadoNegocio.ESTADO_VER);
		
				EnlaceDetalle enlaceDetalle = estadoNegocio.getEnlaceDetalle();
				
				NegocioDesacoplado negocioMaestro = (NegocioDesacoplado)  H2hResolusor.getSingleton().getLocMultiNegocio().getServicio(enlaceDetalle.getCabecera().getLocacion() + "Negocio");
				EstadoNegocio estadoNegocioMaestro = negocioMaestro.getNegocioOperaciones().getEstadoNegocio(estadoAplicacion);
				ValorHaciaPeticion valorSiguienteCapa = estadoNegocioMaestro.getValorSiguienteCapa(negocioMaestro.getNegocioOperaciones().getEspacioDeNombre() + "DtoCabeza");
				HthFormato formatoMaestro = (HthFormato)valorSiguienteCapa.getObjetoEnPeticion();
				
				datosAplicacion.setDatoPeticion("tipoFormato", formatoMaestro.getTipoFormato());
		
		if(!segneg.isVerDisponible()){
			throw new ExcepcionDeSeguridad("No tiene permiso para consultar en " + getNegocioOperaciones().getEspacioDeNombre());
		}
		Object dtoId = validadorDeDatos.leerDtoId(datosAplicacion, cmv, ValidadorDeDatos.SIN_ENLLAVADOR, ValidadorDeDatos.SIN_ESPACIO_NOMBRE);		
		Object dto =  null;
		try{
			dto = cargarDtoParaVer(dtoId, seg);
			guardarDtoSiguienteCapa(datosAplicacion, dto);
		}catch(ExcepcionEnNegocio eneg){
			eneg.printStackTrace();
			cmv.agregarExcepcionEnNegocio(eneg);			
		}
		if(cmv.isMensajes()){			
			return doListado(datosAplicacion);
		}else{			
			estadoAplicacion.addUbicacion(negocioOperaciones.getEspacioDeNombre(), 
					"cancelarVer", 
					recursos.getMessage(negocioOperaciones.getEspacioDeNombre() + ".bc.etiqueta.principal"), 
					getIdUbicacionActual(datosAplicacion));
			return proximaEjecucion("ver", datosAplicacion); 
		}
	}

	public ProximaEjecucion doActualizar(DatosAplicacion datosAplicacion) throws Exception {
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		estadoNegocio.setEstado(EstadoNegocio.ESTADO_ACTUALIZACION);		
		ValidadorDeDatos validadorDeDatos = getValidadorDeDatos();
		CreadorDeMensajes cmv = getCreadorDeMensajes(datosAplicacion);
		AmbienteDeSeguridad seg = getAmbienteDeSeguridad(datosAplicacion);
		RecursosBundle recursos = negocioOperaciones.getLocalizadorDeServiciosAplicacion().getRecursos();
		
		EnlaceDetalle enlaceDetalle = estadoNegocio.getEnlaceDetalle();
		
		NegocioDesacoplado negocioMaestro = (NegocioDesacoplado)  H2hResolusor.getSingleton().getLocMultiNegocio().getServicio(enlaceDetalle.getCabecera().getLocacion() + "Negocio");
		EstadoNegocio estadoNegocioMaestro = negocioMaestro.getNegocioOperaciones().getEstadoNegocio(estadoAplicacion);
		ValorHaciaPeticion valorSiguienteCapa = estadoNegocioMaestro.getValorSiguienteCapa(negocioMaestro.getNegocioOperaciones().getEspacioDeNombre() + "DtoCabeza");
		HthFormato formatoMaestro = (HthFormato)valorSiguienteCapa.getObjetoEnPeticion();
		
		datosAplicacion.setDatoPeticion("tipoFormato", formatoMaestro.getTipoFormato());
		
		if(!getSeguridadNegocio(datosAplicacion).isActualizarDisponible()){
			throw new ExcepcionDeSeguridad("No tiene permiso para actualizar registros en " + getNegocioOperaciones().getEspacioDeNombre());
		}
		
		if(negocioOperaciones.existeCreadoresBusquedaCombo()){
			ambientarCombosLov(datosAplicacion);
		}
		
		Object dtoId = validadorDeDatos.leerDtoId(datosAplicacion, cmv, ValidadorDeDatos.SIN_ENLLAVADOR, ValidadorDeDatos.SIN_ESPACIO_NOMBRE);
		Object dto =  null;
		try{
			dto = cargarDtoParaActualizar(dtoId,seg);
			if(esEditable(dto, cmv)){
				estadoNegocio.setCache(dto);
				guardarDtoSiguienteCapa(datosAplicacion, dto);
			}
		}catch(ExcepcionEnNegocio eneg){
			cmv.agregarExcepcionEnNegocio(eneg);			
		}
		
		if(cmv.isMensajes()){
			if(estadoNegocio.getEstadoAnterior() == EstadoNegocio.ESTADO_DETALLE){
				estadoAplicacion.actualizarUbicacion(getIdUbicacionRetorno(datosAplicacion), getIdUbicacionActual(datosAplicacion));
				return doDetalle(datosAplicacion);
			}else{
				return doListado(datosAplicacion);
			}
		}else{
			estadoAplicacion.addUbicacion(negocioOperaciones.getEspacioDeNombre(), 
					"cancelarActualizar", 
					recursos.getMessage(negocioOperaciones.getEspacioDeNombre() + ".bc.etiqueta.principal"),
					getIdUbicacionActual(datosAplicacion));
			
			guardarDtoSiguienteCapa(datosAplicacion, dto);
			return proximaEjecucion("actualizar", datosAplicacion); 
		}
    }
	
	public ProximaEjecucion doFiltrar(DatosAplicacion datosAplicacion) throws Exception {
		AmbienteDeSeguridad seg = getAmbienteDeSeguridad(datosAplicacion);
		EstadoAplicacion estadoAplicacion = datosAplicacion.getEstadoAplicacion();
		NegocioOperaciones negocioOperaciones = getNegocioOperaciones();
		EstadoNegocio estadoNegocio = negocioOperaciones.getEstadoNegocio(estadoAplicacion);
		RecursosBundle recursos = negocioOperaciones.getLocalizadorDeServiciosAplicacion().getRecursos();
		
				EnlaceDetalle enlaceDetalle = estadoNegocio.getEnlaceDetalle();
				
				NegocioDesacoplado negocioMaestro = (NegocioDesacoplado)  H2hResolusor.getSingleton().getLocMultiNegocio().getServicio(enlaceDetalle.getCabecera().getLocacion() + "Negocio");
				EstadoNegocio estadoNegocioMaestro = negocioMaestro.getNegocioOperaciones().getEstadoNegocio(estadoAplicacion);
				ValorHaciaPeticion valorSiguienteCapa = estadoNegocioMaestro.getValorSiguienteCapa(negocioMaestro.getNegocioOperaciones().getEspacioDeNombre() + "DtoCabeza");
				HthFormato formatoMaestro = (HthFormato)valorSiguienteCapa.getObjetoEnPeticion();
				
				datosAplicacion.setDatoPeticion("tipoFormato", formatoMaestro.getTipoFormato());
		
		if(!getSeguridadNegocio(datosAplicacion).isFiltrarDisponible()
				&& !isEnlaceBusquedaAplicado(estadoNegocio)){
			throw new ExcepcionDeSeguridad("No tiene permiso para filtrar registros en " + getNegocioOperaciones().getEspacioDeNombre());
		}
		
		estadoNegocio.setEstado(EstadoNegocio.ESTADO_FILTRO);		
		estadoNegocio.setCache(estadoNegocio.getFiltro());
		
		if(negocioOperaciones.existeCreadoresBusquedaCombo()){
			ambientarCombosLov(datosAplicacion);
		}
		
		if(estadoNegocio.getFiltro() !=null){
			Map mapaDeValores = estadoNegocio.getFiltro().getMapaDeValores();
			for (Iterator iterator = mapaDeValores.entrySet().iterator(); iterator.hasNext();) {
				Map.Entry entr = (Map.Entry) iterator.next();
			}
			datosAplicacion.setDatoPeticion(getNegocioOperaciones().getEspacioDeNombre() + "Filtro", mapaDeValores);
		}
		estadoAplicacion.addUbicacion(negocioOperaciones.getEspacioDeNombre(), 
				"cancelarFiltrar", 
				recursos.getMessage(negocioOperaciones.getEspacioDeNombre() + ".bc.etiqueta.principal"), 
				getIdUbicacionActual(datosAplicacion));
		
		return proximaEjecucion("filtrar", datosAplicacion);
	}
	

}