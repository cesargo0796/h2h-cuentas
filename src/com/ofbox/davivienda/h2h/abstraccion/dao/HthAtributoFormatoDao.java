package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoFormato;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;    
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoEncabezado;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthAtributoDetalle;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthAtributoFormatoDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna id_atributo_formato mapeado
	 * al atributo IdAtributoFormato.
     */
    int IDATRIBUTOFORMATO = 13001;
    /**
     * Atributo que almacena el identificador de la columna id_atributo_encabezado mapeado
	 * al atributo IdAtributoEncabezado.
     */
    int IDATRIBUTOENCABEZADO = 13012;
    /**
     * Atributo que almacena el identificador de la columna id_atributo_detalle mapeado
	 * al atributo IdAtributoDetalle.
     */
    int IDATRIBUTODETALLE = 13013;
    /**
     * Atributo que almacena el identificador de la columna id_formato mapeado
	 * al atributo IdFormato.
     */
    int IDFORMATO = 13014;
    /**
     * Atributo que almacena el identificador de la columna posicion mapeado
	 * al atributo Posicion.
     */
    int POSICION = 13002;
    /**
     * Atributo que almacena el identificador de la columna posicion_ini mapeado
	 * al atributo PosicionIni.
     */
    int POSICIONINI = 13003;
    /**
     * Atributo que almacena el identificador de la columna posicion_fin mapeado
	 * al atributo PosicionFin.
     */
    int POSICIONFIN = 13004;
    /**
     * Atributo que almacena el identificador de la columna tipo_dato_atributo mapeado
	 * al atributo TipoDatoAtributo.
     */
    int TIPODATOATRIBUTO = 13005;
    /**
     * Atributo que almacena el identificador de la columna formato_atributo mapeado
	 * al atributo FormatoAtributo.
     */
    int FORMATOATRIBUTO = 13006;
    /**
     * Atributo que almacena el identificador de la columna opcional mapeado
	 * al atributo Opcional.
     */
    int OPCIONAL = 13007;
    /**
     * Atributo que almacena el identificador de la columna usuario_creacion mapeado
	 * al atributo UsuarioCreacion.
     */
    int USUARIOCREACION = 13008;
    /**
     * Atributo que almacena el identificador de la columna fecha_creacion mapeado
	 * al atributo FechaCreacin.
     */
    int FECHACREACIN = 13009;
    /**
     * Atributo que almacena el identificador de la columna usuario_ult_act mapeado
	 * al atributo UsuarioUltAct.
     */
    int USUARIOULTACT = 13010;
    /**
     * Atributo que almacena el identificador de la columna fecha_ult_act mapeado
	 * al atributo FechaUltAct.
     */
    int FECHAULTACT = 13011;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthAtributoFormato buscarPorID(Long idAtributoFormatoId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idAtributoFormatoId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthAtributoFormato hthAtributoFormato) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthAtributoFormato hthAtributoFormato) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthAtributoFormato resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    HthAtributoEncabezado getHthAtributoEncabezado(HthAtributoFormato hthAtributoFormato)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	HthAtributoEncabezado getHthAtributoEncabezado(HthAtributoFormato hthAtributoFormato, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    HthAtributoDetalle getHthAtributoDetalle(HthAtributoFormato hthAtributoFormato)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	HthAtributoDetalle getHthAtributoDetalle(HthAtributoFormato hthAtributoFormato, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    HthFormato getHthFormato(HthAtributoFormato hthAtributoFormato)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	HthFormato getHthFormato(HthAtributoFormato hthAtributoFormato, Transaccion tx)throws ExcepcionEnDAO;
    	 
}