package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthPagoServicios;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

public interface HthPagoServiciosDao {
	
	
	int IDPAGOSERVICIO = 5001;
	
	
	
	int IDCLIENTE = 5002;
	
	
	
	int NUMCUENTA = 5003;
	
	
	
	int ESTADO = 5004;
	
	
	int NIT = 5005;
	
	
	
	HthPagoServicios buscarPorID(HthPagoServicios hthPagoServicios) throws ExcepcionEnDAO;
	
	
	
	void eliminar(HthPagoServicios hthPagoServicios) throws ExcepcionEnDAO;	
	
	
	
	void actualizar(HthPagoServicios hthPagoServicios) throws ExcepcionEnDAO;	
	
	
	
	void crear(HthPagoServicios hthPagoServicios) throws ExcepcionEnDAO;	
	
	
	
	HthPagoServicios resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	
	
	BsCliente getBsCliente(HthPagoServicios hthPagoServicios)throws ExcepcionEnDAO;
	 
	
	
	BsCliente getBsCliente(HthPagoServicios hthPagoServicios, Transaccion tx)throws ExcepcionEnDAO;

}
