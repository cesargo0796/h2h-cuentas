package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioach;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;    
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsConvenioachDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna Convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 2001;
    /**
     * Atributo que almacena el identificador de la columna TipoConvenio mapeado
	 * al atributo Tipoconvenio.
     */
    int TIPOCONVENIO = 2002;
    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 2020;
    /**
     * Atributo que almacena el identificador de la columna Nombre mapeado
	 * al atributo Nombre.
     */
    int NOMBRE = 2003;
    /**
     * Atributo que almacena el identificador de la columna Cuenta mapeado
	 * al atributo Cuenta.
     */
    int CUENTA = 2004;
    /**
     * Atributo que almacena el identificador de la columna NombreCta mapeado
	 * al atributo Nombrecta.
     */
    int NOMBRECTA = 2005;
    /**
     * Atributo que almacena el identificador de la columna TipoMoneda mapeado
	 * al atributo Tipomoneda.
     */
    int TIPOMONEDA = 2006;
    /**
     * Atributo que almacena el identificador de la columna Descripcion mapeado
	 * al atributo Descripcion.
     */
    int DESCRIPCION = 2007;
    /**
     * Atributo que almacena el identificador de la columna PolParticipante mapeado
	 * al atributo Polparticipante.
     */
    int POLPARTICIPANTE = 2008;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 2009;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 2010;
    /**
     * Atributo que almacena el identificador de la columna AutorizacionWeb mapeado
	 * al atributo Autorizacionweb.
     */
    int AUTORIZACIONWEB = 2011;
    /**
     * Atributo que almacena el identificador de la columna Clasificacion mapeado
	 * al atributo Clasificacion.
     */
    int CLASIFICACION = 2012;
    /**
     * Atributo que almacena el identificador de la columna ComisionXOperacionPropiaOK mapeado
	 * al atributo Comisionxoperacionpropiaok.
     */
    int COMISIONXOPERACIONPROPIAOK = 2013;
    /**
     * Atributo que almacena el identificador de la columna ComisionXOperacionPropiaError mapeado
	 * al atributo Comisionxoperacionpropiaerror.
     */
    int COMISIONXOPERACIONPROPIAERROR = 2014;
    /**
     * Atributo que almacena el identificador de la columna ComisionXOperacionNoPropiaOK mapeado
	 * al atributo Comisionxoperacionnopropiaok.
     */
    int COMISIONXOPERACIONNOPROPIAOK = 2015;
    /**
     * Atributo que almacena el identificador de la columna ComisionXOperacionNoPropiaError mapeado
	 * al atributo Comisionxoperacionnopropiaerror.
     */
    int COMISIONXOPERACIONNOPROPIAERROR = 2016;
    /**
     * Atributo que almacena el identificador de la columna MultiplesCuentas mapeado
	 * al atributo Multiplescuentas.
     */
    int MULTIPLESCUENTAS = 2017;
    /**
     * Atributo que almacena el identificador de la columna Activo mapeado
	 * al atributo Activo.
     */
    int ACTIVO = 2018;
    /**
     * Atributo que almacena el identificador de la columna bitBorrado mapeado
	 * al atributo Bitborrado.
     */
    int BITBORRADO = 2019;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsConvenioach buscarPorID(Long convenioId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long convenioId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsConvenioach bsConvenioach) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsConvenioach bsConvenioach) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsConvenioach resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsCliente getBsCliente(BsConvenioach bsConvenioach)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsCliente getBsCliente(BsConvenioach bsConvenioach, Transaccion tx)throws ExcepcionEnDAO;
    	 
}