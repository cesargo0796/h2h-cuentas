package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthProcesoMonitoreo;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;    
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthVistaConvenio;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsEmpresaodepto;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormatoEncabezado;
import com.ofbox.davivienda.h2h.abstraccion.dto.HthFormato;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthProcesoMonitoreoDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna id_proceso_monitoreo mapeado
	 * al atributo IdProcesoMonitoreo.
     */
    int IDPROCESOMONITOREO = 19001;
    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 19021;
    /**
     * Atributo que almacena el identificador de la columna Id_Vista_Convenio mapeado
	 * al atributo IdVistaConvenio.
     */
    int IDVISTACONVENIO = 19022;
    /**
     * Atributo que almacena el identificador de la columna Convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 19002;
    /**
     * Atributo que almacena el identificador de la columna TipoConvenio mapeado
	 * al atributo Tipoconvenio.
     */
    int TIPOCONVENIO = 19003;
    /**
     * Atributo que almacena el identificador de la columna EmpresaODepto mapeado
	 * al atributo Empresaodepto.
     */
    int EMPRESAODEPTO = 19023;
    /**
     * Atributo que almacena el identificador de la columna convenio_PE_ACH mapeado
	 * al atributo ConvenioPEACH.
     */
    int CONVENIOPEACH = 19004;
    /**
     * Atributo que almacena el identificador de la columna convenio_nombre mapeado
	 * al atributo ConvenioNombre.
     */
    int CONVENIONOMBRE = 19005;
    /**
     * Atributo que almacena el identificador de la columna nombre_archivo mapeado
	 * al atributo NombreArchivo.
     */
    int NOMBREARCHIVO = 19006;
    /**
     * Atributo que almacena el identificador de la columna ruta_sftp mapeado
	 * al atributo RutaSftp.
     */
    int RUTASFTP = 19007;
    /**
     * Atributo que almacena el identificador de la columna ruta_archivos_in mapeado
	 * al atributo RutaArchivosIn.
     */
    int RUTAARCHIVOSIN = 19008;
    /**
     * Atributo que almacena el identificador de la columna ruta_archivos_out mapeado
	 * al atributo RutaArchivosOut.
     */
    int RUTAARCHIVOSOUT = 19009;
    /**
     * Atributo que almacena el identificador de la columna ruta_procesados mapeado
	 * al atributo RutaProcesados.
     */
    int RUTAPROCESADOS = 19010;
    /**
     * Atributo que almacena el identificador de la columna tiempo_monitoreo mapeado
	 * al atributo TiempoMonitoreo.
     */
    int TIEMPOMONITOREO = 19011;
    /**
     * Atributo que almacena el identificador de la columna linea_encabezado mapeado
	 * al atributo LineaEncabezado.
     */
    int LINEAENCABEZADO = 19012;
    /**
     * Atributo que almacena el identificador de la columna id_formato_encabezado mapeado
	 * al atributo IdFormatoEncabezado.
     */
    int IDFORMATOENCABEZADO = 19024;
    /**
     * Atributo que almacena el identificador de la columna procesamiento_superusuario mapeado
	 * al atributo ProcesamientoSuperusuario.
     */
    int PROCESAMIENTOSUPERUSUARIO = 19013;
    /**
     * Atributo que almacena el identificador de la columna id_formato_detalle mapeado
	 * al atributo IdFormatoDetalle.
     */
    int IDFORMATODETALLE = 19025;
    /**
     * Atributo que almacena el identificador de la columna monto_maximo_procesar mapeado
	 * al atributo MontoMaximoProcesar.
     */
    int MONTOMAXIMOPROCESAR = 19014;
    /**
     * Atributo que almacena el identificador de la columna estado mapeado
	 * al atributo Estado.
     */
    int ESTADO = 19015;
    /**
     * Atributo que almacena el identificador de la columna usuario_creacion mapeado
	 * al atributo UsuarioCreacion.
     */
    int USUARIOCREACION = 19016;
    /**
     * Atributo que almacena el identificador de la columna fecha_creacion mapeado
	 * al atributo FechaCreacion.
     */
    int FECHACREACION = 19017;
    /**
     * Atributo que almacena el identificador de la columna usuario_ult_act mapeado
	 * al atributo UsuarioUltAct.
     */
    int USUARIOULTACT = 19018;
    /**
     * Atributo que almacena el identificador de la columna fecha_ult_act mapeado
	 * al atributo FechaUltAct.
     */
    int FECHAULTACT = 19019;
    /**
     * Atributo que almacena el identificador de la columna email_cliente mapeado
	 * al atributo EmailCliente.
     */
    int EMAILCLIENTE = 19020;
    /**
     * Atributo que almacena el identificador de la columna host_sftp mapeado
	 * al atributo HostSftp.
     */
    int PMHOSTSFTP = 19026;
    /**
     * Atributo que almacena el identificador de la columna puerto_sftp mapeado
	 * al atributo PuertoSftp.
     */
    int PMPUERTOSFTP = 19027;
    /**
     * Atributo que almacena el identificador de la columna usuario_sftp mapeado
	 * al atributo UsuarioSftp.
     */
    int PMUSUARIOSFTP = 19028;
    /**
     * Atributo que almacena el identificador de la columna password_sftp mapeado
	 * al atributo PasswordSftp.
     */
    int PMPASSWORDSFTP = 19029;
    /**
     * Atributo que almacena el identificador de la columna llave_sftp mapeado
	 * al atributo LlaveSftp.
     */
    int PMLLAVESFTP = 19030;
    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthProcesoMonitoreo buscarPorID(Long idProcesoMonitoreoId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idProcesoMonitoreoId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthProcesoMonitoreo hthProcesoMonitoreo) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthProcesoMonitoreo hthProcesoMonitoreo) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthProcesoMonitoreo resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsCliente getBsCliente(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsCliente getBsCliente(HthProcesoMonitoreo hthProcesoMonitoreo, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    HthVistaConvenio getHthVistaConvenio(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	HthVistaConvenio getHthVistaConvenio(HthProcesoMonitoreo hthProcesoMonitoreo, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsEmpresaodepto getBsEmpresaodepto(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsEmpresaodepto getBsEmpresaodepto(HthProcesoMonitoreo hthProcesoMonitoreo, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    HthFormatoEncabezado getHthFormatoEncabezado(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	HthFormatoEncabezado getHthFormatoEncabezado(HthProcesoMonitoreo hthProcesoMonitoreo, Transaccion tx)throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    HthFormato getHthFormato(HthProcesoMonitoreo hthProcesoMonitoreo)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	HthFormato getHthFormato(HthProcesoMonitoreo hthProcesoMonitoreo, Transaccion tx)throws ExcepcionEnDAO;
    	 
}