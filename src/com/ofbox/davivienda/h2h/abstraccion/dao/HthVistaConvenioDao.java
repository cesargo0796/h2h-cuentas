package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.HthVistaConvenio;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface HthVistaConvenioDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna Id_Vista_Convenio mapeado
	 * al atributo IdVistaConvenio.
     */
    int IDVISTACONVENIO = 20001;
    /**
     * Atributo que almacena el identificador de la columna TipoConvenio mapeado
	 * al atributo Tipoconvenio.
     */
    int TIPOCONVENIO = 20002;
    /**
     * Atributo que almacena el identificador de la columna Convenio mapeado
	 * al atributo Convenio.
     */
    int CONVENIO = 20003;
    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 20004;
    /**
     * Atributo que almacena el identificador de la columna Nombre mapeado
	 * al atributo Nombre.
     */
    int NOMBRE = 20005;
    /**
     * Atributo que almacena el identificador de la columna Cuenta mapeado
	 * al atributo Cuenta.
     */
    int CUENTA = 20006;
    /**
     * Atributo que almacena el identificador de la columna NombreCta mapeado
	 * al atributo Nombrecta.
     */
    int NOMBRECTA = 20007;
    /**
     * Atributo que almacena el identificador de la columna TipoMoneda mapeado
	 * al atributo Tipomoneda.
     */
    int TIPOMONEDA = 20008;
    /**
     * Atributo que almacena el identificador de la columna Descripcion mapeado
	 * al atributo Descripcion.
     */
    int DESCRIPCION = 20009;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	HthVistaConvenio buscarPorID(Long idVistaConvenioId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long idVistaConvenioId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(HthVistaConvenio hthVistaConvenio) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(HthVistaConvenio hthVistaConvenio) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	HthVistaConvenio resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}