package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsBancoAch;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsCodigosBanco;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsRedOperacionBanco;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsCodigosBancoDao extends Dao{
	
    /**
     * Atributo que almacena el identificador de la columna CodBanco mapeado
	 * al atributo Codbanco.
     */
    int IDCODBANCO = 5001;
    /**
     * Atributo que almacena el codigo swift
	 * al atributo CodigoSwift.
     */
    int CODIGOSWIFT = 5002;
    /**
     * Atributo que almacena el codigo UNI
	 * al atributo CodigoUni.
     */
    int CODIGOUNI = 5003;
    /**
     * Atributo que almacena el codigo Transfer
	 * al atributo CodigoTransfer.
     */
    int CODIGOTRANSFER = 5004;
    /**
     * Atributo que almacena el identificador de la red de operacion
	 * al atributo IdRedOperacion.
     */
    int IDREDOPERACION = 5005;
    /**
     * Atributo que almacena el identificador de la red de operacion
	 * al atributo IdRedOperacion.
     */
    int STATUS = 5006;
    /**
     * Atributo que almacena el identificador de la red de operacion
	 * al atributo IdRedOperacion.
     */
    int CODIBANCO = 5007;
    
    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
    BsCodigosBanco buscarPorID(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsCodigosBanco bsCodigosBanco) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsCodigosBanco BsCodigosBanco) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsCodigosBanco resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsRedOperacionBanco getBsRedOperacionBanco(BsCodigosBanco bsCodigosBanco)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
    BsRedOperacionBanco getBsRedOperacionBanco(BsCodigosBanco bsCodigosBanco, Transaccion tx)throws ExcepcionEnDAO;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsBancoAch getBsBancoAch(BsCodigosBanco bsCodigosBanco)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
    BsBancoAch getBsBancoAch(BsCodigosBanco bsCodigosBanco, Transaccion tx)throws ExcepcionEnDAO;
	
    	 
}