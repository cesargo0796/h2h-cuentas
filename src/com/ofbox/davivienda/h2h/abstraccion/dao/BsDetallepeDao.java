package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetallepe;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetallepeID;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.foraneos.DataConValoresForaneos;    
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepe;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsDetallepeDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna Instalacion mapeado
	 * al atributo Instalacion.
     */
    int INSTALACION = 6001;
    /**
     * Atributo que almacena el identificador de la columna Lote mapeado
	 * al atributo Lote.
     */
    int LOTE = 6002;
    /**
     * Atributo que almacena el identificador de la columna Operacion mapeado
	 * al atributo Operacion.
     */
    int OPERACION = 6003;
    /**
     * Atributo que almacena el identificador de la columna Cuenta mapeado
	 * al atributo Cuenta.
     */
    int CUENTA = 6004;
    /**
     * Atributo que almacena el identificador de la columna TipoOperacion mapeado
	 * al atributo Tipooperacion.
     */
    int TIPOOPERACION = 6005;
    /**
     * Atributo que almacena el identificador de la columna Monto mapeado
	 * al atributo Monto.
     */
    int MONTO = 6006;
    /**
     * Atributo que almacena el identificador de la columna MontoAplicado mapeado
	 * al atributo Montoaplicado.
     */
    int MONTOAPLICADO = 6007;
    /**
     * Atributo que almacena el identificador de la columna Adenda mapeado
	 * al atributo Adenda.
     */
    int ADENDA = 6008;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 6009;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 6010;
    /**
     * Atributo que almacena el identificador de la columna IDPago mapeado
	 * al atributo Idpago.
     */
    int IDPAGO = 6011;
    /**
     * Atributo que almacena el identificador de la columna AutorizacionHost mapeado
	 * al atributo Autorizacionhost.
     */
    int AUTORIZACIONHOST = 6012;
    /**
     * Atributo que almacena el identificador de la columna EnviarAHost mapeado
	 * al atributo Enviarahost.
     */
    int ENVIARAHOST = 6013;
    /**
     * Atributo que almacena el identificador de la columna Contrasena mapeado
	 * al atributo Contrasena.
     */
    int CONTRASENA = 6014;
    /**
     * Atributo que almacena el identificador de la columna NombreBenef mapeado
	 * al atributo Nombrebenef.
     */
    int NOMBREBENEF = 6015;
    /**
     * Atributo que almacena el identificador de la columna DireccionBenef mapeado
	 * al atributo Direccionbenef.
     */
    int DIRECCIONBENEF = 6016;
    /**
     * Atributo que almacena el identificador de la columna Email mapeado
	 * al atributo Email.
     */
    int EMAIL = 6017;
    /**
     * Atributo que almacena el identificador de la columna ComisionAplicada mapeado
	 * al atributo Comisionaplicada.
     */
    int COMISIONAPLICADA = 6018;
    /**
     * Atributo que almacena el identificador de la columna IVAAplicado mapeado
	 * al atributo Ivaaplicado.
     */
    int IVAAPLICADO = 6019;
    /**
     * Atributo que almacena el identificador de la columna NombreCuenta mapeado
	 * al atributo Nombrecuenta.
     */
    int NOMBRECUENTA = 6020;
    /**
     * Atributo que almacena el identificador de la columna NIU mapeado
	 * al atributo Niu.
     */
    int NIU = 6021;
    /**
     * Atributo que almacena el identificador de la columna Autorizador mapeado
	 * al atributo Autorizador.
     */
    int AUTORIZADOR = 6022;
    /**
     * Atributo que almacena el identificador de la columna Sublote mapeado
	 * al atributo Sublote.
     */
    int SUBLOTE = 6023;
    /**
     * Atributo que almacena el identificador de la columna CuentaDebito mapeado
	 * al atributo Cuentadebito.
     */
    int CUENTADEBITO = 6024;
    /**
     * Atributo que almacena el identificador de la columna NombreDeCuenta mapeado
	 * al atributo Nombredecuenta.
     */
    int NOMBREDECUENTA = 6025;
    /**
     * Atributo que almacena el identificador de la columna MontoImpuesto mapeado
	 * al atributo Montoimpuesto.
     */
    int MONTOIMPUESTO = 6026;
    /**
     * Atributo que almacena el identificador de la columna CodigoORefInt mapeado
	 * al atributo Codigoorefint.
     */
    int CODIGOOREFINT = 6027;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsDetallepe buscarPorID(BsDetallepeID bsDetallepeID) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(BsDetallepeID bsDetallepeID) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsDetallepe bsDetallepe) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsDetallepe bsDetallepe) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsDetallepe resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
	/**
	 * M&eacute;todo que devuelve un DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado.
	 * @param <code>Objeto</code> DTO
	 * @return <code>Objeto</code> - DTO for&aacute;neo
	 * @throws ExcepcionEnDAO
	 */
    BsLotepe getBsLotepe(BsDetallepe bsDetallepe)throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve el DTO for&aacute;neo, relacionado con el DTO espec&iacute;ficado,
     * y utilizando un objeto de tipo <code>Transaccion</code>.
     * 
     * @param <code>Objeto</code> DTO
     * @param <code>tx</code> objeto del tipo {@link Transaccion}
     * @return <code>Objeto</code> - DTO for&aacute;neo
     * @throws ExcepcionEnDAO
     */
	BsLotepe getBsLotepe(BsDetallepe bsDetallepe, Transaccion tx)throws ExcepcionEnDAO;
    	 
}