package com.ofbox.davivienda.h2h.abstraccion.dao;

import com.ofbox.davivienda.h2h.abstraccion.dto.BsCliente;
import java.lang.Long;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionEnDAO;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.ExcepcionPorDatoNoEncontrado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Paginador;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.PeticionDeDatos;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Transaccion;

/**
 * Interfaz que define el comportamiento para un objeto de negocio espec&iacute;fico, para que pueda interactuar con la capa DAO,
 * extiende de la interfaz {@link com.ofbox.f3.app.distribuibles.abstraccion.comunes.dao.Dao Dao}.
 * @author System Out of the Box
 * @version f3.1
 *  
 */
public interface BsClienteDao extends Dao{

    /**
     * Atributo que almacena el identificador de la columna Cliente mapeado
	 * al atributo Cliente.
     */
    int CLIENTE = 1;
    /**
     * Atributo que almacena el identificador de la columna Nombre mapeado
	 * al atributo Nombre.
     */
    int NOMBRE = 2;
    /**
     * Atributo que almacena el identificador de la columna Modulos mapeado
	 * al atributo Modulos.
     */
    int MODULOS = 3;
    /**
     * Atributo que almacena el identificador de la columna Direccion mapeado
	 * al atributo Direccion.
     */
    int DIRECCION = 4;
    /**
     * Atributo que almacena el identificador de la columna Telefono mapeado
	 * al atributo Telefono.
     */
    int TELEFONO = 5;
    /**
     * Atributo que almacena el identificador de la columna Fax mapeado
	 * al atributo Fax.
     */
    int FAX = 6;
    /**
     * Atributo que almacena el identificador de la columna Email mapeado
	 * al atributo Email.
     */
    int EMAIL = 7;
    /**
     * Atributo que almacena el identificador de la columna NombreContacto mapeado
	 * al atributo Nombrecontacto.
     */
    int NOMBRECONTACTO = 8;
    /**
     * Atributo que almacena el identificador de la columna ApellidoContacto mapeado
	 * al atributo Apellidocontacto.
     */
    int APELLIDOCONTACTO = 9;
    /**
     * Atributo que almacena el identificador de la columna NumInstalacion mapeado
	 * al atributo Numinstalacion.
     */
    int NUMINSTALACION = 10;
    /**
     * Atributo que almacena el identificador de la columna FechaEstatus mapeado
	 * al atributo Fechaestatus.
     */
    int FECHAESTATUS = 11;
    /**
     * Atributo que almacena el identificador de la columna Estatus mapeado
	 * al atributo Estatus.
     */
    int ESTATUS = 12;
    /**
     * Atributo que almacena el identificador de la columna FechaCreacion mapeado
	 * al atributo Fechacreacion.
     */
    int FECHACREACION = 13;
    /**
     * Atributo que almacena el identificador de la columna SolicitudPendiente mapeado
	 * al atributo Solicitudpendiente.
     */
    int SOLICITUDPENDIENTE = 14;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridadTEx mapeado
	 * al atributo Nivelseguridadtex.
     */
    int NIVELSEGURIDADTEX = 15;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridadPImp mapeado
	 * al atributo NivelseguridadPIMP.
     */
    int NIVELSEGURIDADPIMP = 16;
    /**
     * Atributo que almacena el identificador de la columna nombreRepresentante mapeado
	 * al atributo Nombrerepresentante.
     */
    int NOMBREREPRESENTANTE = 17;
    /**
     * Atributo que almacena el identificador de la columna apellidoRepresentante mapeado
	 * al atributo Apellidorepresentante.
     */
    int APELLIDOREPRESENTANTE = 18;
    /**
     * Atributo que almacena el identificador de la columna proveedorInternet mapeado
	 * al atributo Proveedorinternet.
     */
    int PROVEEDORINTERNET = 19;
    /**
     * Atributo que almacena el identificador de la columna Banca mapeado
	 * al atributo Banca.
     */
    int BANCA = 20;
    /**
     * Atributo que almacena el identificador de la columna PoliticaCobroTEx mapeado
	 * al atributo Politicacobrotex.
     */
    int POLITICACOBROTEX = 21;
    /**
     * Atributo que almacena el identificador de la columna ComisionTransExt mapeado
	 * al atributo Comisiontransext.
     */
    int COMISIONTRANSEXT = 22;
    /**
     * Atributo que almacena el identificador de la columna CodigoTablaTransExt mapeado
	 * al atributo Codigotablatransext.
     */
    int CODIGOTABLATRANSEXT = 23;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridadISSS mapeado
	 * al atributo Nivelseguridadisss.
     */
    int NIVELSEGURIDADISSS = 24;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridadAFP mapeado
	 * al atributo Nivelseguridadafp.
     */
    int NIVELSEGURIDADAFP = 25;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridadCCre mapeado
	 * al atributo Nivelseguridadccre.
     */
    int NIVELSEGURIDADCCRE = 26;
    /**
     * Atributo que almacena el identificador de la columna NivelSeguridadPPag mapeado
	 * al atributo Nivelseguridadppag.
     */
    int NIVELSEGURIDADPPAG = 27;
    /**
     * Atributo que almacena el identificador de la columna CargaPersonalizable mapeado
	 * al atributo Cargapersonalizable.
     */
    int CARGAPERSONALIZABLE = 28;
    /**
     * Atributo que almacena el identificador de la columna TipoDoctoRepresentante mapeado
	 * al atributo Tipodoctorepresentante.
     */
    int TIPODOCTOREPRESENTANTE = 29;
    /**
     * Atributo que almacena el identificador de la columna DoctoRepresentante mapeado
	 * al atributo Doctorepresentante.
     */
    int DOCTOREPRESENTANTE = 30;
    /**
     * Atributo que almacena el identificador de la columna Ciudad mapeado
	 * al atributo Ciudad.
     */
    int CIUDAD = 31;
    /**
     * Atributo que almacena el identificador de la columna Cargo mapeado
	 * al atributo Cargo.
     */
    int CARGO = 32;
    /**
     * Atributo que almacena el identificador de la columna LimiteXArchivo mapeado
	 * al atributo Limitexarchivo.
     */
    int LIMITEXARCHIVO = 33;
    /**
     * Atributo que almacena el identificador de la columna LimiteXLote mapeado
	 * al atributo Limitexlote.
     */
    int LIMITEXLOTE = 34;
    /**
     * Atributo que almacena el identificador de la columna LimiteXTransaccion mapeado
	 * al atributo Limitextransaccion.
     */
    int LIMITEXTRANSACCION = 35;
    /**
     * Atributo que almacena el identificador de la columna PermiteDebitos mapeado
	 * al atributo Permitedebitos.
     */
    int PERMITEDEBITOS = 36;
    /**
     * Atributo que almacena el identificador de la columna LimiteCreditos mapeado
	 * al atributo Limitecreditos.
     */
    int LIMITECREDITOS = 37;
    /**
     * Atributo que almacena el identificador de la columna LimiteDebitos mapeado
	 * al atributo Limitedebitos.
     */
    int LIMITEDEBITOS = 38;
    /**
     * Atributo que almacena el identificador de la columna ComisionTrnACH mapeado
	 * al atributo Comisiontrnach.
     */
    int COMISIONTRNACH = 39;

    /**
     * M&eacute;todo  que devuelve un DTO, formado con un objeto devuelto por la capa DAO,
	 * obtenido a trav&eacute;s de la b&uacute;squeda por id.<br/>
     * El DTO devuelto representa un registro a nivel de base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO.
     * @return <code>Objeto</code>
     * @throws ExcepcionEnDA
     */
	BsCliente buscarPorID(Long clienteId) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que debe eliminar un objeto a trav&eacute;s de un id.<br/>
     * Elimina un registro a nivel de la base de datos.
     * @param <code>id</code> Objeto que identifica la llave del DTO. 
     * @throws ExcepcionEnDAO
     */
	void eliminar(Long clienteId) throws ExcepcionEnDAO;	
    /**
     * M&eacute;todo que debe actualizar un objeto.
     * Actualiza un registro a nivel de la base de datos.
     * @param <code>Objeto</code> DTO
     * @throws ExcepcionEnDAO
     */	
	void actualizar(BsCliente bsCliente) throws ExcepcionEnDAO;
	/**
	 * M&eacute;todo que debe crear un objeto para el negocio. 
     * Crea un nuevo registro a nivel de base de datos.
	 * @param <code>Objeto</code> DTO.
	 * @throws ExcepcionEnDAO
	 */	
	void crear(BsCliente bsCliente) throws ExcepcionEnDAO;
    /**
     * M&eacute;todo que devuelve un DTO, solicitado a trav&eacute;s de un objeto de tipo {@link PeticionDeDatos}.
     * @param peticion objeto de tipo <code>PeticionDeDatos</code>
     * @return <code>Objeto</code> DTO.
     * @throws ExcepcionEnDAO
     * @throws ExcepcionPorDatoNoEncontrado
     */
	BsCliente resultadoUnico(PeticionDeDatos peticion) throws ExcepcionEnDAO, ExcepcionPorDatoNoEncontrado;
	
    	 
}