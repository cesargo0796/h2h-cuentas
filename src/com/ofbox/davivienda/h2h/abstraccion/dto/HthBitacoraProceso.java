package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthBitacoraProceso implements Serializable{
public static final String resultado_proceso_OK = "O";
public static final String resultado_proceso_ERROR = "E";
public static final String resultado_proceso_ADVERTENCIA = "A";
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idBitacoraProceso;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idBitacoraProceso;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idBitacoraProceso = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private java.util.Date fechaProceso;
	 
     private String resultadoProceso;
	 
     private String descripcionResultado;
	 
     private String stackTrace;
	 
     private String cliente;
	 
     private String convenio;
	 
     private String archivo;
	 
     private String lineasBitacora;

	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdBitacoraProceso(){
		return this.idBitacoraProceso;         
     }

     public void setIdBitacoraProceso( Long idBitacoraProceso){
		this.idBitacoraProceso = idBitacoraProceso;
     }
     public java.util.Date getFechaProceso(){
		return this.fechaProceso;         
     }

     public void setFechaProceso( java.util.Date fechaProceso){
		this.fechaProceso = fechaProceso;
     }
     public String getResultadoProceso(){
		return this.resultadoProceso;         
     }

     public void setResultadoProceso( String resultadoProceso){
		this.resultadoProceso = resultadoProceso;
     }
     public String getDescripcionResultado(){
		return this.descripcionResultado;         
     }

     public void setDescripcionResultado( String descripcionResultado){
		this.descripcionResultado = descripcionResultado;
     }
     public String getStackTrace(){
		return this.stackTrace;         
     }

     public void setStackTrace( String stackTrace){
		this.stackTrace = stackTrace;
     }
     public String getCliente(){
		return this.cliente;         
     }

     public void setCliente( String cliente){
		this.cliente = cliente;
     }
     public String getConvenio(){
		return this.convenio;         
     }

     public void setConvenio( String convenio){
		this.convenio = convenio;
     }
     public String getArchivo(){
		return this.archivo;         
     }

     public void setArchivo( String archivo){
		this.archivo = archivo;
     }
     public String getLineasBitacora(){
		return this.lineasBitacora;         
     }

     public void setLineasBitacora( String lineasBitacora){
		this.lineasBitacora = lineasBitacora;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthBitacoraProceso otherHthBitacoraProceso = (HthBitacoraProceso) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(fechaProceso, otherHthBitacoraProceso.fechaProceso);
           eq.append(resultadoProceso, otherHthBitacoraProceso.resultadoProceso);
           eq.append(descripcionResultado, otherHthBitacoraProceso.descripcionResultado);
           eq.append(stackTrace, otherHthBitacoraProceso.stackTrace);
           eq.append(cliente, otherHthBitacoraProceso.cliente);
           eq.append(convenio, otherHthBitacoraProceso.convenio);
           eq.append(archivo, otherHthBitacoraProceso.archivo);
           eq.append(lineasBitacora, otherHthBitacoraProceso.lineasBitacora);
           eq.append(idBitacoraProceso, otherHthBitacoraProceso.idBitacoraProceso);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(fechaProceso)
                  .append(resultadoProceso)
                  .append(descripcionResultado)
                  .append(stackTrace)
                  .append(cliente)
                  .append(convenio)
                  .append(archivo)
                  .append(lineasBitacora)
                  .append(idBitacoraProceso)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("fechaProceso",fechaProceso)
                  .append("resultadoProceso",resultadoProceso)
                  .append("descripcionResultado",descripcionResultado)
                  .append("stackTrace",stackTrace)
                  .append("cliente",cliente)
                  .append("convenio",convenio)
                  .append("archivo",archivo)
                  .append("lineasBitacora",lineasBitacora)
                  .append("idBitacoraProceso",idBitacoraProceso)
                  .toString();
   }    
    
    
}
