package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * The persistent class for the BS_RedOperacionBanco database table.
 * 
 */
public class BsRedOperacionBanco implements Serializable {

	private Long idRedOperacion;
	private String interpretacion;

	/**
	 * Devuelve el objeto que almacena el identificador del DTO.
	 * 
	 * @return
	 */
	public Long getDtoId() {
		return idRedOperacion;
	}

	/**
	 * Coloca el objeto que almacena el identificador del DTO.
	 * 
	 * @param dtoId
	 *            objeto que almacena la llave que identifica al DTO.
	 */
	public void setDtoId(Long dtoId) {
		this.idRedOperacion = dtoId;
	}

	public Long getIdRedOperacion() {
		return this.idRedOperacion;
	}

	public void setIdRedOperacion(Long idRedOperacion) {
		this.idRedOperacion = idRedOperacion;
	}

	public String getInterpretacion() {
		return this.interpretacion;
	}

	public void setInterpretacion(String interpretacion) {
		this.interpretacion = interpretacion;
	}
	
    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsRedOperacionBanco otherBsCliente = (BsRedOperacionBanco) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(idRedOperacion, otherBsCliente.idRedOperacion);
           eq.append(interpretacion, otherBsCliente.interpretacion);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(idRedOperacion)
                  .append(interpretacion)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("idRedOperacion",idRedOperacion)
                  .append("interpretacion",interpretacion)
                  .toString();
   }   

}