package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthParametroGeneral implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idParametroGeneral;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idParametroGeneral;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idParametroGeneral = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private String nombreParametro;
	 
     private String valorParametro;
	 
     private String usuarioCreacion;
	 
     private java.util.Date fechaCreacion;
	 
     private String usuarioUltAct;
	 
     private java.util.Date fechaUltAct;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdParametroGeneral(){
		return this.idParametroGeneral;         
     }

     public void setIdParametroGeneral( Long idParametroGeneral){
		this.idParametroGeneral = idParametroGeneral;
     }
     public String getNombreParametro(){
		return this.nombreParametro;         
     }

     public void setNombreParametro( String nombreParametro){
		this.nombreParametro = nombreParametro;
     }
     public String getValorParametro(){
		return this.valorParametro;         
     }

     public void setValorParametro( String valorParametro){
		this.valorParametro = valorParametro;
     }
     public String getUsuarioCreacion(){
		return this.usuarioCreacion;         
     }

     public void setUsuarioCreacion( String usuarioCreacion){
		this.usuarioCreacion = usuarioCreacion;
     }
     public java.util.Date getFechaCreacion(){
		return this.fechaCreacion;         
     }

     public void setFechaCreacion( java.util.Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
     }
     public String getUsuarioUltAct(){
		return this.usuarioUltAct;         
     }

     public void setUsuarioUltAct( String usuarioUltAct){
		this.usuarioUltAct = usuarioUltAct;
     }
     public java.util.Date getFechaUltAct(){
		return this.fechaUltAct;         
     }

     public void setFechaUltAct( java.util.Date fechaUltAct){
		this.fechaUltAct = fechaUltAct;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthParametroGeneral otherHthParametroGeneral = (HthParametroGeneral) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(nombreParametro, otherHthParametroGeneral.nombreParametro);
           eq.append(valorParametro, otherHthParametroGeneral.valorParametro);
           eq.append(usuarioCreacion, otherHthParametroGeneral.usuarioCreacion);
           eq.append(fechaCreacion, otherHthParametroGeneral.fechaCreacion);
           eq.append(usuarioUltAct, otherHthParametroGeneral.usuarioUltAct);
           eq.append(fechaUltAct, otherHthParametroGeneral.fechaUltAct);
           eq.append(idParametroGeneral, otherHthParametroGeneral.idParametroGeneral);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(nombreParametro)
                  .append(valorParametro)
                  .append(usuarioCreacion)
                  .append(fechaCreacion)
                  .append(usuarioUltAct)
                  .append(fechaUltAct)
                  .append(idParametroGeneral)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("nombreParametro",nombreParametro)
                  .append("valorParametro",valorParametro)
                  .append("usuarioCreacion",usuarioCreacion)
                  .append("fechaCreacion",fechaCreacion)
                  .append("usuarioUltAct",usuarioUltAct)
                  .append("fechaUltAct",fechaUltAct)
                  .append("idParametroGeneral",idParametroGeneral)
                  .toString();
   }    
    
    
}
