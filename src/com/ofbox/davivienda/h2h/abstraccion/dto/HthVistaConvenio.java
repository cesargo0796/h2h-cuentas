package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthVistaConvenio implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idVistaConvenio;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idVistaConvenio;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idVistaConvenio = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long tipoconvenio;
	 
     private Long convenio;
	 
     private Long cliente;
	 
     private String nombre;
	 
     private String cuenta;
	 
     private String nombrecta;
	 
     private Long tipomoneda;
	 
     private String descripcion;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdVistaConvenio(){
		return this.idVistaConvenio;         
     }

     public void setIdVistaConvenio( Long idVistaConvenio){
		this.idVistaConvenio = idVistaConvenio;
     }
     public Long getTipoconvenio(){
		return this.tipoconvenio;         
     }

     public void setTipoconvenio( Long tipoconvenio){
		this.tipoconvenio = tipoconvenio;
     }
     public Long getConvenio(){
		return this.convenio;         
     }

     public void setConvenio( Long convenio){
		this.convenio = convenio;
     }
     public Long getCliente(){
		return this.cliente;         
     }

     public void setCliente( Long cliente){
		this.cliente = cliente;
     }
     public String getNombre(){
		return this.nombre;         
     }

     public void setNombre( String nombre){
		this.nombre = nombre;
     }
     public String getCuenta(){
		return this.cuenta;         
     }

     public void setCuenta( String cuenta){
		this.cuenta = cuenta;
     }
     public String getNombrecta(){
		return this.nombrecta;         
     }

     public void setNombrecta( String nombrecta){
		this.nombrecta = nombrecta;
     }
     public Long getTipomoneda(){
		return this.tipomoneda;         
     }

     public void setTipomoneda( Long tipomoneda){
		this.tipomoneda = tipomoneda;
     }
     public String getDescripcion(){
		return this.descripcion;         
     }

     public void setDescripcion( String descripcion){
		this.descripcion = descripcion;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthVistaConvenio otherHthVistaConvenio = (HthVistaConvenio) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(tipoconvenio, otherHthVistaConvenio.tipoconvenio);
           eq.append(convenio, otherHthVistaConvenio.convenio);
           eq.append(cliente, otherHthVistaConvenio.cliente);
           eq.append(nombre, otherHthVistaConvenio.nombre);
           eq.append(cuenta, otherHthVistaConvenio.cuenta);
           eq.append(nombrecta, otherHthVistaConvenio.nombrecta);
           eq.append(tipomoneda, otherHthVistaConvenio.tipomoneda);
           eq.append(descripcion, otherHthVistaConvenio.descripcion);
           eq.append(idVistaConvenio, otherHthVistaConvenio.idVistaConvenio);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(tipoconvenio)
                  .append(convenio)
                  .append(cliente)
                  .append(nombre)
                  .append(cuenta)
                  .append(nombrecta)
                  .append(tipomoneda)
                  .append(descripcion)
                  .append(idVistaConvenio)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("tipoconvenio",tipoconvenio)
                  .append("convenio",convenio)
                  .append("cliente",cliente)
                  .append("nombre",nombre)
                  .append("cuenta",cuenta)
                  .append("nombrecta",nombrecta)
                  .append("tipomoneda",tipomoneda)
                  .append("descripcion",descripcion)
                  .append("idVistaConvenio",idVistaConvenio)
                  .toString();
   }    
    
    
}
