package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BsBancoAch implements Serializable {
	private Long codBanco;
	private String codPayBank;
	private Long digitoChequeo;
	private Long enlinea;
	private String esActivo;
	private Long idAs400;
	private Long longitudCtaAhorro;
	private Long longitudCtaCorriente;
	private Long longitudCtaPrestamo;
	private Long longitudTjCredito;
	private Long moneda;
	private String nombre;
	
	/**
	 * Devuelve el objeto que almacena el identificador del DTO.
	 * 
	 * @return
	 */
	public Long getDtoId() {
		return codBanco;
	}

	/**
	 * Coloca el objeto que almacena el identificador del DTO.
	 * 
	 * @param dtoId
	 *            objeto que almacena la llave que identifica al DTO.
	 */
	public void setDtoId(Long dtoId) {
		this.codBanco = dtoId;
	}
	
	public Long getCodBanco() {
		return codBanco;
	}
	public void setCodBanco(Long codBanco) {
		this.codBanco = codBanco;
	}
	public String getCodPayBank() {
		return codPayBank;
	}
	public void setCodPayBank(String codPayBank) {
		this.codPayBank = codPayBank;
	}
	public Long getDigitoChequeo() {
		return digitoChequeo;
	}
	public void setDigitoChequeo(Long digitoChequeo) {
		this.digitoChequeo = digitoChequeo;
	}
	public Long getEnlinea() {
		return enlinea;
	}
	public void setEnlinea(Long enlinea) {
		this.enlinea = enlinea;
	}
	public String getEsActivo() {
		return esActivo;
	}
	public void setEsActivo(String esActivo) {
		this.esActivo = esActivo;
	}
	public Long getIdAs400() {
		return idAs400;
	}
	public void setIdAs400(Long idAs400) {
		this.idAs400 = idAs400;
	}
	public Long getLongitudCtaAhorro() {
		return longitudCtaAhorro;
	}
	public void setLongitudCtaAhorro(Long longitudCtaAhorro) {
		this.longitudCtaAhorro = longitudCtaAhorro;
	}
	public Long getLongitudCtaCorriente() {
		return longitudCtaCorriente;
	}
	public void setLongitudCtaCorriente(Long longitudCtaCorriente) {
		this.longitudCtaCorriente = longitudCtaCorriente;
	}
	public Long getLongitudCtaPrestamo() {
		return longitudCtaPrestamo;
	}
	public void setLongitudCtaPrestamo(Long longitudCtaPrestamo) {
		this.longitudCtaPrestamo = longitudCtaPrestamo;
	}
	public Long getLongitudTjCredito() {
		return longitudTjCredito;
	}
	public void setLongitudTjCredito(Long longitudTjCredito) {
		this.longitudTjCredito = longitudTjCredito;
	}
	public Long getMoneda() {
		return moneda;
	}
	public void setMoneda(Long moneda) {
		this.moneda = moneda;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsBancoAch otherBsCliente = (BsBancoAch) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(codBanco, otherBsCliente.codBanco);
           eq.append(codPayBank, otherBsCliente.codPayBank);
           eq.append(digitoChequeo, otherBsCliente.digitoChequeo);
           eq.append(enlinea, otherBsCliente.enlinea);
           eq.append(esActivo, otherBsCliente.esActivo);
           eq.append(idAs400, otherBsCliente.idAs400);
           eq.append(longitudCtaAhorro, otherBsCliente.longitudCtaAhorro);
           eq.append(longitudCtaCorriente, otherBsCliente.longitudCtaCorriente);
           eq.append(longitudCtaPrestamo, otherBsCliente.longitudCtaPrestamo);
           eq.append(longitudTjCredito, otherBsCliente.longitudTjCredito);
           eq.append(moneda, otherBsCliente.moneda);
           eq.append(nombre, otherBsCliente.nombre);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(codBanco)
                  .append(codPayBank)
                  .append(digitoChequeo)
                  .append(enlinea)
                  .append(esActivo)
                  .append(idAs400)
                  .append(longitudCtaAhorro)
                  .append(longitudCtaCorriente)
                  .append(longitudCtaPrestamo)
                  .append(longitudTjCredito)
                  .append(moneda)
                  .append(nombre)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("codBanco",codBanco)
                  .append("codPayBank",codPayBank)
                  .append("digitoChequeo",digitoChequeo)
                  .append("enlinea",enlinea)
                  .append("esActivo",esActivo)
                  .append("idAs400",idAs400)
                  .append("longitudCtaAhorro",longitudCtaAhorro)
                  .append("longitudCtaCorriente",longitudCtaCorriente)
                  .append("longitudCtaPrestamo",longitudCtaPrestamo)
                  .append("longitudTjCredito",longitudTjCredito)
                  .append("moneda",moneda)
                  .append("nombre",nombre)
                  .toString();
   } 
}
