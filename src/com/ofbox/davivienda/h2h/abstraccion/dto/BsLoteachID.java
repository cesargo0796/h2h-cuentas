package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class BsLoteachID implements Serializable{


     private Long codlote;
     
     private Long instalacion;
     
     private Long lote;
     
   
     
     public Long getCodlote(){
         return this.codlote;
     }

     public void setCodlote( Long codlote){
          this.codlote = codlote;
     }
     
     
     public Long getInstalacion(){
         return this.instalacion;
     }

     public void setInstalacion( Long instalacion){
          this.instalacion = instalacion;
     }
     
     
     public Long getLote(){
         return this.lote;
     }

     public void setLote( Long lote){
          this.lote = lote;
     }
     

   public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsLoteachID otherBsLoteachID = (BsLoteachID) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(codlote, otherBsLoteachID.codlote);
           eq.append(instalacion, otherBsLoteachID.instalacion);
           eq.append(lote, otherBsLoteachID.lote);
    	   return eq.isEquals();
    }
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(codlote)
                  .append(instalacion)
                  .append(lote)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("codlote",codlote)
                  .append("instalacion",instalacion)
                  .append("lote",lote)
                  .toString();
   }
    
}
