package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class HthProcesoMonitoreo implements Serializable{
public static final String convenio_PE_ACH_PE = "PE";
public static final String convenio_PE_ACH_ACH = "ACH";
public static final String convenio_PS = "PS";
public static final String convenio_TEXT = "TEXT";
public static final String convenio_nombre_PLANILLA = "PLANILLA";
public static final String convenio_nombre_PROVEEDORES = "PROVEEDORES";
public static final String convenio_nombre_COBROS = "COBROS";
public static final String convenio_nombre_CHEQUES = "CHEQUES";
public static final String convenio_nombre_PAGO_PLANILLAS_ACH = "ACH_PLANILLA";
public static final String convenio_nombre_PAGO_PROVEEDORES_ACH = "ACH_PROVEEDORES";
public static final String linea_encabezado_SI = "S";
public static final String linea_encabezado_NO = "N";
public static final String procesamiento_superusuario_SI = "S";
public static final String procesamiento_superusuario_NO = "N";
public static final String estado_ACTIVADO = "A";
public static final String estado_DESACTIVADO = "D";
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long idProcesoMonitoreo;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return idProcesoMonitoreo;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.idProcesoMonitoreo = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long convenio;
	 
     private Long tipoconvenio;
	 
     private String convenioPEACH;
	 
     private String convenioNombre;
	 
     private String nombreArchivo;
	 
     private String rutaSftp;
	 
     private String rutaArchivosIn;
	 
     private String rutaArchivosOut;
	 
     private String rutaProcesados;
	 
     private Long tiempoMonitoreo;
	 
     private String lineaEncabezado;
	 
     private String procesamientoSuperusuario;
	 
     private java.math.BigDecimal montoMaximoProcesar;
	 
     private String estado;
	 
     private String usuarioCreacion;
	 
     private java.util.Date fechaCreacion;
	 
     private String usuarioUltAct;
	 
     private java.util.Date fechaUltAct;
	 
     private String emailCliente;
     
     private String pmHostSftp;
	 
     private String pmPuertoSftp;
	 
     private String pmUsuarioSftp;
	 
     private String pmPasswordSftp;
	 
     private String pmLlaveSftp;
	 

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsCliente bsCliente;
    private HthVistaConvenio hthVistaConvenio;
    private BsEmpresaodepto bsEmpresaodepto;
    private HthFormatoEncabezado hthFormatoEncabezado;
    private HthFormato hthFormato;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getIdProcesoMonitoreo(){
		return this.idProcesoMonitoreo;         
     }

     public void setIdProcesoMonitoreo( Long idProcesoMonitoreo){
		this.idProcesoMonitoreo = idProcesoMonitoreo;
     }
     public Long getConvenio(){
		return this.convenio;         
     }

     public void setConvenio( Long convenio){
		this.convenio = convenio;
     }
     public Long getTipoconvenio(){
		return this.tipoconvenio;         
     }

     public void setTipoconvenio( Long tipoconvenio){
		this.tipoconvenio = tipoconvenio;
     }
     public String getConvenioPEACH(){
		return this.convenioPEACH;         
     }

     public void setConvenioPEACH( String convenioPEACH){
		this.convenioPEACH = convenioPEACH;
     }
     public String getConvenioNombre(){
		return this.convenioNombre;         
     }

     public void setConvenioNombre( String convenioNombre){
		this.convenioNombre = convenioNombre;
     }
     public String getNombreArchivo(){
		return this.nombreArchivo;         
     }

     public void setNombreArchivo( String nombreArchivo){
		this.nombreArchivo = nombreArchivo;
     }
     public String getRutaSftp(){
		return this.rutaSftp;         
     }

     public void setRutaSftp( String rutaSftp){
		this.rutaSftp = rutaSftp;
     }
     public String getRutaArchivosIn(){
		return this.rutaArchivosIn;         
     }

     public void setRutaArchivosIn( String rutaArchivosIn){
		this.rutaArchivosIn = rutaArchivosIn;
     }
     public String getRutaArchivosOut(){
		return this.rutaArchivosOut;         
     }

     public void setRutaArchivosOut( String rutaArchivosOut){
		this.rutaArchivosOut = rutaArchivosOut;
     }
     public String getRutaProcesados(){
		return this.rutaProcesados;         
     }

     public void setRutaProcesados( String rutaProcesados){
		this.rutaProcesados = rutaProcesados;
     }
     public Long getTiempoMonitoreo(){
		return this.tiempoMonitoreo;         
     }

     public void setTiempoMonitoreo( Long tiempoMonitoreo){
		this.tiempoMonitoreo = tiempoMonitoreo;
     }
     public String getLineaEncabezado(){
		return this.lineaEncabezado;         
     }

     public void setLineaEncabezado( String lineaEncabezado){
		this.lineaEncabezado = lineaEncabezado;
     }
     public String getProcesamientoSuperusuario(){
		return this.procesamientoSuperusuario;         
     }

     public void setProcesamientoSuperusuario( String procesamientoSuperusuario){
		this.procesamientoSuperusuario = procesamientoSuperusuario;
     }
     public java.math.BigDecimal getMontoMaximoProcesar(){
		return this.montoMaximoProcesar;         
     }

     public void setMontoMaximoProcesar( java.math.BigDecimal montoMaximoProcesar){
		this.montoMaximoProcesar = montoMaximoProcesar;
     }
     public String getEstado(){
		return this.estado;         
     }

     public void setEstado( String estado){
		this.estado = estado;
     }
     public String getUsuarioCreacion(){
		return this.usuarioCreacion;         
     }

     public void setUsuarioCreacion( String usuarioCreacion){
		this.usuarioCreacion = usuarioCreacion;
     }
     public java.util.Date getFechaCreacion(){
		return this.fechaCreacion;         
     }

     public void setFechaCreacion( java.util.Date fechaCreacion){
		this.fechaCreacion = fechaCreacion;
     }
     public String getUsuarioUltAct(){
		return this.usuarioUltAct;         
     }

     public void setUsuarioUltAct( String usuarioUltAct){
		this.usuarioUltAct = usuarioUltAct;
     }
     public java.util.Date getFechaUltAct(){
		return this.fechaUltAct;         
     }

     public void setFechaUltAct( java.util.Date fechaUltAct){
		this.fechaUltAct = fechaUltAct;
     }
     public String getEmailCliente(){
		return this.emailCliente;         
     }

     public void setEmailCliente( String emailCliente){
		this.emailCliente = emailCliente;
     }
     
     public String getPmHostSftp(){
		return this.pmHostSftp;         
     }

     public void setPmHostSftp( String pmHostSftp){
		this.pmHostSftp = pmHostSftp;
     }
     public String getPmPuertoSftp(){
		return this.pmPuertoSftp;         
     }

     public void setPmPuertoSftp( String pmPuertoSftp){
		this.pmPuertoSftp = pmPuertoSftp;
     }
     public String getPmUsuarioSftp(){
		return this.pmUsuarioSftp;         
     }

     public void setPmUsuarioSftp( String pmUsuarioSftp){
		this.pmUsuarioSftp = pmUsuarioSftp;
     }
     public String getPmPasswordSftp(){
		return this.pmPasswordSftp;         
     }

     public void setPmPasswordSftp( String pmPasswordSftp){
		this.pmPasswordSftp = pmPasswordSftp;
     }
     public String getPmLlaveSftp(){
		return this.pmLlaveSftp;         
     }

     public void setPmLlaveSftp( String pmLlaveSftp){
		this.pmLlaveSftp = pmLlaveSftp;
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getCliente(){
		if(bsCliente!=null){
        	return bsCliente.getCliente();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setCliente( Long cliente){
		if(bsCliente != null && UtileriaDeTiposDeDatos.isEquals(cliente, bsCliente.getCliente()))
    		return;
			
		if(bsCliente==null || bsCliente.getCliente()!=null){
			bsCliente = new BsCliente();			
		}         
		bsCliente.setCliente(cliente);   
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdVistaConvenio(){
		if(hthVistaConvenio!=null){
        	return hthVistaConvenio.getIdVistaConvenio();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdVistaConvenio( Long idVistaConvenio){
		if(hthVistaConvenio != null && UtileriaDeTiposDeDatos.isEquals(idVistaConvenio, hthVistaConvenio.getIdVistaConvenio()))
    		return;
			
		if(hthVistaConvenio==null || hthVistaConvenio.getIdVistaConvenio()!=null){
			hthVistaConvenio = new HthVistaConvenio();			
		}         
		hthVistaConvenio.setIdVistaConvenio(idVistaConvenio);   
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getEmpresaodepto(){
		if(bsEmpresaodepto!=null){
        	return bsEmpresaodepto.getEmpresaodepto();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setEmpresaodepto( Long empresaodepto){
		if(bsEmpresaodepto != null && UtileriaDeTiposDeDatos.isEquals(empresaodepto, bsEmpresaodepto.getEmpresaodepto()))
    		return;
			
		if(bsEmpresaodepto==null || bsEmpresaodepto.getEmpresaodepto()!=null){
			bsEmpresaodepto = new BsEmpresaodepto();			
		}         
		bsEmpresaodepto.setEmpresaodepto(empresaodepto);   
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdFormatoEncabezado(){
		if(hthFormatoEncabezado!=null){
        	return hthFormatoEncabezado.getIdFormato();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdFormatoEncabezado( Long idFormatoEncabezado){
		if(hthFormatoEncabezado != null && UtileriaDeTiposDeDatos.isEquals(idFormatoEncabezado, hthFormatoEncabezado.getIdFormato()))
    		return;
			
		if(hthFormatoEncabezado==null || hthFormatoEncabezado.getIdFormato()!=null){
			hthFormatoEncabezado = new HthFormatoEncabezado();			
		}         
		hthFormatoEncabezado.setIdFormato(idFormatoEncabezado);   
    }
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getIdFormatoDetalle(){
		if(hthFormato!=null){
        	return hthFormato.getIdFormato();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setIdFormatoDetalle( Long idFormatoDetalle){
		if(hthFormato != null && UtileriaDeTiposDeDatos.isEquals(idFormatoDetalle, hthFormato.getIdFormato()))
    		return;
			
		if(hthFormato==null || hthFormato.getIdFormato()!=null){
			hthFormato = new HthFormato();			
		}         
		hthFormato.setIdFormato(idFormatoDetalle);   
    }

    /** Metodos que manejan los DTO foraneos */		
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsCliente getBsCliente(){
		if(bsCliente==null){
			bsCliente = new BsCliente();			
		}
        return bsCliente;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsCliente(BsCliente bsCliente){
        this.bsCliente = bsCliente;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public HthVistaConvenio getHthVistaConvenio(){
		if(hthVistaConvenio==null){
			hthVistaConvenio = new HthVistaConvenio();			
		}
        return hthVistaConvenio;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setHthVistaConvenio(HthVistaConvenio hthVistaConvenio){
        this.hthVistaConvenio = hthVistaConvenio;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsEmpresaodepto getBsEmpresaodepto(){
		if(bsEmpresaodepto==null){
			bsEmpresaodepto = new BsEmpresaodepto();			
		}
        return bsEmpresaodepto;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsEmpresaodepto(BsEmpresaodepto bsEmpresaodepto){
        this.bsEmpresaodepto = bsEmpresaodepto;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public HthFormatoEncabezado getHthFormatoEncabezado(){
		if(hthFormatoEncabezado==null){
			hthFormatoEncabezado = new HthFormatoEncabezado();			
		}
        return hthFormatoEncabezado;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setHthFormatoEncabezado(HthFormatoEncabezado hthFormatoEncabezado){
        this.hthFormatoEncabezado = hthFormatoEncabezado;
    }
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public HthFormato getHthFormato(){
		if(hthFormato==null){
			hthFormato = new HthFormato();			
		}
        return hthFormato;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setHthFormato(HthFormato hthFormato){
        this.hthFormato = hthFormato;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   HthProcesoMonitoreo otherHthProcesoMonitoreo = (HthProcesoMonitoreo) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(convenio, otherHthProcesoMonitoreo.convenio);
           eq.append(tipoconvenio, otherHthProcesoMonitoreo.tipoconvenio);
           eq.append(convenioPEACH, otherHthProcesoMonitoreo.convenioPEACH);
           eq.append(convenioNombre, otherHthProcesoMonitoreo.convenioNombre);
           eq.append(nombreArchivo, otherHthProcesoMonitoreo.nombreArchivo);
           eq.append(rutaSftp, otherHthProcesoMonitoreo.rutaSftp);
           eq.append(rutaArchivosIn, otherHthProcesoMonitoreo.rutaArchivosIn);
           eq.append(rutaArchivosOut, otherHthProcesoMonitoreo.rutaArchivosOut);
           eq.append(rutaProcesados, otherHthProcesoMonitoreo.rutaProcesados);
           eq.append(tiempoMonitoreo, otherHthProcesoMonitoreo.tiempoMonitoreo);
           eq.append(lineaEncabezado, otherHthProcesoMonitoreo.lineaEncabezado);
           eq.append(procesamientoSuperusuario, otherHthProcesoMonitoreo.procesamientoSuperusuario);
           eq.append(montoMaximoProcesar, otherHthProcesoMonitoreo.montoMaximoProcesar);
           eq.append(estado, otherHthProcesoMonitoreo.estado);
           eq.append(usuarioCreacion, otherHthProcesoMonitoreo.usuarioCreacion);
           eq.append(fechaCreacion, otherHthProcesoMonitoreo.fechaCreacion);
           eq.append(usuarioUltAct, otherHthProcesoMonitoreo.usuarioUltAct);
           eq.append(fechaUltAct, otherHthProcesoMonitoreo.fechaUltAct);
           eq.append(emailCliente, otherHthProcesoMonitoreo.emailCliente);
           eq.append(pmHostSftp, otherHthProcesoMonitoreo.pmHostSftp);
           eq.append(pmPuertoSftp, otherHthProcesoMonitoreo.pmPuertoSftp);
           eq.append(pmUsuarioSftp, otherHthProcesoMonitoreo.pmUsuarioSftp);
           eq.append(pmPasswordSftp, otherHthProcesoMonitoreo.pmPasswordSftp);
           eq.append(pmLlaveSftp, otherHthProcesoMonitoreo.pmLlaveSftp);
           eq.append(idProcesoMonitoreo, otherHthProcesoMonitoreo.idProcesoMonitoreo);
           if(conForaneos){
               eq.append(bsCliente, otherHthProcesoMonitoreo.bsCliente);
               eq.append(hthVistaConvenio, otherHthProcesoMonitoreo.hthVistaConvenio);
               eq.append(bsEmpresaodepto, otherHthProcesoMonitoreo.bsEmpresaodepto);
               eq.append(hthFormatoEncabezado, otherHthProcesoMonitoreo.hthFormatoEncabezado);
               eq.append(hthFormato, otherHthProcesoMonitoreo.hthFormato);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(convenio)
                  .append(tipoconvenio)
                  .append(convenioPEACH)
                  .append(convenioNombre)
                  .append(nombreArchivo)
                  .append(rutaSftp)
                  .append(rutaArchivosIn)
                  .append(rutaArchivosOut)
                  .append(rutaProcesados)
                  .append(tiempoMonitoreo)
                  .append(lineaEncabezado)
                  .append(procesamientoSuperusuario)
                  .append(montoMaximoProcesar)
                  .append(estado)
                  .append(usuarioCreacion)
                  .append(fechaCreacion)
                  .append(usuarioUltAct)
                  .append(fechaUltAct)
                  .append(emailCliente)
                  .append(pmHostSftp)
                  .append(pmPuertoSftp)
                  .append(pmUsuarioSftp)
                  .append(pmPasswordSftp)
                  .append(pmLlaveSftp)
                  .append(idProcesoMonitoreo)
                  .append(bsCliente)
                  .append(hthVistaConvenio)
                  .append(bsEmpresaodepto)
                  .append(hthFormatoEncabezado)
                  .append(hthFormato)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("convenio",convenio)
                  .append("tipoconvenio",tipoconvenio)
                  .append("convenioPEACH",convenioPEACH)
                  .append("convenioNombre",convenioNombre)
                  .append("nombreArchivo",nombreArchivo)
                  .append("rutaSftp",rutaSftp)
                  .append("rutaArchivosIn",rutaArchivosIn)
                  .append("rutaArchivosOut",rutaArchivosOut)
                  .append("rutaProcesados",rutaProcesados)
                  .append("tiempoMonitoreo",tiempoMonitoreo)
                  .append("lineaEncabezado",lineaEncabezado)
                  .append("procesamientoSuperusuario",procesamientoSuperusuario)
                  .append("montoMaximoProcesar",montoMaximoProcesar)
                  .append("estado",estado)
                  .append("usuarioCreacion",usuarioCreacion)
                  .append("fechaCreacion",fechaCreacion)
                  .append("usuarioUltAct",usuarioUltAct)
                  .append("fechaUltAct",fechaUltAct)
                  .append("emailCliente",emailCliente)
                  .append("pmHostSftp",pmHostSftp)
                  .append("pmPuertoSftp",pmPuertoSftp)
                  .append("pmUsuarioSftp",pmUsuarioSftp)
                  .append("pmPasswordSftp",pmPasswordSftp)
                  .append("pmLlaveSftp",pmLlaveSftp)
                  .append("idProcesoMonitoreo",idProcesoMonitoreo)
                  .append("bsCliente", bsCliente)
                  .append("hthVistaConvenio", hthVistaConvenio)
                  .append("bsEmpresaodepto", bsEmpresaodepto)
                  .append("hthFormatoEncabezado", hthFormatoEncabezado)
                  .append("hthFormato", hthFormato)
                  .toString();
   }    
    
    
}
