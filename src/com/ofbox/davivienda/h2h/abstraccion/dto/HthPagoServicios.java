package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;


public class HthPagoServicios implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Long idPagoServicio;
    private String numCuenta;
    private String estado;
    private String nit;
    
    
    public Long getDtoId(){
    	return idPagoServicio;
    }
    
    public void setDto(Long dtoId) {
    	this.idPagoServicio = dtoId;
    }
    
    
    private BsCliente bsCliente;   
       
     
     public BsCliente getBsCliente() {
 		if(bsCliente ==null){
 			bsCliente = new BsCliente();			
 		}
 		return bsCliente;
 	 }
     public void setBsCliente(BsCliente bsCliente) {
 		this.bsCliente = bsCliente;
 	}
     
      public Long getIdCliente() {
    	 if(bsCliente!= null) {
    		 return bsCliente.getCliente();
    	 }
    	 return null;    	 
     }
     
     public void setIdCliente( Long idCliente) {
    	 if(bsCliente != null && UtileriaDeTiposDeDatos.isEquals(idCliente,bsCliente.getCliente()))
    		 return;
    	 
    	 if(bsCliente==null || bsCliente.getCliente()!= null) {
    		 bsCliente = new BsCliente();
       	 }
    	 bsCliente.setCliente(idCliente);    	 
     }
     public Long getIdPagoServicio(){
    	 return idPagoServicio;
     }
     
     public void setIdPagoServicio(Long idPagoServicio) {
    	 this.idPagoServicio = idPagoServicio;
     }   
     public String getNumCuenta(){
    	 return numCuenta;
     }
     public void setNumCuenta(String numCuenta) {
    	 this.numCuenta = numCuenta;
     }
     public String getNit(){
    	 return nit;
     }
     public void setNit(String nit) {
    	 this.nit = nit;
     }
     public String getEstado(){
    	 return estado;
     }
     public void setEstado(String estado) {
    	 this.estado = estado;
     }
     public boolean equals(Object obj){
         return equals(obj,true);
     }
     
     public boolean equals(Object obj, boolean conForaneos) {
  	        if (obj == null) { return false; }
  	        if (obj == this) { return true; }
  	        if (obj.getClass() != getClass()) {
  	        return false;
  	        }
  	        HthPagoServicios hthPagoServicios = (HthPagoServicios) obj;
  	       EqualsBuilder eq = new EqualsBuilder();
           eq.append(idPagoServicio, hthPagoServicios.idPagoServicio);
           eq.append(numCuenta, hthPagoServicios.numCuenta);       
           eq.append(estado, hthPagoServicios.estado);     
           eq.append(nit, hthPagoServicios.nit);
           if(conForaneos){
        	   eq.append(bsCliente, hthPagoServicios.bsCliente);          
  	        
           }
           return eq.isEquals();
  	        
     }

     public int hashCode() {
    	 return new HashCodeBuilder()
    			 .append(idPagoServicio)
    			 .append(numCuenta)
    			 .append(estado)
    			 .append(nit)
    			 .append(bsCliente)
    			 .toHashCode();
     }
     
    
  


     
}
