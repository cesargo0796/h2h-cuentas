package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsDetallepeID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsDetallepe implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsDetallepeID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsDetallepeID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsDetallepeID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsDetallepeID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private String cuenta;
	 
     private Long tipooperacion;
	 
     private java.math.BigDecimal monto;
	 
     private java.math.BigDecimal montoaplicado;
	 
     private String adenda;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private String idpago;
	 
     private Long autorizacionhost;
	 
     private Long enviarahost;
	 
     private String contrasena;
	 
     private String nombrebenef;
	 
     private String direccionbenef;
	 
     private String email;
	 
     private java.math.BigDecimal comisionaplicada;
	 
     private java.math.BigDecimal ivaaplicado;
	 
     private String nombrecuenta;
	 
     private Long niu;
	 
     private String autorizador;
	 
     private Long sublote;
	 
     private String cuentadebito;
	 
     private String nombredecuenta;
	 
     private java.math.BigDecimal montoimpuesto;
	 
     private String codigoorefint;
	 

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsLotepe bsLotepe;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getLote(){
		return getDtoId().getLote();
     }

     public void setLote( Long lote){
		getDtoId().setLote(lote);             	
     }
     public Long getOperacion(){
		return getDtoId().getOperacion();
     }

     public void setOperacion( Long operacion){
		getDtoId().setOperacion(operacion);             	
     }
     public String getCuenta(){
		return this.cuenta;         
     }

     public void setCuenta( String cuenta){
		this.cuenta = cuenta;
     }
     public Long getTipooperacion(){
		return this.tipooperacion;         
     }

     public void setTipooperacion( Long tipooperacion){
		this.tipooperacion = tipooperacion;
     }
     public java.math.BigDecimal getMonto(){
		return this.monto;         
     }

     public void setMonto( java.math.BigDecimal monto){
		this.monto = monto;
     }
     public java.math.BigDecimal getMontoaplicado(){
		return this.montoaplicado;         
     }

     public void setMontoaplicado( java.math.BigDecimal montoaplicado){
		this.montoaplicado = montoaplicado;
     }
     public String getAdenda(){
		return this.adenda;         
     }

     public void setAdenda( String adenda){
		this.adenda = adenda;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public String getIdpago(){
		return this.idpago;         
     }

     public void setIdpago( String idpago){
		this.idpago = idpago;
     }
     public Long getAutorizacionhost(){
		return this.autorizacionhost;         
     }

     public void setAutorizacionhost( Long autorizacionhost){
		this.autorizacionhost = autorizacionhost;
     }
     public Long getEnviarahost(){
		return this.enviarahost;         
     }

     public void setEnviarahost( Long enviarahost){
		this.enviarahost = enviarahost;
     }
     public String getContrasena(){
		return this.contrasena;         
     }

     public void setContrasena( String contrasena){
		this.contrasena = contrasena;
     }
     public String getNombrebenef(){
		return this.nombrebenef;         
     }

     public void setNombrebenef( String nombrebenef){
		this.nombrebenef = nombrebenef;
     }
     public String getDireccionbenef(){
		return this.direccionbenef;         
     }

     public void setDireccionbenef( String direccionbenef){
		this.direccionbenef = direccionbenef;
     }
     public String getEmail(){
		return this.email;         
     }

     public void setEmail( String email){
		this.email = email;
     }
     public java.math.BigDecimal getComisionaplicada(){
		return this.comisionaplicada;         
     }

     public void setComisionaplicada( java.math.BigDecimal comisionaplicada){
		this.comisionaplicada = comisionaplicada;
     }
     public java.math.BigDecimal getIvaaplicado(){
		return this.ivaaplicado;         
     }

     public void setIvaaplicado( java.math.BigDecimal ivaaplicado){
		this.ivaaplicado = ivaaplicado;
     }
     public String getNombrecuenta(){
		return this.nombrecuenta;         
     }

     public void setNombrecuenta( String nombrecuenta){
		this.nombrecuenta = nombrecuenta;
     }
     public Long getNiu(){
		return this.niu;         
     }

     public void setNiu( Long niu){
		this.niu = niu;
     }
     public String getAutorizador(){
		return this.autorizador;         
     }

     public void setAutorizador( String autorizador){
		this.autorizador = autorizador;
     }
     public Long getSublote(){
		return this.sublote;         
     }

     public void setSublote( Long sublote){
		this.sublote = sublote;
     }
     public String getCuentadebito(){
		return this.cuentadebito;         
     }

     public void setCuentadebito( String cuentadebito){
		this.cuentadebito = cuentadebito;
     }
     public String getNombredecuenta(){
		return this.nombredecuenta;         
     }

     public void setNombredecuenta( String nombredecuenta){
		this.nombredecuenta = nombredecuenta;
     }
     public java.math.BigDecimal getMontoimpuesto(){
		return this.montoimpuesto;         
     }

     public void setMontoimpuesto( java.math.BigDecimal montoimpuesto){
		this.montoimpuesto = montoimpuesto;
     }
     public String getCodigoorefint(){
		return this.codigoorefint;         
     }

     public void setCodigoorefint( String codigoorefint){
		this.codigoorefint = codigoorefint;
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getInstalacion(){
		if(bsLotepe!=null){
        	return bsLotepe.getInstalacion();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setInstalacion( Long instalacion){
		if(bsLotepe != null && UtileriaDeTiposDeDatos.isEquals(instalacion, bsLotepe.getInstalacion()))
    		return;
			
		if(bsLotepe==null || bsLotepe.getInstalacion()!=null){
			bsLotepe = new BsLotepe();			
		}         
		bsLotepe.setInstalacion(instalacion);   
		getDtoId().setInstalacion(instalacion);             	
    }

    /** Metodos que manejan los DTO foraneos */		
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsLotepe getBsLotepe(){
		if(bsLotepe==null){
			bsLotepe = new BsLotepe();			
		}
        return bsLotepe;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsLotepe(BsLotepe bsLotepe){
        this.bsLotepe = bsLotepe;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsDetallepe otherBsDetallepe = (BsDetallepe) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(cuenta, otherBsDetallepe.cuenta);
           eq.append(tipooperacion, otherBsDetallepe.tipooperacion);
           eq.append(monto, otherBsDetallepe.monto);
           eq.append(montoaplicado, otherBsDetallepe.montoaplicado);
           eq.append(adenda, otherBsDetallepe.adenda);
           eq.append(fechaestatus, otherBsDetallepe.fechaestatus);
           eq.append(estatus, otherBsDetallepe.estatus);
           eq.append(idpago, otherBsDetallepe.idpago);
           eq.append(autorizacionhost, otherBsDetallepe.autorizacionhost);
           eq.append(enviarahost, otherBsDetallepe.enviarahost);
           eq.append(contrasena, otherBsDetallepe.contrasena);
           eq.append(nombrebenef, otherBsDetallepe.nombrebenef);
           eq.append(direccionbenef, otherBsDetallepe.direccionbenef);
           eq.append(email, otherBsDetallepe.email);
           eq.append(comisionaplicada, otherBsDetallepe.comisionaplicada);
           eq.append(ivaaplicado, otherBsDetallepe.ivaaplicado);
           eq.append(nombrecuenta, otherBsDetallepe.nombrecuenta);
           eq.append(niu, otherBsDetallepe.niu);
           eq.append(autorizador, otherBsDetallepe.autorizador);
           eq.append(sublote, otherBsDetallepe.sublote);
           eq.append(cuentadebito, otherBsDetallepe.cuentadebito);
           eq.append(nombredecuenta, otherBsDetallepe.nombredecuenta);
           eq.append(montoimpuesto, otherBsDetallepe.montoimpuesto);
           eq.append(codigoorefint, otherBsDetallepe.codigoorefint);
           eq.append(dtoId, otherBsDetallepe.dtoId);
           if(conForaneos){
               eq.append(bsLotepe, otherBsDetallepe.bsLotepe);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(cuenta)
                  .append(tipooperacion)
                  .append(monto)
                  .append(montoaplicado)
                  .append(adenda)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(idpago)
                  .append(autorizacionhost)
                  .append(enviarahost)
                  .append(contrasena)
                  .append(nombrebenef)
                  .append(direccionbenef)
                  .append(email)
                  .append(comisionaplicada)
                  .append(ivaaplicado)
                  .append(nombrecuenta)
                  .append(niu)
                  .append(autorizador)
                  .append(sublote)
                  .append(cuentadebito)
                  .append(nombredecuenta)
                  .append(montoimpuesto)
                  .append(codigoorefint)
                  .append(dtoId)
                  .append(bsLotepe)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("cuenta",cuenta)
                  .append("tipooperacion",tipooperacion)
                  .append("monto",monto)
                  .append("montoaplicado",montoaplicado)
                  .append("adenda",adenda)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("idpago",idpago)
                  .append("autorizacionhost",autorizacionhost)
                  .append("enviarahost",enviarahost)
                  .append("contrasena",contrasena)
                  .append("nombrebenef",nombrebenef)
                  .append("direccionbenef",direccionbenef)
                  .append("email",email)
                  .append("comisionaplicada",comisionaplicada)
                  .append("ivaaplicado",ivaaplicado)
                  .append("nombrecuenta",nombrecuenta)
                  .append("niu",niu)
                  .append("autorizador",autorizador)
                  .append("sublote",sublote)
                  .append("cuentadebito",cuentadebito)
                  .append("nombredecuenta",nombredecuenta)
                  .append("montoimpuesto",montoimpuesto)
                  .append("codigoorefint",codigoorefint)
                  .append("dtoId",dtoId)
                  .append("bsLotepe", bsLotepe)
                  .toString();
   }    
    
    
}
