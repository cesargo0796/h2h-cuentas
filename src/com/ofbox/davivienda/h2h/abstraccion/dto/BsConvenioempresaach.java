package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsConvenioempresaachID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsConvenioempresaach implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsConvenioempresaachID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsConvenioempresaachID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsConvenioempresaachID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsConvenioempresaachID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsConvenioach bsConvenioach;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getEmpresaodepto(){
		return getDtoId().getEmpresaodepto();
     }

     public void setEmpresaodepto( Long empresaodepto){
		getDtoId().setEmpresaodepto(empresaodepto);             	
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getConvenio(){
		if(bsConvenioach!=null){
        	return bsConvenioach.getConvenio();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setConvenio( Long convenio){
		if(bsConvenioach != null && UtileriaDeTiposDeDatos.isEquals(convenio, bsConvenioach.getConvenio()))
    		return;
			
		if(bsConvenioach==null || bsConvenioach.getConvenio()!=null){
			bsConvenioach = new BsConvenioach();			
		}         
		bsConvenioach.setConvenio(convenio);   
		getDtoId().setConvenio(convenio);             	
    }

    /** Metodos que manejan los DTO foraneos */		
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsConvenioach getBsConvenioach(){
		if(bsConvenioach==null){
			bsConvenioach = new BsConvenioach();			
		}
        return bsConvenioach;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsConvenioach(BsConvenioach bsConvenioach){
        this.bsConvenioach = bsConvenioach;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenioempresaach otherBsConvenioempresaach = (BsConvenioempresaach) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(dtoId, otherBsConvenioempresaach.dtoId);
           if(conForaneos){
               eq.append(bsConvenioach, otherBsConvenioempresaach.bsConvenioach);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(dtoId)
                  .append(bsConvenioach)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("dtoId",dtoId)
                  .append("bsConvenioach", bsConvenioach)
                  .toString();
   }    
    
    
}
