package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsCliente implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long cliente;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return cliente;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.cliente = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private String nombre;
	 
     private Long modulos;
	 
     private String direccion;
	 
     private String telefono;
	 
     private String fax;
	 
     private String email;
	 
     private String nombrecontacto;
	 
     private String apellidocontacto;
	 
     private Long numinstalacion;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private java.util.Date fechacreacion;
	 
     private Long solicitudpendiente;
	 
     private Long nivelseguridadtex;
	 
     private Long nivelseguridadPIMP;
	 
     private String nombrerepresentante;
	 
     private String apellidorepresentante;
	 
     private String proveedorinternet;
	 
     private Long banca;
	 
     private Long politicacobrotex;
	 
     private java.math.BigDecimal comisiontransext;
	 
     private Long codigotablatransext;
	 
     private Long nivelseguridadisss;
	 
     private Long nivelseguridadafp;
	 
     private Long nivelseguridadccre;
	 
     private Long nivelseguridadppag;
	 
     private String cargapersonalizable;
	 
     private Long tipodoctorepresentante;
	 
     private String doctorepresentante;
	 
     private String ciudad;
	 
     private String cargo;
	 
     private java.math.BigDecimal limitexarchivo;
	 
     private java.math.BigDecimal limitexlote;
	 
     private java.math.BigDecimal limitextransaccion;
	 
     private String permitedebitos;
	 
     private java.math.BigDecimal limitecreditos;
	 
     private java.math.BigDecimal limitedebitos;
	 
     private java.math.BigDecimal comisiontrnach;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getCliente(){
		return this.cliente;         
     }

     public void setCliente( Long cliente){
		this.cliente = cliente;
     }
     public String getNombre(){
		return this.nombre;         
     }

     public void setNombre( String nombre){
		this.nombre = nombre;
     }
     public Long getModulos(){
		return this.modulos;         
     }

     public void setModulos( Long modulos){
		this.modulos = modulos;
     }
     public String getDireccion(){
		return this.direccion;         
     }

     public void setDireccion( String direccion){
		this.direccion = direccion;
     }
     public String getTelefono(){
		return this.telefono;         
     }

     public void setTelefono( String telefono){
		this.telefono = telefono;
     }
     public String getFax(){
		return this.fax;         
     }

     public void setFax( String fax){
		this.fax = fax;
     }
     public String getEmail(){
		return this.email;         
     }

     public void setEmail( String email){
		this.email = email;
     }
     public String getNombrecontacto(){
		return this.nombrecontacto;         
     }

     public void setNombrecontacto( String nombrecontacto){
		this.nombrecontacto = nombrecontacto;
     }
     public String getApellidocontacto(){
		return this.apellidocontacto;         
     }

     public void setApellidocontacto( String apellidocontacto){
		this.apellidocontacto = apellidocontacto;
     }
     public Long getNuminstalacion(){
		return this.numinstalacion;         
     }

     public void setNuminstalacion( Long numinstalacion){
		this.numinstalacion = numinstalacion;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public java.util.Date getFechacreacion(){
		return this.fechacreacion;         
     }

     public void setFechacreacion( java.util.Date fechacreacion){
		this.fechacreacion = fechacreacion;
     }
     public Long getSolicitudpendiente(){
		return this.solicitudpendiente;         
     }

     public void setSolicitudpendiente( Long solicitudpendiente){
		this.solicitudpendiente = solicitudpendiente;
     }
     public Long getNivelseguridadtex(){
		return this.nivelseguridadtex;         
     }

     public void setNivelseguridadtex( Long nivelseguridadtex){
		this.nivelseguridadtex = nivelseguridadtex;
     }
     public Long getNivelseguridadPIMP(){
		return this.nivelseguridadPIMP;         
     }

     public void setNivelseguridadPIMP( Long nivelseguridadPIMP){
		this.nivelseguridadPIMP = nivelseguridadPIMP;
     }
     public String getNombrerepresentante(){
		return this.nombrerepresentante;         
     }

     public void setNombrerepresentante( String nombrerepresentante){
		this.nombrerepresentante = nombrerepresentante;
     }
     public String getApellidorepresentante(){
		return this.apellidorepresentante;         
     }

     public void setApellidorepresentante( String apellidorepresentante){
		this.apellidorepresentante = apellidorepresentante;
     }
     public String getProveedorinternet(){
		return this.proveedorinternet;         
     }

     public void setProveedorinternet( String proveedorinternet){
		this.proveedorinternet = proveedorinternet;
     }
     public Long getBanca(){
		return this.banca;         
     }

     public void setBanca( Long banca){
		this.banca = banca;
     }
     public Long getPoliticacobrotex(){
		return this.politicacobrotex;         
     }

     public void setPoliticacobrotex( Long politicacobrotex){
		this.politicacobrotex = politicacobrotex;
     }
     public java.math.BigDecimal getComisiontransext(){
		return this.comisiontransext;         
     }

     public void setComisiontransext( java.math.BigDecimal comisiontransext){
		this.comisiontransext = comisiontransext;
     }
     public Long getCodigotablatransext(){
		return this.codigotablatransext;         
     }

     public void setCodigotablatransext( Long codigotablatransext){
		this.codigotablatransext = codigotablatransext;
     }
     public Long getNivelseguridadisss(){
		return this.nivelseguridadisss;         
     }

     public void setNivelseguridadisss( Long nivelseguridadisss){
		this.nivelseguridadisss = nivelseguridadisss;
     }
     public Long getNivelseguridadafp(){
		return this.nivelseguridadafp;         
     }

     public void setNivelseguridadafp( Long nivelseguridadafp){
		this.nivelseguridadafp = nivelseguridadafp;
     }
     public Long getNivelseguridadccre(){
		return this.nivelseguridadccre;         
     }

     public void setNivelseguridadccre( Long nivelseguridadccre){
		this.nivelseguridadccre = nivelseguridadccre;
     }
     public Long getNivelseguridadppag(){
		return this.nivelseguridadppag;         
     }

     public void setNivelseguridadppag( Long nivelseguridadppag){
		this.nivelseguridadppag = nivelseguridadppag;
     }
     public String getCargapersonalizable(){
		return this.cargapersonalizable;         
     }

     public void setCargapersonalizable( String cargapersonalizable){
		this.cargapersonalizable = cargapersonalizable;
     }
     public Long getTipodoctorepresentante(){
		return this.tipodoctorepresentante;         
     }

     public void setTipodoctorepresentante( Long tipodoctorepresentante){
		this.tipodoctorepresentante = tipodoctorepresentante;
     }
     public String getDoctorepresentante(){
		return this.doctorepresentante;         
     }

     public void setDoctorepresentante( String doctorepresentante){
		this.doctorepresentante = doctorepresentante;
     }
     public String getCiudad(){
		return this.ciudad;         
     }

     public void setCiudad( String ciudad){
		this.ciudad = ciudad;
     }
     public String getCargo(){
		return this.cargo;         
     }

     public void setCargo( String cargo){
		this.cargo = cargo;
     }
     public java.math.BigDecimal getLimitexarchivo(){
		return this.limitexarchivo;         
     }

     public void setLimitexarchivo( java.math.BigDecimal limitexarchivo){
		this.limitexarchivo = limitexarchivo;
     }
     public java.math.BigDecimal getLimitexlote(){
		return this.limitexlote;         
     }

     public void setLimitexlote( java.math.BigDecimal limitexlote){
		this.limitexlote = limitexlote;
     }
     public java.math.BigDecimal getLimitextransaccion(){
		return this.limitextransaccion;         
     }

     public void setLimitextransaccion( java.math.BigDecimal limitextransaccion){
		this.limitextransaccion = limitextransaccion;
     }
     public String getPermitedebitos(){
		return this.permitedebitos;         
     }

     public void setPermitedebitos( String permitedebitos){
		this.permitedebitos = permitedebitos;
     }
     public java.math.BigDecimal getLimitecreditos(){
		return this.limitecreditos;         
     }

     public void setLimitecreditos( java.math.BigDecimal limitecreditos){
		this.limitecreditos = limitecreditos;
     }
     public java.math.BigDecimal getLimitedebitos(){
		return this.limitedebitos;         
     }

     public void setLimitedebitos( java.math.BigDecimal limitedebitos){
		this.limitedebitos = limitedebitos;
     }
     public java.math.BigDecimal getComisiontrnach(){
		return this.comisiontrnach;         
     }

     public void setComisiontrnach( java.math.BigDecimal comisiontrnach){
		this.comisiontrnach = comisiontrnach;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsCliente otherBsCliente = (BsCliente) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(nombre, otherBsCliente.nombre);
           eq.append(modulos, otherBsCliente.modulos);
           eq.append(direccion, otherBsCliente.direccion);
           eq.append(telefono, otherBsCliente.telefono);
           eq.append(fax, otherBsCliente.fax);
           eq.append(email, otherBsCliente.email);
           eq.append(nombrecontacto, otherBsCliente.nombrecontacto);
           eq.append(apellidocontacto, otherBsCliente.apellidocontacto);
           eq.append(numinstalacion, otherBsCliente.numinstalacion);
           eq.append(fechaestatus, otherBsCliente.fechaestatus);
           eq.append(estatus, otherBsCliente.estatus);
           eq.append(fechacreacion, otherBsCliente.fechacreacion);
           eq.append(solicitudpendiente, otherBsCliente.solicitudpendiente);
           eq.append(nivelseguridadtex, otherBsCliente.nivelseguridadtex);
           eq.append(nivelseguridadPIMP, otherBsCliente.nivelseguridadPIMP);
           eq.append(nombrerepresentante, otherBsCliente.nombrerepresentante);
           eq.append(apellidorepresentante, otherBsCliente.apellidorepresentante);
           eq.append(proveedorinternet, otherBsCliente.proveedorinternet);
           eq.append(banca, otherBsCliente.banca);
           eq.append(politicacobrotex, otherBsCliente.politicacobrotex);
           eq.append(comisiontransext, otherBsCliente.comisiontransext);
           eq.append(codigotablatransext, otherBsCliente.codigotablatransext);
           eq.append(nivelseguridadisss, otherBsCliente.nivelseguridadisss);
           eq.append(nivelseguridadafp, otherBsCliente.nivelseguridadafp);
           eq.append(nivelseguridadccre, otherBsCliente.nivelseguridadccre);
           eq.append(nivelseguridadppag, otherBsCliente.nivelseguridadppag);
           eq.append(cargapersonalizable, otherBsCliente.cargapersonalizable);
           eq.append(tipodoctorepresentante, otherBsCliente.tipodoctorepresentante);
           eq.append(doctorepresentante, otherBsCliente.doctorepresentante);
           eq.append(ciudad, otherBsCliente.ciudad);
           eq.append(cargo, otherBsCliente.cargo);
           eq.append(limitexarchivo, otherBsCliente.limitexarchivo);
           eq.append(limitexlote, otherBsCliente.limitexlote);
           eq.append(limitextransaccion, otherBsCliente.limitextransaccion);
           eq.append(permitedebitos, otherBsCliente.permitedebitos);
           eq.append(limitecreditos, otherBsCliente.limitecreditos);
           eq.append(limitedebitos, otherBsCliente.limitedebitos);
           eq.append(comisiontrnach, otherBsCliente.comisiontrnach);
           eq.append(cliente, otherBsCliente.cliente);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(nombre)
                  .append(modulos)
                  .append(direccion)
                  .append(telefono)
                  .append(fax)
                  .append(email)
                  .append(nombrecontacto)
                  .append(apellidocontacto)
                  .append(numinstalacion)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(fechacreacion)
                  .append(solicitudpendiente)
                  .append(nivelseguridadtex)
                  .append(nivelseguridadPIMP)
                  .append(nombrerepresentante)
                  .append(apellidorepresentante)
                  .append(proveedorinternet)
                  .append(banca)
                  .append(politicacobrotex)
                  .append(comisiontransext)
                  .append(codigotablatransext)
                  .append(nivelseguridadisss)
                  .append(nivelseguridadafp)
                  .append(nivelseguridadccre)
                  .append(nivelseguridadppag)
                  .append(cargapersonalizable)
                  .append(tipodoctorepresentante)
                  .append(doctorepresentante)
                  .append(ciudad)
                  .append(cargo)
                  .append(limitexarchivo)
                  .append(limitexlote)
                  .append(limitextransaccion)
                  .append(permitedebitos)
                  .append(limitecreditos)
                  .append(limitedebitos)
                  .append(comisiontrnach)
                  .append(cliente)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("nombre",nombre)
                  .append("modulos",modulos)
                  .append("direccion",direccion)
                  .append("telefono",telefono)
                  .append("fax",fax)
                  .append("email",email)
                  .append("nombrecontacto",nombrecontacto)
                  .append("apellidocontacto",apellidocontacto)
                  .append("numinstalacion",numinstalacion)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("fechacreacion",fechacreacion)
                  .append("solicitudpendiente",solicitudpendiente)
                  .append("nivelseguridadtex",nivelseguridadtex)
                  .append("nivelseguridadPIMP",nivelseguridadPIMP)
                  .append("nombrerepresentante",nombrerepresentante)
                  .append("apellidorepresentante",apellidorepresentante)
                  .append("proveedorinternet",proveedorinternet)
                  .append("banca",banca)
                  .append("politicacobrotex",politicacobrotex)
                  .append("comisiontransext",comisiontransext)
                  .append("codigotablatransext",codigotablatransext)
                  .append("nivelseguridadisss",nivelseguridadisss)
                  .append("nivelseguridadafp",nivelseguridadafp)
                  .append("nivelseguridadccre",nivelseguridadccre)
                  .append("nivelseguridadppag",nivelseguridadppag)
                  .append("cargapersonalizable",cargapersonalizable)
                  .append("tipodoctorepresentante",tipodoctorepresentante)
                  .append("doctorepresentante",doctorepresentante)
                  .append("ciudad",ciudad)
                  .append("cargo",cargo)
                  .append("limitexarchivo",limitexarchivo)
                  .append("limitexlote",limitexlote)
                  .append("limitextransaccion",limitextransaccion)
                  .append("permitedebitos",permitedebitos)
                  .append("limitecreditos",limitecreditos)
                  .append("limitedebitos",limitedebitos)
                  .append("comisiontrnach",comisiontrnach)
                  .append("cliente",cliente)
                  .toString();
   }    
    
    
}
