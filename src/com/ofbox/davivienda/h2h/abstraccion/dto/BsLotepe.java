package com.ofbox.davivienda.h2h.abstraccion.dto;
import com.ofbox.davivienda.h2h.abstraccion.dto.BsLotepeID;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.validacion.UtileriaDeTiposDeDatos;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsLotepe implements Serializable{
    /**
     * Atributo que almacena los valores que funciona como llaves para un objeto de Negocio.
     */
     private BsLotepeID dtoId;
    /**
     * Devuelve el objeto que almacena los identificadores del DTO.
     * @return
     */
     public BsLotepeID getDtoId(){     
     	if(dtoId == null){
     		dtoId = new BsLotepeID();
     	}     	
     	return dtoId;        
     }
    /**
     * Coloca el objeto que almacena los identificadores del DTO.
     * @param dtoId objeto que almacena las llaves que identifican al DTO. 
     */
     public void setDtoId(BsLotepeID dtoId){
     	this.dtoId = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long convenio;
	 
     private Long empresaodepto;
	 
     private Long tipolote;
	 
     private java.math.BigDecimal montototal;
	 
     private Long numoperaciones;
	 
     private String nombrelote;
	 
     private java.util.Date fechaenviado;
	 
     private java.util.Date fecharecibido;
	 
     private java.util.Date fechaaplicacion;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private Long numeroreintento;
	 
     private String usuarioingreso;
	 
     private Long autorizaciones;
	 
     private Long aplicaciondebitohost;
	 
     private java.util.Date sysMarcatiempo;
	 
     private Long sysProcessid;
	 
     private Long sysLowdatetime;
	 
     private Long sysHighdatetime;
	 
     private Long ponderacion;
	 
     private Long firmas;
	 
     private java.util.Date fechaenviohost;
	 
     private java.util.Date listoparahost;
	 
     private java.util.Date fechahoraprocesado;
	 
     private Long token;
	 
     private String comentariorechazo;
	 
     private java.math.BigDecimal montoimpuesto;
	 

    /**
     * Atributos que almacenan los DTOs de objetos de negocios que funcionan como objetos foraneos.
     */    		
    private BsConvenio bsConvenio;


     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getInstalacion(){
		return getDtoId().getInstalacion();
     }

     public void setInstalacion( Long instalacion){
		getDtoId().setInstalacion(instalacion);             	
     }
     public Long getLote(){
		return getDtoId().getLote();
     }

     public void setLote( Long lote){
		getDtoId().setLote(lote);             	
     }
     public Long getConvenio(){
		return this.convenio;         
     }

     public void setConvenio( Long convenio){
		this.convenio = convenio;
     }
     public Long getEmpresaodepto(){
		return this.empresaodepto;         
     }

     public void setEmpresaodepto( Long empresaodepto){
		this.empresaodepto = empresaodepto;
     }
     public Long getTipolote(){
		return this.tipolote;         
     }

     public void setTipolote( Long tipolote){
		this.tipolote = tipolote;
     }
     public java.math.BigDecimal getMontototal(){
		return this.montototal;         
     }

     public void setMontototal( java.math.BigDecimal montototal){
		this.montototal = montototal;
     }
     public Long getNumoperaciones(){
		return this.numoperaciones;         
     }

     public void setNumoperaciones( Long numoperaciones){
		this.numoperaciones = numoperaciones;
     }
     public String getNombrelote(){
		return this.nombrelote;         
     }

     public void setNombrelote( String nombrelote){
		this.nombrelote = nombrelote;
     }
     public java.util.Date getFechaenviado(){
		return this.fechaenviado;         
     }

     public void setFechaenviado( java.util.Date fechaenviado){
		this.fechaenviado = fechaenviado;
     }
     public java.util.Date getFecharecibido(){
		return this.fecharecibido;         
     }

     public void setFecharecibido( java.util.Date fecharecibido){
		this.fecharecibido = fecharecibido;
     }
     public java.util.Date getFechaaplicacion(){
		return this.fechaaplicacion;         
     }

     public void setFechaaplicacion( java.util.Date fechaaplicacion){
		this.fechaaplicacion = fechaaplicacion;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public Long getNumeroreintento(){
		return this.numeroreintento;         
     }

     public void setNumeroreintento( Long numeroreintento){
		this.numeroreintento = numeroreintento;
     }
     public String getUsuarioingreso(){
		return this.usuarioingreso;         
     }

     public void setUsuarioingreso( String usuarioingreso){
		this.usuarioingreso = usuarioingreso;
     }
     public Long getAutorizaciones(){
		return this.autorizaciones;         
     }

     public void setAutorizaciones( Long autorizaciones){
		this.autorizaciones = autorizaciones;
     }
     public Long getAplicaciondebitohost(){
		return this.aplicaciondebitohost;         
     }

     public void setAplicaciondebitohost( Long aplicaciondebitohost){
		this.aplicaciondebitohost = aplicaciondebitohost;
     }
     public java.util.Date getSysMarcatiempo(){
		return this.sysMarcatiempo;         
     }

     public void setSysMarcatiempo( java.util.Date sysMarcatiempo){
		this.sysMarcatiempo = sysMarcatiempo;
     }
     public Long getSysProcessid(){
		return this.sysProcessid;         
     }

     public void setSysProcessid( Long sysProcessid){
		this.sysProcessid = sysProcessid;
     }
     public Long getSysLowdatetime(){
		return this.sysLowdatetime;         
     }

     public void setSysLowdatetime( Long sysLowdatetime){
		this.sysLowdatetime = sysLowdatetime;
     }
     public Long getSysHighdatetime(){
		return this.sysHighdatetime;         
     }

     public void setSysHighdatetime( Long sysHighdatetime){
		this.sysHighdatetime = sysHighdatetime;
     }
     public Long getPonderacion(){
		return this.ponderacion;         
     }

     public void setPonderacion( Long ponderacion){
		this.ponderacion = ponderacion;
     }
     public Long getFirmas(){
		return this.firmas;         
     }

     public void setFirmas( Long firmas){
		this.firmas = firmas;
     }
     public java.util.Date getFechaenviohost(){
		return this.fechaenviohost;         
     }

     public void setFechaenviohost( java.util.Date fechaenviohost){
		this.fechaenviohost = fechaenviohost;
     }
     public java.util.Date getListoparahost(){
		return this.listoparahost;         
     }

     public void setListoparahost( java.util.Date listoparahost){
		this.listoparahost = listoparahost;
     }
     public java.util.Date getFechahoraprocesado(){
		return this.fechahoraprocesado;         
     }

     public void setFechahoraprocesado( java.util.Date fechahoraprocesado){
		this.fechahoraprocesado = fechahoraprocesado;
     }
     public Long getToken(){
		return this.token;         
     }

     public void setToken( Long token){
		this.token = token;
     }
     public String getComentariorechazo(){
		return this.comentariorechazo;         
     }

     public void setComentariorechazo( String comentariorechazo){
		this.comentariorechazo = comentariorechazo;
     }
     public java.math.BigDecimal getMontoimpuesto(){
		return this.montoimpuesto;         
     }

     public void setMontoimpuesto( java.math.BigDecimal montoimpuesto){
		this.montoimpuesto = montoimpuesto;
     }

	/**   Metodos que manejan los identificadores de los DTO foraneos */ 
    /**
     * Devuelve el atributo llave del DTO foraneo.
     * Verifica si es null antes de devolverlo.
     * @return llave del DTO del negocio maestro
     */
	public Long getTipoconvenio(){
		if(bsConvenio!=null){
        	return bsConvenio.getTipoconvenio();
		}
		return null;        	         
	}
    /**
     * Coloca el valor de la llave en el atributo espec&iacute;fico del DTO foraneo,
	 * crea una nueva instancia del objeto foraneo si este es nulo.
     * @param id Objeto que almacena la llave de un DTO que referencia a un negocio maestro.
     */
    public void setTipoconvenio( Long tipoconvenio){
		if(bsConvenio != null && UtileriaDeTiposDeDatos.isEquals(tipoconvenio, bsConvenio.getTipoconvenio()))
    		return;
			
		if(bsConvenio==null || bsConvenio.getTipoconvenio()!=null){
			bsConvenio = new BsConvenio();			
		}         
		bsConvenio.setTipoconvenio(tipoconvenio);   
    }

    /** Metodos que manejan los DTO foraneos */		
    /**
     * Devuelve el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @return
     */
    public BsConvenio getBsConvenio(){
		if(bsConvenio==null){
			bsConvenio = new BsConvenio();			
		}
        return bsConvenio;
    }
    /**
     * Coloca el objeto del DTO foraneo, que es la referencia a un objeto que representa un negocio maestro.
	 * @param DTO objeto foraneo
     */
    public void setBsConvenio(BsConvenio bsConvenio){
        this.bsConvenio = bsConvenio;
    }

    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsLotepe otherBsLotepe = (BsLotepe) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(convenio, otherBsLotepe.convenio);
           eq.append(empresaodepto, otherBsLotepe.empresaodepto);
           eq.append(tipolote, otherBsLotepe.tipolote);
           eq.append(montototal, otherBsLotepe.montototal);
           eq.append(numoperaciones, otherBsLotepe.numoperaciones);
           eq.append(nombrelote, otherBsLotepe.nombrelote);
           eq.append(fechaenviado, otherBsLotepe.fechaenviado);
           eq.append(fecharecibido, otherBsLotepe.fecharecibido);
           eq.append(fechaaplicacion, otherBsLotepe.fechaaplicacion);
           eq.append(fechaestatus, otherBsLotepe.fechaestatus);
           eq.append(estatus, otherBsLotepe.estatus);
           eq.append(numeroreintento, otherBsLotepe.numeroreintento);
           eq.append(usuarioingreso, otherBsLotepe.usuarioingreso);
           eq.append(autorizaciones, otherBsLotepe.autorizaciones);
           eq.append(aplicaciondebitohost, otherBsLotepe.aplicaciondebitohost);
           eq.append(sysMarcatiempo, otherBsLotepe.sysMarcatiempo);
           eq.append(sysProcessid, otherBsLotepe.sysProcessid);
           eq.append(sysLowdatetime, otherBsLotepe.sysLowdatetime);
           eq.append(sysHighdatetime, otherBsLotepe.sysHighdatetime);
           eq.append(ponderacion, otherBsLotepe.ponderacion);
           eq.append(firmas, otherBsLotepe.firmas);
           eq.append(fechaenviohost, otherBsLotepe.fechaenviohost);
           eq.append(listoparahost, otherBsLotepe.listoparahost);
           eq.append(fechahoraprocesado, otherBsLotepe.fechahoraprocesado);
           eq.append(token, otherBsLotepe.token);
           eq.append(comentariorechazo, otherBsLotepe.comentariorechazo);
           eq.append(montoimpuesto, otherBsLotepe.montoimpuesto);
           eq.append(dtoId, otherBsLotepe.dtoId);
           if(conForaneos){
               eq.append(bsConvenio, otherBsLotepe.bsConvenio);
           }
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(convenio)
                  .append(empresaodepto)
                  .append(tipolote)
                  .append(montototal)
                  .append(numoperaciones)
                  .append(nombrelote)
                  .append(fechaenviado)
                  .append(fecharecibido)
                  .append(fechaaplicacion)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(numeroreintento)
                  .append(usuarioingreso)
                  .append(autorizaciones)
                  .append(aplicaciondebitohost)
                  .append(sysMarcatiempo)
                  .append(sysProcessid)
                  .append(sysLowdatetime)
                  .append(sysHighdatetime)
                  .append(ponderacion)
                  .append(firmas)
                  .append(fechaenviohost)
                  .append(listoparahost)
                  .append(fechahoraprocesado)
                  .append(token)
                  .append(comentariorechazo)
                  .append(montoimpuesto)
                  .append(dtoId)
                  .append(bsConvenio)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("convenio",convenio)
                  .append("empresaodepto",empresaodepto)
                  .append("tipolote",tipolote)
                  .append("montototal",montototal)
                  .append("numoperaciones",numoperaciones)
                  .append("nombrelote",nombrelote)
                  .append("fechaenviado",fechaenviado)
                  .append("fecharecibido",fecharecibido)
                  .append("fechaaplicacion",fechaaplicacion)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("numeroreintento",numeroreintento)
                  .append("usuarioingreso",usuarioingreso)
                  .append("autorizaciones",autorizaciones)
                  .append("aplicaciondebitohost",aplicaciondebitohost)
                  .append("sysMarcatiempo",sysMarcatiempo)
                  .append("sysProcessid",sysProcessid)
                  .append("sysLowdatetime",sysLowdatetime)
                  .append("sysHighdatetime",sysHighdatetime)
                  .append("ponderacion",ponderacion)
                  .append("firmas",firmas)
                  .append("fechaenviohost",fechaenviohost)
                  .append("listoparahost",listoparahost)
                  .append("fechahoraprocesado",fechahoraprocesado)
                  .append("token",token)
                  .append("comentariorechazo",comentariorechazo)
                  .append("montoimpuesto",montoimpuesto)
                  .append("dtoId",dtoId)
                  .append("bsConvenio", bsConvenio)
                  .toString();
   }    
    
    
}
