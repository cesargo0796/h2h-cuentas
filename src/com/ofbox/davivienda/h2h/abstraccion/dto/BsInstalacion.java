package com.ofbox.davivienda.h2h.abstraccion.dto;
import java.lang.Long;
import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
/**
 * Clase POJO(Plain Old Java Object), que permite almacenar los valores de las propiedades de un objeto de negocio, tambi&eacute;n conocido como DTO(Data Transfer Object).
 * Este DTO es el que viaja entre la capa DAO y la capa de negocios, y que posteriormente es manipulado en la capa de la vista.  
 * 
 * @author Systems Out of the Box
 * @version f3.1 --
 */
 
public class BsInstalacion implements Serializable{
	/**
     * Atributo que almacena el valor que funciona como llave para un objeto de Negocio.
     */
     private Long instalacion;
    /**
     * Devuelve el objeto que almacena el identificador del DTO.
     * @return
     */
     public Long getDtoId(){
        return instalacion;
     }
    /**
     * Coloca el objeto que almacena el identificador del DTO.
     * @param dtoId objeto que almacena la llave que identifica al DTO. 
     */
     public void setDtoId(Long dtoId){
     	this.instalacion = dtoId;
     }
    
    /** Atributos que almacenan los valores para las propiedades de un objeto DTO correspondiente a un objeto Negocio. */
     
     private Long cliente;
	 
     private Long modulos;
	 
     private String nombre;
	 
     private String id;
	 
     private String password;
	 
     private Long pesoa;
	 
     private Long pesob;
	 
     private Long pesoc;
	 
     private Long pesod;
	 
     private Long pesoe;
	 
     private Long estatuscom;
	 
     private java.util.Date fechaestatus;
	 
     private Long estatus;
	 
     private Long envioCliente;
	 
     private String version;
	 
     private String versionpe;
	 
     private Long origenlotes;
	 
     private Long siguientelotecc;
	 
     private Long siguientelotepe;
	 
     private Long siguientelotepp;
	 
     private Long siguienteloteps;
	 
     private Long siguienteloteach;
	 



     
    
    /** M&eacute;todos que devuelven y colocan los valores de los atributos respectivos del DTO. */
     
     public Long getInstalacion(){
		return this.instalacion;         
     }

     public void setInstalacion( Long instalacion){
		this.instalacion = instalacion;
     }
     public Long getCliente(){
		return this.cliente;         
     }

     public void setCliente( Long cliente){
		this.cliente = cliente;
     }
     public Long getModulos(){
		return this.modulos;         
     }

     public void setModulos( Long modulos){
		this.modulos = modulos;
     }
     public String getNombre(){
		return this.nombre;         
     }

     public void setNombre( String nombre){
		this.nombre = nombre;
     }
     public String getId(){
		return this.id;         
     }

     public void setId( String id){
		this.id = id;
     }
     public String getPassword(){
		return this.password;         
     }

     public void setPassword( String password){
		this.password = password;
     }
     public Long getPesoa(){
		return this.pesoa;         
     }

     public void setPesoa( Long pesoa){
		this.pesoa = pesoa;
     }
     public Long getPesob(){
		return this.pesob;         
     }

     public void setPesob( Long pesob){
		this.pesob = pesob;
     }
     public Long getPesoc(){
		return this.pesoc;         
     }

     public void setPesoc( Long pesoc){
		this.pesoc = pesoc;
     }
     public Long getPesod(){
		return this.pesod;         
     }

     public void setPesod( Long pesod){
		this.pesod = pesod;
     }
     public Long getPesoe(){
		return this.pesoe;         
     }

     public void setPesoe( Long pesoe){
		this.pesoe = pesoe;
     }
     public Long getEstatuscom(){
		return this.estatuscom;         
     }

     public void setEstatuscom( Long estatuscom){
		this.estatuscom = estatuscom;
     }
     public java.util.Date getFechaestatus(){
		return this.fechaestatus;         
     }

     public void setFechaestatus( java.util.Date fechaestatus){
		this.fechaestatus = fechaestatus;
     }
     public Long getEstatus(){
		return this.estatus;         
     }

     public void setEstatus( Long estatus){
		this.estatus = estatus;
     }
     public Long getEnvioCliente(){
		return this.envioCliente;         
     }

     public void setEnvioCliente( Long envioCliente){
		this.envioCliente = envioCliente;
     }
     public String getVersion(){
		return this.version;         
     }

     public void setVersion( String version){
		this.version = version;
     }
     public String getVersionpe(){
		return this.versionpe;         
     }

     public void setVersionpe( String versionpe){
		this.versionpe = versionpe;
     }
     public Long getOrigenlotes(){
		return this.origenlotes;         
     }

     public void setOrigenlotes( Long origenlotes){
		this.origenlotes = origenlotes;
     }
     public Long getSiguientelotecc(){
		return this.siguientelotecc;         
     }

     public void setSiguientelotecc( Long siguientelotecc){
		this.siguientelotecc = siguientelotecc;
     }
     public Long getSiguientelotepe(){
		return this.siguientelotepe;         
     }

     public void setSiguientelotepe( Long siguientelotepe){
		this.siguientelotepe = siguientelotepe;
     }
     public Long getSiguientelotepp(){
		return this.siguientelotepp;         
     }

     public void setSiguientelotepp( Long siguientelotepp){
		this.siguientelotepp = siguientelotepp;
     }
     public Long getSiguienteloteps(){
		return this.siguienteloteps;         
     }

     public void setSiguienteloteps( Long siguienteloteps){
		this.siguienteloteps = siguienteloteps;
     }
     public Long getSiguienteloteach(){
		return this.siguienteloteach;         
     }

     public void setSiguienteloteach( Long siguienteloteach){
		this.siguienteloteach = siguienteloteach;
     }



    public boolean equals(Object obj){
        return equals(obj,true);
    }
    
    
    public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsInstalacion otherBsInstalacion = (BsInstalacion) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(cliente, otherBsInstalacion.cliente);
           eq.append(modulos, otherBsInstalacion.modulos);
           eq.append(nombre, otherBsInstalacion.nombre);
           eq.append(id, otherBsInstalacion.id);
           eq.append(password, otherBsInstalacion.password);
           eq.append(pesoa, otherBsInstalacion.pesoa);
           eq.append(pesob, otherBsInstalacion.pesob);
           eq.append(pesoc, otherBsInstalacion.pesoc);
           eq.append(pesod, otherBsInstalacion.pesod);
           eq.append(pesoe, otherBsInstalacion.pesoe);
           eq.append(estatuscom, otherBsInstalacion.estatuscom);
           eq.append(fechaestatus, otherBsInstalacion.fechaestatus);
           eq.append(estatus, otherBsInstalacion.estatus);
           eq.append(envioCliente, otherBsInstalacion.envioCliente);
           eq.append(version, otherBsInstalacion.version);
           eq.append(versionpe, otherBsInstalacion.versionpe);
           eq.append(origenlotes, otherBsInstalacion.origenlotes);
           eq.append(siguientelotecc, otherBsInstalacion.siguientelotecc);
           eq.append(siguientelotepe, otherBsInstalacion.siguientelotepe);
           eq.append(siguientelotepp, otherBsInstalacion.siguientelotepp);
           eq.append(siguienteloteps, otherBsInstalacion.siguienteloteps);
           eq.append(siguienteloteach, otherBsInstalacion.siguienteloteach);
           eq.append(instalacion, otherBsInstalacion.instalacion);
    	   return eq.isEquals();
    }
    
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(cliente)
                  .append(modulos)
                  .append(nombre)
                  .append(id)
                  .append(password)
                  .append(pesoa)
                  .append(pesob)
                  .append(pesoc)
                  .append(pesod)
                  .append(pesoe)
                  .append(estatuscom)
                  .append(fechaestatus)
                  .append(estatus)
                  .append(envioCliente)
                  .append(version)
                  .append(versionpe)
                  .append(origenlotes)
                  .append(siguientelotecc)
                  .append(siguientelotepe)
                  .append(siguientelotepp)
                  .append(siguienteloteps)
                  .append(siguienteloteach)
                  .append(instalacion)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("cliente",cliente)
                  .append("modulos",modulos)
                  .append("nombre",nombre)
                  .append("id",id)
                  .append("password",password)
                  .append("pesoa",pesoa)
                  .append("pesob",pesob)
                  .append("pesoc",pesoc)
                  .append("pesod",pesod)
                  .append("pesoe",pesoe)
                  .append("estatuscom",estatuscom)
                  .append("fechaestatus",fechaestatus)
                  .append("estatus",estatus)
                  .append("envioCliente",envioCliente)
                  .append("version",version)
                  .append("versionpe",versionpe)
                  .append("origenlotes",origenlotes)
                  .append("siguientelotecc",siguientelotecc)
                  .append("siguientelotepe",siguientelotepe)
                  .append("siguientelotepp",siguientelotepp)
                  .append("siguienteloteps",siguienteloteps)
                  .append("siguienteloteach",siguienteloteach)
                  .append("instalacion",instalacion)
                  .toString();
   }    
    
    
}
