package com.ofbox.davivienda.h2h.abstraccion.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


public class BsConvenioempresaID implements Serializable{


     private Long tipoconvenio;
     
     private Long convenio;
     
     private Long empresaodepto;
     
   
     
     public Long getTipoconvenio(){
         return this.tipoconvenio;
     }

     public void setTipoconvenio( Long tipoconvenio){
          this.tipoconvenio = tipoconvenio;
     }
     
     
     public Long getConvenio(){
         return this.convenio;
     }

     public void setConvenio( Long convenio){
          this.convenio = convenio;
     }
     
     
     public Long getEmpresaodepto(){
         return this.empresaodepto;
     }

     public void setEmpresaodepto( Long empresaodepto){
          this.empresaodepto = empresaodepto;
     }
     

   public boolean equals(Object obj, boolean conForaneos) {
    	   if (obj == null) { return false; }
    	   if (obj == this) { return true; }
    	   if (obj.getClass() != getClass()) {
    	     return false;
    	   }
    	   BsConvenioempresaID otherBsConvenioempresaID = (BsConvenioempresaID) obj;
    	   EqualsBuilder eq = new EqualsBuilder();
           eq.append(tipoconvenio, otherBsConvenioempresaID.tipoconvenio);
           eq.append(convenio, otherBsConvenioempresaID.convenio);
           eq.append(empresaodepto, otherBsConvenioempresaID.empresaodepto);
    	   return eq.isEquals();
    }
    
     public int hashCode() {
	     return new HashCodeBuilder()
                  .append(tipoconvenio)
                  .append(convenio)
                  .append(empresaodepto)
	              .toHashCode();
   }
    
    
   public String toString() {
     return new ToStringBuilder(this)
                  .append("tipoconvenio",tipoconvenio)
                  .append("convenio",convenio)
                  .append("empresaodepto",empresaodepto)
                  .toString();
   }
    
}
