package com.ofbox.davivienda.h2h.dependenciasGlobales;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import com.ofbox.f3.app.distribuibles.abstraccion.comunes.ayuda.Ayuda;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.Log;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.log.LogServiceLocator;
import com.ofbox.f3.app.distribuibles.implementaciones.dao.jdbcsql.SQLEntidadMeta;

/**
 * Implementacion sencilla de la ayuda del sistema
 * basada en un properties localizado en la raiz 
 * @author engellt
 *
 */
public class AyudaSistema implements Ayuda{

	private static final String SEPARADOR = ".";

	Log log = LogServiceLocator.getDefaultImplementation().getLog(AyudaSistema.class);
	
	private Properties properties;
	
	public AyudaSistema(){
		properties = new Properties();
		try {
			properties.load(getClass().getResourceAsStream("/ayuda.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getAyudaPropiedad(Object dao, int column) {
		SQLEntidadMeta entidadMeta = getSqlEntidadMeta(dao);
		if(entidadMeta != null){
			String nombreEntidad = entidadMeta.getNombreEntidad();
			String nombreColumna = entidadMeta.getSQLColumnaMetaPorId(column).getColumnaSql();
			String llave = nombreEntidad + SEPARADOR + nombreColumna;
			llave = llave.toUpperCase();
			System.err.println("OBTENIENDO AYUDA DE " + llave);
			return getAyuda(llave);
		}else{
			return "No existe ayuda disponible";
		}
	}

	public String getAyuda(String key) {
		if(properties.containsKey(key)){
			return properties.getProperty(key);
		}else{
			return "No existe ayuda disponible";
		}
	}

	private SQLEntidadMeta getSqlEntidadMeta(Object dao) {
		try {
			return (SQLEntidadMeta) dao.getClass().getField("SQL_ENTIDAD_META").get(dao);
		} catch (Exception e){
			log.error("Ocurrio un error obteniendo el sql entidad meta de " + dao, e);
		}
		return null;
	}
}
