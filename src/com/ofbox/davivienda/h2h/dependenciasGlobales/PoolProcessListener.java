package com.ofbox.davivienda.h2h.dependenciasGlobales;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.proc.PoolProcessManager;

/**
 * Application Lifecycle Listener implementation class PoolProcessListener
 *
 */
public class PoolProcessListener implements ServletContextListener {

	Logger logger = LoggerFactory.getLogger(PoolProcessListener.class);
	
    /**
     * Default constructor. 
     */
    public PoolProcessListener() {
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent evt) {
    	logger.info("CONTEXTO DEL APLICATIVO INICIALIZANDO");
    	PoolProcessManager pm = PoolProcessManager.getInstance();
    	
    	try {
			pm.initialize();
			logger.info("Se han iniciado todos los procesos del calendarizador");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("--- ADVERTENCIA --- ");
			logger.error("Ha ocurrido un error al tratar de inciar todos los procesos del calendarizador.  Verificar que todos los procesos se hayan iniciado.  De lo contrario sera necesario reiniciar el servidor de aplicaciones.");
			logger.error("--- ADVERTENCIA --- ");
		}
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent evt) {
    	logger.info("CONTEXTO DEL APLICATIVO FINALIZANDO.  SE PROCEDE A DETENER TODOS LOS HILOS DEL CALENDARIZADOR...");
    	PoolProcessManager pm = PoolProcessManager.getInstance();
    	
    	try {
			pm.stopAll();
			logger.info("Se han detenido todos los procesos del calendarizador");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("--- ADVERTENCIA --- ");
			logger.error("Ha ocurrido un error al tratar de detener todos los procesos del calendarizador.  Verificar que todos los procesos se hayan detenido.  De lo contrario sera necesario reiniciar el servidor de aplicaciones.");
			logger.error("--- ADVERTENCIA --- ");
		}
    	
    	
    }
	
}
