package com.ofbox.davivienda.h2h.seguridad;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.ExcepcionEnLocalizacionDeServicio;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.dependencias.impl.LocalizadorDeSerivicioBase;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.DatosAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.EstadoAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.estado.ExcepcionEnUtileriaDeEstado;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.AmbienteDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.multiAplicacion.dto.RepositorioSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.multiAplicacion.dto.SeguridadDeUsuarioAplicacion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.multiAplicacion.dto.Usuario;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.multiAplicacion.dto.adaptadores.AmbienteDeSeguridadImpl;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.seguridad.multiAplicacion.xml.impl.ConstructorDeSeguridad;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.jdbc.LocalizadorDeConexion;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.xml.LocalizadorNodoDeAcceso;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.xml.NodoDeAcceso;
import com.ofbox.f3.app.distribuibles.implementaciones.web.triton.impl.servlet.TritonServletHelper;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final String APP_CONTEXT = "/app";

	
	private static final long serialVersionUID = 1L;

	private static RepositorioSeguridad repositorioSeguridad;
	
	private static final Map<String, Integer> ACCIONES = new Hashtable<String, Integer>();
	
	
	public static final class  AmbienteDeSeguridadPorUsuarioLoc extends LocalizadorDeSerivicioBase implements Serializable{
		
		public AmbienteDeSeguridadPorUsuarioLoc(String usuario) {
			super();
			this.usuario = usuario;
		}
		
		private String usuario;
		
		public Object getServicio() throws ExcepcionEnLocalizacionDeServicio {
			SeguridadDeUsuarioAplicacion segUsuario = repositorioSeguridad.getUniAplicacion().getSeguridadDeUsuarioAplicacionC(usuario);
			if(segUsuario.isUsuarioAsignado()){
				segUsuario.setUsuario(repositorioSeguridad.getUsuario(usuario));	
			}
			
			return new AmbienteDeSeguridadImpl(segUsuario);		
		}
		
	}
	
	 
	public static final int AUTENTICAR = 1;
	public static final int MENU = 2;
	public static final int BANNER = 3;
	public static final int BLANK = 4;
	public static final int FRAMESET = 5;
	public static final int WAP = 6;
	public static final int HTML = 7;
	public static final int RIA = 8;
	public static final int LOGIN = 9;
	public static final int SALIR = 10;
	public static final int RECARGAR_SEGURIDAD = 11;
	public static final int REDIRECT = 12;
	public static final int SELECCIONAR_ENTIDAD = 13;
	public static final int INGRESO_ENTIDAD = 14;
	
	 /*Especial para actuar sin login*/
	public static final int INICIAR = 99;	

	
	static{
		ACCIONES.put("autenticar", new Integer(AUTENTICAR));
		ACCIONES.put("menu", new Integer(MENU));
		ACCIONES.put("banner", new Integer(BANNER));
		ACCIONES.put("blank", new Integer(BLANK));
		ACCIONES.put("frameset", new Integer(FRAMESET));
		ACCIONES.put("salir", new Integer(SALIR));
		ACCIONES.put("recargarSeguridad", new Integer(RECARGAR_SEGURIDAD));
		ACCIONES.put("entidadMunicipalSelect", new Integer(SELECCIONAR_ENTIDAD));
		ACCIONES.put("ingresoEntidad", new Integer(INGRESO_ENTIDAD));
		
		ACCIONES.put("wap", new Integer(WAP));
		ACCIONES.put("html", new Integer(HTML));
		ACCIONES.put("ria", new Integer(RIA));
		ACCIONES.put("redirect", new Integer(REDIRECT));
		ACCIONES.put("inicio", new Integer(INICIAR));
	}
	
	
	public static int parseAccion(String accion){
		Integer accionInt = ACCIONES.get(accion);
		if(accionInt == null){
			return INICIAR;
		}
		return accionInt.intValue();
	}
	
	
	public static int parseTipo(String tipo){
		Integer tipoInt = ACCIONES.get(tipo);
		if(tipoInt==null){
			return HTML;
		}
		return tipoInt.intValue();
	}
	
	
	private static String pathConfiguracion;
	
	public static void recargarSeguridad() throws ServletException{
		try{
			/** 
			 * SEGURIDAD XML */ 
			NodoDeAcceso nodo = LocalizadorNodoDeAcceso.getNodoDeAccesoST(new FileInputStream(pathConfiguracion));
			repositorioSeguridad = new RepositorioSeguridad();
			ConstructorDeSeguridad.construirUniSeguridad(nodo, repositorioSeguridad);
			
			/*  
			 * SEGURIDAD POR BASE DE DATOS
			 *
			 */
			/*NodoDeAcceso nodo = LocalizadorNodoDeAcceso.getNodoDeAccesoST(new FileInputStream(pathConfiguracion));
			repositorioSeguridad = new RepositorioSeguridad();
			ConstructorDeSeguridadSql constructorDeSeguridad = new ConstructorDeSeguridadSql();
			constructorDeSeguridad.setLocalizadorDeConexion(BancaMovilResolusor.getSingleton().getLocalizadorDeConexionPrincipal());
			constructorDeSeguridad.setRespositorioSeguridad(repositorioSeguridad);
			constructorDeSeguridad.setNodoDeAcceso(nodo);
			constructorDeSeguridad.construirUniSeguridad();*/
			
		}catch(Exception e){
			e.printStackTrace();
			throw new ServletException(e);
		}
		
		
	}
	
	public void init(ServletConfig config) throws ServletException {	
			if(repositorioSeguridad==null){
				pathConfiguracion =	config.getServletContext().getRealPath(config.getInitParameter("Configuracion"));
				System.out.println("PathConfiguracion PathConfiguracion PathConfiguracion  "+pathConfiguracion);
				recargarSeguridad();
			}
			super.init(config);	
	}

	
	
	/**
     * @see HttpServlet#HttpSeq\rvlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		proc(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   proc(request, response);	
	}

	
	public static Usuario obtenerUsuario(String username){
		return repositorioSeguridad.getUsuario(username);		
	}
	
	
	protected void proc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String requestedUri =  request.getRequestURI();
		String servletPath = request.getServletPath();
		String deployMent = request.getContextPath();
		String pathLimpio =  "";
		
		
		HttpSession session = request.getSession();
		if(session!=null){
			System.out.println("Session>>> " + session.getId());
			System.out.println("Created>>> " + session.getCreationTime());
		}
		
		RequestDispatcher rd = null;		
		Collection<String> errores = new LinkedList<String>();
		request.setAttribute("errores", errores);
		
		String sesion = request.getParameter("sesion");
		if(sesion != null){
			verificarSesionActiva(request, response,rd);
			return;
		}
		
		/*listado de variables comunes:*/
		
		TritonServletHelper tritonServletHelper = null;
		DatosAplicacion datosAplicacion =   null;
		EstadoAplicacion estadoAplicacion = null;
		AmbienteDeSeguridad ambienteDeSeguridad  = null;
		HttpSession sUser = null;
		
		request.setAttribute("sPath", APP_CONTEXT);
		request.setAttribute("locale", "es_SV");
		
		if(requestedUri.length() > servletPath.length() + deployMent.length() + 1){
			int inicioDeProcesador = servletPath.length() + deployMent.length() + 1;
			pathLimpio = requestedUri.substring(inicioDeProcesador);
		}
		
		String[] acciones =  pathLimpio.split("/");
		System.out.println("===============================================================");
		System.out.println("path >>> " + pathLimpio);
		System.out.println("****************************************************************");
		
		
		int tipo = RIA;
		int accion = INICIAR;

		
		switch (acciones.length) {
		case 0:
			/*no trae nada valores por defecto*/
			break;
		case 1:
			/*unicamente tiene el tipo */
			tipo = parseTipo(acciones[0]);
		     break;
		case 2:
		default:
			/*contiene tipo y accion*/
			tipo = parseTipo(acciones[1]);
			accion = parseAccion(acciones[0]);
			break;
		}
		
		
		/*switch (accion) {
		   case RECARGAR_SEGURIDAD:
			   recargarSeguridad();
			   accion = LOGIN;
			   break;
		   case SALIR:					
				accion = LOGIN;
				break;
		}*/
		
		accion:
		switch (accion) {
		case REDIRECT:
			System.out.println("Accion redirect...");
			rd = request.getRequestDispatcher("/WEB-INF/global/jsp/redirect.jsp");
			break;
		case MENU:
			System.out.println("Accion menu...");
		   tritonServletHelper = TritonServletHelper.getTritonServletHelper(H2hResolusor.getSingleton());
		   datosAplicacion =  tritonServletHelper.getDatosAplicacion(request, response);
		   estadoAplicacion = datosAplicacion.getEstadoAplicacion();		   
		   ambienteDeSeguridad = estadoAplicacion.getAmbienteDeSeguridad();
		   Collection itemsDeMenu = ambienteDeSeguridad.getItemsDeMenu();
		   System.out.println("Colocando items de menu : CASE MENU");
		   
		   request.getSession().setAttribute("itemsDeMenu", itemsDeMenu);
		   rd = request.getRequestDispatcher("/WEB-INF/global/sec/menuRia.jsp");
		   
			try {
				tritonServletHelper.actualizarEstadoAplicacion(datosAplicacion);
			} catch (ExcepcionEnUtileriaDeEstado e1) {
				e1.printStackTrace();
			}
			break;		 
		case BLANK:
				System.out.println("Accion blank...");
			   AmbienteDeSeguridadPorUsuarioLoc ambienteSeguridadLocal = new AmbienteDeSeguridadPorUsuarioLoc("admin");
			   tritonServletHelper = TritonServletHelper.getTritonServletHelper(H2hResolusor.getSingleton());
			   datosAplicacion =  tritonServletHelper.getDatosAplicacion(request, response);
			   estadoAplicacion = datosAplicacion.getEstadoAplicacion();
			   estadoAplicacion.setAmbienteDeSeguridad(null);
			   estadoAplicacion.setAmbienteDeSeguridadLoc(ambienteSeguridadLocal);
			   
			   Collection itemsDeMenu2 = estadoAplicacion.getAmbienteDeSeguridad().getItemsDeMenu();  
			   System.out.println("Colocando items de menu CASE BLANK> " + itemsDeMenu2);
			   
			   request.getSession().setAttribute("itemsDeMenu", itemsDeMenu2);
			   rd = request.getRequestDispatcher("/WEB-INF/global/sec/blankHtml.jsp");
			   
			break;		
		case BANNER:
			System.out.println("Accion banner...");
				rd = request.getRequestDispatcher("/WEB-INF/global/sec/bannerRia.jsp");
			break;
		case INICIAR:
		default:
			System.out.println("Default...");
			 /*Aca lo que hay que hacer es capturar la url, sacar el parametro 'usuario'
			 e irlo a comparar a la tabla USUARIOS de la base de datos, si esta y esta activo, entonces chivo,
			 si no esta, entonces mandarlo a una pagina que indique que el usuario no se encuentra o no tiene permisos.*/
			String usuario = request.getParameter("usuario");
			Usuario usuarioObj = null; 
			try{
				validarUsuarioRequest(usuario);
				usuarioObj = obtenerUsuario("admin");
				usuarioObj.setNombre(usuario);
				 /*El ambiente de seguridad que debe cargar debe ser el mismo, pues el unico usuario
				 que hay es 'admin' -> WEB-INF/conf/seguridad.xml*/
				AmbienteDeSeguridadPorUsuarioLoc ambienteSeguridadLoc = new AmbienteDeSeguridadPorUsuarioLoc("admin");
				tritonServletHelper = TritonServletHelper.getTritonServletHelper(H2hResolusor.getSingleton());
				datosAplicacion =  tritonServletHelper.getDatosAplicacion(request, response);
				estadoAplicacion = datosAplicacion.getEstadoAplicacion();
				estadoAplicacion.setAmbienteDeSeguridadLoc(ambienteSeguridadLoc);

				ambienteDeSeguridad = estadoAplicacion.getAmbienteDeSeguridad();
				Collection itemsDeMenu3 = ambienteDeSeguridad.getItemsDeMenu();
				System.out.println("Colocando items de menu CASE INICIAR");
				request.getSession().setAttribute("itemsDeMenu", itemsDeMenu3);
				request.getSession().setAttribute(AmbienteDeSeguridad.OBJETO_USUARIO, usuarioObj);
				try {
					tritonServletHelper.actualizarEstadoAplicacion(datosAplicacion);
				} catch (ExcepcionEnUtileriaDeEstado e) {
					e.printStackTrace();
				}
				
				System.out.println("Buscando frameset...");
				rd = request.getRequestDispatcher("/WEB-INF/global/sec/framesetRia.jsp");
			}catch(Exception e){
				e.printStackTrace();
				errores.add(e.getMessage());
				if("USUARIO NO EXISTE".equals(e.getMessage().substring(0, 17))){
					rd = request.getRequestDispatcher("/restringido.jsp");
				}
				request.setAttribute("exception", e);
				rd = request.getRequestDispatcher("/error.jsp");
			}

		}
		rd.forward(request, response);
	}


	private void validarUsuarioRequest(String usuarioReq) throws Exception{
		 /*TODO Modificar hasta que se haga el paso hacia modulo SRPC*/
	}
	
	private void validarUsuarioRequestOld(String usuarioReq) throws Exception{
		
		LocalizadorDeConexion locConn = (LocalizadorDeConexion) H2hResolusor.getSingleton().getLocMulti().getServicio("seguridad-conn");
		System.out.println("LocConn LocConn LocConn  "+locConn.toString());
		Connection conn = locConn.getConnection();
		
		try{
			QueryRunner runner = new QueryRunner();
		
			Object idUsuario = runner.query(conn, "SELECT IDUSUARIO FROM USUARIO WHERE USUARIO = ? AND ESTADO = 'A'", new ScalarHandler(), usuarioReq);
			
			if(idUsuario == null){
				System.out.println("ADVERTENCIA: El usuario '" + usuarioReq + "' no existe o no se encuentra activo dentro del esquema de seguridad de COLECTURIA");
				throw new IllegalAccessException("USUARIO NO EXISTE. '" + usuarioReq + "' no se encuetra activo o no existe ");
			}
		}finally{
			if(conn!=null) try{ conn.close();}catch(Exception ignored){}
		}
	}
	
	
	private void verificarSesionActiva(HttpServletRequest request, HttpServletResponse response, RequestDispatcher rd) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Usuario usuario = (Usuario)session.getAttribute(AmbienteDeSeguridad.OBJETO_USUARIO);
		StringBuffer dataJson = new StringBuffer("{\"valor\":}");
		int pos = dataJson.lastIndexOf(":") + 1;
		if(usuario == null){
			dataJson.insert(pos, false);
		}else{
			dataJson.insert(pos, true);
		}
		
		String jsonData = dataJson.toString();
		request.setAttribute("jsonData", jsonData);
		rd = request.getRequestDispatcher("/WEB-INF/global/jsp/json.jsp");
		rd.forward(request, response);
	}

}
