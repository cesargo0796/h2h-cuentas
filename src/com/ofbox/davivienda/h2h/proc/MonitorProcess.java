package com.ofbox.davivienda.h2h.proc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import com.ofbox.davivienda.h2h.dependenciasGlobales.H2hResolusor;
import com.ofbox.davivienda.h2h.mail.Correo;
import com.ofbox.davivienda.h2h.mail.ManejadorDeCorreos;
import com.ofbox.davivienda.h2h.proc.file.AntPathMatcher;
import com.ofbox.davivienda.h2h.proc.file.FileNameMatcher;
import com.ofbox.davivienda.h2h.proc.file.FileProcessor;
import com.ofbox.davivienda.h2h.proc.file.FileTransferHelper;
import com.ofbox.f3.app.distribuibles.abstraccion.comunes.util.jdbc.LocalizadorDeConexion;
import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.PasswordAuthenticationClient;
import com.sshtools.j2ssh.sftp.SftpFile;
import com.sshtools.j2ssh.transport.IgnoreHostKeyVerification;


public class MonitorProcess implements StatefulJob{

	
	Logger logger = LoggerFactory.getLogger(MonitorProcess.class);
	
	public static final int STATE_JOB_INIT 			= 1;
	public static final int STATE_FILE_FOUND 		= 2;
	public static final int STATE_FILE_CAN_READ 	= 3;
	public static final int STATE_JOB_COMPLETED 	= 4;
	
	private AntPathMatcher matcher = new AntPathMatcher();
	
	
	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		
		try{
			logger.debug("Iniciando iteracion del proceso...");
			JobDataMap mergedJobDataMap = context.getMergedJobDataMap();
			int currentState = STATE_JOB_INIT;
			if(mergedJobDataMap.containsKey(Vars.JOB_EXEC_STATE)){
				currentState = mergedJobDataMap.getInt(Vars.JOB_EXEC_STATE);
			}
			if(currentState <= 0){
				currentState = STATE_JOB_INIT;
			}
			String proceso = context.getJobDetail().getKey().getName();
			int idProceso = mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO);
			ProcessLogHandler handler = getLogHandler(mergedJobDataMap);
			
			logger.debug("current state: " + currentState);
			switch(currentState){
				case STATE_JOB_INIT:
					jobInit(context, handler);
					break;
				case STATE_FILE_FOUND:
					fileFound(context, handler);
					break;
				case STATE_FILE_CAN_READ:
					fileCanRead(context, handler);
					break;
				case STATE_JOB_COMPLETED:
					jobCompleted(context, handler);
					break;
				default:
					logger.error("ESTADO NO VALIDO PARA EL PROCESO {}-{}, ESTADO: {}", new Object[]{idProceso, proceso, currentState});
					System.out.println("ESTADO NO VALIDO!: " + currentState);	
				
			}
			
			logger.debug("Finalizada la iteracion con estado: " + currentState);
			if(currentState == STATE_JOB_COMPLETED){
				logger.info("Iteraciones finalizadas.  No se vuelve a ejecutar este proceso");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error inesperado durante la ejecucion del proceso. " + e.getMessage(), e);
			
		}
	}

	
	
	public void jobInit(JobExecutionContext context, ProcessLogHandler handler) throws Exception {
		 /*Buscar el archivo en la ruta*/
		logger.debug("JOB-INIT: Buscando archivo en ruta ...");
		
		JobDataMap mergedJobDataMap = context.getMergedJobDataMap();
		PoolProcessManager poolManager = (PoolProcessManager) H2hResolusor.getSingleton().getInstancia("poolProcessManager");
		FileTransferHelper helper = createFileTransferHelper(mergedJobDataMap);
		
		int idProceso = mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO);
		
		
		logger.debug("Verificando si archivo {} existe en la ruta sftp {}", new Object[]{helper.getFilterBy(), helper.getOrigin()});
		long exists = -1;
		try{
			exists = helper.checkFileExists();
		}catch(Exception e){
			e.printStackTrace();
			
			List<String> nombreArchivoList = new ArrayList<String>();
			nombreArchivoList.add(helper.getFilterBy());
			
			handler.log ("Error verificando existencia del archivo de convenio. " + e.getMessage());
			handler.setLastException(e);
			handler.setResultado("E");
			handler.setDescripcionResultado("No fue posible buscar el archivo en la ruta : " + helper.getOrigin() + ".  Error: " + e.getMessage());
			handler.setNombreArchivoList(nombreArchivoList);
			gotoState(context, STATE_JOB_COMPLETED);
		}
		if(exists > 0){
			String message = "Archivo encontrado. Se espera a la siguiente verificacion para corroborar que el archivo no haya sido modificado";
			handler.log(message);
			logger.debug("El archivo fue encontrado.  Se esperara a la siguiente verificacion para corroborar que el archivo no haya sido modificado", new Object[]{helper.getFilterBy(), helper.getOrigin()});
			context.getJobDetail().getJobDataMap().put(Vars.JOB_EXEC_FILE_SIZE, exists);
			gotoState(context, STATE_FILE_FOUND);
		}else{
			logger.debug("El archivo no ha sido encontrado...esperando siguiente ejecucion...");
		}
	}
	
	public void fileFound(JobExecutionContext context, ProcessLogHandler handler) {
		logger.debug("JOB FILE FOUND: Archivo fue encontrado...");
		JobDataMap mergedJobDataMap = context.getMergedJobDataMap();
		try{
			FileTransferHelper helper = createFileTransferHelper(mergedJobDataMap);
			 /*Verificando tamaño de archivo */
			long fileSizeFound = mergedJobDataMap.getLong(Vars.JOB_EXEC_FILE_SIZE);
			
			long currentFileSize = helper.checkFileExists();
			logger.debug("Comparando tamaño de archivo leido previamente {} contra tamaño leido actualmente: {} ", new Object[]{fileSizeFound, currentFileSize});
			if(fileSizeFound == currentFileSize){
				logger.debug("Tamaño de archivo coincide.  Se pasa al siguiente estado.");
				gotoState(context, STATE_FILE_CAN_READ);
			}else{
				String message = "Tamaño de archivo no coincide con lectura anterior. Posiblemente el archivo aun se esta escribiendo";
				handler.log(message);
				logger.info(message);
				 /*Se actualiza el tamaño de archivo hacia el siguiente valor encontrado.*/
				context.getJobDetail().getJobDataMap().remove(Vars.JOB_EXEC_FILE_SIZE);
				context.getJobDetail().getJobDataMap().put(Vars.JOB_EXEC_FILE_SIZE, currentFileSize);
				logger.info("Tamaño de archivo actualizado : {}", currentFileSize);
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error en lectura de archivo ");
			handler.log("Error al tratar de recuperar el archivo de carpeta remota : " + e.getMessage());
			gotoState(context, STATE_JOB_COMPLETED);
		}
	}
	
	public void fileCanRead(JobExecutionContext context, ProcessLogHandler handler) {
		logger.debug("JOB CAN READ: El archivo ya puede ser consumido...");
		JobDataMap mergedJobDataMap = context.getMergedJobDataMap();
		//int idProceso = mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO);
		
		handler.log("Iniciando transferencia del archivo...");
		FileTransferHelper helper = createFileTransferHelper(mergedJobDataMap);
		
		String destLocal   = helper.getDestinationLocal();
				//+ File.separator + 
				//mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO) + "-" + mergedJobDataMap.getInt(Vars.PM_CLIENTE) + "-" + 
				//mergedJobDataMap.getInt(Vars.PM_CONVENIO); 
		
		handler.log("Verificando mergedJobDataMap: " + mergedJobDataMap);
		handler.log("Verificando helper: " + helper);
		
		
		logger.debug("Iniciando traslado de archivo hacia carpeta local...");
		try{
			helper.sftpDownload(destLocal);
						
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error al tratar de descargar archivo hacia carpeta local. " + e.getMessage(), e);
			
			handler.log("Error al descargar archivo hacia ruta : " + helper.getDestinationLocal());
			handler.log("Error: " + e.getMessage());
			handler.setLastException(e);
			handler.setResultado("E");
			handler.setDescripcionResultado("Error en descarga de archivo");
			gotoState(context, STATE_JOB_COMPLETED);
			return;
		}
		
		handler.log("Verificando helper.getDestination(): " + helper.getDestinationLocal());
		handler.log("Verificando helper.getFilterBy(): " + helper.getFilterBy());
		
		
		String localDirectory = helper.getDestinationLocal();
		File directoryLocal = new File(localDirectory);
		
		logger.debug("Archivo trasladado... hacia {}", localDirectory);
		handler.log("Archivo copiado hacia " + localDirectory);
		
		logger.debug("directoryLocal {}", directoryLocal);
		
		List<String> nombreArhivo = new ArrayList<String>();
		for(File fileNameValid : directoryLocal.listFiles()){
			logger.debug("Validando nombre de archivo {}", fileNameValid.getName());
			logger.debug("Validando nombre de archivo {}  .toUpperCase()  ", fileNameValid.getName().toUpperCase());
			System.out.println("Validando nombre de archivo  "+fileNameValid.getName());
			System.out.println("Validando nombre de archivo fileNameValid  "+fileNameValid.getName().toUpperCase());
			
			if(matcher.match(helper.getFilterBy().toUpperCase(), fileNameValid.getName().toUpperCase())){
				
				handler.log("Iniciando procesamiento de archivo en carpeta local: " + localDirectory);
				logger.debug("Iniciando procesamiento de archivo local [{}]...", fileNameValid.getName());	
				
				FileProcessor fileProcessor = createFileProcessor(mergedJobDataMap);
				File fileToProcess = fileNameValid;
				String fileName = fileToProcess.getName();
				System.out.println("Validando nombre de archivo fileName  "+fileName);
				System.out.println("Validando nombre de archivo fileName  "+fileName.toUpperCase());
				nombreArhivo.add(fileName);
				context.getJobDetail().getJobDataMap().put(Vars.JOB_EXEC_LOCAL_FILE, fileName);
				
				fileProcessor.process(fileToProcess);
				logger.debug("Archivo procesado.  Procediendo a colocar archivo de resultado...");
				handler.setNombreArchivoList(nombreArhivo);
				writeResultFile(helper, mergedJobDataMap, fileProcessor.getResultMessage(), fileName);
				
				logger.debug("Finalizado de escribir archivo de respuesta.  Se procede a eliminar archivo procesado.");
				handler.log("Eliminando archivo " + fileToProcess.getName());
				System.out.println("Validando nombre de archivo fileToProcess.getName()  "+fileToProcess.getName().toUpperCase());
				try{
					if(fileToProcess.delete()){
						logger.debug("Archivo local {} fue eliminado. ", fileName);
					}else{
						logger.debug("El archivo {} no ha podido ser eliminado", fileName);
						handler.log("ADVERTENCIA: No fue posible eliminar el archivo local " + fileName);
					}
				}catch(Exception e){
					logger.warn("No fue posible eliminar el archivo local " + fileToProcess.getName() + " de la ruta de archivos locales. " + e.getMessage(), e );
					handler.log("ADVERTENCIA: No fue posible eliminar archivo local de la ruta: " + fileToProcess.getName());
				}
				
				gotoState(context, STATE_JOB_COMPLETED);
				
				
			}else{
				logger.debug("El archivo ["+fileNameValid.getName()+"] no es seleccionable por el filtro ["+helper.getFilterBy()+"]");
			}
		}   
	}
	
	public static void main(String[] args) {
		File directoryLocal = new File("c:/test");
		System.out.println("que paso... "+directoryLocal.listFiles());
	}
	
	public void jobCompleted(JobExecutionContext context, ProcessLogHandler handler){
		logger.debug("JOB COMPLETED: El job ha finalizado...");
		JobDataMap mergedJobDataMap = context.getMergedJobDataMap();
		int idProceso = mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO);
		
		handler.log("Finaliza el proceso de monitoreo.");
		
		if(!"E".equals(handler.getResultado())){
			sendConfirmationEmail(handler, mergedJobDataMap);
		}
		if(!"O".equals(handler.getResultado())){
			sendWarningEmail(handler, mergedJobDataMap);
		}
		
		
		LogHandlerContainer.getSingleton().saveLogHandler(handler);
		
		Scheduler scheduler = context.getScheduler();
		String jobName = context.getJobDetail().getKey().getName();
		String groupName = context.getJobDetail().getKey().getGroup();
		try {
			scheduler.deleteJob(new JobKey(jobName, groupName));
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		logger.debug("JOB COMPLETED: El job no se ejecutara mas");
	}
	
	private void sendWarningEmail(ProcessLogHandler handler, JobDataMap mergedJobDataMap){
		
		logger.debug("Iniciando envio de correo electronico por error en procesamiento ");
		
		String emailHabilitado = mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_HABILITAR_ENVIO_CORREO);
		String mensaje = "";
		if("S".equals(emailHabilitado)){
			String dest = mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_LISTA_ENVIO_CORREO);
			
			if(StringUtils.isNotEmpty(dest)){
				logger.debug("Destinatarios de envio de correos habilitados: {}", dest);
				
				String[] recpts = dest.split(",");
				if(recpts.length == 0){ 
					recpts = dest.split(" ");
				}
				
				Correo mm = new Correo();
				mm.setRemitente(mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_REMITENTE_ENVIO_CORREO));
				mm.setAsunto(mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_ASUNTO_ENVIO_CORREO));
				for(String r : recpts){
					mm.agregarDestinatarios(r);
				}
				mm.setHtml(true);
				
				mensaje ="<html>\n"+
						"<body>\n" +
						"<h2> Sistema de Procesamiento - Host to Host - Banca Empresa Plus </h2>\n";
				mensaje+="<p> El procesamiento del archivo ";
						
				if (handler.getNombreArchivoList() != null && handler.getNombreArchivoList().size() > 0) {
					for (String nombreArchivo : handler.getNombreArchivoList()) {
						mensaje += "<strong>"+ nombreArchivo + "</strong>";
					}
				}
				
				mensaje+=" ha generado un error.   Resultado: <strong>" + handler.getDescripcionResultado() + " </strong> </p>\n" + 
						"<br/>\n" + 
						"\n" + 
						"<ul>\n" + 
						"	<li><strong> Cliente :</strong> " + handler.getCliente() + " </li>\n";
				
				if (handler.getNombreArchivoList() != null &&  handler.getNombreArchivoList().size() > 0) {
					for (String nombreArchivo : handler.getNombreArchivoList()) {
						mensaje+="	<li><strong> Archivo :</strong> " + nombreArchivo + " </li>\n";
					}
				}
				
				mensaje+="	<li><strong> Convenio: </strong>" + handler.getConvenio() + "</li>\n" +
						"	<li><strong> Lineas encontradas: </strong>" + handler.getLineasArchivo() +" </li>\n" +
						"	<li><strong> Lineas de detalle procesadas : </strong>" + handler.getLineasProcesadas() + " </li>\n" + 
						"</ul>\n"+
						"<br/> <p> Para mayor informacion ingrese al M&oacute;dulo de Administracion del Sistema Host to Host </p>" +
						"</body>\n" + 
						"</html>";
				System.out.println("Mensaje... "+mensaje);
				mm.setMensaje(mensaje);
				
				ManejadorDeCorreos mailSender = (ManejadorDeCorreos) H2hResolusor.getSingleton().getInstancia("mailSender");
				try{
					mailSender.sendMail(mm);
				}catch(Exception e){
					e.printStackTrace();
					logger.error("Error en envio de correo: " + e.getMessage(), e);
					
					handler.log("Error en envio de correo por falla en procesamiento de archivo. " + e.getMessage());
				}
			}
		}
		
		
		
	}
	
	
	private void sendConfirmationEmail(ProcessLogHandler handler, JobDataMap mergedJobDataMap){
		
		logger.debug("Iniciando envio de correo electronico de confirmacion ");
		
		String dest = mergedJobDataMap.getString(Vars.PM_EMAIL_CLIENTE);
		if(StringUtils.isNotEmpty(dest)){
			logger.debug("Destinatarios del cliente definidos.  Se envia correo electronico a: {}", dest);
			
			String[] recpts = dest.split(",");
			if(recpts.length == 0){ 
				recpts = dest.split(" ");
			}
			
			String lineaEncabezado = mergedJobDataMap.getString(Vars.PM_LINEA_ENCABEZADO);
			if("S".equals(lineaEncabezado)){
				lineaEncabezado = "<li><strong> Linea de encabezado : </strong> Si </li>";
			}else{
				lineaEncabezado = "<li><strong> Linea de encabezado : </strong> No </li>";
			}
			
			Correo mm = new Correo();
			mm.setRemitente(mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_REMITENTE_ENVIO_CORREO));
			mm.setAsunto(mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_ASUNTO_ENVIO_CORREO));
			for(String r : recpts){
				mm.agregarDestinatarios(r);
				System.out.println("rrrrrrrrrrrr  "+r);
			}
			mm.setHtml(true);
			String mensaje = "";
			mensaje = "<html>\n" + 
			"<body>\n" + 
			"<h2> Sistema de Procesamiento - Host to Host - Banca Empresa Plus </h2>\n" + 
			"<p> Banco Davivienda le informa que el archivo cargado a trav&eacute;s de la plataforma <strong>Host to Host de Banca Empresa Plus</strong>"
			+ " ha sido procesado.   Resultado: <strong>" + handler.getDescripcionResultado() + " </strong> </p>\n" + 
			"<br/>\n" + 
			"\n" + 
			"<ul>\n" + 
			"	<li><strong> Cliente :</strong> " + handler.getCliente() + " </li>\n";
			if (handler.getNombreArchivoList() != null &&  handler.getNombreArchivoList().size() > 0) {
				for (String nombreArchivo : handler.getNombreArchivoList()) {
					mensaje+="	<li><strong> Archivo :</strong> " + nombreArchivo + " </li>\n";
				}
			}
			
			mensaje+="	<li><strong> Convenio: </strong>" + handler.getConvenio() + "</li>\n" + 
			"	<li><strong> Lineas encontradas: </strong>" + handler.getLineasArchivo() +" </li>\n" + lineaEncabezado +
			"	<li><strong> Lineas de detalle procesadas : </strong>" + handler.getLineasProcesadas() + " </li>\n" + 
			"</ul>\n"+
			"<br/> <p> Ingrese a su cuenta en la plataforma de Banca Empresa Plus para conocer el estado de sus operaciones </p>" +
			"</body>\n" + 
			"</html>";
			System.out.println("Mensaje... "+mensaje);
			mm.setMensaje(mensaje);
			
			ManejadorDeCorreos mailSender = (ManejadorDeCorreos) H2hResolusor.getSingleton().getInstancia("mailSender");
			try{
				mailSender.sendMail(mm);
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Error en envio de correo: " + e.getMessage(), e);
				
				handler.log("Error en envio de correo de confirmacion de procesamiento de archivo. " + e.getMessage());
			}
		}else{
			logger.warn("No se encontraron destinatarios para envio de correo de confirmacion de archivo procesado.");
			handler.log("No se encontraron destinatarios para envio de correo de confirmacion. ");
		}
		
	}
	
	
	private ProcessLogHandler getLogHandler(JobDataMap mergedJobDataMap){
		int idProceso = mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO);
		ProcessLogHandler handler = null;
		if(LogHandlerContainer.getSingleton().logHanderExists(idProceso)){
			handler = LogHandlerContainer.getSingleton().getLogHandler(idProceso);
		}else{
			handler = LogHandlerContainer.getSingleton().createLogHandler(idProceso);
			 /*Como es primera vez que se crea, se colocan los valores foraneos*/
			handler.setCliente(mergedJobDataMap.getInt(Vars.PM_CLIENTE) + " - " + mergedJobDataMap.getString(Vars.PM_NOMBRE_CLIENTE));
			handler.setConvenio(mergedJobDataMap.getInt(Vars.PM_CONVENIO) + " - " + mergedJobDataMap.getString(Vars.PM_CONVENIO_NOMBRE));
			handler.setIdProceso(idProceso);
		}
		return handler;
	}
	
	private FileTransferHelper createFileTransferHelper(JobDataMap mergedJobDataMap){
		ProcessLogHandler handler = getLogHandler(mergedJobDataMap);
		logger.debug("createFileTransferHelper handlerhandler:  "+handler.toString());
		PoolProcessManager poolManager = (PoolProcessManager) H2hResolusor.getSingleton().getInstancia("poolProcessManager");
		FileTransferHelper helper = poolManager.createFileTransferHelper(mergedJobDataMap);
		 /*Colocar los valores de la ruta y el archivo a buscar...*/
		logger.debug("createFileTransferHelper mergedJobDataMap:  "+mergedJobDataMap.toString());
		String origin = mergedJobDataMap.getString(Vars.PM_RUTA_SFTP) + "/" + mergedJobDataMap.getString(Vars.PM_RUTA_ARCHIVOS_IN); 
		String destRemoto   = mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_DIRECTORIO_LOCAL_DESTINO) + File.separator + 
						mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_CARPETA_LECTURA_DEFAULT) + File.separator + 
						mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO) + "-" + mergedJobDataMap.getInt(Vars.PM_CLIENTE) + "-" + 
						mergedJobDataMap.getInt(Vars.PM_CONVENIO); 
		
		String destLocal   = helper.getDestinationLocal() + File.separator + 
				mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO) + "-" + mergedJobDataMap.getInt(Vars.PM_CLIENTE) + "-" + 
				mergedJobDataMap.getInt(Vars.PM_CONVENIO); 
		
		logger.debug("origin:  "+origin);
		logger.debug("destRemoto:  "+destRemoto);
		logger.debug("destLocal:  "+destLocal);
		
		String fileName = mergedJobDataMap.getString(Vars.PM_NOMBRE_ARCHIVO);
		logger.debug("createFileTransferHelper fileName:  "+fileName);
		logger.debug("Validando si existe el directorio destino.  Se creara si no existe");
		try{			
			File fileDestLocal = new File(destLocal);
			if(!fileDestLocal.exists()){
				fileDestLocal.mkdir();
				
				if (!fileDestLocal.exists()) {
					logger.error("No es posible crear la ruta destino local para la transferencia de archivo : " + fileName + ", -> ");
					handler.log("No es posible crear ruta destino local: " + destLocal);
				} 
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("No es posible crear la ruta destino local para la transferencia de archivo : " + fileName + ", -> " + e.getMessage(), e);
			handler.log("No es posible crear ruta destino local: " + destLocal);
		}
		logger.debug("mergedJobDataMap.getWrappedMap()  "+mergedJobDataMap.getWrappedMap());
		FileNameMatcher nameMatcher = new FileNameMatcher(fileName, mergedJobDataMap.getWrappedMap());
		String resolvedFileName = nameMatcher.getResolvedFilename();
		logger.debug("resolvedFileNameresolvedFileName            "+resolvedFileName);
		String processedDir = mergedJobDataMap.getString(Vars.PM_RUTA_PROCESADOS);
		
		helper.setOrigin(origin);
		helper.setSftpDestination(destRemoto);
		helper.setDestinationLocal(destLocal);
		helper.setFilterBy(resolvedFileName);
		helper.setProcessedDir(processedDir);
		
		return helper;
	}
	
	private FileProcessor createFileProcessor(JobDataMap map){
		try{
			LocalizadorDeConexion localizadorDeConexion = H2hResolusor.getSingleton().getLocalizadorDeConexionPrincipal();
			FileProcessor fileProcessor = new FileProcessor(map, localizadorDeConexion);
			return fileProcessor;
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error al tratar de recuperar DataSource de objeto Resolusor: " + e.getMessage(), e);
		}
		return null;
	}
	
	
	private void gotoState(JobExecutionContext context, int newState){
		context.getJobDetail().getJobDataMap().remove(Vars.JOB_EXEC_STATE); 
		context.getJobDetail().getJobDataMap().put(Vars.JOB_EXEC_STATE, newState);
	}
	
	
	private void writeResultFile(FileTransferHelper helper, JobDataMap mergedJobDataMap, String resultMessage, String fileName){
		ProcessLogHandler handler = getLogHandler(mergedJobDataMap);
		logger.info("Iniciando escritura de archivo resultado.  Determinando ruta...");
		String localPath = mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_DIRECTORIO_LOCAL_DESTINO) + File.separator + 
						   mergedJobDataMap.getString(Vars.PARAMETRO_SISTEMA_CARPETA_ESCRITURA_DEFAULT);
		String nombreCliente = mergedJobDataMap.getString(Vars.PM_NOMBRE_CLIENTE);
		nombreCliente = nombreCliente.replaceAll("\\s+","");
		String localResultFile = localPath + File.separator + nombreCliente + File.separator + fileName;
		String localResultDir = localPath + File.separator + nombreCliente;
		logger.debug("Escribiendo [{}] hacia el archivo {}", resultMessage, localResultFile);
		handler.log("Generando archivo de resultado en ruta : " + localResultFile);
		String sftpDestinationOut = "";
		
		try {
			File fileDir = new File(localResultDir);
			if(!fileDir.exists()){
				logger.debug("Creando directorio destino para el archivo de resultado: {}", localResultDir);
				fileDir.mkdirs();
			}
			
			PrintWriter writer = new PrintWriter(localResultFile, "UTF-8");
			writer.write(resultMessage);
			writer.close();
			
			logger.debug("Archivo escrito hacia ruta : {}", localResultFile);
			
			String sftpBase = mergedJobDataMap.getString(Vars.PM_RUTA_SFTP);
			String sftpOut  = mergedJobDataMap.getString(Vars. PM_RUTA_ARCHIVOS_OUT);
			String sftpDestination = sftpBase + "/" + sftpOut + "/" + fileName;
			sftpDestinationOut = sftpBase + "/" + sftpOut;
					
			helper.sftpUploadRemote(helper.getSftpDestination(), helper.getDestinationLocal(), fileName);
			
			logger.debug("Archivo de respuesta enviado hacia directorio destino en SFTP.  Eliminando archivo local.");
			handler.log("Archivo de respuesta colocado en: " + sftpDestination);
			try{
				File file = new File(localResultFile);
				if(file.delete()){
					logger.debug("Archivo de resultado local borrado correctamente");
				}else{
					logger.warn("El archivo {} no pudo ser eliminado del directorio local. ", localResultFile);
				}
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Error al eliminar archivo de resultado de directorio local. " + e.getMessage(), e);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("No fue posible escribir el archivo de respuesta. " + e.getMessage(), e);
			List<String> nombreArchivoList = new ArrayList<String>();
			int idProceso = mergedJobDataMap.getInt(Vars.PM_ID_PROCESO_MONITOREO);
			nombreArchivoList.add(helper.getFilterBy());

			ProcessLogHandler processLogHandler = LogHandlerContainer.getSingleton().createLogHandler(idProceso);
			processLogHandler.setCliente(mergedJobDataMap.getInt(Vars.PM_CLIENTE) + " - " + mergedJobDataMap.getString(Vars.PM_NOMBRE_CLIENTE));
			processLogHandler.setConvenio(mergedJobDataMap.getInt(Vars.PM_CONVENIO) + " - " + mergedJobDataMap.getString(Vars.PM_CONVENIO_NOMBRE));
			processLogHandler.setIdProceso(idProceso);
			processLogHandler.log ("Error tratando de escribir el archivo de respuesta. " + e.getMessage());
			processLogHandler.setLastException(e);
			processLogHandler.setResultado("E");
			processLogHandler.setDescripcionResultado("No fue posible escribir el archivo de respuesta en la ruta : " + sftpDestinationOut + ".  Error: " + e.getMessage());
			processLogHandler.setNombreArchivoList(nombreArchivoList);
		}
		
	}
	
	
}
