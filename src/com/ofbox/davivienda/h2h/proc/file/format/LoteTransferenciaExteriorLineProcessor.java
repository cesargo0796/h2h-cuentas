package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoteTransferenciaExteriorLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(LoteTransferenciaExteriorLineProcessor.class);
	
	private Long transferencia;
	private String cuentaOrd;
	private String nombreOr;
	private String direccionOrd;
	private String telOrd;
	private String faxOrd;
	private Integer via;
	private String codigoRef;
	private String bancoBen;
	private String direccionBancoBen;
	private String cuentaBen;
	private String nombreBen;
	private String direccionBen;
	private BigDecimal monto;
	private Integer monedaMonto;
	private Integer politicaComision;
	private String politicaAplicacion;
	private Date fechaAplicacion;
	private Integer politicaFechaAplic;
	private String autorizacionHost;
	private Integer estatus;
	private Date fechaCreacion;
	private Integer instalacion;
	private Integer cliente;
	private String usuario;
	private Integer autorizaciones;
	private String proposito;
	private String bancoIntermediario;
	private Integer viaIntermediario;
	private String codigoRefIntermediario;
	private String descripcion1;
	private String descripcion2;
	private String descripcion3;
	private String descripcion4;
	private Integer sysProcessId;
	private Integer sysLowDateTime;
	private Integer sysHighDateTime;	
	private Integer ponderacion;
	private Integer firmas;
	private Integer estatusBen;
	private String emailBen;
	private String emailOrd;  
	private String paisBanco;
	private Integer esPlantilla;
	private Integer estatusImp;
	private Integer frecuencia;
	private Integer noPagos;
	private String autorizantes;
	private String fechaAutoriza;
	private String cancela;  
	private Date fechaCancela;  
	private Integer token;   
	private String comentarioRechazo;
	private Integer sesion;   
	private String archivo;  
	private String mensaje;
	private Integer lote;   
	private String monedaOrdenante;
	private String monedaBeneficiario;
	private BigDecimal tasaCambio;
	private BigDecimal montoImpuesto; 
	private String exentoImpuesto;
	private Integer niu;       
	private String codigoTransaccion;
	private Date fechaFormulario;	
	private String comentario;  
	private String nombrePersona;
	private String tipoDocumento;
	private String numeroDocumento;	
	private String municipio;  
	private String codigoConcepto;
	private String paisSiti;
	
	
	private int aplicacionDebitoHost;
	
	public static final String SELECT_MAX_PS = "SELECT MAX(Transferencia) + 1 FROM BS_TransferenciaExterior ";
	
	public static final String INSERT_BS_TEXT = "BS_TransferenciaExterior" + 
			"      (" + 
				"" + 
				"Transferencia,CuentaOrd,NombreOrd,DireccionOrd,TelOrd,FaxOrd," + 
				"Via,CodigoRef,BancoBen,DireccionBancoBen,CuentaBen,NombreBen," + 
				"DireccionBen,Monto,MonedaMonto,PoliticaComision,PoliticaAplicacion," + 
				"FechaAplicacion,PoliticaFechaAplic,AutorizacionHost,Estatus," + 
				"FechaCreacion,Instalacion,Cliente,Usuario,Autorizaciones,Proposito," + 
				"bancoIntermediario,viaIntermediario,codigoRefIntermediario," + 
				"descripcion1,descripcion2,descripcion3,descripcion4," + 
				"Sys_ProcessId,Sys_LowDateTime,Sys_HighDateTime,Ponderacion," + 
				"Firmas,EstatusBen,EmailBen,EmailOrd,PaisBanco,EsPlantilla,EstatusImp," + 
				"Frecuencia,NoPagos,Autorizantes,FechaAutoriza,Cancela,FechaCancela," + 
				"Token,ComentarioRechazo,Sesion,Archivo,Mensaje,lote,MonedaOrdenante," + 
				"MonedaBeneficiario,TasaCambio,MontoImpuesto,ExentoImpuesto,niu," + 
				"codigo_transaccion,fecha_formulario,comentario,nombre_persona," + 
				"tipo_documento,numero_documento,municipio,codigo_concepto,pais_siti" + 
			"    ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"+
						 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"+
	 					 "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	public LoteTransferenciaExteriorLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}
	
	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Iniciando procesamiento de linea de encabezado para Convenio de Pago Servicio.  {}", businessData.getReferenceInfo());
		
		
		if(line == null){
			logger.info("El archivo {} no posee linea de encabezado definido.  Se toman los valores por defecto");
			defaultValues();
		}else{
			logger.debug("Se toman los valores de la linea: {}", line);
			if(formatoLinea==null){
				logger.error("Error, no existe el formato de la linea de encabezado del archivo definido!. Se asumen valores por defecto.");
				defaultValues();
			}else{
				parseLine(line);
			}
		}
		
		logger.debug("Recuperando valores de la linea para proceder a insercion");
		if(line!=null){
			readValuesFromLine();
		}
		
		logger.debug("Ingresando valores en registro BS_TransferenciaExterior");
		insertValues();
		
		logger.debug("Finalizado de ingresar registro en BS_TransferenciaExterior");
	}
	
	private void defaultValues(){
		transferencia = 0L;
		cuentaOrd = null;
		nombreOr = null;
		direccionOrd = null;
		telOrd = null;
		faxOrd = null;
		via = 0;
		codigoRef = null;
		bancoBen = null;
		direccionBancoBen = null;
		cuentaBen = null;
		nombreBen = null;
		direccionBen = null;
		monto = new BigDecimal(0);
		monedaMonto = 0;
		politicaComision = 0;
		politicaAplicacion = null;
		fechaAplicacion = new Date();
		politicaFechaAplic = 0;
		autorizacionHost = null;
		estatus = 0;
		fechaCreacion = new Date();
		instalacion = 0;
		cliente = 0;
		usuario = "H2H";
		autorizaciones = 0;
		proposito = null;
		bancoIntermediario = null;
		viaIntermediario = 0;
		codigoRefIntermediario = null;
		descripcion1 = null;
		descripcion2 = null;
		descripcion3 = null;
		descripcion4 = null;
		sysProcessId = 0;
		sysLowDateTime = 0;
		sysHighDateTime	 = 0;
		ponderacion = 0;
		firmas = 0;
		estatusBen = 0;
		emailBen = null;
		emailOrd = null;
		paisBanco = null;
		esPlantilla = 0;
		estatusImp = 0;
		frecuencia = 0;
		noPagos = 0;
		autorizantes = null;
		fechaAutoriza = null;
		cancela = null;  
		fechaCancela= new Date(); 
		token  = 0;  
		comentarioRechazo = null;
		sesion = 0;
		archivo = null;  
		mensaje = null;
		lote = 0;
		monedaOrdenante = null;
		monedaBeneficiario = null;
		tasaCambio = new BigDecimal(0);
		montoImpuesto = new BigDecimal(0); 
		exentoImpuesto = null;
		niu = 0;       
		codigoTransaccion = null;
		fechaFormulario = new Date();	
		comentario = null;  
		nombrePersona = null;
		tipoDocumento = null;
		numeroDocumento = null;	
		municipio = null;  
		codigoConcepto = null;
		paisSiti = null;
	}
	
	
	public void readValuesFromLine() throws LineProcessException{
		List<CampoLinea> campos = formatoLinea.getCampos();
		logger.debug("Iniciando busqueda de campos correspondientes al encabezado de un Lote de TEXT");
		transferencia = (Long) buscarValorColumna("Transferencia", campos);
		cuentaOrd = (String) buscarValorColumna("CuentaOrd", campos);
		nombreOr = (String) buscarValorColumna("NombreOrd", campos);
		direccionOrd = (String) buscarValorColumna("DireccionOrd", campos);
		telOrd = (String) buscarValorColumna("TelOrd", campos);
		faxOrd = (String) buscarValorColumna("FaxOrd", campos);
		via = (Integer) buscarValorColumna("Via", campos);
		codigoRef = (String) buscarValorColumna("CodigoRef", campos);
		bancoBen = (String) buscarValorColumna("BancoBen", campos);
		direccionBancoBen = (String) buscarValorColumna("DireccionBancoBen", campos);
		cuentaBen = (String) buscarValorColumna("CuentaBen", campos);
		nombreBen = (String) buscarValorColumna("NombreBen", campos);
		direccionBen = (String) buscarValorColumna("DireccionBen", campos);
		monto = (BigDecimal) buscarValorColumna("Monto", campos);
		monedaMonto = (Integer) buscarValorColumna("MonedaMonto", campos);
		politicaComision = (Integer) buscarValorColumna("PoliticaComision", campos);
		politicaAplicacion = (String) buscarValorColumna("PoliticaAplicacion", campos);
		fechaAplicacion = (Date) buscarValorColumna("FechaAplicacion", campos);
		politicaFechaAplic = (Integer) buscarValorColumna("PoliticaFechaAplic", campos);
		autorizacionHost = (String) buscarValorColumna("AutorizacionHost", campos);
		estatus = (Integer) buscarValorColumna("Estatus", campos);
		fechaCreacion = (Date) buscarValorColumna("FechaCreacion", campos);
		instalacion = (Integer) buscarValorColumna("Instalacion", campos);
		cliente = (Integer) buscarValorColumna("Cliente", campos);
		usuario = "H2H";
		autorizaciones = (Integer) buscarValorColumna("Autorizaciones", campos);
		proposito = (String) buscarValorColumna("Proposito", campos);
		bancoIntermediario = (String) buscarValorColumna("bancoIntermediario", campos);
		viaIntermediario = (Integer) buscarValorColumna("viaIntermediario", campos);
		codigoRefIntermediario = (String) buscarValorColumna("codigoRefIntermediario", campos);
		descripcion1 = (String) buscarValorColumna("descripcion1", campos);
		descripcion2 = (String) buscarValorColumna("descripcion2", campos);
		descripcion3 = (String) buscarValorColumna("descripcion3", campos);
		descripcion4 = (String) buscarValorColumna("descripcion4", campos);
		sysProcessId = (Integer) buscarValorColumna("Sys_ProcessId", campos);
		sysLowDateTime = (Integer) buscarValorColumna("Sys_LowDateTime", campos);
		sysHighDateTime	 = (Integer) buscarValorColumna("Sys_HighDateTime", campos);
		ponderacion = (Integer) buscarValorColumna("Ponderacion", campos);
		firmas = (Integer) buscarValorColumna("Firmas", campos);
		estatusBen = (Integer) buscarValorColumna("EstatusBen", campos);
		emailBen = (String) buscarValorColumna("EmailBen", campos);
		emailOrd = (String) buscarValorColumna("EmailOrd", campos);
		paisBanco = (String) buscarValorColumna("PaisBanco", campos);
		esPlantilla = (Integer) buscarValorColumna("EsPlantilla", campos);
		estatusImp = (Integer) buscarValorColumna("EstatusImp", campos);
		frecuencia = (Integer) buscarValorColumna("Frecuencia", campos);
		noPagos = (Integer) buscarValorColumna("NoPagos", campos);
		autorizantes = (String) buscarValorColumna("Autorizantes", campos);
		fechaAutoriza = (String) buscarValorColumna("FechaAutoriza", campos);
		cancela = (String) buscarValorColumna("Cancela", campos);  
		fechaCancela= (Date) buscarValorColumna("FechaCancela", campos); 
		token  = (Integer) buscarValorColumna("Token", campos);  
		comentarioRechazo = (String) buscarValorColumna("ComentarioRechazo", campos);
		sesion = (Integer) buscarValorColumna("Sesion", campos);
		archivo = (String) buscarValorColumna("Archivo", campos);  
		mensaje = (String) buscarValorColumna("Mensaje", campos);
		lote = (Integer) buscarValorColumna("lote", campos);
		monedaOrdenante = (String) buscarValorColumna("MonedaOrdenante", campos);
		monedaBeneficiario = (String) buscarValorColumna("MonedaBeneficiario", campos);
		tasaCambio = (BigDecimal) buscarValorColumna("TasaCambio", campos);
		montoImpuesto = (BigDecimal) buscarValorColumna("MontoImpuesto", campos); 
		exentoImpuesto = (String) buscarValorColumna("ExentoImpuesto", campos);
		niu = (Integer) buscarValorColumna("niu", campos);       
		codigoTransaccion = (String) buscarValorColumna("codigo_transaccion", campos);
		fechaFormulario = (Date) buscarValorColumna("fecha_formulario", campos);	
		comentario = (String) buscarValorColumna("comentario", campos);  
		nombrePersona = (String) buscarValorColumna("nombre_persona", campos);
		tipoDocumento = (String) buscarValorColumna("tipo_documento", campos);
		numeroDocumento = (String) buscarValorColumna("numero_documento", campos);	
		municipio = (String) buscarValorColumna("municipio", campos);  
		codigoConcepto = (String) buscarValorColumna("codigo_concepto", campos);
		paisSiti = (String) buscarValorColumna("pais_siti", campos);
		
		
		logger.debug("readValuesFromLine campos: {} ", campos);			
		logger.debug("Campos recuperados correctamente...");
	}
	
	public void insertValues() throws LineProcessException{
		
		QueryRunner runner = new QueryRunner();
		Connection conn = null;
		boolean autocommit = true;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);
			
			logger.debug("Obteniendo valor de siguiente secuencia de lote ...");
			transferencia = runner.query(conn, SELECT_MAX_PS, new ResultSetHandler<Long>(){
				public Long handle(ResultSet rs) throws SQLException {
					if(rs.next()){
						return rs.getLong(1);
					}
					return 1L;
				}
			});
			logger.debug("Siguiente valor para num de Lote en tabla BS_TransferenciaExterior: {}", lote);
			
			
			businessData.getEncabezadoData().setAplicacionDebitoHost(aplicacionDebitoHost);
						
			
			Object[] parameters = {
					transferencia,cuentaOrd,nombreOr,direccionOrd,telOrd,
					faxOrd,via,codigoRef,bancoBen,direccionBancoBen,cuentaBen,
					nombreBen,direccionBen,monto,monedaMonto,politicaComision,
					politicaAplicacion,fechaAplicacion,politicaFechaAplic,autorizacionHost,
					estatus,fechaCreacion,instalacion,cliente,usuario,autorizaciones,
					proposito,bancoIntermediario,viaIntermediario,codigoRefIntermediario,
					descripcion1,descripcion2,descripcion3,descripcion4,
					sysProcessId,sysLowDateTime,sysHighDateTime,ponderacion,firmas,
					estatusBen,emailBen,emailOrd,paisBanco,esPlantilla,
					estatusImp,frecuencia,noPagos,autorizantes,fechaAutoriza,
					cancela,fechaCancela,token,comentarioRechazo,
					sesion,archivo,mensaje,lote,monedaOrdenante,monedaBeneficiario,
					tasaCambio,montoImpuesto,exentoImpuesto,niu,codigoTransaccion,
					fechaFormulario,comentario,nombrePersona,tipoDocumento,numeroDocumento,	
					municipio,codigoConcepto,paisSiti	
			};
			
			logger.debug("Realizando insert en tabla BS_TransferenciaExterior ...");
			runner.update(conn, INSERT_BS_TEXT, parameters);
			logger.debug("Insert realizado.  Procediendo a realizar commit de la operacion...");
			conn.commit();
			logger.debug("Transaccion realizada correctamente...");
			conn.setAutoCommit(autocommit);
			
		}catch(SQLException e){
			if(conn!=null) try{ conn.rollback(); }catch(Exception ignored) {} 
			logger.error("Error al intentar realizar la insercion de registro en BS_TransferenciaExterior. " + e.getMessage(), e);
			throw new LineProcessException(e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
}
