package com.ofbox.davivienda.h2h.proc.file.format;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LotePagoServicioLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(LotePagoServicioLineProcessor.class);


	private Long pagoServicio;
	private String cuentaDebito;
	private Long moneda;
	private Double monto;
	private Long proveedor;
	private Date fechaDocumento;
	private String nombreDato1;
	private String nombreDato2;
	private String nombreDato3;
	private String nombreDato4;
	private String nombreDato5;
	private String nombreDato6;
	private String dato1;
	private String dato2;
	private String dato3;
	private String dato4;
	private String dato5;
	private String dato6;
	private Date fechaCreacion;
	private Long instalacion;
	private Long cliente;
	private String usuario;
	private Long token;
	private String comentarioRechazo;
	private Long lote;
	private Long estatus;
	private Date fechaEstatus;
	private String detalleXml;
	private String consultaXml;
	private String esPagoDirecto;
	private String autorizacion;
	private String xmlRespuestaConsulta;
	private Double montoImpuesto;
	
	
	private int aplicacionDebitoHost;
	
	public static final String SELECT_MAX_PS = "SELECT MAX(pagoServicio) + 1 FROM BS_PagoServicio ";
	
	public static final String INSERT_BS_PS = "dbo.BS_PagoServicio" + 
			"    (" + 
			"        PagoServicio," + 
			"        CuentaDebito," + 
			"        Moneda," + 
			"        Monto," + 
			"        Proveedor," + 
			"        FechaDocumento," + 
			"        NombreDato1," + 
			"        NombreDato2," + 
			"        NombreDato3," + 
			"        NombreDato4," + 
			"        NombreDato5," + 
			"        NombreDato6," + 
			"        Dato1," + 
			"        Dato2," + 
			"        Dato3," + 
			"        Dato4," + 
			"        Dato5," + 
			"        Dato6," + 
			"        FechaCreacion," + 
			"        Instalacion," + 
			"        Cliente," + 
			"        Usuario," + 
			"        Token," + 
			"        ComentarioRechazo," + 
			"        Lote," + 
			"        Estatus," + 
			"        FechaEstatus," + 
			"        DetalleXml," + 
			"        ConsultaXml," + 
			"        EsPagoDirecto," + 
			"        Autorizacion," + 
			"        XmlRespuestaConsulta," + 
			"        MontoImpuesto" + 
			"    )  VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
	
	public LotePagoServicioLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}
	
	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Iniciando procesamiento de linea de encabezado para Convenio de Pago Servicio.  {}", businessData.getReferenceInfo());
		
		
		if(line == null){
			logger.info("El archivo {} no posee linea de encabezado definido.  Se toman los valores por defecto");
			defaultValues();
		}else{
			logger.debug("Se toman los valores de la linea: {}", line);
			if(formatoLinea==null){
				logger.error("Error, no existe el formato de la linea de encabezado del archivo definido!. Se asumen valores por defecto.");
				defaultValues();
			}else{
				parseLine(line);
			}
		}
		
		logger.debug("Recuperando valores de la linea para proceder a insercion");
		if(line!=null){
			readValuesFromLine();
		}
		
		logger.debug("Ingresando valores en registro BS_PagoServicio");
		insertValues();
		
		logger.debug("Finalizado de ingresar registro en BS_PagoServicio");
	}
	

	

	
	private void defaultValues(){
		moneda = 0L;
		monto = 0.0;
		proveedor = 0L;
		fechaCreacion = new Date();
		cliente = 1L;
		usuario = "H2H";		 
		
	}
	
	
	public void readValuesFromLine() throws LineProcessException{
		List<CampoLinea> campos = formatoLinea.getCampos();
		logger.debug("Iniciando busqueda de campos correspondientes al encabezado de un Lote de PS");
		cuentaDebito = (String) buscarValorColumna("CuentaDebito", campos);
		moneda = (Long) buscarValorColumna("Moneda", campos);
		monto = (Double) buscarValorColumna("Monto", campos);
		proveedor =(Long) buscarValorColumna("Proveedor", campos);
		fechaDocumento = (Date) buscarValorColumna("FechaDocumento", campos);
		nombreDato1 = (String) buscarValorColumna("NombreDato1", campos);
		nombreDato2 = (String) buscarValorColumna("NombreDato2", campos);
		nombreDato3 = (String) buscarValorColumna("NombreDato3", campos);
		nombreDato4 = (String) buscarValorColumna("NombreDato4", campos);
		nombreDato5 = (String) buscarValorColumna("NombreDato5", campos);
		nombreDato6 = (String) buscarValorColumna("NombreDato6", campos);
		dato1 = (String) buscarValorColumna("Dato1", campos);
		dato2 = (String) buscarValorColumna("Dato2", campos);
		dato3 = (String) buscarValorColumna("Dato3", campos);
		dato4 = (String) buscarValorColumna("Dato4", campos);
		dato5 = (String) buscarValorColumna("Dato5", campos);
		dato6 = (String) buscarValorColumna("Dato6", campos);
		fechaCreacion = new Date();
		instalacion = (Long) buscarValorColumna("Instalacion", campos);
		cliente = (Long) buscarValorColumna("Cliente", campos);
		usuario = (String) buscarValorColumna("Usuario", campos);
		token = (Long) buscarValorColumna("Token", campos);
		comentarioRechazo = (String) buscarValorColumna("ComentarioRechazo", campos);
		lote = (Long) buscarValorColumna("Lote", campos);
		estatus = (Long) buscarValorColumna("Estatus", campos);
		fechaEstatus = (Date) buscarValorColumna("FechaEstatus", campos);
		detalleXml = (String) buscarValorColumna("DetalleXml", campos);
		consultaXml = (String) buscarValorColumna("ConsultaXml", campos);
		esPagoDirecto = (String) buscarValorColumna("EsPagoDirecto", campos);
		autorizacion = (String) buscarValorColumna("Autorizacion", campos);
		xmlRespuestaConsulta = (String) buscarValorColumna("XmlRespuestaConsulta", campos);
		montoImpuesto = (Double) buscarValorColumna("MontoImpuesto", campos);
		
		
		logger.debug("readValuesFromLine campos: {} ", campos);			
		logger.debug("Campos recuperados correctamente...");
	}
	
	public void insertValues() throws LineProcessException{
		
		QueryRunner runner = new QueryRunner();
		Connection conn = null;
		boolean autocommit = true;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);
			
			logger.debug("Obteniendo valor de siguiente secuencia de lote ...");
			pagoServicio = runner.query(conn, SELECT_MAX_PS, new ResultSetHandler<Long>(){
				public Long handle(ResultSet rs) throws SQLException {
					if(rs.next()){
						return rs.getLong(1);
					}
					 /*Si no trajimos nada puede que se trate del primer registro!*/
					return 1L;
				}
			});
			logger.debug("Siguiente valor para num de Lote en tabla BS_PagoServicio: {}", lote);
			
			
			businessData.getEncabezadoData().setAplicacionDebitoHost(aplicacionDebitoHost);
						
			
			Object[] parameters = {
					pagoServicio,cuentaDebito,moneda,monto,proveedor,fechaDocumento,nombreDato1,nombreDato2,
					nombreDato3,nombreDato4,nombreDato5,nombreDato6,dato1,dato2,dato3,dato4,dato5,dato6,
					fechaCreacion,instalacion,cliente,usuario,token,comentarioRechazo,lote,estatus,
					fechaEstatus,detalleXml,consultaXml,esPagoDirecto,autorizacion,xmlRespuestaConsulta,
					montoImpuesto	
			};
			
			logger.debug("Realizando insert en tabla BS_PagoServicio ...");
			runner.update(conn, INSERT_BS_PS, parameters);
			logger.debug("Insert realizado.  Procediendo a realizar commit de la operacion...");
			conn.commit();
			logger.debug("Transaccion realizada correctamente...");
			conn.setAutoCommit(autocommit);
			
		}catch(SQLException e){
			if(conn!=null) try{ conn.rollback(); }catch(Exception ignored) {} 
			logger.error("Error al intentar realizar la insercion de registro en BS_PagoServicio. " + e.getMessage(), e);
			throw new LineProcessException(e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
}
