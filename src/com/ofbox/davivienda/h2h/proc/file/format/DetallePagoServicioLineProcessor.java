package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.proc.Vars;

public class DetallePagoServicioLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(DetallePagoServicioLineProcessor.class);
	
	private Integer instalacion;
	private Integer lote;
	private Long operacion;
	private String cuenta;
	private String npe;
	private Long idColector;
	private Integer estatus;
	private Date fechaEstatus;
	private Long enviarAHost;
	private Boolean cuentaValida;
	private String nombreDeCuenta;
	private Boolean esPagoCombinado;
	private String atributosXML;
	private Long tipoCuenta;
	private Long moneda;
	private String detalleXML;
	private String consultaXML;
	private Boolean esPagoDirecto;
	private Long codError;
	private String descripcionError;
	private String autorizacion;
	private BigDecimal monto;
	private String nombreColector;
	private BigDecimal montoImpuesto;
	
	//private Integer aplicacionDebitoHost;
	
	
	private static final String INSERT_BS_DETALLE_PS = "INSERT INTO BS_DetallePS" + 
			"    (" + 
			"        Instalacion,lote,Operacion,Cuenta,Npe,IdColector,Estatus," + 
			"        fechaEstatus,EnviarAHost,CuentaValida,NombreDeCuenta,EsPagoCombinado," + 
			"        AtributosXML,TipoCuenta,Moneda,DetalleXML,ConsultaXML,EsPagoDirecto," + 
			"        CodError,DescripcionError,Autorizacion,Monto,NombreColector,MontoImpuesto" + 
			"    ) " + 
			"    VALUES" + 
			"    (" + 
			"        ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + 
			"        ?,?,?,?,?,?,?,?" + 
			"    )";
	
	QueryRunner queryRunner = null;
	InvocadorJ2Entorno invocadorEntorno;
	
	public DetallePagoServicioLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		
		super(businessData, formatoLinea);
		queryRunner = new QueryRunner();
		invocadorEntorno = new InvocadorJ2Entorno();
	}

	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Procesando linea de archivo : [{}]", line);
		clearValues();
		parseLine(line);
		logger.debug("validarcuentaDefinida cuenta1.2.2: "+cuenta);
		logger.debug("Iniciando lectura de campos de la linea segun definicion del formato de detalle en la configuracion del proceso. [{}]", businessData.getReferenceInfo());
		setupValues();
		logger.debug("validarcuentaDefinida cuenta1.2.3: "+cuenta);
		readValues();
		
		logger.debug("validarcuentaDefinida cuenta1.2: "+cuenta);
		if(cuenta != null) {						
		}else {
			cuenta = "000000000000";
		}	
		boolean convenioChequeA = 4 == businessData.getEncabezadoData().getLote();
		logger.debug("convenioChequeAconvenioChequeA   "+convenioChequeA); 
		logger.debug("Ejecutando validaciones de la cuenta ...");
		logger.debug("linelinelinelinelineline  "+line); 
		logger.debug("validarcuentaDefinida()   "+validarcuentaDefinida() ); 
		logger.debug("validarCuentaMultiplesCuentas()  "+validarCuentaMultiplesCuentas()); 
		logger.debug("validarCuentasPolParticipante()  "+validarCuentasPolParticipante()); 
		logger.debug("validarCuentaServicioEntorno()  "+validarCuentaServicioEntorno()); 

		if( convenioChequeA && validarcuentaDefinida()   &&		/*Se valida que la cuenta este en la linea o bien, que se utilice la cuenta definida en el convenio */
				validarCuentaMultiplesCuentas() &&						/*Se valida el flag convenios multiples cuentas*/
				validarCuentasPolParticipante()  						/*Se valida la cuenta que viene en el archivo		*/			
			  ){
			logger.debug("Se cumplió la validación: convenioChequeA, validarcuentaDefinida(), validarCuentaMultiplesCuentas(), validarCuentasPolParticipante()");
			estatus = 1;			
			logger.debug("Se aumenta el monto total del pago de servicio ya que el estado de las validaciones es = 1");
			businessData.getEncabezadoData().addMontoTotal(monto);
			
		}else if( validarcuentaDefinida()   &&	/*Se valida que la cuenta este en la linea o bien, que se utilice la cuenta definida en el convenio */
			validarCuentaMultiplesCuentas() &&			/*Se valida el flag convenios multiples cuentas*/
			validarCuentasPolParticipante() && 			/*Se valida la cuenta que viene en el archivo*/
			validarCuentaServicioEntorno()				/*Validar contra servicio Entorno*/
		  ){
			logger.debug("Se cumplió las validaciones: validarcuentaDefinida(), validarCuentaMultiplesCuentas(), validarCuentasPolParticipante(), validarCuentaServicioEntorno()");
			estatus = 1;			
			logger.debug("Se aumenta el monto total del pago de servicio ya que el estado de las validaciones es = 1");
			businessData.getEncabezadoData().addMontoTotal(monto);
			
		}else{
			logger.debug("El registro no ha superado las validaciones de la cuenta. ");
		}

		
		try {
			guardarDetalle();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al guardar registro de convenio en bs_detalleps.  " + businessData.getReferenceInfo() + " -> " + e.getMessage(), e);
			throw new LineProcessException("Error al guardar registro de Detalle del pago de servicio. ", e);
		}
		
	}

	
	
	public void guardarDetalle() throws SQLException{
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			 /*Primero determinar si se trata de convenio de pago o de cobro para saber como se 
			 Guardaran los registros, siempre y cuando el estatus = 1*/
			logger.debug("businessData.getEncabezadoData().getLote()   "+businessData.getEncabezadoData().getLote());
			logger.debug("estatus estatus estatus   "+estatus);
			/*Se determina que es un convenio de pago si tipopago de servicio = 2*/
			boolean convenioPago = 2 == businessData.getEncabezadoData().getLote();
			boolean convenioCheque = 4 == businessData.getEncabezadoData().getLote();
			/*Se determina que es un convenio de cobro si tipopago de servicio = 1*/
			boolean convenioCobro = 1 == businessData.getEncabezadoData().getLote();
			
			int lineNumber = businessData.getArchivoConEncabezado().equals(Boolean.TRUE) ? businessData.getLineaEnArchivo() - 1 : businessData.getLineaEnArchivo();
			logger.debug("businessData.getArchivoConEncabezado(): "+businessData.getArchivoConEncabezado());
			logger.debug("lineNumber Inicial: "+lineNumber);
			
			int tipoOperacion = 0;
			int tipoOperacionCuentaOrigen = 0;
			logger.debug("convenioPago convenioPago   "+convenioPago);
			logger.debug("convenioCobro convenioCobro   "+convenioCobro);
			logger.debug("convenioCheque convenioCheque   "+convenioCheque);
			if(convenioPago || convenioCheque){
				tipoOperacion = 2;
				tipoOperacionCuentaOrigen = 1;
			}
			
			if(convenioCobro){
				tipoOperacion = 1;
				tipoOperacionCuentaOrigen = 2;
			}
			
			if(estatus!=1){
				operacion++;
				
				logger.debug("estatus != 1, por lo tanto se guardara un solo registro en bs_detalleps");
				logger.debug("operacion   "+operacion);
				logger.debug("parametros insert cuenta1.1: "+cuenta);
				logger.debug("parametros insert nombreDeCuenta1.1.1: "+nombreDeCuenta);
				logger.debug("parametros insert cuenta1.1.2: "+cuenta);
				
				if(nombreDeCuenta != null && !nombreDeCuenta.equals("") && !nombreDeCuenta.equals("null")) {					
					logger.debug("Entro en el nombreDeCuenta nombreDeCuenta nombreDeCuenta  "+nombreDeCuenta);
				}else if (nombreDeCuenta == null){
					logger.debug("Entro en el else nombreDeCuenta  "+nombreDeCuenta);
					nombreDeCuenta = "";			
				}
				logger.debug("parametros insert tipoOperacion1: "+tipoOperacion);
				montoImpuesto = BigDecimal.ZERO;
				Object[] params = new Object[]{
					    instalacion,lote,operacion + 1,cuenta != null ? cuenta.trim() : null,npe,idColector,estatus,
					    fechaEstatus,enviarAHost,cuentaValida,nombreDeCuenta,esPagoCombinado,
					    atributosXML,tipoCuenta,moneda,detalleXML,consultaXML,esPagoDirecto,
					    codError,descripcionError,autorizacion,monto,nombreColector,montoImpuesto 
				};	
				logger.debug("parametros params.length1: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params[i]:  "+params[i]);
				}
				logger.debug("parametros insert INSERT_BS_DETALLE_PS: "+INSERT_BS_DETALLE_PS);
				queryRunner.update(conn, INSERT_BS_DETALLE_PS, params);
				logger.debug("Registro agregado en bs_detalleps");
				
				
				/******************* kpalacios - 19/08/2020 ***********************
				 Validación para insertar solamente 1 vez cuenta, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				return;
			}
			
			
			logger.debug("convenioCheque convenioCheque22   "+convenioCheque);
			if(convenioPago || convenioCheque){
				
				operacion++;
				logger.debug("Realizando insercion de operacion de Credito para un convenio de PAGO");
				logger.debug("operacion   "+operacion);
				logger.debug("guardarDetalle instalacion   "+instalacion);
				logger.debug("guardarDetalle lote   "+lote);
				logger.debug("guardarDetalle operacion   "+operacion);
				logger.debug("parametros insert cuenta: "+cuenta);
				logger.debug("parametros insert npe: "+npe);
				logger.debug("parametros insert idColector: "+idColector);
				logger.debug("parametros insert estatus: "+estatus);
				logger.debug("parametros insert fechaEstatus: "+fechaEstatus);
				logger.debug("parametros insert enviarAHost: "+enviarAHost);
				logger.debug("parametros insert cuentaValida: "+cuentaValida);
				logger.debug("parametros insert nombreDeCuenta: "+nombreDeCuenta);
				logger.debug("parametros insert esPagoCombinado: "+esPagoCombinado);
				logger.debug("parametros insert atributosXML: "+atributosXML);
				logger.debug("parametros insert tipoCuenta: "+tipoCuenta);
				logger.debug("parametros insert moneda: "+moneda);
				logger.debug("parametros insert detalleXML: "+detalleXML);
				logger.debug("parametros insert consultaXML: "+consultaXML);
				logger.debug("parametros insert esPagoDirecto: "+esPagoDirecto);
				logger.debug("parametros insert codError: "+codError);
				logger.debug("parametros insert descripcionError: "+descripcionError);
				logger.debug("parametros insert autorizacion: "+autorizacion);
				logger.debug("parametros insert monto: "+monto);
				logger.debug("parametros insert nombreColector: "+nombreColector);
				logger.debug("parametros insert montoImpuesto: "+montoImpuesto);
				montoImpuesto = BigDecimal.ZERO;			
				Object[] params = new Object[]{
					    instalacion,lote,operacion + 1,cuenta != null ? cuenta.trim() : null,npe,idColector,estatus,
					    fechaEstatus,enviarAHost,cuentaValida,nombreDeCuenta,esPagoCombinado,
					    atributosXML,tipoCuenta,moneda,detalleXML,consultaXML,esPagoDirecto,
					    codError,descripcionError,autorizacion,monto,nombreColector,montoImpuesto
					};
				logger.debug("parametros params.length2: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params[i]:  "+params[i]);
					logger.debug("parametros params[i]:  "+params[i]);
				}
				queryRunner.update(conn, INSERT_BS_DETALLE_PS, params);
				logger.debug("Registro agregado en bs_detalleps para la operacion de Credito");
				
				
				
				/******************* kpalacios - 19/08/2016 ***********************
				 Validación para insertar solamente 1 vez cuenta, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				
				logger.debug("Finalizado el proceso de insercion");
				return;
			}
			
			
			if(convenioCobro){
				if (validarcuentaDefinida()) {
				}
				
				operacion++;
				logger.debug("Realizando insercion de operacion de Debito para un convenio de COBRO");
				logger.debug("operacion   "+operacion);
				logger.debug("parametros insert cuenta5: "+cuenta);
				logger.debug("parametros insert nombreDeCuenta5: "+nombreDeCuenta);
				if(nombreDeCuenta != null && !nombreDeCuenta.equals("") && !nombreDeCuenta.equals("null")) {					
					logger.debug("Entro en el nombreDeCuenta nombreDeCuenta nombreDeCuenta  "+nombreDeCuenta);
				}else if (nombreDeCuenta == null){
					logger.debug("Entro en el else nombreDeCuenta  "+nombreDeCuenta);
					nombreDeCuenta = "";			
				}
				lote = lote+1;
				montoImpuesto = BigDecimal.ZERO;
				logger.debug("parametros insert tipoOperacion5: "+tipoOperacion);
				Object[]  params = new Object[]{
					    instalacion,lote,operacion + 1,cuenta != null ? cuenta.trim() : null,npe,idColector,estatus,
					    fechaEstatus,enviarAHost,cuentaValida,nombreDeCuenta,esPagoCombinado,
					    atributosXML,tipoCuenta,moneda,detalleXML,consultaXML,esPagoDirecto,
					    codError,descripcionError,autorizacion,monto,nombreColector,montoImpuesto
					    };
				
				
				logger.debug("parametros params.length4: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params["+i+"]:  "+params[i]);
				}
				queryRunner.update(conn, INSERT_BS_DETALLE_PS, params);
				logger.debug("Registro agregado en bs_detalleps para la operacion de Debito");
				
				

				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				logger.debug("Finalizado el proceso de insercion");
				return;
			}
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
	
	public void guardarDetalleEncabezado(int tipoOperacionCuentaOrigen, Connection conn) throws SQLException {
		logger.debug("Realizando insercion de operacion de monto total");					
		
		BigDecimal montoTotal = businessData.getEncabezadoData().getMontoTotal();
		logger.debug("MontoTotal [{}] "+montoTotal);
		nombreDeCuenta = businessData.getEncabezadoData().getNombreCuenta();
		estatus = businessData.getEncabezadoData().getEstatus();
		if(tipoOperacionCuentaOrigen == 1) {
			nombreDeCuenta = "";
		}
		Object[] params = new Object[]{
			    instalacion,lote,operacion + 1,cuenta != null ? cuenta.trim() : null,npe,idColector,estatus,
			    fechaEstatus,enviarAHost,cuentaValida,nombreDeCuenta,esPagoCombinado,
			    atributosXML,tipoCuenta,moneda,detalleXML,consultaXML,esPagoDirecto,
			    codError,descripcionError,autorizacion,monto,nombreColector,montoTotal
			};
		logger.debug("parametros params: "+params.length);
		for (int i = 0; i < params.length; i++) {
			System.out.println("parametros params[i]:  "+params[i]);
		}
		queryRunner.update(conn, INSERT_BS_DETALLE_PS, params);
		logger.debug("Registro agregado en bs_detalleps para la operacion de monto total");
	}
	
	public boolean validarcuentaDefinida(){
		logger.debug("Validando la cuenta de Debito provista en el archivo (si existe) o la cuenta definida segun convenio" );
		String cuentaSegunConvenio = businessData.getEncabezadoData().getCuentaDebitoCredito();
		logger.debug("parametros insert cuenta7: "+cuenta);	
		if(isEmptyString(cuenta)){
			logger.debug("No se ha definido la cuenta de Debito/Credito en la linea.  Se toma la cuenta definida segun convenio: [{}]", cuentaSegunConvenio);
			cuenta = cuentaSegunConvenio;
		}
		logger.debug("parametros insert cuenta8: "+cuenta);	
		if(isEmptyString(cuenta)){
			logger.debug("Cuenta segun convenio NO esta definida.  Por lo tanto quedara con estatus = 97 (Cuenta de debito sin permiso)");
			estatus = 97;
			return false;
		}
		
		return true;
	}
	
	public boolean validarCuentaMultiplesCuentas(){
		logger.debug("Validando si el convenio es de multiples cuentas");
		boolean esMultiplesCuentas = businessData.getEncabezadoData().getMultiplesCuentas();
		if(!esMultiplesCuentas){
			logger.debug("Convenio de Multiples cuentas es false.");
			return true;
		}
		logger.debug("Convenio Multiples Cuentas es verdadero.  Por lo tanto se validara si la cuenta se encuentra definida en 'BS_Convenio' primero, y si no en el listado de 'Bs_ConvenioCuenta'");
		
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		Integer cliente = businessData.getInt(Vars.PM_CLIENTE);
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			logger.debug("parametros insert cuenta9: "+cuenta);	
			logger.debug("Validando la cuenta {} se encuentre definida en tabla 'Bs_Convenio' para Convenio: {}, Cliente: {} ", new Object[]{cuenta, convenio, cliente});
			String ctaConvenio = (String) queryRunner.query(conn, "select Cuenta from BS_Convenio where Convenio = ? and Cliente = ? ", 
					new ScalarHandler("Cuenta"), new Object[]{convenio, cliente});
			logger.debug("Cuenta default definida en Bs_Convenio para Convenio: [{}], Cliente: [{}] se encontro Cuenta: [{}]", new Object[]{convenio, cliente, ctaConvenio});
			logger.debug("parametros insert cuenta10: "+cuenta);	
			if(ctaConvenio.trim().equals(cuenta.trim())){
				logger.debug("La cuenta {} se encuentra definida en 'BS_Convenio' para el convenio {} ", cuenta, convenio);
				return true;
			}
			logger.debug("parametros insert cuenta11: "+cuenta);	
			logger.debug("La cuenta {} no se encuentra en 'BS_Convenio', por lo que se buscara en 'BS_ConvenioCuenta' ", cuenta);
			logger.debug("parametros insert cuenta12: "+cuenta);	
			Integer cant = (Integer)queryRunner.query(conn,"select count(Cuenta) from BS_ConvenioCuenta where TipoConvenio = ? and Convenio = ? and Cuenta = ?", new ScalarHandler(), tipoconvenio, convenio, cuenta);
			if(cant <= 0){
				logger.debug("NO se encontro la cuenta definida en 'BS_ConvenioCuenta' para convenio de multiples cuentas. [{}]", businessData.getReferenceInfo());
				estatus = 97;
				return false;
			}
			logger.debug("La cuenta [{}] se encuentra definida en el listado de Bs_ConvenioCuenta", cuenta);
			return true;
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("Ocurrio un error al hacer la consulta de la cuenta [{}] en el registro 'Bs_ConvenioCuenta'. " + e.getMessage(), e);
			estatus = 7;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		return false;
	}
	
	public boolean validarCuentasPolParticipante(){
		logger.debug("Iniciando validacion de las cuentas especificadas en el archivo segun convenio abierto/cerrado");
		int polParticipante = businessData.getEncabezadoData().getPolParticipante();
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		Connection conn = null;
		logger.debug("polParticipante: [{}]", polParticipante);
		if(polParticipante == 1){
			try{
				conn = businessData.getLocalizadorDeConexion().getConnection();
				logger.debug("El convenio es cerrado por lo tanto se validara la cuenta [{}] que existe en 'BS_Participante' ", cuenta);
				Map<String, Object> map = queryRunner.query(conn, "select Monto, Estatus from Bs_Participante where TipoConvenio = ? and Convenio = ? and CAST(Cuenta AS NUMERIC) = ?", new MapHandler(), tipoconvenio, convenio, cuenta);
				if(map == null){
					logger.warn("La cuenta [{}] no se encuentra registrada en la tabla Bs_Participante. [{}]", cuenta, businessData.getReferenceInfo());
					logger.debug("Se registra el estatus = 4 (Empleado o Proveedor no registrado)");
					estatus = 4;
					return false;
				}
				
				logger.debug("Se encontraron resultados de Bs_Participante.  Procediendo a validar monto autorizado y estatus");
				Integer polParticipanteEstatus = (Integer) map.get("Estatus");
				Number polParticipanteMonto = (Number) map.get("Monto");
				
				if(polParticipanteEstatus!=1){
					logger.warn("La cuenta [{}] participante no se encuentra habilitada. Se coloca el registro con estatus = 2 (Participante deshabilitado)", cuenta);
					estatus = 2;
					return false;
				}
				
				BigDecimal Monto = new BigDecimal(polParticipanteMonto.doubleValue());
				if(Monto.compareTo(monto)<0){
					logger.warn("La cuenta [{}] participante no cuenta con un monto autorizado ({}) superior al monto indicado en el archivo: ({}). Se coloca estatus = 3 (Monto no Autorizado)", new Object[]{cuenta, Monto, monto});
					estatus = 3;
					return false;
				}
				
				logger.debug("Monto y Estatus autorizados.  La cuenta puede continuar con el proceso de validaciones");
				return true;
				
			}catch(SQLException e){
				e.printStackTrace();
				logger.error("Error al validar cuenta [{}] en Bs_Participante", cuenta, e);
				estatus = 7;
				return false;
			}finally{
				DbUtils.closeQuietly(conn);
			}
		}
		return true;
	}
	
	public boolean validarCuentaServicioEntorno(){
		logger.debug("Validando cuenta [{}] contra servicio de Entorno ...", cuenta);
		Respuesta respNombreCuenta = invocadorEntorno.invocarInformacionNombreCuenta(cuenta);
		
		if(respNombreCuenta == null || respNombreCuenta.getCodigo() != 0){
			logger.warn("Se encontro un error en la validacion de la cuenta desde PagoElectronico contra el servicio fabricaCoreBanca/srvInfoNombreCuenta. ");
			estatus = 7;
			return false;
		}
		respNombreCuenta.primerContenedor();
		if(!respNombreCuenta.siguiente()){
			logger.warn("No se encontraron resultados para la consulta de la cuenta desde PagoElectronico [{}] contra servicio fabricaCoreBanca/srvInfoNombreCuenta", cuenta);
			estatus = 7;
			return false;
		}
		
		nombreDeCuenta = respNombreCuenta.obtenerString("nombreCuenta");
		logger.info("nombreDeCuenta "+nombreDeCuenta);
		if(isEmptyString(nombreDeCuenta)){
			logger.warn("No se ha definido el nombre completo para la cuenta [{}] , desde PagoELectronico segun el servicio fabricaCoreBanca/srvInfoNombreCuenta ", cuenta);
			estatus = 7;
			return false;
		}
		logger.debug("Se ha recuperado el valor de nombre completo desde PagoElectronico [{}] del servicio fabricaCoreBanca/srvInfoNombreCuenta", nombreDeCuenta);
	
		return true;
	}
	
	public void readValues()
			throws LineProcessException {		
		List<CampoLinea> campos = formatoLinea.getCampos();
				
		instalacion = (Integer) buscarValorColumna("Instalacion", campos);
		lote = (Integer) buscarValorColumna("lote", campos);
		operacion = (Long) buscarValorColumna("Operacion", campos);
		cuenta = String.valueOf(buscarValorColumna("Cuenta", campos));
		npe = String.valueOf(buscarValorColumna("Npe", campos));
		idColector = (Long) buscarValorColumna("IdColector", campos);
		estatus = (Integer) buscarValorColumna("Estatus", campos);
		fechaEstatus = (Date) buscarValorColumna("fechaEstatus", campos);
		enviarAHost = (Long) buscarValorColumna("EnviarAHost", campos);
		cuentaValida = (Boolean) buscarValorColumna("CuentaValida", campos);
		nombreDeCuenta = String.valueOf(buscarValorColumna("NombreDeCuenta", campos));
		esPagoCombinado = (Boolean) buscarValorColumna("EsPagoCombinado", campos);
		atributosXML = String.valueOf(buscarValorColumna("AtributosXML", campos));
		tipoCuenta = (Long) buscarValorColumna("TipoCuenta", campos);
		moneda = (Long) buscarValorColumna("Moneda", campos);
		detalleXML = String.valueOf(buscarValorColumna("DetalleXML", campos));
		consultaXML = String.valueOf(buscarValorColumna("ConsultaXML", campos));
		esPagoDirecto = (Boolean) buscarValorColumna("EsPagoDirecto", campos);
		codError = (Long) buscarValorColumna("CodError", campos);
		descripcionError = String.valueOf(buscarValorColumna("DescripcionError", campos));
		autorizacion = String.valueOf(buscarValorColumna("Autorizacion", campos));
		monto = (BigDecimal) buscarValorColumna("Monto", campos);
		nombreColector = String.valueOf(buscarValorColumna("NombreColector", campos));
		montoImpuesto = (BigDecimal) buscarValorColumna("MontoImpuesto", campos);
		

		logger.debug("Entro en el nombreDeCuenta "+nombreDeCuenta + " cuenta  "+cuenta);
		if(cuenta != null && !cuenta.equals("") && !cuenta.equals("null")) {
			logger.debug("Entro en el == cuenta cuenta  "+cuenta);
		}else if (cuenta.equals("null")){
			logger.debug("Entro en el cuenta cuenta  "+cuenta);
			cuenta = null;			
		}else if (cuenta == null){
			logger.debug("Entro en el else cuenta  "+cuenta);
			cuenta = null;			
		}
		
		if(nombreDeCuenta != null && !nombreDeCuenta.equals("") && !nombreDeCuenta.equals("null")) {
			logger.debug("Entro en el == nombreBenef nombreBenef  "+nombreDeCuenta);
		}else if (nombreDeCuenta.equals("null")){
			logger.debug("Entro en el nombreBenef nombreBenef  "+nombreDeCuenta);
			nombreDeCuenta = null;
		}else if (nombreDeCuenta == null){
			logger.debug("Entro en el else nombreBenef  "+nombreDeCuenta);
		}		
	}
	
	public void setupValues(){
		EncabezadoData data = businessData.getEncabezadoData();
		instalacion = data.getInstalacion();
		lote = data.getLote();
		//aplicacionDebitoHost = data.getAplicacionDebitoHost();
		fechaEstatus = new Timestamp(System.currentTimeMillis());
	}
	
	public void clearValues(){
		instalacion = 0;
		lote = 0;
		operacion = 0L;
		cuenta = null;
		npe = null;
		idColector = 0L;
		estatus = 0;
		fechaEstatus = new Date();
		enviarAHost = 0L;
		cuentaValida = false;
		nombreDeCuenta = null;
		esPagoCombinado = false;
		atributosXML = null;
		tipoCuenta = 0L;
		moneda = 0L;
		detalleXML = null;
		consultaXML = null;
		esPagoDirecto = false;
		codError = 0L;
		descripcionError = null;
		autorizacion = null;
		monto = new BigDecimal(0);
		nombreColector = null;
		montoImpuesto = new BigDecimal(0);
	}

}