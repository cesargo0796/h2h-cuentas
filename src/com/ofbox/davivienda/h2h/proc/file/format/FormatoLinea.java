package com.ofbox.davivienda.h2h.proc.file.format;

import java.util.List;

public class FormatoLinea {

	private String tablaDestino;
	
	private boolean formatoDetalle;
	
	private String nombreFormato;
	private String tipoFormato;
	private String tipoSeparador;
	private String caracterSeparador;
	private String loteDestino;
	
	private boolean formatoEncabezado;
	private boolean camposLongitudesFijas;
	
	private List<CampoLinea> campos;

	public String getTablaDestino() {
		return tablaDestino;
	}

	public void setTablaDestino(String tablaDestino) {
		this.tablaDestino = tablaDestino;
	}

	public boolean isFormatoDetalle() {
		return formatoDetalle;
	}

	public void setFormatoDetalle(boolean formatoDetalle) {
		this.formatoDetalle = formatoDetalle;
	}

	public String getNombreFormato() {
		return nombreFormato;
	}

	public void setNombreFormato(String nombreFormato) {
		this.nombreFormato = nombreFormato;
	}

	public String getTipoFormato() {
		return tipoFormato;
	}

	public void setTipoFormato(String tipoFormato) {
		this.tipoFormato = tipoFormato;
		this.formatoEncabezado = "E".equals(tipoFormato);
		this.formatoDetalle = "D".equals(tipoFormato);
	}

	public String getTipoSeparador() {
		return tipoSeparador;
	}

	public void setTipoSeparador(String tipoSeparador) {
		this.tipoSeparador = tipoSeparador;
		this.camposLongitudesFijas = "E".equals(tipoSeparador);
	}

	public String getCaracterSeparador() {
		return caracterSeparador;
	}

	public void setCaracterSeparador(String caracterSeparador) {
		this.caracterSeparador = caracterSeparador;
	}

	public boolean isFormatoEncabezado() {
		return formatoEncabezado;
	}

	public void setFormatoEncabezado(boolean formatoEncabezado) {
		this.formatoEncabezado = formatoEncabezado;
	}

	public boolean isCamposLongitudesFijas() {
		return camposLongitudesFijas;
	}

	public void setCamposLongitudesFijas(boolean camposLongitudesFijas) {
		this.camposLongitudesFijas = camposLongitudesFijas;
	}

	public List<CampoLinea> getCampos() {
		return campos;
	}

	public void setCampos(List<CampoLinea> campos) {
		this.campos = campos;
	}

	public String getLoteDestino() {
		return loteDestino;
	}

	public void setLoteDestino(String loteDestino) {
		this.loteDestino = loteDestino;
	}
	
	
	
}
