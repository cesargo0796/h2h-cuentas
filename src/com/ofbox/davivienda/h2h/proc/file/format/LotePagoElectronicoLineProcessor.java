package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.icu.util.Calendar;
import com.ofbox.davivienda.h2h.proc.Vars;

public class LotePagoElectronicoLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(LotePagoElectronicoLineProcessor.class);


	private int instalacion;
	private int lote;
	private int tipoConvenio;
	private int convenio;
	private int empresaODepto;
	private int tipoLote;
	private BigDecimal montoTotal;
	private Long numOperaciones;
	private String nombreLote;
	private Timestamp fechaEnviado;
	private Timestamp fechaRecibido;
	private Timestamp fechaAplicacion;
	private Timestamp fechaEstatus;
	private int estatus;
	private int numeroReintento;
	private String usuarioIngreso = "H2H";
	
	private int aplicacionDebitoHost;
	
	public static final String SELECT_MAX_LOTE = "SELECT MAX(lote) + 1 FROM BS_LotePE WHERE Instalacion = ? ";
	
	public static final String INSERT_BS_LOTEPE = "INSERT INTO BS_LotePE(Instalacion, Lote, TipoConvenio, Convenio, EmpresaODepto, TipoLote, MontoTotal, NumOperaciones, NombreLote, FechaEnviado, FechaRecibido, FechaAplicacion, FechaEstatus, Estatus, NumeroReintento, UsuarioIngreso, Autorizaciones, AplicacionDebitoHost, Ponderacion, Firmas) " + 
			  "    VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, ?, 0, 0)";
	
	public LotePagoElectronicoLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}
	
	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Iniciando procesamiento de linea de encabezado para Convenio de Pago Electronico.  {}", businessData.getReferenceInfo());
		
		valuesFromBusinessData();
		if(line == null){
			logger.info("El archivo {} no posee linea de encabezado definido.  Se toman los valores por defecto");
			defaultValues();
		}else{
			logger.debug("Se toman los valores de la linea: {}", line);
			if(formatoLinea==null){
				logger.error("Error, no existe el formato de la linea de encabezado del archivo definido!. Se asumen valores por defecto.");
				defaultValues();
			}else{
				parseLine(line);
			}
		}
		
		logger.debug("Recuperando valores de la linea para proceder a insercion");
		if(line!=null){
			readValuesFromLine();
		}
		
		logger.debug("Ingresando valores en registro Bs_LotePE");
		insertValues();
		
		logger.debug("Finalizado de ingresar registro en Bs_LotePE");
	}
	

	
	private void valuesFromBusinessData(){
		EncabezadoData data = businessData.getEncabezadoData();
		instalacion = data.getInstalacion();
		lote = data.getLote();
		tipoConvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		convenio = businessData.getInt(Vars.PM_CONVENIO);
		empresaODepto = businessData.getInt(Vars.PM_EMPRESAODEPTO);
		tipoLote = data.getTipoLote();
		estatus = 70;
		
		fechaEnviado = new Timestamp(System.currentTimeMillis());
		fechaRecibido = new Timestamp(System.currentTimeMillis());
		fechaAplicacion = new Timestamp(System.currentTimeMillis());
		fechaEstatus = new Timestamp(System.currentTimeMillis());
	}
	
	private void defaultValues(){
		montoTotal = new BigDecimal(0);		
		numOperaciones = new Long(0); 	
		aplicacionDebitoHost = 2;	
		 
		/* 
		 * MODIFICACION A PETICION DE DAVIVIENDA -> 14-MAR-2016
		 * 1. El campo FechaAplicacion de la tabla BS_LotePE, Cuando no sea un lote programado el campo debe de quedar con el valor de NULL
		 * Segun definicion de documento, la unica manera de saber si un lote es programado o no 
		 * es si posee encabezado, ya que la fecha se encuentra indicada en el cabezado y segun la documentacion
		 * (DR H2H con BE+.doc) 
		 * */
		fechaAplicacion =  null; 		
		nombreLote = businessData.getEncabezadoData().getFileName();
	}
	
	
	public void readValuesFromLine() throws LineProcessException{
		List<CampoLinea> campos = formatoLinea.getCampos();
		logger.debug("Iniciando busqueda de campos correspondientes al encabezado de un Lote de PE");
		montoTotal = (BigDecimal) buscarValorColumna("MontoTotal", campos);
		numOperaciones = (Long) buscarValorColumna("NumOperaciones", campos);
		logger.debug("numOperacionesnumOperaciones1  : {} ", numOperaciones);
		String strAplicacionDebitoHost = String.valueOf(buscarValorColumna("AplicacionDebitoHost", campos));
		aplicacionDebitoHost = "D".equals(strAplicacionDebitoHost)?1:2;
		logger.debug("readValuesFromLine aplicacionDebitoHost  "+aplicacionDebitoHost); 
		logger.debug("readValuesFromLine campos: {} ", campos);
		java.util.Date udFechaAplicacion = (java.util.Date) buscarValorColumna("FechaAplicacion", campos);
		logger.debug("readValuesFromLine udFechaAplicacion: {} ", udFechaAplicacion);
		if(udFechaAplicacion!=null){
			Long horaAplicacion =  (Long) buscarValorColumna("HoraAplicacion", campos);
			logger.debug("readValuesFromLine horaAplicacion: {} ", horaAplicacion);
			String fecha = "";
			Calendar cal = Calendar.getInstance();
			if(horaAplicacion != null && !horaAplicacion.equals("")) {
				fecha = horaAplicacion.toString();
			}else {
				fecha = "";
			}
			if(fecha.length() == 4) {
				String hora = fecha.substring(0, 2);
				String minutos = fecha.substring(2, 4);
				System.out.println("hora:   "+hora);
				System.out.println("minutos:   "+minutos);
				if(horaAplicacion != null){
					cal.setTime(udFechaAplicacion);
					if(horaAplicacion > 0){
						cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora));
						cal.set(Calendar.MINUTE, Integer.parseInt(minutos));
					}
				}
			}else if(fecha.length() == 3) {
				String hora = fecha.substring(0, 1);
				String minutos = fecha.substring(1, 3);
				System.out.println("hora:   "+hora);
				System.out.println("minutos:   "+minutos);
				if(horaAplicacion != null){
					cal.setTime(udFechaAplicacion);
					if(horaAplicacion > 0){
						cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hora));
						cal.set(Calendar.MINUTE, Integer.parseInt(minutos));
					}
				}
			}
			
			logger.debug("readValuesFromLine cal.getTimeInMillis(): {} ", cal.getTimeInMillis());
			fechaAplicacion = new Timestamp(cal.getTimeInMillis());
			logger.debug("readValuesFromLine fechaAplicacion: {} ", fechaAplicacion);
		}else{
			logger.warn("La fecha de Aplicacion no ha sido definida en el encabezado.  Se toma la fecha de sistema por default.");
			fechaAplicacion = new Timestamp(System.currentTimeMillis());
			logger.debug("readValuesFromLine Else fechaAplicacion: {} ", fechaAplicacion);
		}
		
		
		nombreLote = String.valueOf(buscarValorColumna("NombreLote", campos));
		if(isEmptyString(nombreLote)){
			 /*Si el nombre de lote en la linea del archivo viene null utilizar nombre del archivo*/
			nombreLote = businessData.getEncabezadoData().getFileName();
		}
		
		logger.debug("Campos recuperados correctamente...");
	}
	
	public void insertValues() throws LineProcessException{
		
		QueryRunner runner = new QueryRunner();
		Connection conn = null;
		boolean autocommit = true;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);
			
			logger.debug("Obteniendo valor de siguiente secuencia de lote ...");
			lote = runner.query(conn, SELECT_MAX_LOTE, new ResultSetHandler<Integer>(){
				public Integer handle(ResultSet rs) throws SQLException {
					if(rs.next()){
						return rs.getInt(1);
					}
					 /*Si no trajimos nada puede que se trate del primer registro!*/
					return 1;
				}
			}, instalacion);
			logger.debug("Siguiente valor para num de Lote en tabla Bs_LotePE: {}", lote);
			
			businessData.getEncabezadoData().setLote(lote);
			businessData.getEncabezadoData().setAplicacionDebitoHost(aplicacionDebitoHost);
						
			 /*Preparo los parametros
			kpalacios | 24/08/2016 se restringe a 30 caracteres el nombreLote ya que es la longitud m�xima q permite la BD*/
			logger.debug("numOperacionesnumOperaciones2  : {} ", numOperaciones);
			Object[] parameters = {
					instalacion, lote, tipoConvenio, convenio, empresaODepto, tipoLote, montoTotal, numOperaciones,
					nombreLote.trim().length() < 30 ? nombreLote.trim() : nombreLote.trim().substring(0, 30),
					fechaEnviado,fechaRecibido,fechaAplicacion,fechaEstatus,estatus, numeroReintento,usuarioIngreso, aplicacionDebitoHost	
			};
			
			logger.debug("Realizando insert en tabla BS_LotePE ...");
			runner.update(conn, INSERT_BS_LOTEPE, parameters);
			logger.debug("Insert realizado.  Procediendo a realizar commit de la operacion...");
			conn.commit();
			logger.debug("Transaccion realizada correctamente...");
			conn.setAutoCommit(autocommit);
			
		}catch(SQLException e){
			if(conn!=null) try{ conn.rollback(); }catch(Exception ignored) {} 
			logger.error("Error al intentar realizar la insercion de registro en BS_LotePE. " + e.getMessage(), e);
			throw new LineProcessException(e);
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
}
