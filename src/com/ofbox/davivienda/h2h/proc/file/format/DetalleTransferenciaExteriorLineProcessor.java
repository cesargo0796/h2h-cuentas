package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.proc.Vars;

public class DetalleTransferenciaExteriorLineProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(DetalleTransferenciaExteriorLineProcessor.class);
	
	private int instalacion;
	private int operacion = 0;
	
	private Long transferencia;
	private Integer lote;
	private String nombreBeneficiario;
	private String direccionBeneficiario;
	private String emailBeneficiario;
	private Integer viaBeneficiario;
	private String codigoBancoBeneficiario;
	private String cuentaBeneficiario;
	private String nombreBancoBeneficiario;
	private String direccionBancoBeneficiario;
	private Integer viaIntermediario;
	private String codigoBancoIntermediario;
	private String nombreBancoIntermediario;
	private String cuentaIntermediario;
	private String cuentaOrdenante;
	private String nombreCuentaOrdenante;
	private String direccionOrdenante;
	private String descripcionTransferencia;
	private BigDecimal montoTransferencia;
	private Integer monedaMonto;
	private Date fechaAplicacion;
	private String codigoFormularioEgresoDivisas;
	private String infoFormularioEgresoDivisas;
	private Integer estado;
	private Date fechaEstado;
	private String autorizacionHost;
	private Date fechaCreacion;
	private Integer estatusImp;
	private String paisBanco;
	private BigDecimal montoImpuesto;
	private Integer esExenta;
	private Integer niu;
	private String codigoTransaccion;
	private Date fechaFormulario;
	private String comentario;
	private String nombrePersona;
	private String tipoDocumento;
	private String numeroDocumento;
	private String municipio;
	private String codigoConcepto;
	
	private int aplicacionDebitoHost;
	
	
	private static final String INSERT_BS_DETALLE_PE = "insert into bs_DetalleLoteTransExterior ("
			+ "Transferencia,Lote,NombreBeneficiario,DireccionBeneficiario," + 
			"EmailBeneficiario,ViaBeneficiario,CodigoBancoBeneficiario," + 
			"CuentaBeneficiario,NombreBancoBeneficiario,DireccionBancoBeneficiario," + 
			"ViaIntermediario,CodigoBancoIntermediario,NombreBancoIntermediario," + 
			"CuentaIntermediario,CuentaOrdenante,NombreCuentaOrdenante," + 
			"DireccionOrdenante,DescripcionTransferencia,MontoTransferencia," + 
			"MonedaMonto,FechaAplicacion,CodigoFormularioEgresoDivisas," + 
			"InfoFormularioEgresoDivisas,Estado,FechaEstado,AutorizacionHost," + 
			"FechaCreacion,EstatusImp,PaisBanco,MontoImpuesto,EsExenta," + 
			"niu,codigo_transaccion,fecha_formulario,comentario,nombre_persona," + 
			"tipo_documento,numero_documento,municipio,codigo_concepto,Telefono_ord) "
			+ "VALUES (" + 
			"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," + 
			"?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?" + 
			")";
	
	QueryRunner queryRunner = null;
	InvocadorJ2Entorno invocadorEntorno;
	
	public DetalleTransferenciaExteriorLineProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		
		super(businessData, formatoLinea);
		queryRunner = new QueryRunner();
		invocadorEntorno = new InvocadorJ2Entorno();
	}

	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Procesando linea de archivo : [{}]", line);
		clearValues();
		parseLine(line);
		logger.debug("validarCuentaDebitoDefinida cuentaBeneficiario1.2.2: "+cuentaBeneficiario);
		logger.debug("Iniciando lectura de campos de la linea segun definicion del formato de detalle en la configuracion del proceso. [{}]", businessData.getReferenceInfo());
		setupValues();
		logger.debug("validarCuentaDebitoDefinida cuentaBeneficiario1.2.3: "+cuentaBeneficiario);
		readValues();
		
		logger.debug("validarCuentaDebitoDefinida cuentaBeneficiario1.2: "+cuentaBeneficiario);
		if(cuentaBeneficiario != null) {						
		}else {
			cuentaBeneficiario = "000000000000";
		}	
		boolean convenioChequeA = 4 == businessData.getEncabezadoData().getTipoLote();
		logger.debug("convenioChequeAconvenioChequeA   "+convenioChequeA); 
		logger.debug("Ejecutando validaciones de la cuenta ...");
		logger.debug("linelinelinelinelineline  "+line); 
		logger.debug("validarCuentaDebitoDefinida()   "+validarCuentaDebitoDefinida() ); 
		logger.debug("validarCuentaMultiplesCuentas()  "+validarCuentaMultiplesCuentas()); 
		logger.debug("validarCuentasPolParticipante()  "+validarCuentasPolParticipante()); 
		logger.debug("validarCuentaServicioEntorno()  "+validarCuentaServicioEntorno()); 

		if( convenioChequeA && validarCuentaDebitoDefinida()   &&		/*Se valida que la cuenta este en la linea o bien, que se utilice la cuenta definida en el convenio */
				validarCuentaMultiplesCuentas() &&						/*Se valida el flag convenios multiples cuentas*/
				validarCuentasPolParticipante()  						/*Se valida la cuenta que viene en el archivo		*/			
			  ){
			logger.debug("Se cumplió la validación: convenioChequeA, validarCuentaDebitoDefinida(), validarCuentaMultiplesCuentas(), validarCuentasPolParticipante()");
			estado = 1;			
			logger.debug("Se aumenta el monto total del lote ya que el estado de las validaciones es = 1");
			businessData.getEncabezadoData().addMontoTotal(montoTransferencia);
			
		}else if( validarCuentaDebitoDefinida()   &&	/*Se valida que la cuenta este en la linea o bien, que se utilice la cuenta definida en el convenio */
			validarCuentaMultiplesCuentas() &&			/*Se valida el flag convenios multiples cuentas*/
			validarCuentasPolParticipante() && 			/*Se valida la cuenta que viene en el archivo*/
			validarCuentaServicioEntorno()				/*Validar contra servicio Entorno*/
		  ){
			logger.debug("Se cumplió las validaciones: validarCuentaDebitoDefinida(), validarCuentaMultiplesCuentas(), validarCuentasPolParticipante(), validarCuentaServicioEntorno()");
			estado = 1;			
			logger.debug("Se aumenta el monto total del lote ya que el estado de las validaciones es = 1");
			businessData.getEncabezadoData().addMontoTotal(montoTransferencia);
			
		}else{
			logger.debug("El registro no ha superado las validaciones de la cuenta. ");
		}

		
		try {
			guardarDetalle();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("Error al guardar registro de convenio en bs_DetalleLoteTransExterior.  " + businessData.getReferenceInfo() + " -> " + e.getMessage(), e);
			throw new LineProcessException("Error al guardar registro de Detalle del Lote. ", e);
		}
		
	}

	
	
	public void guardarDetalle() throws SQLException{
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			 /*Primero determinar si se trata de convenio de pago o de cobro para saber como se 
			 Guardaran los registros, siempre y cuando el estado = 1*/
			logger.debug("businessData.getEncabezadoData().getTipoLote()   "+businessData.getEncabezadoData().getTipoLote());
			logger.debug("estatus estatus estado   "+estado);
			/*Se determina que es un convenio de pago si tipoLote = 2*/
			boolean convenioPago = 2 == businessData.getEncabezadoData().getTipoLote();
			boolean convenioCheque = 4 == businessData.getEncabezadoData().getTipoLote();
			/*Se determina que es un convenio de cobro si tipoLote = 1*/
			boolean convenioCobro = 1 == businessData.getEncabezadoData().getTipoLote();
			
			int lineNumber = businessData.getArchivoConEncabezado().equals(Boolean.TRUE) ? businessData.getLineaEnArchivo() - 1 : businessData.getLineaEnArchivo();
			logger.debug("businessData.getArchivoConEncabezado(): "+businessData.getArchivoConEncabezado());
			logger.debug("lineNumber Inicial: "+lineNumber);
			
			int tipoOperacion = 0;
			int tipoOperacionCuentaOrigen = 0;
			logger.debug("convenioPago convenioPago   "+convenioPago);
			logger.debug("convenioCobro convenioCobro   "+convenioCobro);
			logger.debug("convenioCheque convenioCheque   "+convenioCheque);
			boolean flagA = 4 == businessData.getEncabezadoData().getTipoLote();
			if(convenioPago || convenioCheque){
				tipoOperacion = 2;
				tipoOperacionCuentaOrigen = 1;
			}
			
			if(convenioCobro){
				tipoOperacion = 1;
				tipoOperacionCuentaOrigen = 2;
			}
			
			if(estado!=1){
				operacion++;
				
				logger.debug("estatus != 1, por lo tanto se guardara un solo registro en bs_DetalleLoteTransExterior");
				logger.debug("operacion   "+operacion);
				
				logger.debug("guardarDetalle transferencia "+  transferencia);
				logger.debug("guardarDetalle lote "+  lote);
				logger.debug("guardarDetalle nombreBeneficiario "+  nombreBeneficiario);
				logger.debug("guardarDetalle direccionBeneficiario "+  direccionBeneficiario);
				logger.debug("guardarDetalle emailBeneficiario "+  emailBeneficiario);
				logger.debug("guardarDetalle viaBeneficiario "+  viaBeneficiario);
				logger.debug("guardarDetalle codigoBancoBeneficiario "+  codigoBancoBeneficiario);
				logger.debug("guardarDetalle cuentaBeneficiario "+  cuentaBeneficiario);
				logger.debug("guardarDetalle nombreBancoBeneficiario "+  nombreBancoBeneficiario);
				logger.debug("guardarDetalle direccionBancoBeneficiario "+  direccionBancoBeneficiario);
				logger.debug("guardarDetalle viaIntermediario "+  viaIntermediario);
				logger.debug("guardarDetalle codigoBancoIntermediario "+  codigoBancoIntermediario);
				logger.debug("guardarDetalle nombreBancoIntermediario "+  nombreBancoIntermediario);
				logger.debug("guardarDetalle cuentaIntermediario "+  cuentaIntermediario);
				logger.debug("guardarDetalle cuentaOrdenante "+  cuentaOrdenante);
				logger.debug("guardarDetalle nombreCuentaOrdenante "+  nombreCuentaOrdenante);
				logger.debug("guardarDetalle direccionOrdenante "+  direccionOrdenante);
				logger.debug("guardarDetalle descripcionTransferencia "+  descripcionTransferencia);
				logger.debug("guardarDetalle montoTransferencia "+  montoTransferencia);
				logger.debug("guardarDetalle monedaMonto "+  monedaMonto);
				logger.debug("guardarDetalle fechaAplicacion "+  fechaAplicacion);
				logger.debug("guardarDetalle codigoFormularioEgresoDivisas "+  codigoFormularioEgresoDivisas);
				logger.debug("guardarDetalle infoFormularioEgresoDivisas "+  infoFormularioEgresoDivisas);
				logger.debug("guardarDetalle estado "+  estado);
				logger.debug("guardarDetalle fechaEstado "+  fechaEstado);
				logger.debug("guardarDetalle autorizacionHost "+  autorizacionHost);
				logger.debug("guardarDetalle fechaCreacion "+  fechaCreacion);
				logger.debug("guardarDetalle estatusImp "+  estatusImp);
				logger.debug("guardarDetalle paisBanco "+  paisBanco);
				logger.debug("guardarDetalle montoImpuesto "+  montoImpuesto);
				logger.debug("guardarDetalle esExenta "+  esExenta);
				logger.debug("guardarDetalle niu "+  niu);
				logger.debug("guardarDetalle codigoTransaccion "+  codigoTransaccion);
				logger.debug("guardarDetalle fechaFormulario "+  fechaFormulario);
				logger.debug("guardarDetalle comentario "+  comentario);
				logger.debug("guardarDetalle nombrePersona "+  nombrePersona);
				logger.debug("guardarDetalle tipoDocumento "+  tipoDocumento);
				logger.debug("guardarDetalle numeroDocumento "+  numeroDocumento);
				logger.debug("guardarDetalle municipio "+  municipio);
				logger.debug("guardarDetalle codigoConcepto "+  codigoConcepto);
				
				if(nombreBeneficiario != null && !nombreBeneficiario.equals("") && !nombreBeneficiario.equals("null")) {					
					logger.debug("Entro en el nombreDeCuenta nombreDeCuenta nombreBeneficiario  "+nombreBeneficiario);
				}else if (nombreBeneficiario == null){
					logger.debug("Entro en el else nombreDeCuenta  "+nombreBeneficiario);
					nombreBeneficiario = "";			
				}
				logger.debug("parametros insert tipoOperacion1: "+tipoOperacion);
				montoImpuesto = BigDecimal.ZERO;
				Object[] params = new Object[]{
						transferencia,lote,nombreBeneficiario,direccionBeneficiario,
						emailBeneficiario,viaBeneficiario,codigoBancoBeneficiario,
						cuentaBeneficiario != null ? cuentaBeneficiario.trim() : null,nombreBancoBeneficiario,direccionBancoBeneficiario,
						viaIntermediario,codigoBancoIntermediario,nombreBancoIntermediario,
						cuentaIntermediario,cuentaOrdenante,nombreCuentaOrdenante,
						direccionOrdenante,descripcionTransferencia,montoTransferencia,
						monedaMonto,fechaAplicacion,codigoFormularioEgresoDivisas,
						infoFormularioEgresoDivisas,estado,fechaEstado,autorizacionHost,
						fechaCreacion,estatusImp,paisBanco,montoImpuesto,
						esExenta,niu,codigoTransaccion,fechaFormulario,
						comentario,nombrePersona,tipoDocumento,numeroDocumento,municipio,codigoConcepto
				};	
				logger.debug("parametros params.length1: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params[i]:  "+params[i]);
				}
				logger.debug("parametros insert INSERT_BS_DETALLE_PE: "+INSERT_BS_DETALLE_PE);
				queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
				logger.debug("Registro agregado en bs_DetalleLoteTransExterior");
				
				
				/******************* kpalacios - 19/08/2020 ***********************
				 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				return;
			}
			
			
			logger.debug("convenioCheque convenioCheque22   "+convenioCheque);
			if(convenioPago || convenioCheque){
				
				String codigoRef = "";
				
				/******************* kpalacios - 17/08/2016 ***********************
				 Desencadena validación de LIOF para el monto de la operación, esto cuando sea convenio pago Detallado. */
				System.out.println("aplicacionDebitoHost --> "+aplicacionDebitoHost);
				operacion++;
				logger.debug("Realizando insercion de operacion de Credito para un convenio de PAGO");
				logger.debug("operacion   "+operacion);
				logger.debug("guardarDetalle2 transferencia "+  transferencia);
				logger.debug("guardarDetalle2 lote "+  lote);
				logger.debug("guardarDetalle2 nombreBeneficiario "+  nombreBeneficiario);
				logger.debug("guardarDetalle2 direccionBeneficiario "+  direccionBeneficiario);
				logger.debug("guardarDetalle2 emailBeneficiario "+  emailBeneficiario);
				logger.debug("guardarDetalle2 viaBeneficiario "+  viaBeneficiario);
				logger.debug("guardarDetalle2 codigoBancoBeneficiario "+  codigoBancoBeneficiario);
				logger.debug("guardarDetalle2 cuentaBeneficiario "+  cuentaBeneficiario);
				logger.debug("guardarDetalle2 nombreBancoBeneficiario "+  nombreBancoBeneficiario);
				logger.debug("guardarDetalle2 direccionBancoBeneficiario "+  direccionBancoBeneficiario);
				logger.debug("guardarDetalle2 viaIntermediario "+  viaIntermediario);
				logger.debug("guardarDetalle2 codigoBancoIntermediario "+  codigoBancoIntermediario);
				logger.debug("guardarDetalle2 nombreBancoIntermediario "+  nombreBancoIntermediario);
				logger.debug("guardarDetalle2 cuentaIntermediario "+  cuentaIntermediario);
				logger.debug("guardarDetalle2 cuentaOrdenante "+  cuentaOrdenante);
				logger.debug("guardarDetalle2 nombreCuentaOrdenante "+  nombreCuentaOrdenante);
				logger.debug("guardarDetalle2 direccionOrdenante "+  direccionOrdenante);
				logger.debug("guardarDetalle2 descripcionTransferencia "+  descripcionTransferencia);
				logger.debug("guardarDetalle2 montoTransferencia "+  montoTransferencia);
				logger.debug("guardarDetalle2 monedaMonto "+  monedaMonto);
				logger.debug("guardarDetalle2 fechaAplicacion "+  fechaAplicacion);
				logger.debug("guardarDetalle2 codigoFormularioEgresoDivisas "+  codigoFormularioEgresoDivisas);
				logger.debug("guardarDetalle2 infoFormularioEgresoDivisas "+  infoFormularioEgresoDivisas);
				logger.debug("guardarDetalle2 estado "+  estado);
				logger.debug("guardarDetalle2 fechaEstado "+  fechaEstado);
				logger.debug("guardarDetalle2 autorizacionHost "+  autorizacionHost);
				logger.debug("guardarDetalle2 fechaCreacion "+  fechaCreacion);
				logger.debug("guardarDetalle2 estatusImp "+  estatusImp);
				logger.debug("guardarDetalle2 paisBanco "+  paisBanco);
				logger.debug("guardarDetalle2 montoImpuesto "+  montoImpuesto);
				logger.debug("guardarDetalle2 esExenta "+  esExenta);
				logger.debug("guardarDetalle2 niu "+  niu);
				logger.debug("guardarDetalle2 codigoTransaccion "+  codigoTransaccion);
				logger.debug("guardarDetalle2 fechaFormulario "+  fechaFormulario);
				logger.debug("guardarDetalle2 comentario "+  comentario);
				logger.debug("guardarDetalle2 nombrePersona "+  nombrePersona);
				logger.debug("guardarDetalle2 tipoDocumento "+  tipoDocumento);
				logger.debug("guardarDetalle2 numeroDocumento "+  numeroDocumento);
				logger.debug("guardarDetalle2 municipio "+  municipio);
				logger.debug("guardarDetalle2 codigoConcepto "+  codigoConcepto);
				if(direccionBeneficiario != null && !direccionBeneficiario.equals("") && !direccionBeneficiario.equals("null")) {
					logger.debug("Entro en el == direccionBeneficiario direccionBeneficiario  "+direccionBeneficiario);
				}else if (direccionBeneficiario.equals("null")){
					logger.debug("Entro en el direccionBeneficiario direccionBeneficiario  "+direccionBeneficiario);
					direccionBeneficiario = null;
					logger.debug("Entro en el else if direccionBeneficiario");
				}else if (direccionBeneficiario == null){
					logger.debug("Entro en el else direccionBenef  "+direccionBeneficiario);
					direccionBeneficiario = null;			
				}

				montoImpuesto = BigDecimal.ZERO;	
				Object[] params = new Object[]{
						transferencia,lote,nombreBeneficiario,direccionBeneficiario,
						emailBeneficiario,viaBeneficiario,codigoBancoBeneficiario,
						cuentaBeneficiario != null ? cuentaBeneficiario.trim() : null,nombreBancoBeneficiario,direccionBancoBeneficiario,
						viaIntermediario,codigoBancoIntermediario,nombreBancoIntermediario,
						cuentaIntermediario,cuentaOrdenante,nombreCuentaOrdenante,
						direccionOrdenante,descripcionTransferencia,montoTransferencia,
						monedaMonto,fechaAplicacion,codigoFormularioEgresoDivisas,
						infoFormularioEgresoDivisas,estado,fechaEstado,autorizacionHost,
						fechaCreacion,estatusImp,paisBanco,montoImpuesto,
						esExenta,niu,codigoTransaccion,fechaFormulario,
						comentario,nombrePersona,tipoDocumento,numeroDocumento,municipio,codigoConcepto
					};
				logger.debug("parametros params.length2: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params[i]:  "+params[i]);
					logger.debug("parametros params[i]:  "+params[i]);
				}
				queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
				logger.debug("Registro agregado en bs_DetalleLoteTransExterior para la operacion de Credito");
				
				
				
				/******************* kpalacios - 19/08/2016 ***********************
				 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				
				logger.debug("Finalizado el proceso de insercion");
				return;
			}
			
			
			if(convenioCobro){
				
				/******************* kpalacios - 17/08/2016 ***********************
				 Desencadena validación de LIOF para el monto de la operación, 
				 esto cuando sea convenio cobro sin importar si es Detallado o Consolidado. */
				if (validarCuentaDebitoDefinida()) {
				}
				
				operacion++;
				logger.debug("Realizando insercion de operacion de Debito para un convenio de COBRO");
				logger.debug("operacion   "+operacion);
				logger.debug("parametros insert cuentaBeneficiario5: "+cuentaBeneficiario);
				logger.debug("parametros insert nombreBeneficiario5: "+nombreBeneficiario);
				if(nombreBeneficiario != null && !nombreBeneficiario.equals("") && !nombreBeneficiario.equals("null")) {					
					logger.debug("Entro en el nombreDeCuenta nombreBeneficiario nombreBeneficiario  "+nombreBeneficiario);
				}else if (nombreBeneficiario == null){
					logger.debug("Entro en el else nombreDeCuenta  "+nombreBeneficiario);
					nombreBeneficiario = "";			
				}
				montoImpuesto = BigDecimal.ZERO;
				logger.debug("parametros insert tipoOperacion5: "+tipoOperacion);
				Object[]  params = new Object[]{
						transferencia,lote,nombreBeneficiario,direccionBeneficiario,
						emailBeneficiario,viaBeneficiario,codigoBancoBeneficiario,
						cuentaBeneficiario != null ? cuentaBeneficiario.trim() : null,nombreBancoBeneficiario,direccionBancoBeneficiario,
						viaIntermediario,codigoBancoIntermediario,nombreBancoIntermediario,
						cuentaIntermediario,cuentaOrdenante,nombreCuentaOrdenante,
						direccionOrdenante,descripcionTransferencia,montoTransferencia,
						monedaMonto,fechaAplicacion,codigoFormularioEgresoDivisas,
						infoFormularioEgresoDivisas,estado,fechaEstado,autorizacionHost,
						fechaCreacion,estatusImp,paisBanco,montoImpuesto,
						esExenta,niu,codigoTransaccion,fechaFormulario,
						comentario,nombrePersona,tipoDocumento,numeroDocumento,municipio,codigoConcepto
					};
				
				
				logger.debug("parametros params.length4: "+params.length);
				logger.debug("********************************************");
				for (int i = 0; i < params.length; i++) {
					System.out.println("parametros params["+i+"]:  "+params[i]);
				}
				logger.debug("parametros insert detlote4: "+codigoTransaccion);
				queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
				logger.debug("Registro agregado en bs_DetalleLoteTransExterior para la operacion de Debito");
				
				
				/******************* kpalacios - y19/08/2016 ***********************
				 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
				 de las operaciones antes procesadas. */
				if (lineNumber == operacion ) {
					guardarDetalleEncabezado(tipoOperacionCuentaOrigen, conn);
				}
				
				logger.debug("Finalizado el proceso de insercion");
				return;
			}
		}finally{
			DbUtils.closeQuietly(conn);
		}
	}
	
	/******************* kpalacios - 19/08/2020 ***********************
	 Validación para insertar solamente 1 vez CuentaDebito, donde el valor monto es el monto total
	 de las operaciones antes procesadas. */
	public void guardarDetalleEncabezado(int tipoOperacionCuentaOrigen, Connection conn) throws SQLException {
		logger.debug("Realizando insercion de operacion de monto total");					
		
		BigDecimal montoTotal = businessData.getEncabezadoData().getMontoTotal();
		logger.debug("MontoTotal [{}] "+montoTotal);
		nombreBeneficiario = businessData.getEncabezadoData().getNombreCuenta();
		estado = businessData.getEncabezadoData().getEstatus();
		if(tipoOperacionCuentaOrigen == 1) {
			transferencia = null;
			nombreBeneficiario = "";
		}
		Object[] params = new Object[]{
				transferencia,lote,nombreBeneficiario,direccionBeneficiario,
				emailBeneficiario,viaBeneficiario,codigoBancoBeneficiario,
				cuentaBeneficiario != null ? cuentaBeneficiario.trim() : null,nombreBancoBeneficiario,direccionBancoBeneficiario,
				viaIntermediario,codigoBancoIntermediario,nombreBancoIntermediario,
				cuentaIntermediario,cuentaOrdenante,nombreCuentaOrdenante,
				direccionOrdenante,descripcionTransferencia,montoTransferencia,
				monedaMonto,fechaAplicacion,codigoFormularioEgresoDivisas,
				infoFormularioEgresoDivisas,estado,fechaEstado,autorizacionHost,
				fechaCreacion,estatusImp,paisBanco,montoImpuesto,
				esExenta,niu,codigoTransaccion,fechaFormulario,
				comentario,nombrePersona,tipoDocumento,numeroDocumento,municipio,codigoConcepto
			};
		logger.debug("parametros params: "+params.length);
		for (int i = 0; i < params.length; i++) {
			System.out.println("parametros params[i]:  "+params[i]);
		}
		queryRunner.update(conn, INSERT_BS_DETALLE_PE, params);
		logger.debug("Registro agregado en bs_DetalleLoteTransExterior para la operacion de monto total");
	}
	
	public boolean validarCuentaDebitoDefinida(){
		logger.debug("Validando la cuenta de Debito provista en el archivo (si existe) o la cuenta definida segun convenio" );
		String cuentaSegunConvenio = businessData.getEncabezadoData().getCuentaDebitoCredito();
		logger.debug("parametros insert cuentaDebito7: "+cuentaBeneficiario);	
		if(isEmptyString(cuentaBeneficiario)){
			logger.debug("No se ha definido la cuenta de Debito/Credito en la linea.  Se toma la cuenta definida segun convenio: [{}]", cuentaSegunConvenio);
			cuentaBeneficiario = cuentaSegunConvenio;
		}
		/* Puede parecer doble validacion, pero segun la logica definida 
		 en realidad estoy validando si la cuenta segun convenio esta definida.*/
		logger.debug("parametros insert cuentaDebito8: "+cuentaBeneficiario);	
		if(isEmptyString(cuentaBeneficiario)){
			logger.debug("Cuenta segun convenio NO esta definida.  Por lo tanto quedara con estado = 97 (Cuenta de debito sin permiso)");
			estado = 97;
			return false;
		}
		
		return true;
	}
	
	public boolean validarCuentaMultiplesCuentas(){
		logger.debug("Validando si el convenio es de multiples cuentas");
		boolean esMultiplesCuentas = businessData.getEncabezadoData().getMultiplesCuentas();
		if(!esMultiplesCuentas){
			logger.debug("Convenio de Multiples cuentas es false.");
			/* No es convenio multiples cuentas, ya tuvo que haber sido validado
			 por el metodo validarCuentaDebitoDefinida()*/
			return true;
		}
		logger.debug("Convenio Multiples Cuentas es verdadero.  Por lo tanto se validara si la cuenta se encuentra definida en 'BS_Convenio' primero, y si no en el listado de 'Bs_ConvenioCuenta'");
		
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		Integer cliente = businessData.getInt(Vars.PM_CLIENTE);
		Connection conn = null;
		try{
			conn = businessData.getLocalizadorDeConexion().getConnection();
			/* AGREGADO 31-MARZO-2106
			 Busqueda de la cuentaDebito en BS_Convenio previo a buscar en BS_ConvenioCuenta
			 La Cuenta segun Bs_Convenio ya la tengo en la variable 'cuentaDebitoCredito' de 'EncabezadoData'*/
			logger.debug("parametros insert cuentaDebito9: "+cuentaBeneficiario);	
			logger.debug("Validando la cuenta {} se encuentre definida en tabla 'Bs_Convenio' para Convenio: {}, Cliente: {} ", new Object[]{cuentaBeneficiario, convenio, cliente});
			String ctaConvenio = (String) queryRunner.query(conn, "select Cuenta from BS_Convenio where Convenio = ? and Cliente = ? ", 
					new ScalarHandler("Cuenta"), new Object[]{convenio, cliente});
			logger.debug("Cuenta default definida en Bs_Convenio para Convenio: [{}], Cliente: [{}] se encontro Cuenta: [{}]", new Object[]{convenio, cliente, ctaConvenio});
			logger.debug("parametros insert cuentaDebito10: "+cuentaBeneficiario);	
			if(ctaConvenio.trim().equals(cuentaBeneficiario.trim())){
				logger.debug("La cuenta {} se encuentra definida en 'BS_Convenio' para el convenio {} ", cuentaBeneficiario, convenio);
				return true;
			}
			logger.debug("parametros insert cuentaDebito11: "+cuentaBeneficiario);	
			logger.debug("La cuenta {} no se encuentra en 'BS_Convenio', por lo que se buscara en 'BS_ConvenioCuenta' ", cuentaBeneficiario);
			logger.debug("parametros insert cuentaDebito12: "+cuentaBeneficiario);	
			Integer cant = (Integer)queryRunner.query(conn,"select count(Cuenta) from BS_ConvenioCuenta where TipoConvenio = ? and Convenio = ? and Cuenta = ?", new ScalarHandler(), tipoconvenio, convenio, cuentaBeneficiario);
			if(cant <= 0){
				logger.debug("NO se encontro la cuenta definida en 'BS_ConvenioCuenta' para convenio de multiples cuentas. [{}]", businessData.getReferenceInfo());
				estado = 97;
				return false;
			}
			logger.debug("La cuenta [{}] se encuentra definida en el listado de Bs_ConvenioCuenta", cuentaBeneficiario);
			return true;
		}catch(SQLException e){
			e.printStackTrace();
			logger.error("Ocurrio un error al hacer la consulta de la cuenta [{}] en el registro 'Bs_ConvenioCuenta'. " + e.getMessage(), e);
			estado = 7;
		}finally{
			DbUtils.closeQuietly(conn);
		}
		return false;
	}
	
	public boolean validarCuentasPolParticipante(){
		logger.debug("Iniciando validacion de las cuentas especificadas en el archivo segun convenio abierto/cerrado");
		int polParticipante = businessData.getEncabezadoData().getPolParticipante();
		Integer convenio = businessData.getInt(Vars.PM_CONVENIO);
		Integer tipoconvenio = businessData.getInt(Vars.PM_TIPOCONVENIO);
		Connection conn = null;
		logger.debug("polParticipante: [{}]", polParticipante);
		if(polParticipante == 1){
			try{
				conn = businessData.getLocalizadorDeConexion().getConnection();
				logger.debug("El convenio es cerrado por lo tanto se validara la cuenta [{}] que existe en 'BS_Participante' ", cuentaBeneficiario);
				/*kpalacios | 22/08/2016, Se aplico CAST al campo cuenta, ya que el valor de la variable que se el esta enviando no contiene los ceros a la izquierda.*/ 
				Map<String, Object> map = queryRunner.query(conn, "select MontoAutorizado, Estatus from Bs_Participante where TipoConvenio = ? and Convenio = ? and CAST(Cuenta AS NUMERIC) = ?", new MapHandler(), tipoconvenio, convenio, cuentaBeneficiario);
				if(map == null){
					logger.warn("La cuenta [{}] no se encuentra registrada en la tabla Bs_Participante. [{}]", cuentaBeneficiario, businessData.getReferenceInfo());
					logger.debug("Se registra el estado = 4 (Empleado o Proveedor no registrado)");
					estado = 4;
					return false;
				}
				
				logger.debug("Se encontraron resultados de Bs_Participante.  Procediendo a validar monto autorizado y estatus");
				Integer polParticipanteEstatus = (Integer) map.get("Estatus");
				Number polParticipanteMontoAutorizado = (Number) map.get("MontoAutorizado");
				
				if(polParticipanteEstatus!=1){
					logger.warn("La cuenta [{}] participante no se encuentra habilitada. Se coloca el registro con estado = 2 (Participante deshabilitado)", cuentaBeneficiario);
					estado = 2;
					return false;
				}
				
				BigDecimal montoAutorizado = new BigDecimal(polParticipanteMontoAutorizado.doubleValue());
				if(montoAutorizado.compareTo(montoImpuesto)<0){
					logger.warn("La cuenta [{}] participante no cuenta con un monto autorizado ({}) superior al monto indicado en el archivo: ({}). Se coloca estado = 3 (Monto no Autorizado)", new Object[]{cuentaBeneficiario, montoAutorizado, montoImpuesto});
					estado = 3;
					return false;
				}
				
				logger.debug("Monto y Estatus autorizados.  La cuenta puede continuar con el proceso de validaciones");
				return true;
				
			}catch(SQLException e){
				e.printStackTrace();
				logger.error("Error al validar cuenta [{}] en Bs_Participante", cuentaBeneficiario, e);
				estado = 7;
				return false;
			}finally{
				DbUtils.closeQuietly(conn);
			}
		}
		return true;
	}
	
	public boolean validarCuentaServicioEntorno(){
		logger.debug("Validando cuenta [{}] contra servicio de Entorno ...", cuentaBeneficiario);
		Respuesta resp = null;
		resp = new Respuesta();
			Respuesta respNombreCuenta = invocadorEntorno.invocarInformacionNombreCuenta(cuentaBeneficiario);
			
			if(respNombreCuenta == null || respNombreCuenta.getCodigo() != 0){
				logger.warn("Se encontro un error en la validacion de la cuenta desde PagoElectronico contra el servicio fabricaCoreBanca/srvInfoNombreCuenta. ");
				estado = 7;
				return false;
			}
			respNombreCuenta.primerContenedor();
			if(!respNombreCuenta.siguiente()){
				logger.warn("No se encontraron resultados para la consulta de la cuenta desde PagoElectronico [{}] contra servicio fabricaCoreBanca/srvInfoNombreCuenta", cuentaBeneficiario);
				estado = 7;
				return false;
			}
			
			nombreBeneficiario = respNombreCuenta.obtenerString("nombreCuenta");
			logger.info("nombreDeCuenta "+nombreBeneficiario);
			if(isEmptyString(nombreBeneficiario)){
				logger.warn("No se ha definido el nombre completo para la cuenta [{}] , desde PagoELectronico segun el servicio fabricaCoreBanca/srvInfoNombreCuenta ", cuentaBeneficiario);
				estado = 7;
				return false;
			}
			logger.debug("Se ha recuperado el valor de nombre completo desde PagoElectronico [{}] del servicio fabricaCoreBanca/srvInfoNombreCuenta", nombreBeneficiario);
	
		return true;
	}
	
	public void readValues()
			throws LineProcessException {		
		List<CampoLinea> campos = formatoLinea.getCampos();
				
		transferencia = (Long) buscarValorColumna("Transferencia", campos);
		lote = (Integer) buscarValorColumna("Lote", campos);
		nombreBeneficiario = (String) buscarValorColumna("NombreBeneficiario", campos);
		direccionBeneficiario = (String) buscarValorColumna("DireccionBeneficiario", campos);
		emailBeneficiario = (String) buscarValorColumna("EmailBeneficiario", campos);
		viaBeneficiario = (Integer) buscarValorColumna("ViaBeneficiario", campos);
		codigoBancoBeneficiario = (String) buscarValorColumna("CodigoBancoBeneficiario", campos);
		cuentaBeneficiario = (String) buscarValorColumna("CuentaBeneficiario", campos);
		nombreBancoBeneficiario = (String) buscarValorColumna("NombreBancoBeneficiario", campos);
		direccionBancoBeneficiario = (String) buscarValorColumna("DireccionBancoBeneficiario", campos);
		viaIntermediario = (Integer) buscarValorColumna("ViaIntermediario", campos);
		codigoBancoIntermediario = (String) buscarValorColumna("CodigoBancoIntermediario", campos);
		nombreBancoIntermediario = (String) buscarValorColumna("NombreBancoIntermediario", campos);
		cuentaIntermediario = (String) buscarValorColumna("CuentaIntermediario", campos);
		cuentaOrdenante = (String) buscarValorColumna("CuentaOrdenante", campos);
		nombreCuentaOrdenante = (String) buscarValorColumna("NombreCuentaOrdenante", campos);
		direccionOrdenante = (String) buscarValorColumna("DireccionOrdenante", campos);
		descripcionTransferencia = (String) buscarValorColumna("DescripcionTransferencia", campos);
		montoTransferencia = (BigDecimal) buscarValorColumna("MontoTransferencia", campos);
		monedaMonto = (Integer) buscarValorColumna("MonedaMonto", campos);
		fechaAplicacion = (Date) buscarValorColumna("FechaAplicacion", campos);
		codigoFormularioEgresoDivisas = (String) buscarValorColumna("CodigoFormularioEgresoDivisas", campos);
		infoFormularioEgresoDivisas = (String) buscarValorColumna("InfoFormularioEgresoDivisas", campos);
		estado = (Integer) buscarValorColumna("Estado", campos);
		fechaEstado = (Date) buscarValorColumna("FechaEstado", campos);
		autorizacionHost = (String) buscarValorColumna("AutorizacionHost", campos);
		fechaCreacion = (Date) buscarValorColumna("FechaCreacion", campos);
		estatusImp = (Integer) buscarValorColumna("EstatusImp", campos);
		paisBanco = (String) buscarValorColumna("PaisBanco", campos);
		montoImpuesto = (BigDecimal) buscarValorColumna("MontoImpuesto", campos);
		esExenta = (Integer) buscarValorColumna("EsExenta", campos);
		niu = (Integer) buscarValorColumna("niu", campos);
		codigoTransaccion = (String) buscarValorColumna("codigo_transaccion", campos);
		fechaFormulario = (Date) buscarValorColumna("fecha_formulario", campos);
		comentario = (String) buscarValorColumna("comentario", campos);
		nombrePersona = (String) buscarValorColumna("nombre_persona", campos);
		tipoDocumento = (String) buscarValorColumna("tipo_documento", campos);
		numeroDocumento = (String) buscarValorColumna("numero_documento", campos);
		municipio = (String) buscarValorColumna("municipio", campos);
		codigoConcepto = (String) buscarValorColumna("codigo_concepto", campos);
		
		logger.debug("Entro en el email " + emailBeneficiario + " transferencia " + transferencia);
		logger.debug("Entro en el nombreBenef "+nombreBeneficiario + " cuenta  "+cuentaBeneficiario);
		if(cuentaBeneficiario != null && !cuentaBeneficiario.equals("") && !cuentaBeneficiario.equals("null")) {
			logger.debug("Entro en el == cuenta cuenta  "+cuentaBeneficiario);
		}else if (cuentaBeneficiario.equals("null")){
			logger.debug("Entro en el cuenta cuenta  "+cuentaBeneficiario);
			cuentaBeneficiario = null;			
		}else if (cuentaBeneficiario == null){
			logger.debug("Entro en el else cuenta  "+cuentaBeneficiario);
			cuentaBeneficiario = null;			
		}
		if(emailBeneficiario != null && !emailBeneficiario.equals("") && !emailBeneficiario.equals("null")) {
			logger.debug("Entro en el == email email  "+emailBeneficiario);
		}else if (emailBeneficiario.equals("null")){
			logger.debug("Entro en el email email  "+emailBeneficiario);
			emailBeneficiario = null;
		}else if (emailBeneficiario == null){
			logger.debug("Entro en el else email  "+emailBeneficiario);
			emailBeneficiario = null;			
		}
		
		if(nombreBeneficiario != null && !nombreBeneficiario.equals("") && !nombreBeneficiario.equals("null")) {
			logger.debug("Entro en el == nombreBenef nombreBenef  "+nombreBeneficiario);
		}else if (nombreBeneficiario.equals("null")){
			logger.debug("Entro en el nombreBenef nombreBenef  "+nombreBeneficiario);
			nombreBeneficiario = null;
		}else if (nombreBeneficiario == null){
			logger.debug("Entro en el else nombreBenef  "+nombreBeneficiario);
		}
		
		
		logger.debug("codigoTransaccion["+codigoTransaccion+"]");		
	}
	
	public void setupValues(){
		EncabezadoData data = businessData.getEncabezadoData();
		instalacion = data.getInstalacion();
		lote = data.getLote();
		aplicacionDebitoHost = data.getAplicacionDebitoHost();
		fechaCreacion = new Timestamp(System.currentTimeMillis());
	}
	
	public void clearValues(){
		transferencia = 0L;
		lote = 0;
		nombreBeneficiario = null;
		direccionBeneficiario = null;
		emailBeneficiario = null;
		viaBeneficiario = 0;
		codigoBancoBeneficiario = null;
		cuentaBeneficiario = null;
		nombreBancoBeneficiario = null;
		direccionBancoBeneficiario = null;
		viaIntermediario = 0;
		codigoBancoIntermediario = null;
		nombreBancoIntermediario = null;
		cuentaIntermediario = null;
		cuentaOrdenante = null;
		nombreCuentaOrdenante = null;
		direccionOrdenante = null;
		descripcionTransferencia = null;
		montoTransferencia = new BigDecimal(0);
		monedaMonto = 0;
		fechaAplicacion = new Date();
		codigoFormularioEgresoDivisas = null;
		infoFormularioEgresoDivisas = null;
		estado = 0;
		fechaEstado = new Date();
		autorizacionHost = null;
		fechaCreacion = new Date();
		estatusImp = 0;
		paisBanco = null;
		montoImpuesto = new BigDecimal(0);
		esExenta = 0;
		niu = 0;
		codigoTransaccion = null;
		fechaFormulario = new Date();
		comentario = null;
		nombrePersona = null;
		tipoDocumento = null;
		numeroDocumento = null;
		municipio = null;
		codigoConcepto = null;
	}

}