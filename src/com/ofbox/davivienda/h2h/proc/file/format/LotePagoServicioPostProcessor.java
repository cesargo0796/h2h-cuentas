package com.ofbox.davivienda.h2h.proc.file.format;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ofbox.davivienda.h2h.proc.Vars;

public class LotePagoServicioPostProcessor extends Processor{

	Logger logger = LoggerFactory.getLogger(LotePagoServicioPostProcessor.class);
	
	public LotePagoServicioPostProcessor(BusinessData businessData,
			FormatoLinea formatoLinea) {
		super(businessData, formatoLinea);
	}

	@Override
	public void process(String line) throws LineProcessException {
		logger.info("Actualizando los valores de Bs_LotePS ");
		
		int estatusLote = 2;
		BigDecimal montoTotalLote = businessData.getEncabezadoData().getMontoTotal();
		BigDecimal montoMaximo = businessData.getAsBigDecimal(Vars.PM_MONTO_MAXIMO_PROCESAR);
		Integer instalacion = businessData.getEncabezadoData().getInstalacion();
		Integer lote = businessData.getEncabezadoData().getLote(); 
		Integer numOperaciones = businessData.getEncabezadoData().getCantidadOperaciones();
		Timestamp fechaEstatus = new Timestamp(System.currentTimeMillis());
		Integer cliente = businessData.getInt(Vars.PM_CLIENTE);
		logger.debug("numOperacionesnumOperaciones3  : {} ", numOperaciones);
		String cuentaDebito = "";
		BigDecimal montoImpuesto = null;
		
		System.out.println("LineasProcesadas... "+businessData.getHandler().getLineasProcesadas());
		if(businessData.getHandler().getLineasProcesadas() == 0){
			logger.debug("Archivo NO finalizo su proceso, Formato invalido de archivo.");
			estatusLote = 28;
		}else{
			 /*Si posee encabezado, entonces hay una fecha y hora programados*/
			if(businessData.isPoseeEncabezado()){
				logger.debug("El lote posee encabezado. Por lo tanto se asume que hay una fecha y hora programadas.  El estado del lote quedara con valor '4' (En Espera)");
				estatusLote = 4;
			}
			if("N".equalsIgnoreCase(businessData.getString(Vars.PM_PROCESAMIENTO_SUPERUSUARIO))){
				System.out.println("cuentaDebito.trim() if6 "+cuentaDebito.trim());
				logger.debug("La configuracion indica procesamiento de super usuario con 'No'.  El estado de lote quedara en valor 51 (Pendiente de autorizacion)");
				estatusLote = 51; 
			}
			System.out.println("businessData.getEncabezadoData().getAplicacionDebitoHost()...... "+businessData.getEncabezadoData().getAplicacionDebitoHost());
			if (businessData.getEncabezadoData().getAplicacionDebitoHost() == 2) {
				if (validarCuentaDebitoDefinida(cuentaDebito.trim(), estatusLote)) {
				}
			}
						
			
			if(montoMaximo!=null && montoMaximo.compareTo(BigDecimal.ZERO) > 0){
				if(montoTotalLote.compareTo(montoMaximo) > 0){
					estatusLote = 27;  
					businessData.setResultadoMensaje("ERROR,El monto total del lote indicado en el archivo supera el monto total configurado para el convenio");
				}
			}
		}
		
		Connection conn = null;
		QueryRunner runner = new QueryRunner();
		boolean autocommit = true;
		try {
			conn = businessData.getLocalizadorDeConexion().getConnection();
			autocommit = conn.getAutoCommit();
			conn.setAutoCommit(false);
			runner.update(conn,"update Bs_LotePS set MontoTotal = ?, NumOperaciones = ?, Estatus = ?, FechaEstatus = ?, MontoImpuesto = ? where Instalacion = ? and Lote = ? ",
					new Object[] { montoTotalLote, numOperaciones, estatusLote, fechaEstatus, montoImpuesto, instalacion, lote});
			logger.debug("Actualizados los valores del registro de Lote del archivo");
		
			logger.debug("Actualizando valor de siguiente instalacion de Bs_Instalacion");
			Integer siguienteLote = lote + 1;
			runner.update(conn, "update Bs_Instalacion set SiguienteLotePE = ? where Instalacion = ? and Cliente = ?",
					new Object[] { siguienteLote, instalacion, cliente } );
			conn.commit();
			logger.debug("Se han actualizado el valor de la siguiente instalacion hacia el valor: {}", siguienteLote);
			
		} catch (SQLException e) {
			if(conn!=null){
				try{
					conn.rollback();
				}catch(Exception ignored){}
			}
			e.printStackTrace();
			logger.error("Error al realizar actualizacion del Lote. " + businessData.getReferenceInfo() + " -> " + e.getMessage(),e);
			throw new LineProcessException("Error al actualizar los valores totales del Lote despues de procesar las lineas. " + e.getMessage(), e);
		}finally{
			if(conn!=null){
				try{ conn.setAutoCommit(autocommit); }catch(SQLException e){ }
			}
			DbUtils.closeQuietly(conn);
		}
		
	}
	
	/**
	 * Valida cuenta debito a utilizar para el c�lculo del LIOF 
	 * 
	 * @author kpalacios | 18/08/2016
	 * @return
	 */
	public boolean validarCuentaDebitoDefinida(String cuentaDebito, int estatusLote){
		logger.debug("Validando la cuenta de Debito provista en el archivo (si existe) o la cuenta definida segun convenio" );
		logger.debug("validarCuentaDebitoDefinida cuentaDebito  "+cuentaDebito);
		logger.debug("validarCuentaDebitoDefinida estatusLote  "+estatusLote);
		String cuentaSegunConvenio = businessData.getEncabezadoData().getCuentaDebitoCredito();
		if(isEmptyString(cuentaDebito)){
			logger.debug("No se ha definido la cuenta de Debito/Credito en la linea.  Se toma la cuenta definida segun convenio: [{}]", cuentaSegunConvenio.trim());
			cuentaDebito = cuentaSegunConvenio.trim();
		}
		 /*Puede parecer doble validacion, pero seg�n la logica definida 
		 en realidad estoy validando si la cuenta segun convenio esta definida*/
		if(isEmptyString(cuentaDebito)){
			logger.debug("Cuenta seg�n convenio NO esta definida.  Por lo tanto quedara con estatus = 106 (Error validando cuentas)");
			estatusLote = 106;
			return false;
		}
		
		return true;
	}
}
