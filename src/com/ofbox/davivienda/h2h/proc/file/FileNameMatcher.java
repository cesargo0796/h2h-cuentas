package com.ofbox.davivienda.h2h.proc.file;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileNameMatcher {

	Logger logger = LoggerFactory.getLogger(FileNameMatcher.class); 
	
	/**
	 * Formato del nombre del archivo, segun definicion
	 * del usuario.  Este formato podra contener una parte
	 * dinamica denotada por una fecha con el siguiente
	 * formato: <br />
	 * <p>
	 * <code>
	 * 	 FILE_<strong>$D('yyyyMMdd')</strong>_<strong>$P('PARAM')</strong>_OTHER.csv 
	 * </code>
	 * </p>
	 * El cual permite formar el nombre del archivo y hacer
	 * un match utilizando la fecha actual del sistema
	 */
	private String fileNameFormat;
	
	/**
	 * Mapa de parametros del proceso obtenidos en dinamico, desde
	 * donde se sustituiran los valores del nombre del archivo
	 */
	private Map<String, Object> params;
	
	/**
	 * Nombre del archivo ya resuelto
	 */
	protected String resolvedFileName;

	public FileNameMatcher(String fileNameFormat, Map<String, Object> params) {
		super();
		this.fileNameFormat = fileNameFormat;
		this.params = params;
	}
	
	
	public  boolean matches(String fileName){
		if(resolvedFileName==null){
			resolveFilename();
		}
		return resolvedFileName.equals(fileName);
	}
	
	public String getResolvedFilename(){
		if(resolvedFileName==null){
			resolveFilename();
		}
		return resolvedFileName;
	}
	
	public void resolveFilename(){
		logger.debug("Resolviendo nombre de archivo fileNameFormat : {}", fileNameFormat);
		logger.debug("Resolviendo nombre de archivo params : {}", params);
		char[] cArr = fileNameFormat.toCharArray();
		int i = 0;
		LinkedList<Object> tokens = new LinkedList<Object>();
		logger.debug("Procesando fileNameFormat: "+fileNameFormat.toString());
		logger.debug("Procesando cArr.length: "+cArr.length);
		while(i<cArr.length){
			char c = cArr[i];
			logger.debug("Procesando char -> '" + c + "'");
			if(c == '$'){
				StringBuilder sb = new StringBuilder();
				do{
					i++;
					sb.append(c);
					if(i+1<cArr.length) c = cArr[i];
				}while(c != ')' && i+1 < cArr.length);
				sb.append(c);
				i++;
				
				String strExpr = sb.toString();
				logger.debug("Agregando expresion -> {}", strExpr);
				
				Expression expr = new Expression(strExpr, params);
				logger.debug("Procesando exprexpr: "+expr);
				logger.debug("Procesando exprexpr: "+expr.toString());
				tokens.add(expr);
			}else{
				StringBuilder sb = new StringBuilder();
				do{
					i++;
					sb.append(c);
					if(i+1<cArr.length) c = cArr[i];
				}while(c != '$' && i+1 < cArr.length);

				String strText = sb.toString();
				logger.debug("Agregando texto -> {}", strText);
				tokens.add(strText);
			}
		}
		logger.debug("resolveFilename tokens.size() -> {}", tokens.size());
		logger.debug("resolveFilename tokens.toString() -> {}", tokens.toString());
		StringBuilder resolved = new StringBuilder();
		for(Object o : tokens){
			if(o instanceof Expression){
				logger.debug("Evaluando parametro Object: " + o.toString());
				resolved.append(((Expression)o).evaluate());
				logger.debug("Evaluando parametro resolved: " + resolved.toString());
			}else{
				resolved.append(o);
			}
		}

		resolvedFileName =  resolved.toString();
		logger.debug("Resuelto nombre: " + resolvedFileName);
	}
	
}
