package com.ofbox.davivienda.h2h.proc;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;

public class ProcessInformation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Tiempo de monitoreo en segundos
	 */
	private int tiempoMonitoreo;
	
	private int idProcesoMonitoreo;
	
	private int cliente;
	
	private String nombreCliente;
	
	private int convenio;
	
	private int tipoConvenio;
	
	private String nombreConvenio;

	private Map<String, Object> dataMap;

	public int getTiempoMonitoreo() {
		return tiempoMonitoreo;
	}

	public void setTiempoMonitoreo(int tiempoMonitoreo) {
		this.tiempoMonitoreo = tiempoMonitoreo;
	}

	public int getIdProcesoMonitoreo() {
		return idProcesoMonitoreo;
	}

	public void setIdProcesoMonitoreo(int idProcesoMonitoreo) {
		this.idProcesoMonitoreo = idProcesoMonitoreo;
	}

	public int getCliente() {
		return cliente;
	}

	public void setCliente(int cliente) {
		this.cliente = cliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public int getConvenio() {
		return convenio;
	}

	public void setConvenio(int convenio) {
		this.convenio = convenio;
	}

	public int getTipoConvenio() {
		return tipoConvenio;
	}

	public void setTipoConvenio(int tipoConvenio) {
		this.tipoConvenio = tipoConvenio;
	}

	public String getNombreConvenio() {
		return nombreConvenio;
	}

	public void setNombreConvenio(String nombreConvenio) {
		this.nombreConvenio = nombreConvenio;
	}

	public Map<String, Object> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, Object> dataMap) {
		this.dataMap = dataMap;
	}

	
	
	
}
