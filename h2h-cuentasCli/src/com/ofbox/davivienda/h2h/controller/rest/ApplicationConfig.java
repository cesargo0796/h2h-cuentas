package com.ofbox.davivienda.h2h.controller.rest;

import java.util.Set;

@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

   @Override
   public Set<Class<?>> getClasses() {
      Set<Class<?>> resources = new java.util.HashSet<>();
       resources.add(com.ofbox.davivienda.h2h.controller.rest.CuentasController.class);
      return resources;
   }
