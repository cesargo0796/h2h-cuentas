package com.ofbox.davivienda.h2h.controller.rest;



import com.hsbc.sv.desarrollo.contenedores.Respuesta;
import com.ofbox.davivienda.h2h.business.InvocadorJ2Entorno;
import com.ofbox.davivienda.h2h.constants.Constants;
import com.ofbox.davivienda.h2h.dto.InfoValidacionCuentaRequest;
import com.ofbox.davivienda.h2h.dto.InfoValidacionCuentaResponse;
import com.ofbox.davivienda.h2h.util.Utilities;


@Path("Cuentas") 
@Produces(MediaType.APPLICATION_JSON) 
@Consumes(MediaType.APPLICATION_JSON) 
public class CuentasController {
	
    @Path("/ValidarCuenta")
	public InfoValidacionCuentaResponse crearCodigoBanco(InfoValidacionCuentaRequest infoValidacionCuentaRequest) {		
		InfoValidacionCuentaResponse response = new InfoValidacionCuentaResponse();
		InvocadorJ2Entorno invocadorJ2Entorno = new InvocadorJ2Entorno();
		if(infoValidacionCuentaRequest == null)
		{	
			response.setCodigo(5);
			response.setDescripcion("Datos incompletos");
			return response;
		}
		
		if(Utilities.validarCampoVacio(infoValidacionCuentaRequest.getNumeroCuenta())) {
			response.setCodigo(6);
			response.setDescripcion("Numero de cuenta es requerido");
			return response;		
		}
		
		if(!Utilities.validarCampoVacio(infoValidacionCuentaRequest.getNumeroDocumento())) {
			if(Utilities.validarCampoVacio(infoValidacionCuentaRequest.getTipoDocumento())) {
				response.setCodigo(5);
				response.setDescripcion("Datos incompletos - tipo de documento no ingresado");
				return response;	
			}	
			if(!Constants.DOCUMENT_TYPES.contains(infoValidacionCuentaRequest.getTipoDocumento())) {
				response.setCodigo(5);
				response.setDescripcion("Datos incorrectos - tipo de documento incorrecto");
				return response;	
				
			}
		}
		
		//viene numCuenta
		Respuesta respuestaCuenta = invocadorJ2Entorno.invocarInformacionCuentaDeposito
		(infoValidacionCuentaRequest.getNumeroCuenta());
		

		if(respuestaCuenta == null) {
			response.setCodigo(500);
			response.setDescripcion("No ha sido posible obtener informacion de la cuenta de deposito");
			return response;
		}
		
		response.setCodigo(respuestaCuenta.getCodigo());
		response.setDescripcion(respuestaCuenta.getDescripcion());
		if(respuestaCuenta.getCodigo() != 0) {
			return response;
		}
		
		if(!Utilities.validarCampoVacio(infoValidacionCuentaRequest.getNumeroDocumento())) {			
			//viene numDocumento
			Respuesta respuestaNiu = invocadorJ2Entorno.invocarInformacionNiu		
					(infoValidacionCuentaRequest.getNumeroDocumento(), infoValidacionCuentaRequest.getTipoDocumento());
			
			if(respuestaNiu == null) {
				response.setCodigo(500);
				response.setDescripcion("No ha sido posible obtener informacion del Niu");
				return response;
			}

			response.setCodigo(respuestaNiu.getCodigo());
			response.setDescripcion(respuestaNiu.getDescripcion());
			
			if(respuestaNiu.getCodigo() == 0) {
				Respuesta respuestaCliente = invocadorJ2Entorno.invocarInformacionNiu		
						(infoValidacionCuentaRequest.getNumeroDocumento(), infoValidacionCuentaRequest.getTipoDocumento());		
				
				if(respuestaCliente == null) {
					response.setCodigo(500);
					response.setDescripcion("No ha sido posible obtener informacion del Niu por cliente");
					return response;
				}
			}
			else {
				return response;			
			}
		}
		response.setCodigo(0);
		response.setDescripcion("Cuenta valida");		
		return response;

		/*
		 * Campo cuenta debe ser obligatorio
		 * Si viene numero de documento, que venga el tipo de documento y visceversa
		 * Que el tipo de documento sea segun el catalogo DUI , NIT, PASAPORTE, CARNET
		 * Si solo viene la cuenta, que solo invoque el servicio invocarInformacionCuentaDeposito
		 * Si viene documento con numero y tipo, se invocara el servicio obtenerNiu, si la respuesta es exitosa, se invocara el servicio invocarInformacionCliente
		 * 			
		 */
	}

}
