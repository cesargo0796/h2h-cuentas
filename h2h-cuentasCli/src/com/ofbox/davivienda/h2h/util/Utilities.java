package com.ofbox.davivienda.h2h.util;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utilities {

	public static boolean validarCampoVacio(String campo) {
		return campo == null || campo.equals("");
	}

	public static boolean validarListaVacia(List<?> lista) {
		return lista == null || lista.size() ==0;
	}
	
	public static String convertToJson(Object pojo) throws JsonProcessingException {
	    ObjectMapper mapper = new ObjectMapper();
	    return mapper.writeValueAsString(pojo);
	}
	
	public static HttpHeaders createHttpHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		return headers;
	}
}
